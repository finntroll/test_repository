<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170623133224 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE education_level (id INT AUTO_INCREMENT NOT NULL, trans_key VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_2666D6B4E2C1CEB1 (trans_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE education_level_entrance_test (education_level_id INT NOT NULL, entrance_test_id INT NOT NULL, INDEX IDX_70F0D75CD7A5352E (education_level_id), INDEX IDX_70F0D75CBBC36F4F (entrance_test_id), PRIMARY KEY(education_level_id, entrance_test_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE social_links (id INT AUTO_INCREMENT NOT NULL, vkontakte VARCHAR(255) DEFAULT NULL, facebook VARCHAR(255) DEFAULT NULL, instagram VARCHAR(255) DEFAULT NULL, youtube VARCHAR(255) DEFAULT NULL, twitter VARCHAR(255) DEFAULT NULL, telegram VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE slider_data (id INT AUTO_INCREMENT NOT NULL, students INT NOT NULL, staff INT NOT NULL, foreign_students INT NOT NULL, edu_programs INT NOT NULL, edu_programs_foreign INT NOT NULL, international_laboratories INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE audience (id INT AUTO_INCREMENT NOT NULL, trans_key VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE knowledge_area (id INT AUTO_INCREMENT NOT NULL, trans_key VARCHAR(100) NOT NULL, UNIQUE INDEX UNIQ_B8A1775CE2C1CEB1 (trans_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contest_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, title VARCHAR(155) NOT NULL, page_url VARCHAR(255) NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_360BA3DE2C2AC5D3 (translatable_id), UNIQUE INDEX contest_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contest (id INT AUTO_INCREMENT NOT NULL, city_id INT DEFAULT NULL, language_id INT DEFAULT NULL, university_id INT DEFAULT NULL, date_begin DATETIME DEFAULT NULL, date_end DATETIME DEFAULT NULL, date_reception_end DATETIME DEFAULT NULL, is_published TINYINT(1) NOT NULL, project_type VARCHAR(255) NOT NULL, INDEX IDX_1A95CB58BAC62AF (city_id), INDEX IDX_1A95CB582F1BAF4 (language_id), INDEX IDX_1A95CB5309D1878 (university_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contest_knowledge_area_item (contest_id INT NOT NULL, knowledge_area_item_id INT NOT NULL, INDEX IDX_49A5F4BA1CD0F0DE (contest_id), INDEX IDX_49A5F4BABF963A91 (knowledge_area_item_id), PRIMARY KEY(contest_id, knowledge_area_item_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contest_audience (contest_id INT NOT NULL, audience_id INT NOT NULL, INDEX IDX_CA18F73B1CD0F0DE (contest_id), INDEX IDX_CA18F73B848CC616 (audience_id), PRIMARY KEY(contest_id, audience_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE academic_mobility_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, title VARCHAR(155) NOT NULL, page_url VARCHAR(255) NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_978A3B522C2AC5D3 (translatable_id), UNIQUE INDEX academic_mobility_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE program (id INT AUTO_INCREMENT NOT NULL, university_branch_id INT DEFAULT NULL, education_form_id INT DEFAULT NULL, education_level_id INT DEFAULT NULL, knowledge_area_id INT DEFAULT NULL, knowledge_area_item_id INT DEFAULT NULL, budget_places INT DEFAULT NULL, entrance_score INT DEFAULT NULL, contract_places INT DEFAULT NULL, price INT DEFAULT NULL, military_department TINYINT(1) NOT NULL, exchange_study TINYINT(1) NOT NULL, joint_program TINYINT(1) DEFAULT NULL, double_diploma TINYINT(1) NOT NULL, duration INT NOT NULL, is_published TINYINT(1) NOT NULL, INDEX IDX_92ED7784EA6D9B9D (university_branch_id), INDEX IDX_92ED778443D2F4A5 (education_form_id), INDEX IDX_92ED7784D7A5352E (education_level_id), INDEX IDX_92ED7784C2486E13 (knowledge_area_id), INDEX IDX_92ED7784BF963A91 (knowledge_area_item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE program_partner (program_id INT NOT NULL, partner_id INT NOT NULL, INDEX IDX_BAC1C6C23EB8070A (program_id), INDEX IDX_BAC1C6C29393F8FE (partner_id), PRIMARY KEY(program_id, partner_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE program_language (program_id INT NOT NULL, language_id INT NOT NULL, INDEX IDX_55EE8D803EB8070A (program_id), INDEX IDX_55EE8D8082F1BAF4 (language_id), PRIMARY KEY(program_id, language_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE program_entrance_test (program_id INT NOT NULL, entrance_test_id INT NOT NULL, INDEX IDX_EBA3E2603EB8070A (program_id), INDEX IDX_EBA3E260BBC36F4F (entrance_test_id), PRIMARY KEY(program_id, entrance_test_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE knowledge_area_item (id INT AUTO_INCREMENT NOT NULL, area_id INT DEFAULT NULL, trans_key VARCHAR(255) NOT NULL, INDEX IDX_BD93FC0DBD0F409C (area_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE university_branch_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, address VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, reference_phone VARCHAR(11) NOT NULL, reference_mail VARCHAR(50) NOT NULL, selection_committee_phone VARCHAR(11) NOT NULL, selection_committee_mail VARCHAR(50) NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_DEFCF4952C2AC5D3 (translatable_id), UNIQUE INDEX university_branch_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE program_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(155) NOT NULL, url VARCHAR(255) NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_787308072C2AC5D3 (translatable_id), UNIQUE INDEX program_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE city (id INT AUTO_INCREMENT NOT NULL, trans_key VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE academic_mobility (id INT AUTO_INCREMENT NOT NULL, university_id INT DEFAULT NULL, city_id INT DEFAULT NULL, date_begin DATETIME DEFAULT NULL, date_end DATETIME DEFAULT NULL, date_reception_end DATETIME DEFAULT NULL, duration INT DEFAULT NULL, duration_type VARCHAR(255) DEFAULT NULL, mobility_type VARCHAR(255) NOT NULL, is_published TINYINT(1) NOT NULL, INDEX IDX_E960A80B309D1878 (university_id), INDEX IDX_E960A80B8BAC62AF (city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE academic_mobility_language (academic_mobility_id INT NOT NULL, language_id INT NOT NULL, INDEX IDX_BB1B509E41787D81 (academic_mobility_id), INDEX IDX_BB1B509E82F1BAF4 (language_id), PRIMARY KEY(academic_mobility_id, language_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE academic_mobility_knowledge_area_item (academic_mobility_id INT NOT NULL, knowledge_area_item_id INT NOT NULL, INDEX IDX_9BD2D2F41787D81 (academic_mobility_id), INDEX IDX_9BD2D2FBF963A91 (knowledge_area_item_id), PRIMARY KEY(academic_mobility_id, knowledge_area_item_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE academic_mobility_audience (academic_mobility_id INT NOT NULL, audience_id INT NOT NULL, INDEX IDX_920DB53341787D81 (academic_mobility_id), INDEX IDX_920DB533848CC616 (audience_id), PRIMARY KEY(academic_mobility_id, audience_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE university_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, full_name VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, short_name VARCHAR(30) NOT NULL, description LONGTEXT NOT NULL, promo_video VARCHAR(255) NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_A432B7982C2AC5D3 (translatable_id), UNIQUE INDEX university_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE partner (id INT AUTO_INCREMENT NOT NULL, logo VARCHAR(255) NOT NULL, is_foreign TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE agu_user (id INT AUTO_INCREMENT NOT NULL, university_id INT DEFAULT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_7A09C01A92FC23A8 (username_canonical), UNIQUE INDEX UNIQ_7A09C01AA0D96FBF (email_canonical), UNIQUE INDEX UNIQ_7A09C01AC05FB297 (confirmation_token), INDEX IDX_7A09C01A309D1878 (university_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE partner_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(150) NOT NULL, short_name VARCHAR(30) NOT NULL, url VARCHAR(255) NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_FD1AF3212C2AC5D3 (translatable_id), UNIQUE INDEX partner_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE university (id INT AUTO_INCREMENT NOT NULL, main_branch_id INT DEFAULT NULL, slider_data_id INT DEFAULT NULL, social_links_id INT DEFAULT NULL, logo VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_A07A85ECFC606BEB (main_branch_id), UNIQUE INDEX UNIQ_A07A85EC490E8490 (slider_data_id), UNIQUE INDEX UNIQ_A07A85EC4EEFCCA1 (social_links_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE university_partner (university_id INT NOT NULL, partner_id INT NOT NULL, INDEX IDX_91AC9B21309D1878 (university_id), INDEX IDX_91AC9B219393F8FE (partner_id), PRIMARY KEY(university_id, partner_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE language (id INT AUTO_INCREMENT NOT NULL, trans_key VARCHAR(255) NOT NULL, code VARCHAR(2) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE news (id INT AUTO_INCREMENT NOT NULL, type INT DEFAULT 0 NOT NULL, rubric INT DEFAULT 0 NOT NULL, lead VARCHAR(400) DEFAULT NULL, content TEXT NOT NULL, video_link VARCHAR(500) DEFAULT NULL, status INT DEFAULT 2 NOT NULL, university_id INT DEFAULT 0 NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE education_form (id INT AUTO_INCREMENT NOT NULL, trans_key VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_B81140FEE2C1CEB1 (trans_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entrance_test (id INT AUTO_INCREMENT NOT NULL, trans_key VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE university_branch (id INT AUTO_INCREMENT NOT NULL, university_id INT DEFAULT NULL, city_id INT DEFAULT NULL, working_hours VARCHAR(100) DEFAULT NULL, INDEX IDX_36C154FF309D1878 (university_id), INDEX IDX_36C154FF8BAC62AF (city_id), UNIQUE INDEX universityBranch_city (city_id, university_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE education_level_entrance_test ADD CONSTRAINT FK_70F0D75CD7A5352E FOREIGN KEY (education_level_id) REFERENCES education_level (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE education_level_entrance_test ADD CONSTRAINT FK_70F0D75CBBC36F4F FOREIGN KEY (entrance_test_id) REFERENCES entrance_test (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contest_translation ADD CONSTRAINT FK_360BA3DE2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES contest (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contest ADD CONSTRAINT FK_1A95CB58BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE contest ADD CONSTRAINT FK_1A95CB582F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE contest ADD CONSTRAINT FK_1A95CB5309D1878 FOREIGN KEY (university_id) REFERENCES university (id)');
        $this->addSql('ALTER TABLE contest_knowledge_area_item ADD CONSTRAINT FK_49A5F4BA1CD0F0DE FOREIGN KEY (contest_id) REFERENCES contest (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contest_knowledge_area_item ADD CONSTRAINT FK_49A5F4BABF963A91 FOREIGN KEY (knowledge_area_item_id) REFERENCES knowledge_area_item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contest_audience ADD CONSTRAINT FK_CA18F73B1CD0F0DE FOREIGN KEY (contest_id) REFERENCES contest (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contest_audience ADD CONSTRAINT FK_CA18F73B848CC616 FOREIGN KEY (audience_id) REFERENCES audience (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE academic_mobility_translation ADD CONSTRAINT FK_978A3B522C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES academic_mobility (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE program ADD CONSTRAINT FK_92ED7784EA6D9B9D FOREIGN KEY (university_branch_id) REFERENCES university_branch (id)');
        $this->addSql('ALTER TABLE program ADD CONSTRAINT FK_92ED778443D2F4A5 FOREIGN KEY (education_form_id) REFERENCES education_form (id)');
        $this->addSql('ALTER TABLE program ADD CONSTRAINT FK_92ED7784D7A5352E FOREIGN KEY (education_level_id) REFERENCES education_level (id)');
        $this->addSql('ALTER TABLE program ADD CONSTRAINT FK_92ED7784C2486E13 FOREIGN KEY (knowledge_area_id) REFERENCES knowledge_area (id)');
        $this->addSql('ALTER TABLE program ADD CONSTRAINT FK_92ED7784BF963A91 FOREIGN KEY (knowledge_area_item_id) REFERENCES knowledge_area_item (id)');
        $this->addSql('ALTER TABLE program_partner ADD CONSTRAINT FK_BAC1C6C23EB8070A FOREIGN KEY (program_id) REFERENCES program (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE program_partner ADD CONSTRAINT FK_BAC1C6C29393F8FE FOREIGN KEY (partner_id) REFERENCES partner (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE program_language ADD CONSTRAINT FK_55EE8D803EB8070A FOREIGN KEY (program_id) REFERENCES program (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE program_language ADD CONSTRAINT FK_55EE8D8082F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE program_entrance_test ADD CONSTRAINT FK_EBA3E2603EB8070A FOREIGN KEY (program_id) REFERENCES program (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE program_entrance_test ADD CONSTRAINT FK_EBA3E260BBC36F4F FOREIGN KEY (entrance_test_id) REFERENCES entrance_test (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE knowledge_area_item ADD CONSTRAINT FK_BD93FC0DBD0F409C FOREIGN KEY (area_id) REFERENCES knowledge_area (id)');
        $this->addSql('ALTER TABLE university_branch_translation ADD CONSTRAINT FK_DEFCF4952C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES university_branch (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE program_translation ADD CONSTRAINT FK_787308072C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES program (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE academic_mobility ADD CONSTRAINT FK_E960A80B309D1878 FOREIGN KEY (university_id) REFERENCES university (id)');
        $this->addSql('ALTER TABLE academic_mobility ADD CONSTRAINT FK_E960A80B8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE academic_mobility_language ADD CONSTRAINT FK_BB1B509E41787D81 FOREIGN KEY (academic_mobility_id) REFERENCES academic_mobility (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE academic_mobility_language ADD CONSTRAINT FK_BB1B509E82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE academic_mobility_knowledge_area_item ADD CONSTRAINT FK_9BD2D2F41787D81 FOREIGN KEY (academic_mobility_id) REFERENCES academic_mobility (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE academic_mobility_knowledge_area_item ADD CONSTRAINT FK_9BD2D2FBF963A91 FOREIGN KEY (knowledge_area_item_id) REFERENCES knowledge_area_item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE academic_mobility_audience ADD CONSTRAINT FK_920DB53341787D81 FOREIGN KEY (academic_mobility_id) REFERENCES academic_mobility (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE academic_mobility_audience ADD CONSTRAINT FK_920DB533848CC616 FOREIGN KEY (audience_id) REFERENCES audience (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE university_translation ADD CONSTRAINT FK_A432B7982C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES university (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE agu_user ADD CONSTRAINT FK_7A09C01A309D1878 FOREIGN KEY (university_id) REFERENCES university (id)');
        $this->addSql('ALTER TABLE partner_translation ADD CONSTRAINT FK_FD1AF3212C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES partner (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE university ADD CONSTRAINT FK_A07A85ECFC606BEB FOREIGN KEY (main_branch_id) REFERENCES university_branch (id)');
        $this->addSql('ALTER TABLE university ADD CONSTRAINT FK_A07A85EC490E8490 FOREIGN KEY (slider_data_id) REFERENCES slider_data (id)');
        $this->addSql('ALTER TABLE university ADD CONSTRAINT FK_A07A85EC4EEFCCA1 FOREIGN KEY (social_links_id) REFERENCES social_links (id)');
        $this->addSql('ALTER TABLE university_partner ADD CONSTRAINT FK_91AC9B21309D1878 FOREIGN KEY (university_id) REFERENCES university (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE university_partner ADD CONSTRAINT FK_91AC9B219393F8FE FOREIGN KEY (partner_id) REFERENCES partner (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE university_branch ADD CONSTRAINT FK_36C154FF309D1878 FOREIGN KEY (university_id) REFERENCES university (id)');
        $this->addSql('ALTER TABLE university_branch ADD CONSTRAINT FK_36C154FF8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE education_level_entrance_test DROP FOREIGN KEY FK_70F0D75CD7A5352E');
        $this->addSql('ALTER TABLE program DROP FOREIGN KEY FK_92ED7784D7A5352E');
        $this->addSql('ALTER TABLE university DROP FOREIGN KEY FK_A07A85EC4EEFCCA1');
        $this->addSql('ALTER TABLE university DROP FOREIGN KEY FK_A07A85EC490E8490');
        $this->addSql('ALTER TABLE contest_audience DROP FOREIGN KEY FK_CA18F73B848CC616');
        $this->addSql('ALTER TABLE academic_mobility_audience DROP FOREIGN KEY FK_920DB533848CC616');
        $this->addSql('ALTER TABLE program DROP FOREIGN KEY FK_92ED7784C2486E13');
        $this->addSql('ALTER TABLE knowledge_area_item DROP FOREIGN KEY FK_BD93FC0DBD0F409C');
        $this->addSql('ALTER TABLE contest_translation DROP FOREIGN KEY FK_360BA3DE2C2AC5D3');
        $this->addSql('ALTER TABLE contest_knowledge_area_item DROP FOREIGN KEY FK_49A5F4BA1CD0F0DE');
        $this->addSql('ALTER TABLE contest_audience DROP FOREIGN KEY FK_CA18F73B1CD0F0DE');
        $this->addSql('ALTER TABLE program_partner DROP FOREIGN KEY FK_BAC1C6C23EB8070A');
        $this->addSql('ALTER TABLE program_language DROP FOREIGN KEY FK_55EE8D803EB8070A');
        $this->addSql('ALTER TABLE program_entrance_test DROP FOREIGN KEY FK_EBA3E2603EB8070A');
        $this->addSql('ALTER TABLE program_translation DROP FOREIGN KEY FK_787308072C2AC5D3');
        $this->addSql('ALTER TABLE contest_knowledge_area_item DROP FOREIGN KEY FK_49A5F4BABF963A91');
        $this->addSql('ALTER TABLE program DROP FOREIGN KEY FK_92ED7784BF963A91');
        $this->addSql('ALTER TABLE academic_mobility_knowledge_area_item DROP FOREIGN KEY FK_9BD2D2FBF963A91');
        $this->addSql('ALTER TABLE contest DROP FOREIGN KEY FK_1A95CB58BAC62AF');
        $this->addSql('ALTER TABLE academic_mobility DROP FOREIGN KEY FK_E960A80B8BAC62AF');
        $this->addSql('ALTER TABLE university_branch DROP FOREIGN KEY FK_36C154FF8BAC62AF');
        $this->addSql('ALTER TABLE academic_mobility_translation DROP FOREIGN KEY FK_978A3B522C2AC5D3');
        $this->addSql('ALTER TABLE academic_mobility_language DROP FOREIGN KEY FK_BB1B509E41787D81');
        $this->addSql('ALTER TABLE academic_mobility_knowledge_area_item DROP FOREIGN KEY FK_9BD2D2F41787D81');
        $this->addSql('ALTER TABLE academic_mobility_audience DROP FOREIGN KEY FK_920DB53341787D81');
        $this->addSql('ALTER TABLE program_partner DROP FOREIGN KEY FK_BAC1C6C29393F8FE');
        $this->addSql('ALTER TABLE partner_translation DROP FOREIGN KEY FK_FD1AF3212C2AC5D3');
        $this->addSql('ALTER TABLE university_partner DROP FOREIGN KEY FK_91AC9B219393F8FE');
        $this->addSql('ALTER TABLE contest DROP FOREIGN KEY FK_1A95CB5309D1878');
        $this->addSql('ALTER TABLE academic_mobility DROP FOREIGN KEY FK_E960A80B309D1878');
        $this->addSql('ALTER TABLE university_translation DROP FOREIGN KEY FK_A432B7982C2AC5D3');
        $this->addSql('ALTER TABLE agu_user DROP FOREIGN KEY FK_7A09C01A309D1878');
        $this->addSql('ALTER TABLE university_partner DROP FOREIGN KEY FK_91AC9B21309D1878');
        $this->addSql('ALTER TABLE university_branch DROP FOREIGN KEY FK_36C154FF309D1878');
        $this->addSql('ALTER TABLE contest DROP FOREIGN KEY FK_1A95CB582F1BAF4');
        $this->addSql('ALTER TABLE program_language DROP FOREIGN KEY FK_55EE8D8082F1BAF4');
        $this->addSql('ALTER TABLE academic_mobility_language DROP FOREIGN KEY FK_BB1B509E82F1BAF4');
        $this->addSql('ALTER TABLE program DROP FOREIGN KEY FK_92ED778443D2F4A5');
        $this->addSql('ALTER TABLE education_level_entrance_test DROP FOREIGN KEY FK_70F0D75CBBC36F4F');
        $this->addSql('ALTER TABLE program_entrance_test DROP FOREIGN KEY FK_EBA3E260BBC36F4F');
        $this->addSql('ALTER TABLE program DROP FOREIGN KEY FK_92ED7784EA6D9B9D');
        $this->addSql('ALTER TABLE university_branch_translation DROP FOREIGN KEY FK_DEFCF4952C2AC5D3');
        $this->addSql('ALTER TABLE university DROP FOREIGN KEY FK_A07A85ECFC606BEB');
        $this->addSql('DROP TABLE education_level');
        $this->addSql('DROP TABLE education_level_entrance_test');
        $this->addSql('DROP TABLE social_links');
        $this->addSql('DROP TABLE slider_data');
        $this->addSql('DROP TABLE audience');
        $this->addSql('DROP TABLE knowledge_area');
        $this->addSql('DROP TABLE contest_translation');
        $this->addSql('DROP TABLE contest');
        $this->addSql('DROP TABLE contest_knowledge_area_item');
        $this->addSql('DROP TABLE contest_audience');
        $this->addSql('DROP TABLE academic_mobility_translation');
        $this->addSql('DROP TABLE program');
        $this->addSql('DROP TABLE program_partner');
        $this->addSql('DROP TABLE program_language');
        $this->addSql('DROP TABLE program_entrance_test');
        $this->addSql('DROP TABLE knowledge_area_item');
        $this->addSql('DROP TABLE university_branch_translation');
        $this->addSql('DROP TABLE program_translation');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE academic_mobility');
        $this->addSql('DROP TABLE academic_mobility_language');
        $this->addSql('DROP TABLE academic_mobility_knowledge_area_item');
        $this->addSql('DROP TABLE academic_mobility_audience');
        $this->addSql('DROP TABLE university_translation');
        $this->addSql('DROP TABLE partner');
        $this->addSql('DROP TABLE agu_user');
        $this->addSql('DROP TABLE partner_translation');
        $this->addSql('DROP TABLE university');
        $this->addSql('DROP TABLE university_partner');
        $this->addSql('DROP TABLE language');
        $this->addSql('DROP TABLE news');
        $this->addSql('DROP TABLE education_form');
        $this->addSql('DROP TABLE entrance_test');
        $this->addSql('DROP TABLE university_branch');
    }
}
