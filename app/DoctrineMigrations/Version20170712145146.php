<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170712145146 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql(
            'INSERT INTO `language` (`id`, `trans_key`, `code`) VALUES 
                (1, "russian", "RU"),
                (2, "english", "EN"),
                (3, "spanish", "ES"),
                (4, "french", "FR"),
                (5, "chinese", "CN"),
                (6, "german", "DE")
        ');

        $this->addSql(
            'INSERT INTO knowledge_area (`id`,`trans_key`) values
                (1, "social_sciences"),
                (2, "economics_and_management"),
                (3, "humanitarian_and_art"),
                (4, "engineering_and_technical"),
                (5, "mathematical_and_natural"),
                (6, "life_and_medicine")
        ');

        $this->addSql(
            'INSERT INTO knowledge_area_item (`trans_key`, `area_id`) values
                ("education_and_pedagogy", 1),
                ("politics_and_regional", 1),
                ("psychology", 1),
                ("sociology_and_social_work", 1),
                ("jurisprudence", 1),
                ("service_and_tourism", 1),
                ("mass_media_and_librarianship", 1),
                
                ("business_informatics", 2),
                ("state_audit", 2),
                ("state_and_municipal_administration", 2),
                ("management", 2),
                ("economic_security", 2),
                ("commodity_research", 2),
                ("commerse", 2),
                ("human_resource_management", 2),
                ("finance_and_credit", 2),
                ("economy", 2),
                
                ("fine_arts_and_craft", 3),
                ("art_history", 3),
                ("history_and_archeology", 3),
                ("cultutal_and_sociocultural", 3),
                ("musical_art", 3),
                ("stage_arts_and_literary_creation", 3),
                ("theology", 3),
                ("sport_and_physical_culture", 3),
                ("philosophy_ethics_religious", 3),
                ("linguistics_and_literature", 3),
                
                ("aerospace_and_space_tech", 4),
                ("aeronavigation_and_operation_rocketspace_tech", 4),
                ("architecture", 4),
                ("veterinary_and_livestock_breeding", 4),
                ("informatics_and_computer_facilities", 4),
                ("information_security", 4),
                ("mechanical_engineering", 4),
                ("nanotechnollogies_and_nanomaterials", 4),
                ("geology_minig_oil_gas_geodesy", 4),
                ("industrial_ecology_and_biotechnology", 4),
                ("agriculture_foresty_fisheries", 4),
                ("shipbuilding_and_water_transport", 4),
                ("land_transport", 4),
                ("construction", 4),
                ("light_industry", 4),
                ("materials_tech", 4),
                ("technospheric_security_and_environmental_management", 4),
                ("tech_systems_management", 4),
                ("phys_tech_sciences", 4),
                ("photonics_instrumentation_optical_and_biotechnical_systems", 4),
                ("chemical_technology", 4),
                ("electric_and_heat_powe_engineering", 4),
                ("electronics_radio_and_communication_system", 4),
                ("nuclear_power_and_technology", 4),
                
                ("computer_and_information_sciences", 5),
                ("mathematics_and_mechanics", 5),
                ("earth_sciences", 5),
                ("physics_and_astronomy", 5),
                ("chemistry", 5),
                ("biological_sciences", 5),

                ("clinical_medicine", 6),
                ("health_and_preventive_medicine", 6),
                ("nursing", 6),
                ("basic_medicine", 6),
                ("pharmacy", 6)
        ');

        $this->addSql(
            'INSERT INTO education_form (`id`, `trans_key`) VALUES 
                (1, "full_time"),
                (2, "extramural"),
                (3, "part_time")
        ');

        $this->addSql(
            'INSERT INTO education_level (`id`, `trans_key`) VALUES
                (1, "bachelor"),
                (2, "specialty"),
                (3, "master"),
                (4, "postgraduate")
        ');

        $this->addSql(
            'INSERT INTO entrance_test (`id`, `trans_key`) values 
                (1, "russian_language"),
                (2, "mathematica"),
                (3, "physics"),
                (4, "chemistry"),
                (5, "history"),
                (6, "social_studies"),
                (7, "informatics"),
                (8, "biology"),
                (9, "geography"),
                (10, "foreign_language"),
                (11, "literature"),
                (12, "specialty"),
                (13, "colloquy"),
                (14, "portfolio"),
                (15, "creative_contest"),
                (16, "olympiad"),
                (17, "archeology"),
                (18, "higher_mathematics"),
                (19, "journalism"),
                (20, "culturology"),
                (21, "lingustics"),
                (22, "management"),
                (23, "economics"),
                (24, "economics_and_public_management"),
                (25, "ethnography"),
                (26, "law"),
                (27, "phylosophy"),
                (28, "sociology"),
                (29, "other")
        ');

        $this->addSql(
            'INSERT INTO education_level_entrance_test (`education_level_id`, `entrance_test_id`) VALUES 
                (1, 1),  -- russian_language
                (1, 2),  -- mathematica
                (1, 3),  -- physics
                (1, 4),  -- chemistry
                (1, 5),  -- history
                (1, 6),  -- social_studies
                (1, 7),  -- informatics
                (1, 8),  -- biology
                (1, 9),  -- geography
                (1, 10), -- foreign_language
                (1, 11), -- literature
                (1, 12), -- specialty
                (1, 13), -- colloquy
                (1, 14), -- portfolio
                (1, 15), -- creative_contest
                (1, 16), -- olympiad
                
                (2, 1),  -- russian_language
                (2, 2),  -- mathematica
                (2, 3),  -- physics
                (2, 4),  -- chemistry
                (2, 5),  -- history
                (2, 6),  -- social_studies
                (2, 7),  -- informatics
                (2, 8),  -- biology
                (2, 9),  -- geography
                (2, 10), -- foreign_language
                (2, 11), -- literature
                (2, 12), -- specialty
                (2, 13), -- colloquy
                (2, 14), -- portfolio
                (2, 15), -- creative_contest
                (2, 16), -- olympiad
                
                (3, 1),  -- russian_language
                (3, 2),  -- mathematica
                (3, 3),  -- physics
                (3, 4),  -- chemistry
                (3, 5),  -- history
                (3, 6),  -- social_studies
                (3, 7),  -- informatics
                (3, 8),  -- biology
                (3, 9),  -- geography
                (3, 10), -- foreign_language
                (3, 11), -- literature
                (3, 12), -- specialty
                (3, 13), -- colloquy
                (3, 14), -- portfolio
                (3, 15), -- creative_contest
                (3, 16), -- olympiad
                (3, 17), -- archeology
                (3, 18), -- higher_mathematics
                (3, 19), -- journalism
                (3, 20), -- culturology
                (3, 21), -- lingustics
                (3, 22), -- management
                (3, 23), -- economics
                (3, 24), -- economics_and_public_management
                (3, 25), -- ethnography
                (3, 26), -- law
                (3, 27), -- phylosophy
                (3, 28), -- sociology
                (3, 29), -- other
                
                (4, 10), -- foreign_language
                (4, 12), -- specialty
                (4, 13), -- colloquy
                (4, 14), -- portfolio
                (4, 27), -- phylosophy
                (4, 29)  -- other
        ');

        $this->addSql(
            'INSERT INTO city (`id`, `trans_key`) VALUES
                  (1, "spb"),
                  (2, "woscow"),
                  (3, "novosibirsk"),
                  (4, "tomsk"),
                  (5, "kazan"),
                  (6, "vladivostok"),
                  (7, "ekaterinburg"),
                  (8, "samara"),
                  (9, "nizhnii_novgorod"),
                  (10, "krasnoyarsk"),
                  (11, "tymen"),
                  (12, "chelyabinsk"),
                  (13, "perm"),
                  (14, "kaliningrad")
        ');

        $this->addSql('
        INSERT INTO audience (`id`, `trans_key`) VALUES
            (1, "scholar"),
            (2, "bachelors"),
            (3, "masters"),
            (4, "phds"),
            (5, "teachers"),
            (6, "specialists"),
            (7, "researchers")
        ');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM `language`');
        $this->addSql('DELETE FROM knowledge_area_item');
        $this->addSql('DELETE FROM knowledge_area');
        $this->addSql('DELETE FROM education_level_entrance_test');
        $this->addSql('DELETE FROM education_level');
        $this->addSql('DELETE FROM entrance_test');
        $this->addSql('DELETE FROM city');
    }
}
