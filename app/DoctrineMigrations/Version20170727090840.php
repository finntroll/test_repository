<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170727090840 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql(
            'INSERT INTO university (`id`, `logo`) VALUES
                  (1, "itmo.png"),
                  (2, "hse.jpg"),
                  (3, "mipt.png"),
                  (4, "mephi.png"),
                  (5, "spbpu.jpg"),
                  (6, "sibfu.jpg"),
                  (7, "susu.jpg"),
                  (8, "ut.jpg"),
                  (9, "msmu.jpg"),
                  (10, "ikbfu.jpg"),
                  (11, "rudn.jpg"),
                  (12, "kfu.jpg"),
                  (13, "misis.jpg"),                  
                  (14, "tsu.jpg"),
                  (15, "tpu.jpg"),
                  (16, "fefu.jpg"),
                  (17, "urfu.jpg"),
                  (18, "leti.jpg"),
                  (19, "nsu.jpg"),
                  (20, "unn.jpg"),
                  (21, "samu.jpg")
        ');

        $this->addSql(
            'INSERT INTO university_translation (`translatable_id`, `full_name`, `name`, `short_name`, `description`, `promo_video`, `locale`) VALUES 
                    (1, "Санкт-Петербургский национальный исследовательский университет информационных технологий механики и оптики", "Университет ИТМО", "ИТМО", "Санкт-Петербургский национальный исследовательский университет информационных технологий механики и оптики", "link.ru", "ru"),
                    (2, "Национальный исследовательский университет «Высшая школа экономики»", "Университет «Высшая школа экономики»", "ВШЭ", "Национальный исследовательский университет «Высшая школа экономики»", "link.ru", "ru"),
                    (3, "Московский физико-технический институт (государственный университет)", "Московский физико-технический институт", "МФТИ", "Московский физико-технический институт (государственный университет)", "link.ru", "ru"),
                    (4, "Национальный исследовательский ядерный университет «МИФИ»", "Национальный исследовательский ядерный университет «МИФИ»", "МИФИ", "Национальный исследовательский ядерный университет «МИФИ»", "link.ru", "ru"),
                    (5, "Санкт-Петербургский политехнический университет Петра Великого", "Санкт-Петербургский политехнический университет Петра Великого", "Политех", "Санкт-Петербургский политехнический университет Петра Великого", "link.ru", "ru"),
                    (6, "Сибирский федеральный университет", "Сибирский федеральный университет", "СФУ", "Сибирский федеральный университет", "link.ru", "ru"),
                    (7, "Южно-Уральский государственный университет", "Южно-Уральский государственный университет", "ЮУрГУ", "Южно-Уральский государственный университет", "link.ru", "ru"),
                    (8, "Тюменский государственный университет", "Тюменский государственный университет", "ТГУ", "Тюменский государственный университет", "link.ru", "ru"),
                    (9, "Первый московский государственный медицинский университет имени И.М. Сеченова", "Первый МГМУ им. И.М.Сеченова", "МГМУ", "Первый московский государственный медицинский университет имени И.М. Сеченова", "link.ru", "ru"),
                    (10, "Балтийский федеральный университет им. И. Канта", "Балтийский федеральный университет им. И. Канта", "БФУ им. Канта", "Балтийский федеральный университет им. И. Канта", "link.ru", "ru"),
                    (11, "Российский университет дружбы народов", "Российский университет дружбы народов", "РУДН", "Российский университет дружбы народов", "link.ru", "ru"),
                    (12, "Казанский федеральный университет", "Казанский федеральный университет", "КФУ", "Казанский федеральный университет", "link.ru", "ru"),
                    (13, "Национальный исследовательский технологический университет «МИСиС»", "НИТУ «МИСиС»", "МИСиС", "Национальный исследовательский технологический университет «МИСиС»", "link.ru", "ru"),
                    (14, "Национальный исследовательский Томский государственный университет", "Томский государственный университет", "ТГУ", "Национальный исследовательский Томский государственный университет", "link.ru", "ru"),
                    (15, "Национальный исследовательский Томский политехнический университет", "Томский политехнический университет", "ТПУ", "Национальный исследовательский Томский политехнический университет", "link.ru", "ru"),
                    (16, "Дальневосточный федеральный университет", "Дальневосточный федеральный университет", "ДВФУ", "Дальневосточный федеральный университет", "link.ru", "ru"),
                    (17, "Уральский федеральный университет", "Уральский федеральный университет", "УрФУ", "Уральский федеральный университет", "link.ru", "ru"),
                    (18, "Санкт-Петербургский государственный электротехнический университет «ЛЭТИ»", "Санкт-Петербургский государственный электротехнический университет «ЛЭТИ»", "ЛЭТИ", "Санкт-Петербургский государственный электротехнический университет «ЛЭТИ»", "link.ru", "ru"),
                    (19, "Новосибирский национальный исследовательский государственный университет", "Новосибирский государственный университет", "НГУ", "Новосибирский национальный исследовательский государственный университет", "link.ru", "ru"),
                    (20, "Нижегородский государственный университет им. Н.И.Лобачевского", "Нижегородский государственный университет им. Н.И.Лобачевского", "ННГУ", "Нижегородский государственный университет им. Н.И.Лобачевского", "link.ru", "ru"),
                    (21, "Самарский национальный исследовательский университет имени академика С.П. Королёва", "Самарский университет", "Самарский университет", "Самарский национальный исследовательский университет имени академика С.П. Королёва", "link.ru", "ru"),
                    (1, "Saint Petersburg National Research University of Information Technologies, Mechanics and Optics", "ITMO University", "ITMO", "Saint Petersburg National Research University of Information Technologies, Mechanics and Optics", "link.en", "en"),
                    (2, "National Research University Higher School of Economics", "Higher School of Economics", "HSE University", "National Research University Higher School of Economics", "link.en", "en"),
                    (3, "Moscow Institute of Physics and Technology", "Moscow Institute of Physics and Technology", "MIPT", "Moscow Institute of Physics and Technology", "link.en", "en"),
                    (4, "National Research Nuclear University MEPhI (Moscow Engineering Physics Institute)", "National Research Nuclear University MEPhI", "MEPhI", "National Research Nuclear University MEPhI (Moscow Engineering Physics Institute)", "link.en", "en"),
                    (5, "Peter the Great St. Petersburg Polytechnic University", "St. Petersburg Polytechnic University", "SPbPU", "Peter the Great St. Petersburg Polytechnic University", "link.en", "en"),
                    (6, "Siberian Federal University", "Siberian Federal University", "SibFU", "Siberian Federal University", "link.en", "en"),
                    (7, "South Ural State University", "South Ural State University", "SUSU", "South Ural State University", "link.en", "en"),
                    (8, "University of Tyumen", "University of Tyumen", "UT", "University of Tyumen", "link.en", "en"),
                    (9, "I.M. Sechenov First Moscow State Medical University", "Sechenov Medical University", "MSMU", "I.M. Sechenov First Moscow State Medical University", "link.en", "en"),
                    (10, "Immanuel Kant Baltic Federal University", "Immanuel Kant Baltic Federal University", "IKBFU", "Immanuel Kant Baltic Federal University", "link.en", "en"),
                    (11, "Peoples\' Friendship University of Russia", "RUDN University", "RUDN", "Peoples\' Friendship University of Russia", "link.en", "en"),
                    (12, "Kazan Federal University", "Kazan Federal University", "KFU", "Kazan (Volga Region) Federal University", "link.en", "en"),
                    (13, "National University of Science and Technology MISiS", "National University of Science and Technology MISiS", "NUST MISiS", "National University of Science and Technology MISiS", "link.en", "en"),
                    (14, "National Research Tomsk State University", "Tomsk State University", "TSU", "National Research Tomsk State University", "link.en", "en"),
                    (15, "National Research Tomsk Polytechnic University", "Tomsk Polytechnic University", "TPU", "National Research Tomsk Polytechnic University", "link.en", "en"),
                    (16, "Far Eastern Federal University", "Far Eastern Federal University", "FEFU", "Far Eastern Federal University", "link.en", "en"),
                    (17, "Ural Federal University named after the first President of Russia B.N. Yeltsin", "Ural Federal University", "UrFU", "Ural Federal University named after the first President of Russia B.N. Yeltsin", "link.en", "en"),
                    (18, "Saint Petersburg Electrotechnical University LETI", "Saint Petersburg Electrotechnical University LETI", "ETU LETI", "Saint Petersburg Electrotechnical University LETI", "link.en", "en"),
                    (19, "Novosibirsk State University (National Research University)", "Novosibirsk State University", "NSU", "Novosibirsk State University", "link.en", "en"),
                    (20, "National Research Lobachevsky State University of Nizhni Novgorod", "Lobachevsky University", "UNN", "National Research Lobachevsky State University of Nizhni Novgorod (Lobachevsky University)", "link.en", "en"),
                    (21, "Samara National Research University", "Samara University", "Samara University", "Samara National Research University", "link.en", "en")
        ');

//                Cities
//                1 => spb
//                2 => woscow
//                3 => novosibirsk
//                4 => tomsk
//                5 => kazan
//                6 => vladivostok
//                7 => ekaterinburg
//                8 => samara
//                9 => nizhnii_novgorod
//                10 => krasnoyarsk
//                11 => tymen
//                12 => chelyabinsk
//                13 => perm
//                14 => kaliningrad

        $this->addSql(
            'INSERT INTO university_branch (`id`,`university_id`,`city_id`) VALUES
                    (1, 1, 1),
                    (2, 2, 2),
                    (3, 2, 1),
                    (4, 2, 9),
                    (5, 2, 13),
                    (6, 3, 2),
                    (7, 4, 2),
                    (8, 5, 1),
                    (9, 6, 10),
                    (10, 7, 12),
                    (11, 8, 11),
                    (12, 9, 2),
                    (13, 10, 14),
                    (14, 11, 2),
                    (15, 12, 5),
                    (16, 13, 2),
                    (17, 14, 4),
                    (18, 15, 4),
                    (19, 16, 6),
                    (20, 17, 7),
                    (21, 18, 1),
                    (22, 19, 3),
                    (23, 20, 9),
                    (24, 21, 8)
                    '
        );

//       Set main branches to universities
        $this->addSql('UPDATE university SET `main_branch_id` = 1 WHERE id = 1');
        $this->addSql('UPDATE university SET `main_branch_id` = 2 WHERE id = 2');
        $this->addSql('UPDATE university SET `main_branch_id` = 6 WHERE id = 3');
        $this->addSql('UPDATE university SET `main_branch_id` = 7 WHERE id = 4');
        $this->addSql('UPDATE university SET `main_branch_id` = 8 WHERE id = 5');
        $this->addSql('UPDATE university SET `main_branch_id` = 9 WHERE id = 6');
        $this->addSql('UPDATE university SET `main_branch_id` = 10 WHERE id = 7');
        $this->addSql('UPDATE university SET `main_branch_id` = 11 WHERE id = 8');
        $this->addSql('UPDATE university SET `main_branch_id` = 12 WHERE id = 9');
        $this->addSql('UPDATE university SET `main_branch_id` = 13 WHERE id = 10');
        $this->addSql('UPDATE university SET `main_branch_id` = 14 WHERE id = 11');
        $this->addSql('UPDATE university SET `main_branch_id` = 15 WHERE id = 12');
        $this->addSql('UPDATE university SET `main_branch_id` = 16 WHERE id = 13');
        $this->addSql('UPDATE university SET `main_branch_id` = 17 WHERE id = 14');
        $this->addSql('UPDATE university SET `main_branch_id` = 18 WHERE id = 15');
        $this->addSql('UPDATE university SET `main_branch_id` = 19 WHERE id = 16');
        $this->addSql('UPDATE university SET `main_branch_id` = 20 WHERE id = 17');
        $this->addSql('UPDATE university SET `main_branch_id` = 21 WHERE id = 18');
        $this->addSql('UPDATE university SET `main_branch_id` = 22 WHERE id = 19');
        $this->addSql('UPDATE university SET `main_branch_id` = 23 WHERE id = 20');
        $this->addSql('UPDATE university SET `main_branch_id` = 24 WHERE id = 21');

        // @FIXME - This is Fixture. Fix with real data.
        $this->addSql('INSERT INTO agu_user (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `password`, `roles`, `university_id`) VALUES
                            (1, "itmo", "itmo", "agu@ifmo.ru", "agu@ifmo.ru", true, "$2y$13$vNn8SDQSsrsaYJGYPzkwuOG39feU0BjUW/j6Jf9QHUPxqPmEz1MMa", "a:1:{i:0;s:11:\"ROLE_MEMBER\";}", 1),
                            (2, "globaluni", "globaluni", "info@globaluni.ru", "info@globaluni.ru", true, "$2y$13$LZfp5JK5FpETV5w1VQrb9OApkcnBZBAfQUCPHcPVQrtD0HPeH4aM6", "a:1:{i:0;s:11:\"ROLE_MEMBER\";}", 2)
                        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DELETE FROM university_translation');
        $this->addSql('DELETE FROM univeersity_branch_translation');
        $this->addSql('DELETE FROM university');
        $this->addSql('DELETE FROM university_branch');
    }
}
