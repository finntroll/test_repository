<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170814162344 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO education_form (`id`, `trans_key`) VALUES (4, "remote");');
        $this->addSql('DELETE FROM education_level_entrance_test WHERE education_level_id = 1 AND entrance_test_id = 12 LIMIT 1;');
        $this->addSql('UPDATE entrance_test SET trans_key = "special_discipline" WHERE trans_key = "specialty";');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DELETE FROM education_form WHERE id = 4 AND trans_key = "remote" LIMIT 1;');
        $this->addSql('INSERT INTO education_level_entrance_test (`education_level_id`, `entrance_test_id`) VALUES (1, 12);');
        $this->addSql('UPDATE entrance_test SET trans_key = "specialty" WHERE trans_key = "special_discipline";');
    }
}
