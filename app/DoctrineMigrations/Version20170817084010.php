<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170817084010 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // Update and insert (if not exist) slider data for every university
        $this->addSql('ALTER TABLE slider_data ADD slide VARCHAR(255) DEFAULT NULL;');
        $this->addSql('INSERT INTO slider_data (`id`, `students`, `staff`, `foreign_students`, `edu_programs`, `edu_programs_foreign`, `international_laboratories`, `slide`) VALUES             
            (1, 0, 0, 0, 0, 0, 0, ""),
            (2, 0, 0, 0, 0, 0, 0, ""),
            (3, 0, 0, 0, 0, 0, 0, ""),
            (4, 0, 0, 0, 0, 0, 0, ""),
            (5, 0, 0, 0, 0, 0, 0, ""),
            (6, 0, 0, 0, 0, 0, 0, ""),
            (7, 0, 0, 0, 0, 0, 0, ""),
            (8, 0, 0, 0, 0, 0, 0, ""),
            (9, 0, 0, 0, 0, 0, 0, ""),
            (10, 0, 0, 0, 0, 0, 0, ""),
            (11, 0, 0, 0, 0, 0, 0, ""),
            (12, 0, 0, 0, 0, 0, 0, ""),
            (13, 0, 0, 0, 0, 0, 0, ""),
            (14, 0, 0, 0, 0, 0, 0, ""),
            (15, 0, 0, 0, 0, 0, 0, ""),
            (16, 0, 0, 0, 0, 0, 0, ""),
            (17, 0, 0, 0, 0, 0, 0, ""),
            (18, 0, 0, 0, 0, 0, 0, ""),
            (19, 0, 0, 0, 0, 0, 0, ""),
            (20, 0, 0, 0, 0, 0, 0, ""),
            (21, 0, 0, 0, 0, 0, 0, "")
            ON DUPLICATE KEY UPDATE `id` = `id`
            ');

        // Update inserted and existent slider data with values for new field
        $this->addSql('UPDATE slider_data SET `slide` = "itmo.jpg"  WHERE `id` = 1 ');
        $this->addSql('UPDATE slider_data SET `slide` = "hse.jpg"   WHERE `id` = 2 ');
        $this->addSql('UPDATE slider_data SET `slide` = "mipt.png"  WHERE `id` = 3 ');
        $this->addSql('UPDATE slider_data SET `slide` = "mephi.png" WHERE `id` = 4 ');
        $this->addSql('UPDATE slider_data SET `slide` = "spbpu.png" WHERE `id` = 5 ');
        $this->addSql('UPDATE slider_data SET `slide` = "sfu.jpg"   WHERE `id` = 6 ');
        $this->addSql('UPDATE slider_data SET `slide` = "susu.png"  WHERE `id` = 7 ');
        $this->addSql('UPDATE slider_data SET `slide` = "ut.jpg"    WHERE `id` = 8 ');
        $this->addSql('UPDATE slider_data SET `slide` = "msmu.png"  WHERE `id` = 9 ');
        $this->addSql('UPDATE slider_data SET `slide` = "bfu.jpg"   WHERE `id` = 10 ');
        $this->addSql('UPDATE slider_data SET `slide` = "rudn.jpg"  WHERE `id` = 11 ');
        $this->addSql('UPDATE slider_data SET `slide` = "kfu.jpg"   WHERE `id` = 12 ');
        $this->addSql('UPDATE slider_data SET `slide` = "misis.png" WHERE `id` = 13 ');
        $this->addSql('UPDATE slider_data SET `slide` = "tsu.jpg"   WHERE `id` = 14 ');
        $this->addSql('UPDATE slider_data SET `slide` = "tpu.png"   WHERE `id` = 15 ');
        $this->addSql('UPDATE slider_data SET `slide` = "fefu.jpg"  WHERE `id` = 16 ');
        $this->addSql('UPDATE slider_data SET `slide` = "urfu.jpg"  WHERE `id` = 17 ');
        $this->addSql('UPDATE slider_data SET `slide` = "leti.png"  WHERE `id` = 18 ');
        $this->addSql('UPDATE slider_data SET `slide` = "nsu.png"   WHERE `id` = 19 ');
        $this->addSql('UPDATE slider_data SET `slide` = "uun.png"   WHERE `id` = 20 ');
        $this->addSql('UPDATE slider_data SET `slide` = "samu.png"  WHERE `id` = 21 ');

        // Add new field to university table, insert values for new field and add relation to new slider_data
        $this->addSql('ALTER TABLE university ADD location_point POINT DEFAULT NULL COMMENT \'(DC2Type:point)\'');
        $this->addSql('ALTER TABLE university ADD photo VARCHAR(255) DEFAULT NULL;');
        $this->addSql('UPDATE university SET location_point = POINT(30.308397, 59.957155), slider_data_id = 1, photo = "itmo.png" WHERE `id` = 1;');
        $this->addSql('UPDATE university SET location_point = POINT(37.633073, 55.761305), slider_data_id = 2, photo = "hse.png" WHERE `id` = 2;');
        $this->addSql('UPDATE university SET location_point = POINT(37.518210, 55.930215), slider_data_id = 3, photo = "mipt.png" WHERE `id` = 3;');
        $this->addSql('UPDATE university SET location_point = POINT(37.664007, 55.649508), slider_data_id = 4, photo = "mephi.png" WHERE `id` = 4;');
        $this->addSql('UPDATE university SET location_point = POINT(30.372929, 60.007346), slider_data_id = 5, photo = "spbpu.jpg" WHERE `id` = 5;');
        $this->addSql('UPDATE university SET location_point = POINT(92.772062, 56.004612), slider_data_id = 6, photo = "sibfu.png" WHERE `id` = 6;');
        $this->addSql('UPDATE university SET location_point = POINT(61.370298, 55.160480), slider_data_id = 7, photo = "susu.jpg" WHERE `id` = 7;');
        $this->addSql('UPDATE university SET location_point = POINT(65.530953, 57.159602), slider_data_id = 8, photo = "ut.png" WHERE `id` = 8;');
        $this->addSql('UPDATE university SET location_point = POINT(37.568697, 55.731255), slider_data_id = 9, photo = "mgmu.png" WHERE `id` = 9;');
        $this->addSql('UPDATE university SET location_point = POINT(20.527643, 54.724282), slider_data_id = 10, photo = "bfu.png" WHERE `id` = 10;');
        $this->addSql('UPDATE university SET location_point = POINT(37.502086, 55.650482), slider_data_id = 11, photo = "rudn.jpg" WHERE `id` = 11;');
        $this->addSql('UPDATE university SET location_point = POINT(49.121515, 55.790440), slider_data_id = 12, photo = "kfu.jpg" WHERE `id` = 12;');
        $this->addSql('UPDATE university SET location_point = POINT(37.609125, 55.728286), slider_data_id = 13, photo = "misis.png" WHERE `id` = 13;');
        $this->addSql('UPDATE university SET location_point = POINT(84.947869, 56.469491), slider_data_id = 14, photo = "tsu.jpg" WHERE `id` = 14;');
        $this->addSql('UPDATE university SET location_point = POINT(84.950202, 56.465409), slider_data_id = 15, photo = "tpu.png" WHERE `id` = 15;');
        $this->addSql('UPDATE university SET location_point = POINT(131.890864, 43.025019), slider_data_id = 16, photo = "fefu.png" WHERE `id` = 16;');
        $this->addSql('UPDATE university SET location_point = POINT(60.653326, 56.844188), slider_data_id = 17, photo = "urfu.png" WHERE `id` = 17;');
        $this->addSql('UPDATE university SET location_point = POINT(30.322487, 59.972342), slider_data_id = 18, photo = "leti.png" WHERE `id` = 18;');
        $this->addSql('UPDATE university SET location_point = POINT(83.089516, 54.844923), slider_data_id = 19, photo = "nsu.png" WHERE `id` = 19;');
        $this->addSql('UPDATE university SET location_point = POINT(43.982545, 56.299549), slider_data_id = 20, photo = "unn.png" WHERE `id` = 20;');
        $this->addSql('UPDATE university SET location_point = POINT(50.172919, 53.222490), slider_data_id = 21, photo = "samu.png" WHERE `id` = 21;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE university DROP location_point');
        $this->addSql('ALTER TABLE university DROP photo');
        $this->addSql('ALTER TABLE slider_data DROP slide');
    }
}
