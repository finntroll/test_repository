<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170822093233 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // Fix for previous migration
        $this->addSql('ALTER TABLE slider_data CHANGE slide slide VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');

        $this->addSql('CREATE TABLE country_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_A1FE6FA42C2AC5D3 (translatable_id), UNIQUE INDEX country_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE country_translation ADD CONSTRAINT FK_A1FE6FA42C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES country (id) ON DELETE CASCADE');

        // Add countries
        $this->addSql('
            INSERT INTO country (`id`) VALUES
                (1),  # argentina
                (2),  # armenia
                (3),  # austria
                (4),  # china
                (5),  # finland
                (6),  # france
                (7),  # germany
                (8),  # great_britain
                (9),  # hungary
                (10), # netherlands
                (11), # united_states
                (12), # poland
                (13), # estonia
                (14), # kazakhstan
                (15), # bulgaria
                (16), # luxembourg
                (17), # italy
                (18), # russia
                (19)  # kyrgyzstan 
        ');

        // Add countries translations
        $this->addSql('
            INSERT INTO country_translation (`translatable_id`, `locale`, `name`) VALUES 
            (1, "ru", "Аргентина"),
            (1, "en", "Argentina"),

            (2, "ru", "Армения"),
            (2, "en", "Armenia"),

            (3, "ru", "Австрия"),
            (3, "en", "Austria"),

            (4, "ru", "Китай"),
            (4, "en", "China"),

            (5, "ru", "Финляндия"),
            (5, "en", "Finland"),

            (6, "ru", "Франция"),
            (6, "en", "France"),

            (7, "ru", "Германия"),
            (7, "en", "Germany"),

            (8, "ru", "Великобритания"),
            (8, "en", "Great Britain"),

            (9, "ru", "Венгрия"),
            (9, "en", "Hungary"),

            (10, "ru", "Нидерланды"),
            (10, "en", "Netherlands"),

            (11, "ru", "США"),
            (11, "en", "USA"),

            (12, "ru", "Польша"),
            (12, "en", "Poland"),

            (13, "ru", "Эстония"),
            (13, "en", "Estonia"),

            (14, "ru", "Казахстан"),
            (14, "en", "Kazakhstan"),

            (15, "ru", "Болгария"),
            (15, "en", "Bulgaria"),

            (16, "ru", "Люксембург"),
            (16, "en", "Luxembourg"),

            (17, "ru", "Италия"),
            (17, "en", "Italy"),

            (18, "ru", "Россия"),
            (18, "en", "Russia"),
            
            (19, "ru", "Киргризия"),
            (19, "en", "Kyrgyzstan")

        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE country_translation DROP FOREIGN KEY FK_A1FE6FA42C2AC5D3');
        $this->addSql('DROP TABLE country_translation');
        $this->addSql('DROP TABLE country');

        // Rollback fix for previous migration
        $this->addSql('ALTER TABLE slider_data CHANGE slide slide VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
