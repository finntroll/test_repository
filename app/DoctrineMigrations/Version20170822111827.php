<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170822111827 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE partner ADD country_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partner ADD CONSTRAINT FK_312B3E16F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('CREATE INDEX IDX_312B3E16F92F3E70 ON partner (country_id)');

        // Fill partners and partners translations
        $this->addSql('INSERT INTO partner (`id`, `logo`, `is_foreign`, `country_id`) VALUES
            (1, "", TRUE, 1),
            (2, "", TRUE, 1),
            (3, "", TRUE, 2),
            (4, "", TRUE, 3),
            (5, "", TRUE, 3),
            (6, "", TRUE, 3),
            (7, "", TRUE, 4),
            (8, "", TRUE, 4),
            (9, "", TRUE, 4),
            (10, "", TRUE, 5),
            (11, "", TRUE, 5),
            (12, "", TRUE, 5),
            (13, "", TRUE, 6),
            (14, "", TRUE, 6),
            (15, "", TRUE, 6),
            (16, "", TRUE, 6),
            (17, "", TRUE, 6),
            (18, "", TRUE, 6),
            (19, "", TRUE, 6),
            (20, "", TRUE, 6),
            (21, "", TRUE, 6),
            (22, "", TRUE, 6),
            (23, "", TRUE, 6),
            (24, "", TRUE, 6),
            (25, "", TRUE, 7),
            (26, "", TRUE, 7),
            (27, "", TRUE, 7),
            (28, "", TRUE, 7),
            (29, "", TRUE, 7),
            (30, "", TRUE, 8),
            (31, "", TRUE, 8),
            (32, "", TRUE, 8),
            (33, "", TRUE, 8),
            (34, "", TRUE, 8),
            (35, "", TRUE, 8),
            (36, "", TRUE, 8),
            (37, "", TRUE, 8),
            (38, "", TRUE, 8),
            (39, "", TRUE, 8),
            (40, "", TRUE, 8),
            (41, "", TRUE, 9),
            (42, "", TRUE, 9),
            (43, "", TRUE, 9),
            (44, "", TRUE, 10),
            (45, "", TRUE, 10),
            (46, "", TRUE, 10),
            (47, "", TRUE, 10),
            (48, "", TRUE, 10),
            (49, "", TRUE, 10),
            (50, "", TRUE, 11),
            (51, "", TRUE, 11),
            (52, "", TRUE, 11),
            (53, "", TRUE, 12),
            (54, "", TRUE, 13),
            (55, "", TRUE, 14),
            (56, "", TRUE, 14),
            (57, "", TRUE, 15),
            (58, "", TRUE, 16),
            (59, "", TRUE, 17),
            (60, "", FALSE, 18),
            (61, "", FALSE, 18),
            (62, "", FALSE, 18),
            (63, "", FALSE, 18),
            (64, "", FALSE, 18),
            (65, "", FALSE, 18),
            (66, "", FALSE, 18),
            (67, "", FALSE, 18),
            (68, "", FALSE, 18),
            (69, "", FALSE, 18),
            (70, "", FALSE, 18),
            (71, "", FALSE, 18),
            (72, "", FALSE, 18),
            (73, "", FALSE, 18),
            (74, "", FALSE, 18),
            (75, "", FALSE, 18),
            (76, "", FALSE, 18),
            (77, "", FALSE, 18),
            (78, "", FALSE, 18),
            (79, "", FALSE, 18),
            (80, "", FALSE, 18),
            (81, "", TRUE, 19),
            (82, "", TRUE, 4),
            (83, "", TRUE, 7)
        ');

        $this->addSql('
        INSERT INTO partner_translation (`translatable_id`, `locale`, `name`, `short_name`, `url`) VALUES
            (1, "ru", "Университет Центра изучения макроэкономики", "UCEMA", "https://www.ucema.edu.ar/foreign-students"),
            (1, "en", "University of CEMA", "UCEMA", "https://www.ucema.edu.ar/foreign-students"),

            (2, "ru", "Университет бизнеса и социальных наук", "UCES", "https://www.uces.edu.ar"),
            (2, "en", "University of Business and Social Sciences", "UCES", "https://www.uces.edu.ar"),

            (3, "ru", "Российско-Армянский университет", "RAU", "http://www.rau.am/rus/"),
            (3, "en", "Russian-Armenian (Slavonic) University", "RAU", "http://international.rau.am/eng/"),

            (4, "ru", "Международное общество экзистенциального анализа и логотерапии", "GLE-International", "http://www.existenzanalyse.org"),
            (4, "en", "International Society for Logotherapy and Existential Analysis", "GLE-International", "http://www.existenzanalyse.org"),

            (5, "ru", "Университет имени Иоганна Кеплера в Линце", "JKU", "http://www.jku.at/content"),
            (5, "en", "Johannes Kepler University Linz", "JKU", "http://www.jku.at/content"),

            (6, "ru", "Венский Университет Прикладных Наук", "FHWien", "https://www.technikum-wien.at/en/"),
            (6, "en", "University of Applied Sciences Technikum Wien", "FHWien", "https://www.technikum-wien.at/en/"),

            (7, "ru", "Городской университет Гонконга", "CityU", "http://www.cityu.edu.hk"),
            (7, "en", "City University of Hong Kong", "CityU", "http://www.cityu.edu.hk"),

            (8, "ru", "Нанькайский университет", "NKU", "http://english.nankai.edu.cn"),
            (8, "en", "Nankai University of Tianjin", "NKU", "http://english.nankai.edu.cn"),

            (9, "ru", "Нанкинский университет аэронавтики и астронавтики", "NUAA", "http://iao.nuaa.edu.cn"),
            (9, "en", "Nanjing University of Aeronautics and Astronautics", "NUAA", "http://iao.nuaa.edu.cn"),

            (10, "ru", "Лапеенрантский униерситет технологий", "LUT", "https://www.lut.fi/web/ru/"),
            (10, "en", "Lappeenranta University of Technology", "LUT", "https://www.lut.fi/web/en/"),

            (11, "ru", "Университет Аалто", "AU", "http://www.aalto.fi/en/"),
            (11, "en", "Aalto University", "AU", "http://www.aalto.fi/en/"),

            (12, "ru", "Университет Восточной Финляндии", "UEF", "http://www.uef.fi/en/etusivu"),
            (12, "en", "University of Eastern Finland", "UEF", "http://www.uef.fi/en/etusivu"),

            (13, "ru", "Университет Экс-Марсель", "AMU", "https://www.univ-amu.fr"),
            (13, "en", "Aix-Marseille University", "AMU", "https://www.univ-amu.fr"),

            (14, "ru", "Университет Париж 1 Пантеон-Сорбонна", "Paris 1", "http://www.univ-paris1.fr"),
            (14, "en", "Pantheon-Sorbonne University", "Paris 1", "http://www.univ-paris1.fr"),

            (15, "ru", "Высшая школа коммерции", "ESCP", "http://www.escpeurope.eu"),
            (15, "en", "ESCP Europe", "ESCP", "http://www.escpeurope.eu"),

            (16, "ru", "Высшая школа экономических и коммерческих наук", "ESSEC", "http://www.essec.edu/en/"),
            (16, "en", "ESSEC Business School", "ESSEC", "http://www.essec.edu/en/"),

            (17, "ru", "Университет Западный Париж — Нантер-ля-Дефанс", "Nanterre", "http://www.parisnanterre.fr"),
            (17, "en", "Paris West University Nanterre La Défense", "Nanterre", "http://www.parisnanterre.fr"),

            (18, "ru", "Университет Париж IV Сорбонна", "UFC", "http://www.english.paris-sorbonne.fr"),
            (18, "en", "Paris-Sorbonne University (Paris IV)", "UFC", "http://www.english.paris-sorbonne.fr"),

            (19, "ru", "Университет Париж XII Валь-де-Марн", "UPEC", "http://www.u-pec.fr"),
            (19, "en", "University Paris-Est Creteil Val de Marne", "UPEC", "http://www.u-pec.fr"),

            (20, "ru", "Страсбургский университет", "UDS", "https://en.unistra.fr"),
            (20, "en", "University of Strasbourg", "UDS", "https://en.unistra.fr"),

            (21, "ru", "Высшая нормальная школа Кашана ", "EPCSCP", "http://www.ens-cachan.fr/en"),
            (21, "en", "ENS Paris-Saclay", "EPCSCP", "http://www.ens-cachan.fr/en"),

            (22, "ru", "Лилльский институт управления", "IAE Lille 1", "http://www.iae.univ-lille1.fr"),
            (22, "en", "IAE Lille 1", "IAE Lille 1", "http://www.iae.univ-lille1.fr"),

            (23, "ru", "Институт изучения политики в Тулузе", "Sciences Po Toul", "http://www.sciencespo-toulouse.fr"),
            (23, "en", "Sciences Po Toulouse", "Sciences Po Toul", "http://www.sciencespo-toulouse.fr"),

            (24, "ru", "Институт изучения политики", "Sciences Po", "http://www.sciencespo.fr/en"),
            (24, "en", "Sciences Po", "Sciences Po", "http://www.sciencespo.fr/en"),

            (25, "ru", "Берлинский университет имени Гумбольдта", "HU Berlin", "https://www.hu-berlin.de"),
            (25, "en", "Humboldt-Universität zu Berlin", "HU Berlin", "https://www.hu-berlin.de"),

            (26, "ru", "Высшая школа управления Лейпцига", "HHL", "https://www.hhl.de/en/home/"),
            (26, "en", "HHL Leipzig Graduate School of Management", "HHL", "https://www.hhl.de/en/home/"),

            (27, "ru", "Вестфальский университет имени Вильгельма", "WWU Munster", "http://www.uni-muenster.de"),
            (27, "en", "Westfälische Wilhelms-Universität", "WWU Munster", "http://www.uni-muenster.de"),

            (28, "ru", "Фрайбергская горная академия", "TU TUBAF", "http://tu-freiberg.de/en/university"),
            (28, "en", "Freiberg University of Mining and Technology", "TU TUBAF", "http://tu-freiberg.de/en/university"),

            (29, "ru", "Университет Пассау", "UP", "http://www.fim.uni-passau.de"),
            (29, "en", "University of Passau", "UP", "http://www.fim.uni-passau.de"),

            (30, "ru", "Университет Ланкастера", "LUMS", "http://www.lancaster.ac.uk/lums/"),
            (30, "en", "Lancaster University", "LUMS", "http://www.lancaster.ac.uk/lums/"),

            (31, "ru", "Университет Лидса", "Leeds", "https://business.leeds.ac.uk"),
            (31, "en", "Leeds University", "Leeds", "https://business.leeds.ac.uk"),

            (32, "ru", "Университет Рединга", "Reading", "http://www.reading.ac.uk"),
            (32, "en", "University of Reading", "Reading", "http://www.reading.ac.uk"),

            (33, "ru", "Лондонский университет Метрополитен", "London Met", "http://www.londonmet.ac.uk"),
            (33, "en", "London Metropolitan University", "London Met", "http://www.londonmet.ac.uk"),

            (34, "ru", "Лондонская школа экономики и политических наук", "LSE", "http://www.lse.ac.uk"),
            (34, "en", "London School of Economics and Political Science", "LSE", "http://www.lse.ac.uk"),

            (35, "ru", "Университетский колледж Лондона", "UCL", "http://www.ucl.ac.uk"),
            (35, "en", "University College London", "UCL", "http://www.ucl.ac.uk"),

            (36, "ru", "Бирмингемский университет", "UOB", "http://www.birmingham.ac.uk/"),
            (36, "en", "University of Birmingham", "UOB", "http://www.birmingham.ac.uk/"),

            (37, "ru", "Кентский университет", "UOK", "https://www.kent.ac.uk"),
            (37, "en", "University of Kent", "UOK", "https://www.kent.ac.uk"),

            (38, "ru", "Лондонский университет", "UOL", "http://www.london.ac.uk"),
            (38, "en", "University of London", "UOL", "http://www.london.ac.uk"),

            (39, "ru", "Ноттингемский университет", "UON", "https://www.nottingham.ac.uk"),
            (39, "en", "University of Nottingham", "UON", "https://www.nottingham.ac.uk"),

            (40, "ru", "Уорикский университет", "UOW", "http://www2.warwick.ac.uk"),
            (40, "en", "University of Warwick", "UOW", "http://www2.warwick.ac.uk"),

            (41, "ru", "Университет Корвина", "CUB", "http://www.uni-corvinus.hu/index.php?id=eng"),
            (41, "en", "Corvinus University of Budapest", "CUB", "http://www.uni-corvinus.hu/index.php?id=eng"),

            (42, "ru", "Центрально-Европейский университет", "CEU", "https://www.ceu.edu"),
            (42, "en", "Central European University", "CEU", "https://www.ceu.edu"),

            (43, "ru", "Университета Святого Иштвана", "SZIE", "http://sziu.hu"),
            (43, "en", "Szent István University", "SZIE", "http://sziu.hu"),

            (44, "ru", "Амстердамский университет", "UvA", "http://www.uva.nl/en/home"),
            (44, "en", "University of Amsterdam", "UvA", "http://www.uva.nl/en/home"),

            (45, "ru", "Школы Финансов имени Дуйзенберга ", "DSF", "http://www.dsf.nl"),
            (45, "en", "Duisenberg School of Finance", "DSF", "http://www.dsf.nl"),

            (46, "ru", "Университет Эразма", "EUR", "https://www.eur.nl/english/"),
            (46, "en", "Erasmus Universiteit Rotterdam", "EUR", "https://www.eur.nl/english/"),

            (47, "ru", "Маастрихтский университет", "UM", "https://www.maastrichtuniversity.nl"),
            (47, "en", "Maastricht University", "UM", "https://www.maastrichtuniversity.nl"),

            (48, "ru", "Университет Тилбурга", "TU", "http://www.tilburguniversity.edu"),
            (48, "en", "Tilburg University", "TU", "http://www.tilburguniversity.edu"),

            (49, "ru", "Утрехтский университет", "UU", "https://www.uu.nl/en"),
            (49, "en", "Utrecht University", "UU", "https://www.uu.nl/en"),

            (50, "ru", "Университет Джорджа Мейсона", "Mason", "https://www2.gmu.edu"),
            (50, "en", "George Mason University", "Mason", "https://www2.gmu.edu"),

            (51, "ru", "Университет Тафтса", "Tufts", "https://www.tufts.edu"),
            (51, "en", "Tufts University", "Tufts", "https://www.tufts.edu"),

            (52, "ru", "Университет Южной Каролины", "USC", "http://www.sc.edu"),
            (52, "en", "University of South Carolina", "USC", "http://www.sc.edu"),

            (53, "ru", "Варшавский политехнический университет", "WUT", "https://www.pw.edu.pl/engpw"),
            (53, "en", "Warsaw University of Technology", "WUT", "https://www.pw.edu.pl/engpw"),

            (54, "ru", "Таллинский университет", "TLÜ", "https://www.tlu.ee/en"),
            (54, "en", "Tallinn University", "TLÜ", "https://www.tlu.ee/en"),

            (55, "ru", "Евразийский национальный университет имени Л. Н. Гумилёва", "ENU", "http://www.enu.kz/ru/"),
            (55, "en", "L.N.Gumilyov Eurasian National University", "ENU", "http://www.enu.kz/en/"),

            (56, "ru", "Казахский национальный университет имени аль-Фараби", "KazGU", "http://www.kaznu.kz/ru"),
            (56, "en", "Al-Farabi Kazakh National University", "KazGU", "http://www.kaznu.kz/en/"),

            (57, "ru", "Университет экономики г. Варны", "UE-Varna", "https://www.ue-varna.bg/en/"),
            (57, "en", "University of Economics Varna", "UE-Varna", "https://www.ue-varna.bg/en/"),

            (58, "ru", "Университет Люксембурга", "UniLu", "https://wwwen.uni.lu"),
            (58, "en", "University of Luxembourg", "UniLu", "https://wwwen.uni.lu"),

            (59, "ru", "Болонский университет", "UNIBO", "http://www.unibo.it/it"),
            (59, "en", "University of Bologna", "UNIBO", "http://www.unibo.it/it"),

            (60, "ru", "Университет ИТМО", "ITMO", "http://www.ifmo.ru/ru/"),
            (60, "en", "ITMO University", "ITMO", "http://en.ifmo.ru/en/"),

            (61, "ru", "Университет «Высшая школа экономики»", "HSE University", "https://www.hse.ru/"),
            (61, "en", "Higher School of Economics", "HSE University", "https://www.hse.ru/en/"),

            (62, "ru", "Московский физико-технический институт", "MIPT", "https://mipt.ru/"),
            (62, "en", "Moscow Institute of Physics and Technology", "MIPT", "https://mipt.ru/english/"),

            (63, "ru", "НИЯУ МИФИ", "MEPhI", "https://mephi.ru/"),
            (63, "en", "National Research Nuclear University MEPhI", "MEPhI", "https://eng.mephi.ru/"),

            (64, "ru", "Санкт-Петербургский политехнический университет", "SPbPU", "http://www.spbstu.ru/"),
            (64, "en", "St. Petersburg Polytechnic University", "SPbPU", "http://english.spbstu.ru/"),

            (65, "ru", "Сибирский федеральный университет", "SibFU", "http://www.sfu-kras.ru/"),
            (65, "en", "Siberian Federal University", "SibFU", "http://www.sfu-kras.ru/en"),

            (66, "ru", "Южно-Уральский государственный университет", "SUSU", "https://www.susu.ru/"),
            (66, "en", "South Ural State University", "SUSU", "https://www.susu.ru/en"),

            (67, "ru", "Тюменский государственный университет", "UT", "https://www.utmn.ru/"),
            (67, "en", "University of Tyumen", "UT", "https://www.utmn.ru/en/"),

            (68, "ru", "Первый МГМУ им. И.М.Сеченова", "MSMU", "https://www.sechenov.ru/"),
            (68, "en", "Sechenov Medical University", "MSMU", "https://www.sechenov.ru/eng/"),

            (69, "ru", "Балтийский федеральный университет им. И.Канта", "IKBFU", "https://www.kantiana.ru/"),
            (69, "en", "Immanuel Kant Baltic Federal University", "IKBFU", "http://eng.kantiana.ru/"),

            (70, "ru", "Российский университет дружбы народов", "RUDN", "http://www.rudn.ru/"),
            (70, "en", "RUDN University", "RUDN", "http://eng.rudn.ru/"),

            (71, "ru", "Казанский федеральный университет", "KFU", "http://kpfu.ru/"),
            (71, "en", "Kazan Federal University", "KFU", "http://kpfu.ru/eng"),

            (72, "ru", "Университет «МИСиС»", "NUST MISiS", "http://www.misis.ru/"),
            (72, "en", "National University of Science and Technology MISiS", "NUST MISiS", "http://en.misis.ru/"),

            (73, "ru", "Томский государственный университет", "TSU", "http://www.tsu.ru/"),
            (73, "en", "Tomsk State University", "TSU", "http://en.tsu.ru/"),

            (74, "ru", "Томский политехнический университет", "TPU", "https://tpu.ru/"),
            (74, "en", "Tomsk Polytechnic University", "TPU", "https://tpu.ru/en"),

            (75, "ru", "Дальневосточный федеральный университет", "FEFU", "https://www.dvfu.ru/"),
            (75, "en", "Far Eastern Federal University", "FEFU", "https://www.dvfu.ru/en/"),

            (76, "ru", "Уральский федеральный университет", "UrFU", "https://urfu.ru/ru/"),
            (76, "en", "Ural Federal University", "UrFU", "https://urfu.ru/en/"),

            (77, "ru", "СПбГЭУ ЛЭТИ", "ETU LETI", "http://www.eltech.ru/ru/universitet/"),
            (77, "en", "Saint Petersburg Electrotechnical University LETI", "ETU LETI", "http://www.eltech.ru/en/university/"),

            (78, "ru", "Новосибирский государственный университет", "NSU", "http://www.nsu.ru/?lang=ru"),
            (78, "en", "Novosibirsk State University", "NSU", "https://english.nsu.ru/"),

            (79, "ru", "Нижегородский государственный университет им. Лобачевского", "UNN", "http://www.unn.ru/"),
            (79, "en", "Lobachevsky University", "UNN", "http://eng.unn.ru/"),

            (80, "ru", "Самарский университет", "SSAU", "http://www.ssau.ru/"),
            (80, "en", "Samara University", "SSAU", "http://www.ssau.ru/english/"),

            (81, "ru", "Кыргызский Государственный технический университет им. И. Раззакова", "KSTU", "https://kstu.kg/"),
            (81, "en", "Kyrgyz State Technical University", "KSTU", "https://kstu.kg/en/"),
            
            (82, "ru", "Чанчуньский политехнический университет", "CPU", "http://ieec.cust.edu.cn/"),
            (82, "en", "Changchun University of Science and Technology", "CPU", "http://ieec.cust.edu.cn/"),
            
            (83, "ru", "Университет Висмара", "HS WISMAR", "https://www.hs-wismar.de/en/"),
            (83, "en", "Hochschule Wismar", "HS WISMAR", "https://www.hs-wismar.de/en/")
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE partner DROP FOREIGN KEY FK_312B3E16F92F3E70');
        $this->addSql('DROP INDEX IDX_312B3E16F92F3E70 ON partner');
        $this->addSql('ALTER TABLE partner DROP country_id');
    }
}
