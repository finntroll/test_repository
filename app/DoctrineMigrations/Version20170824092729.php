<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170824092729 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('INSERT INTO partner (`id`, `logo`, `is_foreign`, `country_id`) VALUES
            (84, "", TRUE, 13),
            (85, "", TRUE, 14)
        ');

        $this->addSql('
            INSERT INTO partner_translation (`translatable_id`, `locale`, `name`, `short_name`, `url`) VALUES
                (84, "ru", "Таллинский технический университет", "TTU", "https://www.ttu.ee/ru"),
                (84, "en", "Tallinn University of Technology", "TTU", "https://www.ttu.ee/en/"),

                (85, "ru", "Карагандинский государсвтенный технический университет", "KSTU", "http://www.kstu.kz/"),
                (85, "en", "Karaganda State Technical University", "KSTU", "http://www.kstu.kz/?lang=en")
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DELETE FROM partner WHERE id = 84 OR id = 85 LIMIT 2');
    }
}
