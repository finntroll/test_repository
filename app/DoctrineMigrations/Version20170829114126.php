<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170829114126 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');


        $this->addSql('CREATE TABLE academic_mobility_type (id INT AUTO_INCREMENT NOT NULL, trans_key VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('INSERT INTO academic_mobility_type (`id`, `trans_key`) VALUES 
            (1, "internship"),
            (2, "postdoc"),
            (3, "training"),
            (4, "schools"),
            (5, "conference"),
            (6, "seminar"),
            (7, "lecture"),
            (8, "master_class"),
            (9, "online")
        ');

        $this->addSql('ALTER TABLE academic_mobility ADD mobility_type_id INT DEFAULT NULL, DROP mobility_type');
        $this->addSql('ALTER TABLE academic_mobility ADD CONSTRAINT FK_E960A80B5F96F218 FOREIGN KEY (mobility_type_id) REFERENCES academic_mobility_type (id)');
        $this->addSql('CREATE INDEX IDX_E960A80B5F96F218 ON academic_mobility (mobility_type_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE academic_mobility DROP FOREIGN KEY FK_E960A80B5F96F218');
        $this->addSql('DROP INDEX IDX_E960A80B5F96F218 ON academic_mobility');
        $this->addSql('ALTER TABLE academic_mobility ADD mobility_type VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP mobility_type_id');
        $this->addSql('DROP TABLE academic_mobility_type');
    }
}
