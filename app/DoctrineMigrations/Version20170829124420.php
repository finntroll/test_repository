<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170829124420 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contest_language (contest_id INT NOT NULL, language_id INT NOT NULL, INDEX IDX_E30E12961CD0F0DE (contest_id), INDEX IDX_E30E129682F1BAF4 (language_id), PRIMARY KEY(contest_id, language_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contest_language ADD CONSTRAINT FK_E30E12961CD0F0DE FOREIGN KEY (contest_id) REFERENCES contest (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contest_language ADD CONSTRAINT FK_E30E129682F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contest DROP FOREIGN KEY FK_1A95CB582F1BAF4');
        $this->addSql('DROP INDEX IDX_1A95CB582F1BAF4 ON contest');
        $this->addSql('ALTER TABLE contest DROP language_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contest_language');
        $this->addSql('ALTER TABLE contest ADD language_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contest ADD CONSTRAINT FK_1A95CB582F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('CREATE INDEX IDX_1A95CB582F1BAF4 ON contest (language_id)');
    }
}
