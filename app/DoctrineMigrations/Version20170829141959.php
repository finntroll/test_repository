<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170829141959 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contest_type (id INT AUTO_INCREMENT NOT NULL, transKey VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_B54F9F31E773AFFB (transKey), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contest ADD contest_type_id INT DEFAULT NULL, DROP project_type');
        $this->addSql('ALTER TABLE contest ADD CONSTRAINT FK_1A95CB5535280F6 FOREIGN KEY (contest_type_id) REFERENCES contest_type (id)');
        $this->addSql('CREATE INDEX IDX_1A95CB5535280F6 ON contest (contest_type_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contest DROP FOREIGN KEY FK_1A95CB5535280F6');
        $this->addSql('DROP TABLE contest_type');
        $this->addSql('DROP INDEX IDX_1A95CB5535280F6 ON contest');
        $this->addSql('ALTER TABLE contest ADD project_type VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP contest_type_id');
    }
}
