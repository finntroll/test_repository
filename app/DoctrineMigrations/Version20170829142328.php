<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170829142328 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('INSERT INTO contest_type (`id`, `transKey`) VALUES 
            (1, "contest_type.contest_grants"),
            (2, "contest_type.contest_international"),
            (3, "contest_type.contest_niokr")
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DELETE FROM contest_type WHERE id = 1 OR id = 2 OR id = 3 LIMIT 3');
    }
}
