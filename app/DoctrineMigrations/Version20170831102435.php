<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170831102435 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('INSERT INTO agu_user (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `password`, `roles`, `university_id`) VALUES
                            (3, "mipt", "mipt", "mipt@ifmo.ru", "mipt@ifmo.ru", true, "$2y$13$ai9OBRWIQ70LQKED1es27uVh3zXkmyUPoIogHlOtib5dmO5/QHTiO", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 3),
                            (4, "mephi", "mephi", "mephi@globaluni.ru", "mephi@globaluni.ru", true, "$2y$13$MjB0a63QHr/ZqrZYtBUjNuwbqkhGZxtzPu5iuX3AZkvkzQepGvg3y", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 4),
                            (5, "spbpu", "spbpu", "spbpu@globaluni.ru", "spbpu@globaluni.ru", true, "$2y$13$kIDitnRZzweDw1jgaB2lCumdFHmhcKFUX4yszXNyfoxm9eNPkIcSS", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 5),
                            (6, "sibfu", "sibfu", "sibfu@globaluni.ru", "sibfu@globaluni.ru", true, "$2y$13$Qx23TPil1mP8LOTNieHg8OG7qhdhSReFCtDMf/ryWlP4D5R7ndMCu", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 6),
                            (7, "susu", "susu", "susu@globaluni.ru", "susu@globaluni.ru", true, "$2y$13$kMV5NfTdqWkO6nEh4j.6EelhfKxIBYVULqQIYuzDYTOPbLpFgYU7i", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 7),
                            (8, "utmn", "utmn", "utmn@globaluni.ru", "utmn@globaluni.ru", true, "$2y$13$gLn46s31KUG76XOPhKdJeubGLFKA1/R.jahk3S3ksRrsHiELNAkJS", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 8),
                            (9, "msmu", "msmu", "msmu@globaluni.ru", "msmu@globaluni.ru", true, "$2y$13$.QTCyq39LoARvMdZqBlkAulLZFDtKWlfdvK0hWAb5CuG8OdQPY08y", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 9),
                            (10, "ikbfu", "ikbfu", "ikbfu@globaluni.ru", "ikbfu@globaluni.ru", true, "$2y$13$BkqSnFMIqxJ8JUaJcrANDOyyq5kHZW5re1u9NTpoVfVPB7a0RuXOG", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 10),
                            (11, "rudn", "rudn", "rudn@globaluni.ru", "rudn@globaluni.ru", true, "$2y$13$HCMfuQl9mRZjbeSB/TIXAu3Gh.6o4.fKnYsWntcQ.7gkVZxm0QvLW", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 11),
                            (12, "kfu", "kfu", "kfu@globaluni.ru", "kfu@globaluni.ru", true, "$2y$13$uEpzL3Y6Xh4u6cXEIZs5deMSL4lreONmfHhGZNP.PrUg9OMl3wcsS", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 12),
                            (13, "misis", "misis", "misis@globaluni.ru", "misis@globaluni.ru", true, "$2y$13$22C8vMkjdtohV7KjhjAsQOS9NJNJZeM5OXTMVO0rrZbPDMs3KK.0W", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 13),
                            (14, "tsu", "tsu", "tsu@globaluni.ru", "tsu@globaluni.ru", true, "$2y$13$ucqNNCPcAREETYee8X88lOLlBf/ypPe5FOUkiR728t2v/q021cjrS", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 14),
                            (15, "tpu", "tpu", "tpu@globaluni.ru", "tpu@globaluni.ru", true, "$2y$13$TrRNdXYQaSi0wg5subBgXeuoLI5bZGmaouHZkGrtNQl0A39cmkm.S", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 15),
                            (16, "fefu", "fefu", "fefu@globaluni.ru", "fefu@globaluni.ru", true, "$2y$13$Yw5YX1za3O/XMqQ3tSHq6ef5FHAOGXlIp.K7voY3cTruhAhGVUDMi", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 16),
                            (17, "urfu", "urfu", "urfu@globaluni.ru", "urfu@globaluni.ru", true, "$2y$13$478R2uItR3E7jnDwRXrZqeIYJzjf4TxiUBhIOeuSVkG//A/vw9rS6", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 17),
                            (18, "leti", "leti", "leti@globaluni.ru", "leti@globaluni.ru", true, "$2y$13$iOIuuyN3Czg71UPincgcA.lLsDSnhIZLBtq0y.OJcm9U68MLPNw.G", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 18),
                            (19, "nsu", "nsu", "nsu@globaluni.ru", "nsu@globaluni.ru", true, "$2y$13$/ec4NLlLMpo1cwT1g2sRf.7.WOgSVOlTrDGFbBVCrZIW44LojTSe6", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 19),
                            (20, "unn", "unn", "unn@globaluni.ru", "unn@globaluni.ru", true, "$2y$13$lBlvG1Fm14zokvgRrlfOmuuk7I22NB9UkJ3G/kAVikBD17AYDzUaW", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 20),
                            (21, "samu", "samu", "samu@globaluni.ru", "samu@globaluni.ru", true, "$2y$13$ZqmGJpTUyq.7P0PllMFuPu.1KEgIcfbgzMo5Q.RW8thyrMkbW5P9e", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 21)
                        ');
        $this->addSql('UPDATE agu_user set roles = "a:2:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";i:1;s:15:\"ROLE_SITE_ADMIN\";}" WHERE id = 1');
        $this->addSql('UPDATE agu_user set roles = "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}" WHERE id = 2');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

    }
}
