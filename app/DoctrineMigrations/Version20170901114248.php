<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170901114248 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE news_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, title VARCHAR(155) NOT NULL, lead TEXT NOT NULL, content TEXT NOT NULL, videoLink VARCHAR(2000) DEFAULT NULL, sourceLink VARCHAR(2000) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_9D5CF3202C2AC5D3 (translatable_id), UNIQUE INDEX news_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE news_type (id INT AUTO_INCREMENT NOT NULL, trans_key VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_5903A1A2E2C1CEB1 (trans_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE keyword (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_5A93713B5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE news_keyword (news_id INT NOT NULL, keyword_id INT NOT NULL, INDEX IDX_D776DDDB5A459A0 (news_id), INDEX IDX_D776DDD115D4552 (keyword_id), PRIMARY KEY(news_id, keyword_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rubric (id INT AUTO_INCREMENT NOT NULL, trans_key VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_60C4016CE2C1CEB1 (trans_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE news_translation ADD CONSTRAINT FK_9D5CF3202C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES news (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE news_keyword ADD CONSTRAINT FK_D776DDDB5A459A0 FOREIGN KEY (news_id) REFERENCES news (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE news_keyword ADD CONSTRAINT FK_D776DDD115D4552 FOREIGN KEY (keyword_id) REFERENCES keyword (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE news ADD type_id INT DEFAULT NULL, ADD rubric_id INT DEFAULT NULL, ADD is_important TINYINT(1) DEFAULT \'0\' NOT NULL, DROP lead, DROP content, DROP video_link, DROP type, DROP rubric, CHANGE university_id university_id INT DEFAULT NULL, CHANGE updated_at published_at DATETIME DEFAULT NULL, CHANGE status status INT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE news ADD CONSTRAINT FK_1DD39950C54C8C93 FOREIGN KEY (type_id) REFERENCES news_type (id)');
        $this->addSql('ALTER TABLE news ADD CONSTRAINT FK_1DD39950A29EC0FC FOREIGN KEY (rubric_id) REFERENCES rubric (id)');
        $this->addSql('ALTER TABLE news ADD CONSTRAINT FK_1DD39950309D1878 FOREIGN KEY (university_id) REFERENCES university (id)');
        $this->addSql('CREATE INDEX IDX_1DD39950C54C8C93 ON news (type_id)');
        $this->addSql('CREATE INDEX IDX_1DD39950A29EC0FC ON news (rubric_id)');
        $this->addSql('CREATE INDEX IDX_1DD39950309D1878 ON news (university_id)');

        $this->addSql('INSERT INTO rubric (`id`, `trans_key`) VALUES 
            (1, "rubric.science"),
            (2, "rubric.startup_and_business"),
            (3, "rubric.it"),
            (4, "rubric.education")
        ');

        $this->addSql('INSERT INTO news_type (`id`, `trans_key`) VALUES
            (1, "news_type.university_news"),
            (2, "news_type.association_news")
        ');
        $this->addSql('INSERT INTO agu_user (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `password`, `roles`, `university_id`) VALUES
            (22, "god", "god", "god@ifmo.ru", "god@ifmo.ru", true, "$2y$13$UuLDOdapTJUfeYI1dYsKoehPKZGstiNa1pkI5mXlBApqpMEydYHbC", "a:2:{i:0;s:16:\"ROLE_NEWS_EDITOR\";i:1;s:15:\"ROLE_SITE_ADMIN\";}", 1),
            (23, "newseditor", "newseditor", "newseditor@agu.ru", "newseditor@agu.ru", true, "$2y$13$Uz.S.w77YVH6JB7V0/LyZujJT3v.R0O7vRR1e0uM5ZLb4YUGCTGXS", "a:1:{i:0;s:16:\"ROLE_NEWS_EDITOR\";}", null)
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE news DROP FOREIGN KEY FK_1DD39950C54C8C93');
        $this->addSql('ALTER TABLE news_keyword DROP FOREIGN KEY FK_D776DDD115D4552');
        $this->addSql('ALTER TABLE news DROP FOREIGN KEY FK_1DD39950A29EC0FC');
        $this->addSql('DROP TABLE news_translation');
        $this->addSql('DROP TABLE news_type');
        $this->addSql('DROP TABLE keyword');
        $this->addSql('DROP TABLE news_keyword');
        $this->addSql('DROP TABLE rubric');
        $this->addSql('ALTER TABLE news DROP FOREIGN KEY FK_1DD39950309D1878');
        $this->addSql('DROP INDEX IDX_1DD39950C54C8C93 ON news');
        $this->addSql('DROP INDEX IDX_1DD39950A29EC0FC ON news');
        $this->addSql('DROP INDEX IDX_1DD39950309D1878 ON news');
        $this->addSql('ALTER TABLE news ADD lead VARCHAR(400) DEFAULT NULL COLLATE utf8_unicode_ci, ADD content TEXT NOT NULL COLLATE utf8_unicode_ci, ADD video_link VARCHAR(500) DEFAULT NULL COLLATE utf8_unicode_ci, ADD type INT DEFAULT 0 NOT NULL, ADD rubric INT DEFAULT 0 NOT NULL, DROP type_id, DROP rubric_id, DROP is_important, CHANGE university_id university_id INT DEFAULT 0 NOT NULL, CHANGE published_at updated_at DATETIME DEFAULT NULL, CHANGE status status INT DEFAULT 2 NOT NULL');
        $this->addSql('DELETE FROM agu_user WHERE id = 22 OR id = 23;');
    }
}
