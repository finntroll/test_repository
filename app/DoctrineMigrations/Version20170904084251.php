<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170904084251 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE best_practice (id INT AUTO_INCREMENT NOT NULL, scope_id INT DEFAULT NULL, status SMALLINT DEFAULT 1 NOT NULL, INDEX IDX_FE6D461F682B5931 (scope_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE best_practice_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, title VARCHAR(155) NOT NULL, text TEXT NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_D088C1782C2AC5D3 (translatable_id), UNIQUE INDEX best_practice_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE best_practice ADD CONSTRAINT FK_FE6D461F682B5931 FOREIGN KEY (scope_id) REFERENCES scope (id)');
        $this->addSql('ALTER TABLE best_practice_translation ADD CONSTRAINT FK_D088C1782C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES best_practice (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE best_practice ADD university_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE best_practice ADD CONSTRAINT FK_FE6D461F309D1878 FOREIGN KEY (university_id) REFERENCES university (id)');
        $this->addSql('CREATE INDEX IDX_FE6D461F309D1878 ON best_practice (university_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE best_practice_translation DROP FOREIGN KEY FK_D088C1782C2AC5D3');
        $this->addSql('ALTER TABLE best_practice DROP FOREIGN KEY FK_FE6D461F309D1878');
        $this->addSql('DROP TABLE best_practice');
        $this->addSql('DROP TABLE best_practice_translation');
    }
}
