<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170907133943 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO news_type (`id`, `trans_key`) VALUES
            (3, "news_type.mass_media_news")
        ');
        $this->addSql('INSERT INTO agu_user (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `password`, `roles`, `university_id`) VALUES
            (24, "hse", "hse", "hse@agu.ru", "hse@agu.ru", true, "$2y$13$q0sLEQl8gNcUG68yjSWgduiof6Poy7xhTkCAM0DqCx.uqftNHL1zm", "a:1:{i:0;s:21:\"ROLE_UNIVERSITY_ADMIN\";}", 2)
        ');

        $this->addSql('UPDATE agu_user SET roles = "a:1:{i:0;s:15:\"ROLE_SITE_ADMIN\";}", university_id = null WHERE id = 2');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            DELETE FROM news_type WHERE id = 3;
        ');
    }
}
