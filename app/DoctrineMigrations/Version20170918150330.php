<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170918150330 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE best_practice DROP FOREIGN KEY FK_FE6D461F3DA5256D');
        $this->addSql('ALTER TABLE best_practice ADD CONSTRAINT FK_FE6D461F3DA5256D FOREIGN KEY (image_id) REFERENCES image (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE news ADD image_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE news ADD CONSTRAINT FK_1DD399503DA5256D FOREIGN KEY (image_id) REFERENCES image (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1DD399503DA5256D ON news (image_id)');
        $this->addSql('ALTER TABLE partner ADD logo_id INT DEFAULT NULL, DROP logo, CHANGE status status SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE partner ADD CONSTRAINT FK_312B3E16F98F144A FOREIGN KEY (logo_id) REFERENCES image (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_312B3E16F98F144A ON partner (logo_id)');

        $this->addSql('ALTER TABLE university ADD logo_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE university ADD CONSTRAINT FK_A07A85ECF98F144A FOREIGN KEY (logo_id) REFERENCES image (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A07A85ECF98F144A ON university (logo_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE best_practice DROP FOREIGN KEY FK_FE6D461F3DA5256D');
        $this->addSql('ALTER TABLE best_practice ADD CONSTRAINT FK_FE6D461F3DA5256D FOREIGN KEY (image_id) REFERENCES image (id)');
        $this->addSql('ALTER TABLE news DROP FOREIGN KEY FK_1DD399503DA5256D');
        $this->addSql('DROP INDEX UNIQ_1DD399503DA5256D ON news');
        $this->addSql('ALTER TABLE news DROP image_id');
        $this->addSql('ALTER TABLE partner DROP FOREIGN KEY FK_312B3E16F98F144A');
        $this->addSql('DROP INDEX UNIQ_312B3E16F98F144A ON partner');
        $this->addSql('ALTER TABLE partner ADD logo VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP logo_id, CHANGE status status SMALLINT DEFAULT 3 NOT NULL');

        $this->addSql('ALTER TABLE university DROP FOREIGN KEY FK_A07A85ECF98F144A');
        $this->addSql('DROP INDEX UNIQ_A07A85ECF98F144A ON university');
        $this->addSql('ALTER TABLE university DROP logo_id');
    }
}
