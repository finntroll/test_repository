<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170919161441 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE city_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, locale VARCHAR(3) NOT NULL, INDEX IDX_97DD5B602C2AC5D3 (translatable_id), UNIQUE INDEX city_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE city_translation ADD CONSTRAINT FK_97DD5B602C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE city DROP trans_key');
        $this->addSql('INSERT INTO city_translation (`translatable_id`, `name`, `locale`) VALUES
                          (1, "Санкт-Петербург", "ru"),
                          (2, "Москва", "ru"),
                          (3, "Новосибирск", "ru"),
                          (4, "Томск", "ru"),
                          (5, "Казань", "ru"),
                          (6, "Владивосток", "ru"),
                          (7, "Екатеринбург", "ru"),
                          (8, "Самара", "ru"),
                          (9, "Нижний Новгород", "ru"),
                          (10, "Красноярск", "ru"),
                          (11, "Тюмень", "ru"),
                          (12, "Челябинск", "ru"),
                          (13, "Пермь", "ru"),
                          (14, "Калининград", "ru"),
                          (1, "Saint-Petersburg", "en"),
                          (2, "Moscow", "en"),
                          (3, "Novosibirsk", "en"),
                          (4, "Tomsk", "en"),
                          (5, "Kazan", "en"),
                          (6, "Vladivostok", "en"),
                          (7, "Yekaterinburg", "en"),
                          (8, "Samara", "en"),
                          (9, "Nizhniy Novgorod", "en"),
                          (10, "Krasnoyarsk", "en"),
                          (11, "Tyumen", "en"),
                          (12, "Chelyabinsk", "en"),
                          (13, "Perm", "en"),
                          (14, "Kaliningrad", "en")
                     ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE city_translation');
        $this->addSql('ALTER TABLE city ADD trans_key VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql(
            'UPDATE city SET 
                  trans_key = (CASE WHEN id = 1 THEN \'spb\'
                                    WHEN id = 2 THEN \'woscow\'
                                    WHEN id = 3 THEN \'novosibirsk\'
                                    WHEN id = 4 THEN \'tomsk\'
                                    WHEN id = 5 THEN \'kazan\'
                                    WHEN id = 6 THEN \'vladivostok\'
                                    WHEN id = 7 THEN \'ekaterinburg\'
                                    WHEN id = 8 THEN \'samara\'
                                    WHEN id = 9 THEN \'nizhnii_novgorod\'
                                    WHEN id = 10 THEN \'krasnoyarsk\'
                                    WHEN id = 11 THEN \'tymen\'
                                    WHEN id = 12 THEN \'chelyabinsk\'
                                    WHEN id = 13 THEN \'perm\'
                                    WHEN id = 14 THEN \'kaliningrad\'
                                END 
                  ) WHERE id BETWEEN 1 AND 14
        ');
        $this->addSql('DELETE FROM city WHERE id > 14');
    }
}
