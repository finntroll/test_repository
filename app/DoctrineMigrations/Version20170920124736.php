<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170920124736 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE best_practice ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE news ADD updated_at DATETIME DEFAULT NULL, CHANGE created_at created_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE academic_mobility ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE contest ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE partner ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE program ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL');
        $this->addSql('UPDATE best_practice set created_at = NOW(), updated_at = NOW()');
        $this->addSql('UPDATE news set created_at = NOW(), updated_at = NOW()');
        $this->addSql('UPDATE academic_mobility set created_at = NOW(), updated_at = NOW()');
        $this->addSql('UPDATE contest set created_at = NOW(), updated_at = NOW()');
        $this->addSql('UPDATE partner set created_at = NOW(), updated_at = NOW()');
        $this->addSql('UPDATE program set created_at = NOW(), updated_at = NOW()');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE academic_mobility DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE best_practice DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE contest DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE news DROP updated_at');
        $this->addSql('ALTER TABLE partner DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE program DROP created_at, DROP update_at');
    }
}
