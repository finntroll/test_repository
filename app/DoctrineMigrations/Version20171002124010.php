<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171002124010 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agu_user ADD photo_id INT DEFAULT NULL, ADD class_type VARCHAR(20) NOT NULL, ADD association_position VARCHAR(255) DEFAULT NULL, ADD professional_position VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE agu_user ADD CONSTRAINT FK_7A09C01A7E9E4C8C FOREIGN KEY (photo_id) REFERENCES image (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_7A09C01A7E9E4C8C ON agu_user (photo_id)');

        $this->addSql('UPDATE agu_user SET class_type=\'user\'');

        $this->addSql('INSERT INTO agu_user (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `password`, `roles`, `university_id`, `class_type`, `association_position`, `professional_position`) VALUES
            (25, "pr_member", "pr_member", "pr_member@agu.ru", "pr_member@agu.ru", true, "$2y$13$/DNFMrO5mPME9PH90uRWTei1y5NDru74cCvZJ/xpio.KCRua807ra", "a:1:{i:0;s:19:\"ROLE_PROJECT_MEMBER\";}", 1, "personality", "Участник проекта", "Замдиректора"),
            (26, "pr_manager", "pr_manager", "pr_manager@agu.ru", "pr_manager@agu.ru", true, "$2y$13$CpRIuWkqOD3uFwSwTUXav.K0Di42S/Kz3BLjnTVXBSWAyaYFWEO1S", "a:1:{i:0;s:20:\"ROLE_PROJECT_MANAGER\";}", 1, "personality", "Менеджер проекта", "Ассистент"),
            (27, "pr_head", "pr_head", "pr_head@agu.ru", "pr_head@agu.ru", true, "$2y$13$kP.KwAc9haoE5sfnvixNv.Iyxl62SX9VwOuVAmnaTzfMpYMX7AUE6", "a:1:{i:0;s:17:\"ROLE_PROJECT_HEAD\";}", 1, "personality", "Руководитель проекта", "Проректор")
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agu_user DROP FOREIGN KEY FK_7A09C01A7E9E4C8C');
        $this->addSql('DROP INDEX IDX_7A09C01A7E9E4C8C ON agu_user');
        $this->addSql('ALTER TABLE agu_user DROP photo_id, DROP class_type, DROP association_position, DROP professional_position');

        $this->addSql('DELETE FROM agu_user WHERE id = 25 OR id = 26 OR id = 27');
    }
}
