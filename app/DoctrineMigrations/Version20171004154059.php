<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171004154059 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE best_practice_document (best_practice_id INT NOT NULL, document_id INT NOT NULL, INDEX IDX_AFDBDC90E41BD4E5 (best_practice_id), INDEX IDX_AFDBDC90C33F7837 (document_id), PRIMARY KEY(best_practice_id, document_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE university_document (university_id INT NOT NULL, document_id INT NOT NULL, INDEX IDX_6074A8DC309D1878 (university_id), INDEX IDX_6074A8DCC33F7837 (document_id), PRIMARY KEY(university_id, document_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE news_document (news_id INT NOT NULL, document_id INT NOT NULL, INDEX IDX_91572927B5A459A0 (news_id), INDEX IDX_91572927C33F7837 (document_id), PRIMARY KEY(news_id, document_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE uploaded_document (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, path VARCHAR(255) NOT NULL, size INT NOT NULL, mime_type VARCHAR(255) NOT NULL, extension VARCHAR(255) NOT NULL, status INT DEFAULT 0 NOT NULL, internal_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE best_practice_document ADD CONSTRAINT FK_AFDBDC90E41BD4E5 FOREIGN KEY (best_practice_id) REFERENCES best_practice (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE best_practice_document ADD CONSTRAINT FK_AFDBDC90C33F7837 FOREIGN KEY (document_id) REFERENCES uploaded_document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE university_document ADD CONSTRAINT FK_6074A8DC309D1878 FOREIGN KEY (university_id) REFERENCES university (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE university_document ADD CONSTRAINT FK_6074A8DCC33F7837 FOREIGN KEY (document_id) REFERENCES uploaded_document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE news_document ADD CONSTRAINT FK_91572927B5A459A0 FOREIGN KEY (news_id) REFERENCES news (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE news_document ADD CONSTRAINT FK_91572927C33F7837 FOREIGN KEY (document_id) REFERENCES uploaded_document (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE best_practice_document DROP FOREIGN KEY FK_AFDBDC90C33F7837');
        $this->addSql('ALTER TABLE university_document DROP FOREIGN KEY FK_6074A8DCC33F7837');
        $this->addSql('ALTER TABLE news_document DROP FOREIGN KEY FK_91572927C33F7837');
        $this->addSql('DROP TABLE best_practice_document');
        $this->addSql('DROP TABLE university_document');
        $this->addSql('DROP TABLE news_document');
        $this->addSql('DROP TABLE uploaded_document');
    }
}
