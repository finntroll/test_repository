<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171009131030 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE association_position (id INT AUTO_INCREMENT NOT NULL, trans_key VARCHAR(100) NOT NULL, UNIQUE INDEX UNIQ_70E1FC93E2C1CEB1 (trans_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE agu_user ADD association_position_id INT DEFAULT NULL, DROP association_position');
        $this->addSql('ALTER TABLE agu_user ADD CONSTRAINT FK_7A09C01AF610BFB6 FOREIGN KEY (association_position_id) REFERENCES association_position (id)');
        $this->addSql('CREATE INDEX IDX_7A09C01AF610BFB6 ON agu_user (association_position_id)');

        $this->addSql('INSERT INTO association_position (id, trans_key) VALUES 
                            (1, "chairman"),
                            (2, "executive"),
                            (3, "council_member"),
                            (4, "honorary_council_member"),
                            (5, "revision_commettee_member"),
                            (6, "revision_commettee_chairman")
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agu_user DROP FOREIGN KEY FK_7A09C01AF610BFB6');
        $this->addSql('DROP TABLE association_position');
        $this->addSql('DROP INDEX IDX_7A09C01AF610BFB6 ON agu_user');
        $this->addSql('ALTER TABLE agu_user ADD association_position VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, DROP association_position_id');
    }
}
