<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171013115050 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agu_user ADD work_place_id INT DEFAULT NULL, ADD phone VARCHAR(25) DEFAULT NULL, ADD is_deleted TINYINT(1) NOT NULL, ADD registration_token VARCHAR(64) DEFAULT NULL');
        $this->addSql('ALTER TABLE agu_user ADD CONSTRAINT FK_7A09C01AD8132845 FOREIGN KEY (work_place_id) REFERENCES partner (id)');
        $this->addSql('CREATE INDEX IDX_7A09C01AD8132845 ON agu_user (work_place_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agu_user DROP FOREIGN KEY FK_7A09C01AD8132845');
        $this->addSql('DROP INDEX IDX_7A09C01AD8132845 ON agu_user');
        $this->addSql('ALTER TABLE agu_user DROP work_place_id, DROP phone, DROP is_deleted, DROP registration_token');
    }
}
