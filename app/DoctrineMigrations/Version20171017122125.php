<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171017122125 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE project_passport (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project_passport_document (project_passport_id INT NOT NULL, document_id INT NOT NULL, INDEX IDX_26F0C339633BE72B (project_passport_id), INDEX IDX_26F0C339C33F7837 (document_id), PRIMARY KEY(project_passport_id, document_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project_member (id INT AUTO_INCREMENT NOT NULL, personality_id INT DEFAULT NULL, project_id INT DEFAULT NULL, cause VARCHAR(255) DEFAULT NULL, INDEX IDX_67401132CF3DE080 (personality_id), INDEX IDX_67401132166D1F9C (project_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, purpose VARCHAR(255) DEFAULT NULL, locale VARCHAR(3) NOT NULL, INDEX IDX_7CA6B2942C2AC5D3 (translatable_id), UNIQUE INDEX project_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project_outlay (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project_outlay_document (project_outlay_id INT NOT NULL, document_id INT NOT NULL, INDEX IDX_4B2B26C8DC2B1628 (project_outlay_id), INDEX IDX_4B2B26C8C33F7837 (document_id), PRIMARY KEY(project_outlay_id, document_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, head_id INT DEFAULT NULL, manager_id INT DEFAULT NULL, passport_id INT DEFAULT NULL, outlay_id INT DEFAULT NULL, university_id INT DEFAULT NULL, date_start DATE DEFAULT NULL, date_end DATE DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, is_published TINYINT(1) NOT NULL, INDEX IDX_2FB3D0EEF41A619E (head_id), INDEX IDX_2FB3D0EE783E3463 (manager_id), UNIQUE INDEX UNIQ_2FB3D0EEABF410D0 (passport_id), UNIQUE INDEX UNIQ_2FB3D0EECD1753FF (outlay_id), INDEX IDX_2FB3D0EE309D1878 (university_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project_document (project_id INT NOT NULL, document_id INT NOT NULL, INDEX IDX_E52701AD166D1F9C (project_id), INDEX IDX_E52701ADC33F7837 (document_id), PRIMARY KEY(project_id, document_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE project_passport_document ADD CONSTRAINT FK_26F0C339633BE72B FOREIGN KEY (project_passport_id) REFERENCES project_passport (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_passport_document ADD CONSTRAINT FK_26F0C339C33F7837 FOREIGN KEY (document_id) REFERENCES uploaded_document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_member ADD CONSTRAINT FK_67401132CF3DE080 FOREIGN KEY (personality_id) REFERENCES agu_user (id)');
        $this->addSql('ALTER TABLE project_member ADD CONSTRAINT FK_67401132166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE project_translation ADD CONSTRAINT FK_7CA6B2942C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_outlay_document ADD CONSTRAINT FK_4B2B26C8DC2B1628 FOREIGN KEY (project_outlay_id) REFERENCES project_outlay (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_outlay_document ADD CONSTRAINT FK_4B2B26C8C33F7837 FOREIGN KEY (document_id) REFERENCES uploaded_document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEF41A619E FOREIGN KEY (head_id) REFERENCES agu_user (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE783E3463 FOREIGN KEY (manager_id) REFERENCES agu_user (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEABF410D0 FOREIGN KEY (passport_id) REFERENCES project_passport (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EECD1753FF FOREIGN KEY (outlay_id) REFERENCES project_outlay (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE309D1878 FOREIGN KEY (university_id) REFERENCES university (id)');
        $this->addSql('ALTER TABLE project_document ADD CONSTRAINT FK_E52701AD166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_document ADD CONSTRAINT FK_E52701ADC33F7837 FOREIGN KEY (document_id) REFERENCES uploaded_document (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project_passport_document DROP FOREIGN KEY FK_26F0C339633BE72B');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EEABF410D0');
        $this->addSql('ALTER TABLE project_outlay_document DROP FOREIGN KEY FK_4B2B26C8DC2B1628');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EECD1753FF');
        $this->addSql('ALTER TABLE project_member DROP FOREIGN KEY FK_67401132166D1F9C');
        $this->addSql('ALTER TABLE project_translation DROP FOREIGN KEY FK_7CA6B2942C2AC5D3');
        $this->addSql('ALTER TABLE project_document DROP FOREIGN KEY FK_E52701AD166D1F9C');
        $this->addSql('DROP TABLE project_passport');
        $this->addSql('DROP TABLE project_passport_document');
        $this->addSql('DROP TABLE project_member');
        $this->addSql('DROP TABLE project_translation');
        $this->addSql('DROP TABLE project_outlay');
        $this->addSql('DROP TABLE project_outlay_document');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE project_document');
    }
}
