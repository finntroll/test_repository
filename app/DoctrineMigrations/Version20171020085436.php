<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171020085436 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agu_user ADD link VARCHAR(255) DEFAULT NULL');
        $this->addSql('INSERT INTO association_position (id, trans_key) VALUES
                            (7, "member")
        ');
        $this->addSql('DELETE FROM agu_user WHERE id = 25 OR id = 26 OR id = 27');
        $this->addSql('INSERT INTO `partner` (`id`, `is_foreign`, `country_id`, `status`, `logo_id`, `created_at`, `updated_at`) VALUES 
                            (218,0,18,3,NULL,\'2017-10-20 13:58:19\',\'2017-10-20 13:58:19\'),
                            (219,0,18,3,NULL,\'2017-10-20 14:11:08\',\'2017-10-20 14:11:08\')');

        $this->addSql('INSERT INTO `partner_translation` (`id`, `translatable_id`, `name`, `short_name`, `url`, `locale`) VALUES 
                            (421,218,\'АО \"РВК\"\',\'РВК\',\'http://www.rvc.ru/\',\'ru\'),
                            (422,219,\'Московская школа управления \"СКОЛКОВО\"\',\'Сколково\',\'http://school.skolkovo.ru/ru/\',\'ru\')');

        $this->addSql('INSERT INTO `agu_user` (`id`, `university_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `first_name`, `surname`, `new_email`, `photo_id`, `class_type`, `professional_position`, `patronymic`, `association_position_id`, `work_place_id`, `phone`, `is_deleted`, `registration_token`, `link`) VALUES 
                            (25,NULL,\'kuzminov@globaluni.ru\',\'kuzminov@globaluni.ru\',\'kuzminov@globaluni.ru\',\'kuzminov@globaluni.ru\',0,NULL,\'\',NULL,NULL,NULL,\'a:0:{}\',\'Ярослав\',\'Кузьминов\',NULL,NULL,\'personality\',\'Ректор\',\'Иванович\',1,61,\'+7 (000) 000-0000\',0,NULL,\'https://www.hse.ru/rector/\'),
                            (26,NULL,\'karelina@globaluni.ru\',\'karelina@globaluni.ru\',\'karelina@globaluni.ru\',\'karelina@globaluni.ru\',0,NULL,\'\',NULL,NULL,NULL,\'a:0:{}\',\'Ирина\',\'Карелина\',NULL,NULL,\'personality\',\'Директор программы развития\',\'Георгиевна\',2,61,\'+7 (999) 999-9999\',0,NULL,\'https://www.hse.ru/staff/ikarelina\'),
                            (27,NULL,\'koksharov@globaluni.ru\',\'koksharov@globaluni.ru\',\'koksharov@globaluni.ru\',\'koksharov@globaluni.ru\',0,NULL,\'\',NULL,NULL,NULL,\'a:0:{}\',\'Виктор\',\'Кокшаров\',NULL,NULL,\'personality\',\'Ректор\',\'Анатольевич\',3,76,\'+7 (111) 111-1111\',0,NULL,\'https://urfu.ru/ru/about/administration/persons/content/6/\'),
                            (28,NULL,\'strikhanov@globaluni.ru\',\'strikhanov@globaluni.ru\',\'strikhanov@globaluni.ru\',\'strikhanov@globaluni.ru\',0,NULL,\'\',NULL,NULL,NULL,\'a:0:{}\',\'Михаил\',\'Стриханов\',NULL,NULL,\'personality\',\'Ректор\',\'Николаевич\',3,63,\'+7 (222) 222-2222\',0,NULL,\'https://mephi.ru/about/leaders/92766/\'),
                            (29,NULL,\'fedoruk@globaluni.ru\',\'fedoruk@globaluni.ru\',\'fedoruk@globaluni.ru\',\'fedoruk@globaluni.ru\',0,NULL,\'\',NULL,NULL,NULL,\'a:0:{}\',\'Михаил\',\'Федорук\',NULL,NULL,\'personality\',\'Ректор\',\'Петрович\',3,78,\'+7 (333) 333-3333\',0,NULL,\'http://nsu.ru/university/info/rektor\'),
                            (30,NULL,\'chernikova@globaluni.ru\',\'chernikova@globaluni.ru\',\'chernikova@globaluni.ru\',\'chernikova@globaluni.ru\',0,NULL,\'\',NULL,NULL,NULL,\'a:0:{}\',\'Алевтина\',\'Черникова\',NULL,NULL,\'personality\',\'Ректор\',\'Анатольевна\',3,72,\'+7 (444) 444-4444\',0,NULL,\'http://misis.ru/university/management/rector/\'),
                            (31,NULL,\'kudryavtsev@globaluni.ru\',\'kudryavtsev@globaluni.ru\',\'kudryavtsev@globaluni.ru\',\'kudryavtsev@globaluni.ru\',0,NULL,\'\',NULL,NULL,NULL,\'a:0:{}\',\'Николай\',\'Кудрявцев\',NULL,NULL,\'personality\',\'Ректор\',\'Николаевич\',3,62,\'+7 (555) 555-5555\',0,NULL,\'https://mipt.ru/persons/rectorat/kudryavtsev-nikolay-nikolaevich/\'),
                            (32,NULL,\'povalko@globaluni.ru\',\'povalko@globaluni.ru\',\'povalko@globaluni.ru\',\'povalko@globaluni.ru\',0,NULL,\'\',NULL,NULL,NULL,\'a:0:{}\',\'Александр\',\'Повалко\',NULL,NULL,\'personality\',\'Генеральный директор\',\'Борисович\',4,218,\'+7 (666) 666-6666\',0,NULL,\'https://www.rvc.ru/about/governance/sole_executive/povalko/\'),
                            (33,NULL,\'volkov@globaluni.ru\',\'volkov@globaluni.ru\',\'volkov@globaluni.ru\',\'volkov@globaluni.ru\',0,NULL,\'\',NULL,NULL,NULL,\'a:0:{}\',\'Андрей\',\'Волков\',NULL,NULL,\'personality\',\'Профессор\',\'Евгеньевич\',4,219,\'+7 (777) 777-7777\',0,\'43d91f2813e3095317b18c7bb1c26cd477704cabc7ce2ae6c9314f4b7d9f9416\',\'http://www.skolkovo.ru/public/ru/research/sedec/sedec-experts/item/3527-sedec-volkov/\'),
                            (34,NULL,\'shakhmatov@globaluni.ru\',\'shakhmatov@globaluni.ru\',\'shakhmatov@globaluni.ru\',\'shakhmatov@globaluni.ru\',0,NULL,\'\',NULL,NULL,NULL,\'a:0:{}\',\'Евгений\',\'Шахматов\',NULL,NULL,\'personality\',\'Ректор\',\'Владимирович\',6,80,\'+7 (888) 888-8888\',0,NULL,\'http://www.ssau.ru/staff/63194001-Shakhmatov-Evgeniy-Vladimirovich/\'),
                            (35,NULL,\'sycheva@globaluni.ru\',\'sycheva@globaluni.ru\',\'sycheva@globaluni.ru\',\'sycheva@globaluni.ru\',0,NULL,\'\',NULL,NULL,NULL,\'a:0:{}\',\'Елена\',\'Сычева\',NULL,NULL,\'personality\',\'Начальник отдела внутреннего аудита\',\'Сергеевна\',5,75,\'+7 (999) 999-9999\',0,NULL,\'https://www.dvfu.ru/about/rectorate/4921/the-internal-audit-department/\'),
                            (36,NULL,\'sukhushin@globaluni.ru\',\'sukhushin@globaluni.ru\',\'sukhushin@globaluni.ru\',\'sukhushin@globaluni.ru\',0,NULL,\'\',NULL,NULL,NULL,\'a:0:{}\',\'Дмитрий\',\'Сухушин\',NULL,NULL,\'personality\',\'Проректор\',\'Валерьевич\',5,73,\'+7 (000) 000-0000\',0,NULL,\'https://persona.tsu.ru/home/UserProfile/715\')');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agu_user DROP link');
        $this->addSql('DELETE FROM association_position WHERE id = 7');
        $this->addSql('DELETE FROM agu_user WHERE id > 24 AND id < 37');
        $this->addSql('DELETE FROM partner_translation WHERE id = 421 OR id = 422');
        $this->addSql('DELETE FROM partner WHERE id = 218 OR id = 219');
        $this->addSql('INSERT INTO agu_user (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `password`, `roles`, `university_id`, `class_type`, `association_position_id`, `professional_position`, `is_deleted`) VALUES
            (25, "pr_member", "pr_member", "pr_member@agu.ru", "pr_member@agu.ru", TRUE, "$2y$13$/DNFMrO5mPME9PH90uRWTei1y5NDru74cCvZJ/xpio.KCRua807ra", "a:1:{i:0;s:19:\"ROLE_PROJECT_MEMBER\";}", 1, "personality", null, "Замдиректора", false),
            (26, "pr_manager", "pr_manager", "pr_manager@agu.ru", "pr_manager@agu.ru", TRUE, "$2y$13$CpRIuWkqOD3uFwSwTUXav.K0Di42S/Kz3BLjnTVXBSWAyaYFWEO1S", "a:1:{i:0;s:20:\"ROLE_PROJECT_MANAGER\";}", 1, "personality", null, "Ассистент", false),
            (27, "pr_head", "pr_head", "pr_head@agu.ru", "pr_head@agu.ru", TRUE, "$2y$13$kP.KwAc9haoE5sfnvixNv.Iyxl62SX9VwOuVAmnaTzfMpYMX7AUE6", "a:1:{i:0;s:17:\"ROLE_PROJECT_HEAD\";}", 1, "personality", null, "Проректор", false)
        ');
    }
}
