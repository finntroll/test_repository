<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171023074901 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE university DROP FOREIGN KEY FK_A07A85ECF98F144A');
        $this->addSql('DROP INDEX UNIQ_A07A85ECF98F144A ON university');
        $this->addSql('ALTER TABLE university_translation ADD logo_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE university_translation ADD CONSTRAINT FK_A432B798F98F144A FOREIGN KEY (logo_id) REFERENCES image (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A432B798F98F144A ON university_translation (logo_id)');

        $this->addSql('UPDATE university_translation ut INNER JOIN university u ON ut.translatable_id = u.id SET ut.logo_id = u.logo_id WHERE locale = "ru"');

        $this->addSql('ALTER TABLE university DROP logo_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE university ADD logo_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE university ADD CONSTRAINT FK_A07A85ECF98F144A FOREIGN KEY (logo_id) REFERENCES image (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A07A85ECF98F144A ON university (logo_id)');
        $this->addSql('ALTER TABLE university_translation DROP FOREIGN KEY FK_A432B798F98F144A');
        $this->addSql('DROP INDEX UNIQ_A432B798F98F144A ON university_translation');
        $this->addSql('ALTER TABLE university_translation DROP logo_id');
    }
}
