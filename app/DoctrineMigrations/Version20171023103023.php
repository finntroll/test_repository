<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171023103023 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE static_page_data_translation ADD logo_id INT DEFAULT NULL, ADD left_block LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE static_page_data_translation ADD CONSTRAINT FK_C9942693F98F144A FOREIGN KEY (logo_id) REFERENCES image (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_C9942693F98F144A ON static_page_data_translation (logo_id)');

        $this->addSql('UPDATE static_page_data_translation SET 
                            page_type = "about_association", 
                            left_block = "<p>101000, Россия, г. Москва, ул. Мясницкая, 20</p><p><a class=\"university-map-link\" href=\"http://maps.google.com/?q=Россия, г. Москва, ул. Мясницкая, 20\" target=\"_blank\">Посмотреть на карте</a></p><p class=\"about-sub-title\">Справочная</p><p>+7 (495) 772-95-90, доб. 11243<br />+7 (495) 623-3783</p>"
                            WHERE id = 9 OR id = 10');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE static_page_data_translation DROP FOREIGN KEY FK_C9942693F98F144A');
        $this->addSql('DROP INDEX IDX_C9942693F98F144A ON static_page_data_translation');
        $this->addSql('ALTER TABLE static_page_data_translation DROP logo_id, DROP left_block');
        $this->addSql('UPDATE static_page_data_translation SET page_type = "page" WHERE id = 9 OR id = 10');
    }
}
