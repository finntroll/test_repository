<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171024150309 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE uploaded_document ADD content_status INT DEFAULT NULL');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EEABF410D0');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEABF410D0 FOREIGN KEY (passport_id) REFERENCES project_passport (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EECD1753FF');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EECD1753FF FOREIGN KEY (outlay_id) REFERENCES project_outlay (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE uploaded_document ADD archived_at DATETIME DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE uploaded_document DROP content_status');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EEABF410D0');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEABF410D0 FOREIGN KEY (passport_id) REFERENCES project_passport (id)');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EECD1753FF');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EECD1753FF FOREIGN KEY (outlay_id) REFERENCES project_outlay (id)');
        $this->addSql('ALTER TABLE uploaded_document DROP archived_at');
    }
}
