<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171102160106 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, first_name VARCHAR(150) DEFAULT NULL, surname VARCHAR(150) DEFAULT NULL, patronymic VARCHAR(150) DEFAULT NULL, locale VARCHAR(3) NOT NULL, class_type VARCHAR(20) NOT NULL, professional_position VARCHAR(255) DEFAULT NULL, INDEX IDX_1D728CFA2C2AC5D3 (translatable_id), UNIQUE INDEX user_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_translation ADD CONSTRAINT FK_1D728CFA2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES agu_user (id) ON DELETE CASCADE');

        $this->addSql('INSERT INTO user_translation (translatable_id, first_name, surname, patronymic, professional_position, class_type, locale)
                              SELECT id, first_name, surname, patronymic, professional_position, class_type, "ru" FROM agu_user u
                     ');

        $this->addSql('ALTER TABLE agu_user DROP first_name, DROP surname, DROP professional_position, DROP patronymic');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agu_user ADD first_name VARCHAR(150) DEFAULT NULL COLLATE utf8_unicode_ci, ADD surname VARCHAR(150) DEFAULT NULL COLLATE utf8_unicode_ci, ADD professional_position VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD patronymic VARCHAR(150) DEFAULT NULL COLLATE utf8_unicode_ci');

        $this->addSql('UPDATE agu_user u INNER JOIN user_translation ut ON ut.translatable_id = u.id AND ut.locale="ru"
                              SET u.first_name=ut.first_name, u.surname=ut.surname, u.patronymic=ut.patronymic, u.professional_position=ut.professional_position');

        $this->addSql('DROP TABLE user_translation');
    }
}
