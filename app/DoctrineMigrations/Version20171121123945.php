<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Migrations\IrreversibleMigrationException;
use Doctrine\DBAL\Schema\Schema;

class Version20171121123945 extends AbstractMigration
{
    /**
     * This migration fix uploaded document paths, which have problem with production env.
     * Full path to file go through symlink and saves real location with release number.
     * Ex: /var/www/agu.itmo.info/htdocs/releases/41/web/files/project/2/
     * This migration does paths relative from web directory, also all code in UploadedDocumentMover has been updated.
     * After migration must be like that: /files/project/2/
     *
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $qb = $this->connection->createQueryBuilder()
            ->select('ud.id, ud.path')
            ->from('uploaded_document', 'ud')
            ->andWhere('ud.status = 1');
        $result = $qb->execute();
        $newPathsArray = [];
        while ($partialDocumentData = $result->fetch()) {
            $cutFromPosition = mb_strpos($partialDocumentData['path'], '/files/');
            $newPathsArray[$partialDocumentData['id']] = mb_strcut($partialDocumentData['path'], $cutFromPosition, mb_strlen($partialDocumentData['path']));
            $this->addSql('UPDATE uploaded_document SET path="'.$newPathsArray[$partialDocumentData['id']].'" WHERE id = '.$partialDocumentData['id'].';');
        }
    }

    /**
     * This migration cannot be undone!
     *
     * @param Schema $schema
     *
     * @throws IrreversibleMigrationException
     */
    public function down(Schema $schema)
    {
        $this->throwIrreversibleMigrationException('This migration cannot be undone!');
    }
}
