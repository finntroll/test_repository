<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Add city
 */
class Version20171129054913 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('INSERT INTO city (`id`) VALUE (1272)');
        $this->addSql('INSERT INTO city_translation (`translatable_id`, `name`, `locale`) VALUES 
            (1272, "Сочи", "ru"),
            (1272, "Sochi", "en");
            ');
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DELETE FROM city_translation WHERE translatable_id=1272');
        $this->addSql('DELETE FROM city WHERE id=1272');
    }
}
