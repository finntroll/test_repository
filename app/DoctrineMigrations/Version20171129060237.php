<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Set to RUDN branch new city (Sochi)
 */
class Version20171129060237 extends AbstractMigration
{
    protected $branchId = 26;

    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $sochiCity = 1272;
        $this->addSql('UPDATE university_branch SET city_id='.$sochiCity.' WHERE id='.$this->branchId.';' );
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('UPDATE university_branch SET city_id=NULL WHERE id ='.$this->branchId.';');
    }
}
