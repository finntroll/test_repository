<?php

namespace Deployer;

require 'vendor/deployer/deployer/recipe/symfony3.php';

// Configuration
set('repository', 'git@gitlab.urir.ifmo.ru:ifmo/DEMO.AGU.git');
set('shared_dirs', ['var/logs', 'var/sessions', 'web/images', 'web/files', 'web/uploads']);
set('shared_files', ['.authfile', 'app/config/parameters.yml']);

server('dev', '192.168.0.25')
    ->user('web') // Defind SSH username
    ->stage('dev') // Define stage name
    ->identityFile()
    ->set('deploy_path', '/var/www/agu.itmo.info/htdocs') // Define the base path to deploy your project to.
    ->set('branch', 'dev'); // Define the base path to deploy your project to.

server('prod', '185.10.45.126')
    ->user('globaluniru') // Defind SSH username
    ->stage('prod') // Define stage name
    ->identityFile()
    ->set('deploy_path', '/var/www/globaluni.ru/public')
    ->set('branch', 'master'); // Define the base path to deploy your project to.

// Tasks
task('php-fpm-restart',function () {
    run('sudo service php7.1-fpm restart');
});

after('deploy:cache:warmup', 'database:migrate');

after('deploy:symlink', 'php-fpm-restart');

after('deploy:failed', 'deploy:unlock');

