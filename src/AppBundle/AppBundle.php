<?php

namespace AppBundle;

use AppBundle\Event\Subscriber\DynamicRelationSubscriber;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
    public function boot()
    {
        $em = $this->container->get('doctrine.orm.default_entity_manager');
        $em->getEventManager()->addEventSubscriber(new DynamicRelationSubscriber());
    }
}
