<?php

namespace AppBundle\Command;

use AppBundle\Entity\City;
use AppBundle\Entity\CityTranslation;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

class AddCityCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('agu:add:city')
            ->setDescription('Helps to add new Cities to the database with different locales.')
            ->setHelp('Helps to add new Cities to the database with different locales.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');

        $output->writeln([
                'Cities',
                '===============',
                '',
            ]);

        $helper = $this->getHelper('question');
        $city = new City();

        $addMore = new ConfirmationQuestion('Add another locale for this city? [y]', true);

        do {
            $this->addTranslationToCity($city, $input, $output, $helper);
        } while ($helper->ask($input, $output, $addMore));

        if (!$city->getTranslations()->isEmpty()) {
            $em->persist($city);
            $em->flush();

            $output->writeln('New city added!');
        } else {
            $output->writeln('No city was added.');
        }
    }

    private function addTranslationToCity(City $city, InputInterface $input, OutputInterface $output, QuestionHelper $helper)
    {
        $localeQuestion = new Question('Enter locale' . ': ', 'ru');
        $locale = $helper->ask($input, $output, $localeQuestion);

        $nameQuestion = new Question('Enter new city name in [' . $locale . ']' . ': ', null);
        $name = $helper->ask($input, $output, $nameQuestion);

        $confirmation = new ConfirmationQuestion('Is it okay: [' . $locale . '] ' . $name . '? ', true);
        $confirm = $helper->ask($input, $output, $confirmation);

        if ($confirm) {
            $cityTrans = new CityTranslation();
            $cityTrans->setLocale($locale);
            $cityTrans->setName($name);
            $city->addTranslation($cityTrans);

            $output->writeln('New city created: ' . $city->getId() . ' ' . $city->translate($locale)->getName() . ' ' . $locale);
        }
    }
}
