<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NotificationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('agu:notify');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $processor = $this->getContainer()->get('AppBundle\Service\Notifications\NotificationProcessor');
        $processor->process();
    }
}
