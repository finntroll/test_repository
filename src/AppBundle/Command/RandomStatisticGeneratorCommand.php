<?php

namespace AppBundle\Command;

use AppBundle\Entity\AcademicMobility;
use AppBundle\Entity\BestPractice;
use AppBundle\Entity\Contest;
use AppBundle\Entity\News;
use AppBundle\Entity\Program;
use AppBundle\Entity\Statistic;
use AppBundle\Entity\University;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RandomStatisticGeneratorCommand extends ContainerAwareCommand
{

    const SESSIONS_MIN = 50;
    const SESSIONS_MAX = 100;

    const START_NUM = 600000;
    const END_NUM = 900000;

    private $objectsForFun = [
        [
            'name' => 'academicMobility',
            'type' => [
                Statistic::EVENT_EXTERNAL,
            ],
            'class' => AcademicMobility::class,
            'ids' => [],
            'un' => [],
        ],
        [
            'name' => 'bestPractice',
            'type' => [
                Statistic::EVENT_INTERNAL
            ],
            'class' => BestPractice::class,
            'ids' => [],
            'un' => [],
        ],
        [
            'name' => 'contest',
            'type' => [
                Statistic::EVENT_EXTERNAL,
            ],
            'class' => Contest::class,
            'ids' => [],
            'un' => [],
        ],
        [
            'name' => 'news',
            'type' => [
                Statistic::EVENT_EXTERNAL,
                Statistic::EVENT_INTERNAL,
            ],
            'class' => News::class,
            'ids' => [],
            'un' => [],
        ],
        [
            'name' => 'program',
            'type' => [
                Statistic::EVENT_EXTERNAL,
            ],
            'class' => Program::class,
            'ids' => [],
            'un' => [],
        ],
        [
            'name' => 'university',
            'type' => [
                Statistic::EVENT_EXTERNAL,
                Statistic::EVENT_INTERNAL,
            ],
            'class' => University::class,
            'ids' => [],
            'un' => [],
        ]
    ];

    private $sessions = [];

    protected function configure(): void
    {
        $this->setName('agu:statistic:generator');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Start load</info>');
        $this->load();

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $dateNow = new \DateTime();
        $dateAgo = new \DateTime();
        $dateAgo->modify('-12 month');

        $output->writeln('<info>Lets the Random begin</info>');

        $nums = random_int(self::START_NUM, self::END_NUM);

        $output->writeln('<info>Random say ' . $nums . '</info>');

        $mem = $this->convert(memory_get_usage(true));
        $output->writeln('<info>Mem start ' . $mem . '</info>');

        for ($i = 0; $i < $nums; ++$i) {
            $statistic = new Statistic();

            $object = $this->wildRandom($this->objectsForFun);
            $user = $this->wildRandom($this->sessions);
            $event = $this->wildRandom($object['type']);

            $timestamp = random_int($dateAgo->getTimestamp(), $dateNow->getTimestamp());

            $date = new \DateTime();
            $date->setTimestamp($timestamp);
            $k = \random_int(0, \count($object['ids']) - 1);

            $objectId = $object['ids'][$k];
            $unId = $object['un'][$k];

            $statistic->setObject($object['name'])
                ->setObjectId($objectId)
                ->setType($event)
                ->setSession($user)
                ->setUniversityId($unId)
                ->setEventDate($date);

            $em->persist($statistic);

            if ($i % 100 === 0) {
                $em->flush();
                $output->writeln('<info>Flush ' . $i . '</info>');
                $em->clear();
                $mem = $this->convert(\memory_get_usage(true));
                $output->writeln('<info>Mem flush ' . $mem . '</info>');
            }

            unset($statistic, $date);
        }

        $em->flush();

        $output->writeln('<info>Done </info>');


    }

    private function load()
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        foreach ($this->objectsForFun as &$struct) {
            $name = $struct['class'];
            $ids = $em->createQuery("SELECT a.id FROM $name a")->getScalarResult();
            $repository = $em->getRepository($name);
            foreach ($ids as $id) {
                $struct['ids'][] = $id['id'];
                if ($name === University::class) {
                    $struct['un'][] = $id['id'];
                } else {
                    $object = $repository->find($id['id']);
                    $university = $object->getUniversity();
                    if ($university) {
                        $struct['un'][] = $university->getId();
                    } else {
                        $struct['un'][] = null;
                    }
                }
            }
        }

        $usersCount = \random_int(self::SESSIONS_MIN, self::SESSIONS_MAX);

        for ($i = 0; $i < $usersCount; ++$i) {
            $this->sessions[] = \uniqid('debug_ses', true);
        }
    }


    private function wildRandom(array $array)
    {
        $start = 0;
        $end = \count($array) - 1;

        $wildRandom = \random_int($start, $end);

        return $array[$wildRandom];
    }

    private function convert($size)
    {
        $unit = ['b', 'kb', 'mb', 'gb', 'tb', 'pb'];
        return @\round($size / (1024 ** ($i = \floor(\log($size, 1024)))), 2) . ' ' . $unit[(int)$i];
    }
}
