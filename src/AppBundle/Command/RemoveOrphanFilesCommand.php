<?php

namespace AppBundle\Command;

use AppBundle\Entity\UploadedDocuments\UploadedDocument;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class RemoveOrphanFilesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('agu:files:clean')
             ->setDescription('Cleans web/uploads directory of files older than week.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');

        $dir = $container->getParameter('kernel.project_dir').'/web/uploads/';
        $finder = new Finder();
        $files = $finder->files()->in($dir);

        $today = new \DateTimeImmutable();

        /** @var SplFileInfo $file */
        foreach ($files as $file) {
            $changedAt = \DateTimeImmutable::createFromFormat('U', $file->getMTime());
            $interval = $changedAt->diff($today);
            if ($interval->days > 7) {
                $extension = $file->getExtension();
                $internalName = $file->getBasename('.'.$extension);
                $document = $em->getRepository(UploadedDocument::class)
                               ->findOneBy(['internalName' => $internalName]);

                unlink($file->getRealPath());

                if ($document) {
                    $em->remove($document);
                }
            }
        }

        $em->flush();
    }
}
