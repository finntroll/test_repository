<?php

namespace AppBundle\Command;

use AppBundle\Entity\City;
use AppBundle\Entity\CityTranslation;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class SaveCitiesFromCSV extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('agu:cities:load');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');

        $dir = $this->getContainer()->getParameter('kernel.project_dir');
        $csv = fopen($dir . '/cities.csv', 'r');

        if ($csv !== false) {
            $output->writeln('File is loaded! Writing cities to the database...');
            $lastId = -1;
            $city = new City();
            $count = 0;
            while(($row = fgetcsv($csv, 100)) !== false) {
                if ($lastId !== $row[0]) {
                    if (count($city->getTranslations()) === 2) {
                        $em->persist($city);
                        $count++;
                        $city = new City();
                    } elseif  ($lastId !== -1) {
                        $output->writeln('City ' . $lastId . ' has less than 2 translations.');
                    }
                }
                $trans = new CityTranslation();
                $trans->setLocale($row[1]);
                $trans->setName(trim($row[2], '"'));
                $city->addTranslation($trans);

                $lastId = $row[0];
            }

            $output->writeln('Flushing...');
            $em->flush();
            $output->writeln(['Excellent! Saved ' . $count . ' cities.', '']);

            $helper = $this->getHelper('question');
            $removeQuestion = new ConfirmationQuestion('Do you want to delete cities.csv file? [yes]');

            if ($helper->ask($input, $output, $removeQuestion)) {
                unlink($dir . '/cities.csv');
                $output->writeln('File deleted. Have a good day!');
            } else {
                $output->writeln('OK, file cities.csv is left on its place. Have a good day!');
            }
        } else {
            $output->writeln('File cities.csv not found!');
        }
    }
}
