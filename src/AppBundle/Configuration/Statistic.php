<?php

namespace AppBundle\Configuration;

use Doctrine\Common\Annotations\Annotation;
use Doctrine\Common\Annotations\Annotation\Target;
use Doctrine\Common\Annotations\Annotation\Required;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationAnnotation;

/**
 * Class Statistic
 * @package AppBundle\Configuration
 *
 * @Annotation
 * @Target("METHOD")
 */
class Statistic extends ConfigurationAnnotation
{
    /**
     * @Required
     *
     * @var string
     */
    public $objectName;

    /**
     * Sets the object name.
     *
     * @param string $objectName
     */
    public function setValue($objectName)
    {
        $this->setObjectName($objectName);
    }

    /**
     * @return string
     */
    public function getObjectName(): string
    {
        return \lcfirst($this->objectName);
    }

    /**
     * @param string $objectName
     * @return $this
     */
    public function setObjectName(string $objectName)
    {
        $this->objectName = $objectName;
        return $this;
    }

    public function getAliasName(): string
    {
        return 'statistic';
    }

    public function allowArray(): bool
    {
        return false;
    }
}
