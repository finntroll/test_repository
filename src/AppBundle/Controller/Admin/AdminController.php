<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
    /**
     * @Route(
     *     path="/admin",
     *     name="admin.main.blank",
     *     requirements={"_locale": "%route.available_locales%"},
     * )
     * @Security("has_role('ROLE_SITE_ADMIN')")
     */
    public function blankIndexAction(Request $request)
    {
        return $this->redirectToRoute('admin.main.list');
    }

    /**
     * @Route(
     *     path="/{_locale}/admin",
     *     name="admin.main.list",
     *     requirements={"_locale": "%route.available_locales%"},
     * )
     * @Security("has_role('ROLE_SITE_ADMIN')")
     */
    public function indexAction(Request $request)
    {
        return $this->render('AppBundle::admin.html.twig', []);
    }
}
