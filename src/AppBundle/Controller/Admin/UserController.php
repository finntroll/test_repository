<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\User;
use AppBundle\Form\User\Admin\UserFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * @Route(
     *     path="/{_locale}/admin/user",
     *     name="admin.user.list",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     * @Security("has_role('ROLE_SITE_ADMIN')")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $page = $request->get('page', 0);
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findBy([], ['id' => 'ASC'], 10, 10 * $page);

        return $this->render('@App/admin/list.html.twig', ['userList' => $users]);
    }

    /**
     * @Route(
     *     path="/{_locale}/admin/user/edit/{id}",
     *     name="admin.user.edit",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     * @Security("has_role('ROLE_SITE_ADMIN')")
     *
     * @param Request $request
     * @param User    $user
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, User $user)
    {
        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($form->get('submit')->isClicked()) {
                $this->get('fos_user.user_manager')->updateUser($user);
            }

            return $this->redirectToRoute('admin.user.edit', ['id' => $user->getId()]);
        }

        return $this->render('@App/admin/edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route(
     *     path="/{_locale}/admin/user/create",
     *     name="admin.user.create",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     * @Security("has_role('ROLE_SITE_ADMIN')")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $user = new User();

        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($form->get('submit')->isClicked()) {
                $this->get('fos_user.user_manager')->updateUser($user);
                $this->get('fos_user.user_manager')->reloadUser($user);
            }

            return $this->redirectToRoute('admin.user.edit', ['id' => $user->getId()]);
        }

        return $this->render('@App/admin/edit.html.twig', ['form' => $form->createView()]);
    }
}
