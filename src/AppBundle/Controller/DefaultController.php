<?php

namespace AppBundle\Controller;

use AppBundle\Entity\KnowledgeArea;
use AppBundle\Entity\KnowledgeAreaItem;
use AppBundle\Entity\AcademicMobility;
use AppBundle\Entity\News;
use AppBundle\Entity\NewsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * @Route(
     *     path="/{_locale}/",
     *     name="homepage",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     * @Template("@App/default/MainPage/index.html.twig")
     */
    public function mainAction(Request $request)
    {
        $universities = $this->getDoctrine()->getRepository('AppBundle:University')->findForSlider();
        shuffle($universities);

        // Knowledge area repositories
        $knowledgeAreaRepository = $this->getDoctrine()->getRepository(KnowledgeArea::class);
        $knowledgeAreaItemRep = $this->getDoctrine()->getRepository(KnowledgeAreaItem::class);

        $locale = $request->getLocale();
        // Programs
        $knowledgeAreaItemData = $knowledgeAreaItemRep->getFastFiltersData($locale);
        $knowledgeAreaData = $knowledgeAreaRepository->getFastFiltersData($locale);

        // Project and contest
        $knowledgeAreaContestData = $knowledgeAreaRepository->getContestFastFiltersData($locale);
        $knowledgeAreaItemContestData = $knowledgeAreaItemRep->getContestFastFiltersData($locale);

        // Academic mobility
        $knowledgeAreaAcademicData = $knowledgeAreaRepository->getAcademicFastFiltersData($locale);
        $knowledgeAreaItemAcademicData = $knowledgeAreaItemRep->getAcademicFastFiltersData($locale);

        return [
            'universities' => $universities,
            'knowledgeAreaData' => $knowledgeAreaData,
            'knowledgeAreaItemData' => $knowledgeAreaItemData,
            'knowledgeAreaContestData' => $knowledgeAreaContestData,
            'knowledgeAreaItemContestData' => $knowledgeAreaItemContestData,
            'knowledgeAreaAcademicData' => $knowledgeAreaAcademicData,
            'knowledgeAreaItemAcademicData' => $knowledgeAreaItemAcademicData,
        ];
    }

    /**
     * @Template("@App/Default/newsAndEvents.html.twig")
     */
    public function newsAndEventsAction(Request $request, $section = 'all')
    {

        $newsRepository = $this->getDoctrine()->getRepository(News::class);
        $academicMobilityRepository = $this->getDoctrine()
            ->getRepository(AcademicMobility::class);

        $newsUniversity = $newsRepository
            ->findLastNewsForMain(NewsType::UNIVERSITY_NEWS, $request->getLocale());
        $newsAssociations = $newsRepository
            ->findLastNewsForMain(NewsType::ASSOCIATION_NEWS, $request->getLocale());

        $academicMobilities = $academicMobilityRepository
            ->getLastAcademicMobilityForMain(AcademicMobility::ACADEMIC_MOBILITY_EVENT_TYPES, $request->getLocale());

        return [
            'newsUniversity' => $newsUniversity,
            'newsAssociations' => $newsAssociations,
            'academicMobilities' => $academicMobilities,
        ];
    }

}
