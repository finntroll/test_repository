<?php

namespace AppBundle\Controller;

use AppBundle\Serializer\Normalizer\CityNormalizer;
use AppBundle\Serializer\Normalizer\PartnerNormalizer;
use AppBundle\Serializer\Normalizer\TransKeyObjectNormalizer;
use AppBundle\Serializer\Normalizer\UniversityBranchNormalizer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Serializer;

class DictionaryController extends Controller
{
    private function serializeAndResponse(Collection $collection, NormalizerInterface $normalizer)
    {
        $serializer = new Serializer([$normalizer], [new JsonEncoder()]);
        $response = new Response($serializer->serialize($collection, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    private function findData(string $accessor, string $repositoryName, $id)
    {
        $dataOwner = $this->getDoctrine()->getRepository($repositoryName)->find($id);
        if (!$dataOwner) {
            return new ArrayCollection([]);
        }

        return call_user_func([$dataOwner, $accessor]);
    }

    /**
     * @param Request                  $request
     * @param TransKeyObjectNormalizer $normalizer
     *
     * @Route(path="/{_locale}/entrance-tests", requirements={"_locale": "%route.available_locales%"}, name="entrance_tests")
     *
     * @return Response
     */
    public function entranceTestAction(Request $request, TransKeyObjectNormalizer $normalizer)
    {
        $accessor = 'getEntranceTests';
        $repoName = 'AppBundle:EducationLevel';
        $educationLevelId = $request->get('id');

        $collection = $this->findData($accessor, $repoName, $educationLevelId);

        return $this->serializeAndResponse($collection, $normalizer);
    }

    /**
     * @param Request                  $request
     * @param TransKeyObjectNormalizer $normalizer
     *
     * @Route(path="/{_locale}/knowledge-area-items", requirements={"_locale": "%route.available_locales%"}, name="knowledge_area_items")
     *
     * @return Response
     */
    public function knowledgeAreaItemAction(Request $request, TransKeyObjectNormalizer $normalizer)
    {
        $accessor = 'getKnowledgeAreaItems';
        $repoName = 'AppBundle:KnowledgeArea';
        $knowledgeAreaId = $request->get('id');

        $collection = $this->findData($accessor, $repoName, $knowledgeAreaId);

        return $this->serializeAndResponse($collection, $normalizer);
    }

    /**
     * @Route(
     *     name="keywords",
     *     path="/keywords"
     * )
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function keywords(Request $request)
    {
        $name = $request->query->get('q');
        $limit = $request->query->get('page_limit');

        $data = $this->getDoctrine()
                     ->getRepository('AppBundle:Keyword')
                     ->findByName($name, $limit);

        return $this->json($data);
    }

    /**
     * @Route(
     *     path="/{_locale}/cities",
     *     requirements={"_locale": "%route.available_locales%"},
     *     name="cities")
     */
    public function cities(Request $request)
    {
        $name = $request->query->get('q');
        $limit = $request->query->get('page_limit');

        $cities = $this->getDoctrine()
                     ->getRepository('AppBundle:City')
                     ->findByName($name, $request->getLocale(), $limit);

        $data = ['results' => $cities];

        return $this->json($data);
    }

    /**
     * @Route(
     *     path="/{_locale}/personalities",
     *     requirements={"_locale": "%route.available_locales%"},
     *     name="personalities")
     */
    public function personalities(Request $request)
    {
        $name = $request->query->get('q');
        $limit = $request->query->get('page_limit');

        $personalities = $this->getDoctrine()
                       ->getRepository('AppBundle:Personality')
                       ->findByNameWithWorkPlace($name, $request->getLocale(), $limit);

        $data = ['results' => $personalities];

        return $this->json($data);
    }

    /**
     * @param Request                    $request
     * @param UniversityBranchNormalizer $ubn
     *
     * @return Response
     * @Route(
     *     path="/{_locale}/university-branches",
     *     requirements={"_locale": "%route.available_locales%"},
     *     name="university_branches")
     */
    public function universityBranchAction(Request $request, UniversityBranchNormalizer $ubn)
    {
        $university = $request->query->get('id', null); // can get multiple universities!
        $loadAllWhenEmpty = $request->query->get('l', null);
        $rep = $this->getDoctrine()->getRepository('AppBundle:UniversityBranch');
        $branches = $rep->findByUniversity($university, $loadAllWhenEmpty)->getQuery()->execute();

        return $this->serializeAndResponse(new ArrayCollection($branches), $ubn);
    }

    /**
     * @param Request        $request
     * @param CityNormalizer $normalizer
     *
     * @return Response
     * @Route(
     *     path="/{_locale}/university-branch-cities",
     *     requirements={"_locale": "%route.available_locales%"},
     *     name="university_branch_cities")
     */
    public function universityBranchCitiesAction(Request $request, CityNormalizer $normalizer)
    {
        $university = $request->query->get('id', null); // can get multiple universities!
        $loadAllWhenEmpty = $request->query->get('l', null);
        $rep = $this->getDoctrine()->getRepository('AppBundle:City');
        $cities = $rep->findByUniversity($university, $request->getLocale(), $loadAllWhenEmpty)->getQuery()->execute();

        return $this->serializeAndResponse(new ArrayCollection($cities), $normalizer);
    }

    /**
     * @param Request           $request
     * @param PartnerNormalizer $upn
     *
     * @return Response
     *
     * @Route(path="/{_locale}/university-partner", requirements={"_locale": "%route.available_locales%"}, name="university_partners")
     *
     */
    public function universityPartnersAction(Request $request, PartnerNormalizer $upn)
    {
        $universityId = $request->query->get('id', null);
        $rep = $this->getDoctrine()->getRepository('AppBundle:Partner');
        $queryBuilder = $rep->queryUniversity($rep->createQueryBuilderRelated('partner'), $universityId, 'partner');
        $partners = $queryBuilder->getQuery()->execute();

        return $this->serializeAndResponse(new ArrayCollection($partners), $upn);
    }
}
