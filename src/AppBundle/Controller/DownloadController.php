<?php

namespace AppBundle\Controller;

use AppBundle\Entity\UploadedDocuments\UploadedDocument;
use AppBundle\Service\ArchiveManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class DownloadController extends Controller
{
    private $archiveManager;

    /**
     * DownloadController constructor.
     *
     * @param $archiveManager
     */
    public function __construct(ArchiveManager $archiveManager)
    {
        $this->archiveManager = $archiveManager;
    }

    /**
     * @Route(
     *     path="/download/document/{id}",
     *     name="download.document",
     *     requirements={"id": "\d+"},
     * )
     *
     * @param Request $request
     * @param UploadedDocument $file
     * @return Response
     */
    public function downloadAction(Request $request, UploadedDocument $file): Response
    {
        if ($file->isArchived()) {
            return new RedirectResponse($this->archiveManager->getUrl($file));
        }
        $webDir = $this->container->getParameter('web_dir');
        $fullFilePath = $webDir . $file->getRelativePath();
        $downloadName = $file->getDownloadName();

        $response = new BinaryFileResponse($fullFilePath);
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $downloadName,
            \iconv('UTF-8', 'ASCII//TRANSLIT', $downloadName)
        );

        return $response;
    }

}
