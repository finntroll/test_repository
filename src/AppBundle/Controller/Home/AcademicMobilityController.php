<?php

namespace AppBundle\Controller\Home;

use AppBundle\Filter\AcademicMobilityFilter;
use AppBundle\Form\AcademicMobility\AcademicMobilityFilterType;
use AppBundle\Manager\MobilityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AcademicMobilityController extends Controller
{
    /**
     * @var MobilityManager
     */
    private $manager;

    public function __construct(MobilityManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route(
     *     path="/{_locale}/academic-mobilities",
     *     name="main.academic_mobility.list",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     * @Template("@App/default/AcademicMobility/index.html.twig")
     * @param Request $request
     *
     * @return array
     */

    public function indexAction(Request $request)
    {
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('perPage', 12);

        $filter = new AcademicMobilityFilter();
        $form = $this->createForm(AcademicMobilityFilterType::class, $filter, ['locale' => $request->getLocale()]);
        $form->handleRequest($request);

        $mobilities = $this->manager->findPublishedMobilities($filter);
        $paginator = $this->get('knp_paginator');
        $mobilities = $paginator->paginate($mobilities, $page, $limit);

        return [
            'form' => $form->createView(),
            'mobilities' => $mobilities,
        ];
    }
}
