<?php

namespace AppBundle\Controller\Home;

use AppBundle\Configuration\Statistic;
use AppBundle\Entity\BestPractice;
use AppBundle\Filter\BestPracticeFilter;
use AppBundle\Form\BestPractice\BestPracticeFilterType;
use AppBundle\Manager\BestPracticeManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class BestPracticeController extends Controller
{
    /**
     * @var BestPracticeManager
     */
    private $manager;

    public function __construct(BestPracticeManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route(
     *     path="/{_locale}/practice",
     *     name="main.practice.list",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     * @Template("@App/default/Practice/index.html.twig")
     * @param Request $request
     *
     * @return array
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('perPage', 12);

        $filter = new BestPracticeFilter();
        $form = $this->createForm(BestPracticeFilterType::class, $filter, ['locale' => $request->getLocale()]);
        $form->handleRequest($request);

        $query = $this->manager->findPublishedBestPractices($filter);
        $paginator = $this->get('knp_paginator');
        $bestPractices = $paginator->paginate($query, $page, $limit);

        return [
            'form' => $form->createView(),
            'bestPractices' => $bestPractices,
        ];
    }

    /**
     * @Route(
     *     path="/{_locale}/practice/{id}",
     *     name="main.practice.show",
     *     requirements={"_locale": "%route.available_locales%", "id": "\d+"}
     * )
     * @Template("@App/default/Practice/show.html.twig")
     * @Statistic("bestPractice")
     *
     * @param Request $request
     *
     * @return array
     */
    public function showAction(Request $request, BestPractice $bestPractice)
    {
        $locale = $request->getLocale();
        if (!$bestPractice->hasTranslation($locale) || !$bestPractice->isPublished()) {
            throw new NotFoundHttpException();
        }

        $repo = $this->getDoctrine()->getRepository('AppBundle:BestPractice');
        $relatedBestPractices = $repo->findRelated($bestPractice, $request->getLocale());

        return [
            'bestPractice' => $bestPractice,
            'relatedBestPractices' => $relatedBestPractices,
        ];
    }
}
