<?php

namespace AppBundle\Controller\Home;

use AppBundle\Filter\ContestFilter;
use AppBundle\Form\Contest\ContestFilterType;
use AppBundle\Manager\ContestManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ContestController extends Controller
{

    /**
     * @var ContestManager
     */
    private $manager;

    public function __construct(ContestManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route(
     *     path="/{_locale}/funding",
     *     name="main.contest.list",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     * @Template("@App/default/Contest/index.html.twig")
     * @param Request $request
     *
     * @return array
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('perPage', 12);

        $filter = new ContestFilter();
        $form = $this->createForm(ContestFilterType::class, $filter, ['locale' => $request->getLocale()]);
        $form->handleRequest($request);

        $query = $this->manager->findPublishedContests($filter);
        $paginator = $this->get('knp_paginator');
        $contests = $paginator->paginate($query, $page, $limit);

        return [
            'form' => $form->createView(),
            'contests' => $contests,
        ];
    }
}
