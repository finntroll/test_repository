<?php

namespace AppBundle\Controller\Home;

use AppBundle\Configuration\Statistic;
use AppBundle\Filter\NewsSearchFilter;
use AppBundle\Form\News\NewsFilterType;
use AppBundle\Manager\NewsManager;
use AppBundle\Entity\News;
use AppBundle\Manager\BestPracticeManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NewsController extends Controller
{
    private $manager;
    private $managerPractice;

    public function __construct(NewsManager $manager, BestPracticeManager $managerPractice)
    {
        $this->manager = $manager;
        $this->managerPractice = $managerPractice;
    }

    /**
     * @Route(
     *     path="/{_locale}/news/",
     *     name="main.news.index",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     * @Template("@App/default/News/index.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function indexAction(Request $request)
    {
        $importantNews = $this->manager->findImportantNews();
        $associationNews = $this->manager->findAssociationNews();
        $universitiesNews = $this->manager->findAllUniversitiesNews();

        $bestPractices = $this->managerPractice->findLimitedPublishedBestPractices(6);

        return [
            'universityNews' => $universitiesNews,
            'importantNews' => $importantNews,
            'associationNews' => $associationNews,
            'bestPractices' => $bestPractices,
        ];
    }

    /**
     * @Route(
     *     path="/{_locale}/news/search",
     *     name="main.news.search",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     * @Template("@App/default/News/search.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function searchAction(Request $request)
    {
        $filter = new NewsSearchFilter();
        $page = $request->query->get('page', 1);
        $perPage = $request->query->get('perPage', 12);
        $form = $this->createForm(NewsFilterType::class, $filter, ['locale' => $request->getLocale()]);
        $form->handleRequest($request);

        $query = $this->manager->findByFilter($filter);
        $result = $this->get('knp_paginator')->paginate($query, $page, $perPage);

        return [
            'searchForm' => $form->createView(),
            'foundedNews' => $result,
        ];
    }

    /**
     * @Route(
     *     path="/{_locale}/news/{id}",
     *     name="main.news.show",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     * @Template("@App/default/News/show.html.twig")
     *
     * @Statistic("news")
     *
     * @param Request $request
     *
     * @return array
     */
    public function showAction(Request $request, News $news)
    {
        if (!$news->isPublished()) {
            throw new NotFoundHttpException();
        }

        $repo = $this->getDoctrine()->getRepository('AppBundle:News');
        $relatedNews = $repo->findRelatedNews($news, $request->getLocale());

        return [
            'news' => $news,
            'relatedNews' => $relatedNews,
        ];
    }
}
