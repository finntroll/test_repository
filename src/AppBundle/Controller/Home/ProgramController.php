<?php

namespace AppBundle\Controller\Home;

use AppBundle\Manager\ProgramManager;
use AppBundle\Filter\ProgramFilter;
use AppBundle\Form\Program\ProgramFilterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProgramController extends Controller
{
    /**
     * @var ProgramManager
     */
    private $manager;

    public function __construct(ProgramManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route(
     *     path="/{_locale}/programs",
     *     name="main.program.list",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     * @Template("@App/default/Program/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('perPage', 12);

        $filter = new ProgramFilter();
        $form = $this->createForm(ProgramFilterType::class, $filter, ['locale' => $request->getLocale()]);
        $form->handleRequest($request);

        $query =  $this->manager->findPublishedPrograms($filter);

        $paginator = $this->get('knp_paginator');
        $programs = $paginator->paginate($query, $page, $limit);

        return [
            'form' => $form->createView(),
            'programs' => $programs,
        ];
    }
}
