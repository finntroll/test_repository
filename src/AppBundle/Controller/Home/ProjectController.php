<?php

namespace AppBundle\Controller\Home;

use AppBundle\Entity\Project;
use AppBundle\Filter\ProjectFilter;
use AppBundle\Form\AssociationProject\ProjectFilterFormType;
use AppBundle\Manager\ProjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProjectController extends Controller
{
    /**
     * @var ProjectManager
     */
    private $manager;

    public function __construct(ProjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route(
     *     path="/{_locale}/project",
     *     name="main.project.list",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     * @Template("@App/default/Project/index.html.twig")
     * @param Request $request
     *
     * @return array
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('perPage', 12);

        $filter = new ProjectFilter();
        $filterForm = $this->createForm(ProjectFilterFormType::class, $filter);
        $filterForm->handleRequest($request);

        $query = $this->manager->findPublishedProjects($filter);
        $paginator = $this->get('knp_paginator');
        $projects = $paginator->paginate($query, $page, $limit, ['wrap-queries' => true]);

        return [
            'projects' => $projects,
            'filterForm' => $filterForm->createView(),
        ];
    }

    /**
     * @Route(
     *     path="/{_locale}/project/{id}",
     *     name="main.project.show",
     *     requirements={"_locale": "%route.available_locales%", "id": "\d+"}
     * )
     * @Template("@App/default/Project/show.html.twig")
     * @param Request $request
     *
     * @return array
     */
    public function showAction(Request $request, Project $project)
    {
        $locale = $request->getLocale();
        if (!$project->hasTranslation($locale) || !$project->getIsPublished()) {
            throw new NotFoundHttpException();
        }

        $relatedProjects = $this->manager->findRelated($project);

        return [
            'project' => $project,
            'relatedProjects' => $relatedProjects,
        ];
    }
}
