<?php

namespace AppBundle\Controller\Home;

use AppBundle\Configuration\Statistic;
use AppBundle\Entity\News;
use AppBundle\Entity\NewsType;
use AppBundle\Entity\University;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class UniversityController extends Controller
{
    /**
     * @param Request    $request
     * @param University $university
     *
     * @return array
     * @Route(
     *     path="/{_locale}/university/{id}",
     *     name="home.university.index",
     *     requirements={"_locale": "%route.available_locales%"}
     *     )
     * @Template("AppBundle:default/University:index.html.twig")
     * @Statistic("university")
     */
    public function indexAction(Request $request, University $university)
    {
        $newsRepository = $this->getDoctrine()->getRepository(News::class);
        $universityRepository = $this->getDoctrine()->getRepository(University::class);

        $news = $newsRepository->findLastNewsForUniversity($university, NewsType::UNIVERSITY_NEWS, $request->getLocale());

        $counters = $universityRepository->getCountersByUniversity($university->getId());

        return [
            'counters' => $counters[0],
            'university' => $university,
            'news' => $news,
        ];
    }

    // add here other actions for university page, i.e. programList, contestList, mobilityList, partners
}
