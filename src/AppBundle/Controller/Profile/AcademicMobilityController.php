<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\AcademicMobility;
use AppBundle\Event\TargetActionEvent;
use AppBundle\Filter\AcademicMobilityFilter;
use AppBundle\Form\AcademicMobility\AcademicMobilityProfileFilterType;
use AppBundle\Form\AcademicMobility\AcademicMobilityType;
use AppBundle\Manager\MobilityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AcademicMobilityController
 *
 * @Security("is_granted('CAN_ACCESS_ACADEMIC_MOBILITY')")
 */
class AcademicMobilityController extends BaseProfileController
{
    /**
     * @var MobilityManager
     */
    protected $manager;

    public function __construct(MobilityManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/academic/",
     *     name="profile.academic.index",
     *     requirements={"_locale": "%route.available_locales%" }
     * )
     * @Template("@App/Profile/AcademicMobility/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $filter = new AcademicMobilityFilter();
        $filterForm = $this->createForm(AcademicMobilityProfileFilterType::class, $filter, ['locale' => $request->getLocale()]);
        $filterForm->handleRequest($request);

        if ($this->isGranted(['ROLE_SITE_ADMIN', 'ROLE_GLOBAL_UNIVERSITY_ADMIN'])) {
            $mobilities = $this->manager->findMobilities($filter);
        } else {
            $university = $this->getUser()->getUniversity();
            $mobilities = $this->manager->findUniversityMobilities($university, $filter);
        }

        $page = $request->get('page', 1);
        $perPage = $request->get('perPage', 12);
        $paginator = $this->get('knp_paginator');

        $result = $paginator->paginate($mobilities, $page, $perPage);

        return $this->response(['items' => $result, 'filterForm' => $filterForm->createView()]);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/academic/create",
     *     requirements={"_locale": "%route.available_locales%"},
     *     name="profile.academic.create"
     * )
     * @Template("@App/Profile/AcademicMobility/edit.html.twig")
     */
    public function createAction(Request $request)
    {
        $parameters = ! $this->isGranted(['ROLE_SITE_ADMIN', 'ROLE_GLOBAL_UNIVERSITY_ADMIN'])
            ? ['user' => $this->getUser()]
            : [];

        $academicMobility = $this->manager->create($parameters);

        return $this->editAction($request, $academicMobility);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/academic/edit/{id}",
     *     requirements={"_locale": "%route.available_locales%"},
     *     name="profile.academic.edit"
     * )
     * @Template
     */
    public function editAction(Request $request, AcademicMobility $academicMobility)
    {
        $form = $this->createForm(AcademicMobilityType::class, $academicMobility, [
            'user' => $this->getUser(),
            'locale' => $request->getLocale(),
            ]);
        $form->handleRequest($request);
        $dispatchAction = $this->identifyAction($academicMobility);
        if($this->checkFormErrors($form)){
            $this->dispatchActionEvent($academicMobility, TargetActionEvent::FORM_ERROR);

            return $this->response(['form' => $form->createView()]);
        }

        if ($form->isSubmitted() && $form->isValid()) {

            if ($form->get('submitAndPublish')->isClicked()) {
                $this->dispatchActionEvent($academicMobility, TargetActionEvent::PUBLISHED);
                $this->manager->publish($academicMobility);
            }

            $this->manager->update($academicMobility)->store($academicMobility);

            $this->dispatchActionEvent($academicMobility, $dispatchAction);

            return $this->redirectToRoute('profile.academic.index');
        }

        return $this->response(['form' => $form->createView()]);
    }

    /**
     * @Route(path="/{_locale}/profile/academic/delete/{id}", name="profile.academic.delete")
     */
    public function deleteAction(Request $request, AcademicMobility $academicMobility)
    {
        $this->manager->remove($academicMobility)->store($academicMobility);

        $this->dispatchActionEvent($academicMobility, TargetActionEvent::DELETED);

        return $this->redirectToRoute('profile.academic.index');
    }
}
