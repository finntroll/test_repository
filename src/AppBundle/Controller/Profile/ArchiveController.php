<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\UploadedDocuments\UploadedDocument;
use AppBundle\Service\ArchiveManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ArchiveController extends Controller
{
    /**
     * @var ArchiveManager
     */
    private $archiveManager;

    /**
     * ArchiveController constructor.
     *
     * @param ArchiveManager $archiveManager
     */
    public function __construct(ArchiveManager $archiveManager)
    {
        $this->archiveManager = $archiveManager;
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/archive/delete/{id}",
     *     name="profile.archive.delete",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     * @Security("is_granted(['ROLE_SITE_ADMIN', 'ROLE_GLOBAL_UNIVERSITY_ADMIN'])")
     *
     * @param UploadedDocument $document
     *
     * @return JsonResponse
     */
    public function deleteAction(UploadedDocument $document)
    {
        $this->archiveManager->delete($document);

        return new JsonResponse([
            'result' => 'OK',
        ]);
    }
}
