<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\Project;
use AppBundle\Entity\UploadedDocuments\HasUploadedDocumentsInterface;
use AppBundle\Event\TargetActionEvent;
use AppBundle\Filter\ProjectFilter;
use AppBundle\Form\AssociationProject\{AssociationProjectType, ProjectProfileFilterType};
use AppBundle\Manager\ProjectManager;
use AppBundle\Service\ArchiveManager;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AssociationProjectController
 * @package AppBundle\Controller\Profile
 *
 * @Security("is_granted('CAN_ACCESS_PROJECT')")
 */
class AssociationProjectController extends BaseProfileController
{
    const PUBLIC_DOCS = 0;
    const PASSPORT_OR_OUTLAY = 1;

    /**
     * @var ProjectManager
     */
    private $manager;

    /**
     * @var ArchiveManager
     */
    private $archiveManager;

    /**
     * AssociationProjectController constructor.
     *
     * @param ProjectManager $manager
     * @param ArchiveManager $archiveManager
     */
    public function __construct(ProjectManager $manager, ArchiveManager $archiveManager)
    {
        $this->manager = $manager;
        $this->archiveManager = $archiveManager;
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/project/",
     *     name="profile.project.index",
     *     requirements={"_locale": "%route.available_locales%" }
     * )
     * @Template("@App/Profile/Project/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $filter = new ProjectFilter();
        $filterForm = $this->createForm(ProjectProfileFilterType::class, $filter);
        $filterForm->handleRequest($request);

        $page = $request->get('page', 1);
        $perPage = $request->get('perPage', 12);
        $paginator = $this->get('knp_paginator');

        if ($this->isGranted('CAN_CREATE_PROJECT')) {
            $projects = $this->manager->findAllProjects($filter);
        } else {
            $user = $this->getUser();
            $projects = $this->manager->findPersonalityProjects($user, $filter);
        }

        $result = $paginator->paginate($projects, $page, $perPage);

        return $this->response([
                'projects' => $result,
                'filterForm' => $filterForm->createView(),
            ]);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/project/create",
     *     name="profile.project.create",
     *     requirements={"_locale": "%route.available_locales%" }
     * )
     * @Template("@App/Profile/Project/edit.html.twig")
     * @Security("is_granted('CAN_CREATE_PROJECT')")
     */
    public function createAction(Request $request)
    {
        $project = $this->manager->create();

        return $this->editAction($request, $project);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/project/edit/{id}",
     *     name="profile.project.edit",
     *     requirements={"_locale": "%route.available_locales%" }
     * )
     * @Template("@App/Profile/Project/edit.html.twig")
     * @Security("is_granted('CAN_EDIT_PROJECT', project)")
     */
    public function editAction(Request $request, Project $project)
    {
        $form = $this->createForm(AssociationProjectType::class, $project);

        $originalProjectDocuments = $this->collectDocuments($project);
        $originalPassportDocuments = $this->collectDocuments($project->getPassport());
        $originalOutlayDocuments = $this->collectDocuments($project->getOutlay());

        $form->handleRequest($request);

        $dispatchAction = $this->identifyAction($project);

        if ($this->checkFormErrors($form)) {
            $this->dispatchActionEvent($project, TargetActionEvent::FORM_ERROR);
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $this->removeRedundantDocuments($originalProjectDocuments, self::PUBLIC_DOCS, $project);
            $this->removeRedundantDocuments($originalPassportDocuments, self::PASSPORT_OR_OUTLAY, $project->getPassport());
            $this->removeRedundantDocuments($originalOutlayDocuments, self::PASSPORT_OR_OUTLAY, $project->getOutlay());

            $this->manager->update($project)->store($project);
            $this->dispatchActionEvent($project, $dispatchAction);

            return $this->chooseResponse($project, $form);
        }

        return $this->response(['form' => $form->createView(), 'project' => $project, 'archive' => $project->getArchive()]);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/project/delete/{id}",
     *     name="profile.project.delete",
     *     requirements={"_locale": "%route.available_locales%" }
     * )
     * @Template("@App/Profile/Project/edit.html.twig")
     * @Security("is_granted('CAN_DELETE_PROJECT', project)")
     */
    public function deleteAction(Request $request, Project $project)
    {
        $this->manager->remove($project)->store($project);

        $this->dispatchActionEvent($project, TargetActionEvent::DELETED);

        return $this->redirectToRoute('profile.project.index');
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/project/publish/{id}",
     *     name="profile.project.publish",
     *     requirements={"_locale": "%route.available_locales%" }
     * )
     * @Security("is_granted('CAN_PUBLISH_PROJECT', project)")
     */
    public function publish(Request $request, Project $project)
    {
        $this->manager->publish($project)->store($project);

        $this->dispatchActionEvent($project, TargetActionEvent::PUBLISHED);

        return $this->redirectToRoute('profile.project.index');
    }

    /**
     * @param Project $project
     * @param Form    $form
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function chooseResponse(Project $project, Form $form)
    {
        // default submit button
        $response = $this->redirectToRoute('profile.project.index');

        if ($form->has('submitAndPublish') && $form->get('submitAndPublish')->isClicked()) {
            $response = $this->redirectToRoute('profile.project.publish', ['id' => $project->getId()]);
        }

        return $response;
    }

    private function collectDocuments(HasUploadedDocumentsInterface $hasDocuments = null)
    {
        $docs = new ArrayCollection();
        if (!is_null($hasDocuments)) {
            foreach ($hasDocuments->getUploadedDocuments() as $doc) {
                $docs->add($doc);
            }
        }

        return $docs;
    }

    private function removeRedundantDocuments(ArrayCollection $documents, $type, HasUploadedDocumentsInterface $hasDocuments = null)
    {
        $em = $this->getDoctrine()->getManager();

        if (!is_null($hasDocuments)) {
            foreach ($documents as $doc) {
                if (false === $hasDocuments->getUploadedDocuments()->contains($doc)) {
                    switch ($type) {
                        case self::PUBLIC_DOCS:
                            if ($doc->isApproved()) {
                                $this->archiveManager->archive($doc);
                                $hasDocuments->addUploadedDocument($doc);
                            } else {
                                $em->remove($doc); // flush in store;
                            }
                            break;
                        case self::PASSPORT_OR_OUTLAY:
                            $this->archiveManager->archive($doc);
                            $hasDocuments->addUploadedDocument($doc);
                            break;
                    }
                }
            }
        }
    }
}
