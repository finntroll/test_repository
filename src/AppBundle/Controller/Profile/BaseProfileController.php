<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Event\TargetActionEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseProfileController extends Controller
{
    public function response(array $data = [])
    {
        return array_merge(
            [
                'locale'    => $this->get('request_stack')->getCurrentRequest()->getLocale(),
            ],
            $data
        );
    }

    public function dispatchActionEvent($target, $action)
    {
        $event = new TargetActionEvent();
        $event->setTarget($target);
        $dispatcher = $this->container->get('event_dispatcher');
        $dispatcher->dispatch($action, $event);
    }

    public function identifyAction($target)
    {
       return $target->getId() ? TargetActionEvent::UPDATED : TargetActionEvent::CREATED;
    }

    public function checkFormErrors($form)
    {
        if(count($form->getErrors(true)) > 0){
            return true;
        }

        return false;
    }

}
