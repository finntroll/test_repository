<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\BestPractice;
use AppBundle\Event\TargetActionEvent;
use AppBundle\Filter\BestPracticeFilter;
use AppBundle\Form\BestPractice\BestPracticeProfileFilterType;
use AppBundle\Form\BestPractice\BestPracticeType;
use AppBundle\Manager\BestPracticeManager;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class BestPracticesController
 *
 * @Security("is_granted('CAN_ACCESS_BEST_PRACTICE')")
 *
 */
class BestPracticesController extends BaseProfileController
{
    /**
     * @var BestPracticeManager
     */
    private $bestPracticeManager;

    /**
     * BestPracticesController constructor.
     *
     * @param BestPracticeManager $bestPracticeManager
     */
    public function __construct(BestPracticeManager $bestPracticeManager)
    {
        $this->bestPracticeManager = $bestPracticeManager;
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/practice/",
     *     requirements={"_locale": "%route.available_locales%" },
     *     name="profile.practice.index"
     * )
     * @Template("@App/Profile/BestPractice/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $filter = new BestPracticeFilter();
        $searchForm = $this->createForm(BestPracticeProfileFilterType::class, $filter, ['locale' => $request->getLocale()]);
        $searchForm->handleRequest($request);

        if ($this->isGranted('CAN_CREATE_BEST_PRACTICE')) {
            $bestPractices = $this->bestPracticeManager->findAllBestPractices($filter);
        } elseif ($this->isGranted('CAN_CREATE_UNIVERSITY_BEST_PRACTICE')) {
            $university = $this->getUser()->getUniversity();
            $bestPractices = $this->bestPracticeManager->findUniversityBestPractices($university, $filter);
        } else {
            throw new NotFoundHttpException();
        }

        $page = $request->get('page', 1);
        $perPage = $request->get('perPage', 12);
        $paginatedBestPractices = $this->get('knp_paginator')->paginate($bestPractices, $page, $perPage);

        return $this->response([
            'bestPractices' => $paginatedBestPractices,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/practice/create",
     *     requirements={"_locale": "%route.available_locales%" },
     *     name="profile.practice.create"
     * )
     * @Template("@App/Profile/BestPractice/edit.html.twig")
     * @Security("is_granted('CAN_CREATE_UNIVERSITY_BEST_PRACTICE')")
     */
    public function createAction(Request $request)
    {
        $bestPractice = $this->bestPracticeManager->create(['user' => $this->getUser()]);

        return $this->editAction($request, $bestPractice);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/practice/edit/{id}",
     *     requirements={"_locale": "%route.available_locales%" },
     *     name="profile.practice.edit"
     * )
     * @Template("@App/Profile/BestPractice/edit.html.twig")
     * @Security("is_granted('CAN_EDIT_UNIVERSITY_BEST_PRACTICE', bestPractice)")
     */
    public function editAction(Request $request, BestPractice $bestPractice)
    {
        $form = $this->createForm(BestPracticeType::class, $bestPractice, [
            'user' => $this->getUser(),
            'locale' => $request->getLocale(),
        ]);
        $em = $this->getDoctrine()->getManager();
        $originalDocuments = new ArrayCollection();

        foreach ($bestPractice->getUploadedDocuments() as $doc) {
            $originalDocuments->add($doc);
        }

        $form->handleRequest($request);
        $dispatchAction = $this->identifyAction($bestPractice);

        if ($this->checkFormErrors($form)) {
            $this->dispatchActionEvent($bestPractice, TargetActionEvent::FORM_ERROR);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ArrayCollection $originalDocuments */
            foreach ($originalDocuments as $doc) {
                if (false === $bestPractice->getUploadedDocuments()->contains($doc)) {
                    $em->remove($doc); // flush in store;
                }
            }

            $this->bestPracticeManager->update($bestPractice)->store($bestPractice);
            $this->dispatchActionEvent($bestPractice, $dispatchAction);

            return $this->chooseResponse($bestPractice, $form);
        }

        return $this->response(['form' => $form->createView(), 'bestPractice' => $bestPractice]);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/practice/delete/{id}",
     *     requirements={"_locale": "%route.available_locales%"},
     *     name="profile.practice.delete"
     * )
     * @Security("is_granted('CAN_REMOVE_BEST_PRACTICE', bestPractice)")
     */
    public function deleteAction(Request $request, BestPractice $bestPractice)
    {
        $this->bestPracticeManager->remove($bestPractice)->store($bestPractice);

        $this->dispatchActionEvent($bestPractice, TargetActionEvent::DELETED);

        return $this->redirectToRoute('profile.practice.index');
    }

    /**
     * @Route(
     *     path="{_locale}/profile/practice/moderate/{id}",
     *     name="profile.practice.moderate",
     *     requirements={"_locale": "%route.available_locales%", "id": "\d+"},
     * )
     * @Security("is_granted('CAN_EDIT_UNIVERSITY_BEST_PRACTICE', bestPractice)")
     * @param Request      $request
     * @param BestPractice $bestPractice
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function moderate(Request $request, BestPractice $bestPractice)
    {
        $this->bestPracticeManager->moderate($bestPractice)->store($bestPractice);

        $this->dispatchActionEvent($bestPractice, TargetActionEvent::MODERATION);

        return $this->redirectToRoute('profile.practice.index');
    }

    /**
     * @Route(
     *     path="{_locale}/profile/practice/publish/{id}",
     *     name="profile.practice.publish",
     *     requirements={"_locale": "%route.available_locales%", "id": "\d+"},
     * )
     * @Security("is_granted('CAN_PUBLISH_BEST_PRACTICE', bestPractice)")
     * @param Request      $request
     * @param BestPractice $bestPractice
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function publish(Request $request, BestPractice $bestPractice)
    {
        $this->bestPracticeManager->publish($bestPractice)->store($bestPractice);

        $this->dispatchActionEvent($bestPractice, TargetActionEvent::PUBLISHED);

        return $this->redirectToRoute('profile.practice.index');
    }

    /**
     * @Route(
     *     path="{_locale}/profile/practice/decline/{id}",
     *     name="profile.practice.decline",
     *     requirements={"_locale": "%route.available_locales%", "id": "\d+"},
     * )
     * @Security("is_granted('CAN_PUBLISH_BEST_PRACTICE', bestPractice)")
     * @param Request      $request
     * @param BestPractice $bestPractice
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function decline(Request $request, BestPractice $bestPractice)
    {
        $this->bestPracticeManager->decline($bestPractice)->store($bestPractice);

        $this->dispatchActionEvent($bestPractice, TargetActionEvent::DECLINED);

        return $this->redirectToRoute('profile.practice.index');
    }

    /**
     * @param BestPractice $bestPractice
     * @param Form         $form
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function chooseResponse(BestPractice $bestPractice, Form $form)
    {
        // default submit button
        $response = $this->redirectToRoute('profile.practice.edit', ['id' => $bestPractice->getId()]);

        if ($form->has('submitForModeration') && $form->get('submitForModeration')->isClicked()) {
            $response = $this->redirectToRoute('profile.practice.moderate', ['id' => $bestPractice->getId()]);
        }

        if ($form->has('submitAndPublish') && $form->get('submitAndPublish')->isClicked()) {
            $response = $this->redirectToRoute('profile.practice.publish', ['id' => $bestPractice->getId()]);
        }

        if ($form->has('submitAndSaveAsDraft') && $form->get('submitAndSaveAsDraft')->isClicked()) {
            $response = $this->redirectToRoute('profile.practice.decline', ['id' => $bestPractice->getId()]);
        }

        return $response;
    }
}
