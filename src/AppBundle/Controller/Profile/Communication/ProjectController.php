<?php

namespace AppBundle\Controller\Profile\Communication;

use AppBundle\Controller\Profile\BaseProfileController;
use AppBundle\Entity\Project;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class ProjectController extends BaseProfileController
{
    /**
     * @Route(
     *     path="/{_locale}/profile/communication/project/{id}",
     *     name="profile.communication.project.main",
     *     requirements={"_locale": "%route.available_locales%" }
     * )
     *
     * @Template("@App/Profile/Communication/Project/main.html.twig")
     * @Security("is_granted('CAN_BROWSE_PROJECT', project)")
     */
    public function mainAction(Request $request, Project $project)
    {
        return $this->response([
            'project' => $project,
        ]);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/communication/project/members/{id}",
     *     name="profile.communication.project.members",
     *     requirements={"_locale": "%route.available_locales%" }
     * )
     *
     * @Template("@App/Profile/Communication/Project/members.html.twig")
     * @Security("is_granted('CAN_BROWSE_PROJECT', project)")
     */
    public function membersAction(Request $request, Project $project)
    {
        return $this->response(['project' => $project]);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/communication/project/docs/{id}",
     *     name="profile.communication.project.docs",
     *     requirements={"_locale": "%route.available_locales%" }
     * )
     *
     * @Template("@App/Profile/Communication/Project/docs.html.twig")
     * @Security("is_granted('CAN_BROWSE_PROJECT', project)")
     */
    public function docsAction(Request $request, Project $project)
    {
        return $this->response(['project' => $project]);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/communication/project/archive/{id}",
     *     name="profile.communication.project.archive",
     *     requirements={"_locale": "%route.available_locales%" }
     * )
     *
     * @Template("@App/Profile/Communication/Project/archive.html.twig")
     * @Security("is_granted('CAN_BROWSE_PROJECT', project)")
     */
    public function archiveAction(Request $request, Project $project)
    {
        return $this->response(['project' => $project]);
    }
}
