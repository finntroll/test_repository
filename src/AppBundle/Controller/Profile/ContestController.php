<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\Contest;
use AppBundle\Event\TargetActionEvent;
use AppBundle\Filter\ContestFilter;
use AppBundle\Form\Contest\ContestProfileFilterType;
use AppBundle\Form\Contest\ContestType;
use AppBundle\Manager\ContestManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ContestController
 *
 * @Security("is_granted('CAN_ACCESS_CONTEST')")
 */
class ContestController extends BaseProfileController
{
    protected $contestManager;

    public function __construct(ContestManager $manager)
    {
        $this->contestManager = $manager;
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/contest/",
     *     name="profile.contest.index"
     * )
     * @Template("@App/Profile/Contest/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $paginator = $this->get('knp_paginator');
        $page = $request->get('page', 1);
        $perPage = $request->get('perPage', 12);

        $filter = new ContestFilter();

        $filterForm = $this->createForm(ContestProfileFilterType::class, $filter, ['locale' => $request->getLocale()]);
        $filterForm->handleRequest($request);

        if ($this->isGranted(['ROLE_SITE_ADMIN', 'ROLE_GLOBAL_UNIVERSITY_ADMIN', 'ROLE_NEWS_EDITOR'])) {
            $contests = $this->contestManager->findContestsFilter($filter);
        } else {
            $university = $this->getUser()->getUniversity();
            $contests = $this->contestManager->findUniversityContests($university, $filter);
        }

        $result = $paginator->paginate($contests, $page, $perPage);

        return $this->response([
            'items' => $result,
            'filterForm' => $filterForm->createView(),
        ]);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/contest/create",
     *     requirements={"_locale": "%route.available_locales%"},
     *     name="profile.contest.create"
     * )
     * @Template("@App/Profile/Contest/edit.html.twig")
     */
    public function createAction(Request $request)
    {
        $contest = $this->contestManager->create(['user' => $this->getUser()]);

        return $this->editAction($request, $contest);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/contest/edit/{id}",
     *     requirements={"_locale": "%route.available_locales%"},
     *     name="profile.contest.edit"
     * )
     * @Template
     */
    public function editAction(Request $request, Contest $contest)
    {
        $form = $this->createForm(ContestType::class, $contest, [
            'user' => $this->getUser(),
            'locale' => $request->getLocale()
        ]);
        $form->handleRequest($request);
        $dispatchAction = $this->identifyAction($contest);
        if($this->checkFormErrors($form)){
            $this->dispatchActionEvent($contest, TargetActionEvent::FORM_ERROR);

            return $this->response(['form' => $form->createView()]);
        }

        if ($form->isSubmitted() && $form->isValid()) {

            if ($form->get('submitAndPublish')->isClicked()) {
                $this->dispatchActionEvent($contest, TargetActionEvent::PUBLISHED);
                $this->contestManager->publish($contest);
            }

            $this->contestManager->update($contest)->store($contest);

            $this->dispatchActionEvent($contest, $dispatchAction);

            return $this->redirectToRoute('profile.contest.index');
        }

        return $this->response(['form' => $form->createView()]);
    }

    /**
     * @Route(path="/{_locale}/profile/contest/delete/{id}", name="profile.contest.delete")
     */
    public function deleteAction(Request $request, Contest $contest)
    {
        $this->contestManager->remove($contest)->store($contest);

        $this->dispatchActionEvent($contest, TargetActionEvent::DELETED);

        return $this->redirectToRoute('profile.contest.index');
    }
}
