<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\News;
use AppBundle\Event\TargetActionEvent;
use AppBundle\Filter\NewsFilter;
use AppBundle\Form\News\{NewsProfileFilterType, NewsType};
use AppBundle\Manager\NewsManager;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class NewsController
 *
 * @Security("is_granted('CAN_ACCESS_NEWS')")
 */
class NewsController extends BaseProfileController
{
    /**
     * @var NewsManager
     */
    private $manager;

    public function __construct(NewsManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/news/",
     *     name="profile.news.index",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     * @Template("@App/Profile/News/list.html.twig")
     */
    public function indexAction(Request $request)
    {
        $page = $request->get('page', 1);
        $perPage = $request->get('perPage', 12);
        $filter = new NewsFilter();
        $filterForm = $this->createForm(NewsProfileFilterType::class, $filter, ['locale' => $request->getLocale()]);

        $filterForm->handleRequest($request);

        if ($this->isGranted('CAN_CREATE_NEWS')) {
            $news = $this->manager->findAllNews($filter);
        } elseif ($this->isGranted('CAN_CREATE_UNIVERSITY_NEWS')) {
            $news = $this->manager->findUniversityNews($filter, $this->getUser()->getUniversity());
        } else {
            throw new NotFoundHttpException();
        }

        $paginatedNewsFeed = $this->get('knp_paginator')->paginate($news, $page, $perPage);

        return $this->response([
            'newsFeed' => $paginatedNewsFeed,
            'searchForm' => $filterForm->createView(),
        ]);
    }

    /**
     * @Route(
     *     path="{_locale}/profile/news/create/",
     *     name="profile.news.create",
     *     requirements={"_locale": "%route.available_locales%"},
     * )
     * @Template("@App/Profile/News/edit.html.twig")
     * @Security("is_granted('CAN_CREATE_UNIVERSITY_NEWS')")
     */
    public function createAction(Request $request)
    {
        $userUniversity = $this->getUser()->getUniversity();
        $options = [];
        $defaultType = $this->getDoctrine()->getRepository('AppBundle:NewsType')->find(1);
        $options['university'] = $userUniversity ?? null;
        $options['type'] = $userUniversity ? $defaultType : null;

        $news = $this->manager->create($options);

        return $this->editAction($request, $news);
    }

    /**
     * @Route(
     *     path="{_locale}/profile/news/edit/{id}",
     *     name="profile.news.edit",
     *     requirements={"_locale": "%route.available_locales%", "id": "\d+"},
     * )
     * @Template("@App/Profile/News/edit.html.twig")
     * @Security("is_granted('CAN_EDIT_UNIVERSITY_NEWS', news)")
     *
     * @param Request $request
     * @param News    $news
     *
     * @return array|Response
     */
    public function editAction(Request $request, News $news)
    {
        $form = $this->createForm(NewsType::class, $news, ['locale' => $request->getLocale()]);
        $em = $this->getDoctrine()->getManager();
        $originalDocuments = new ArrayCollection();

        foreach ($news->getUploadedDocuments() as $doc) {
            $originalDocuments->add($doc);
        }

        $form->handleRequest($request);
        $dispatchAction = $this->identifyAction($news);
        if ($this->checkFormErrors($form)) {
            $this->dispatchActionEvent($news, TargetActionEvent::FORM_ERROR);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ArrayCollection $originalDocuments */
            foreach ($originalDocuments as $doc) {
                if (false === $news->getUploadedDocuments()->contains($doc)) {
                    $em->remove($doc); // flush in store;
                }
            }
            $this->manager->update($news)->store($news);
            $this->dispatchActionEvent($news, $dispatchAction);

            return $this->chooseResponse($news, $form);
        }

        return $this->response(['form' => $form->createView(), 'news' => $news]);
    }

    /**
     * @Route(
     *     path="{_locale}/profile/news/remove/{id}",
     *     name="profile.news.remove",
     *     requirements={"_locale": "%route.available_locales%", "id": "\d+"},
     * )
     * @Security("is_granted('CAN_REMOVE_NEWS', news)")
     *
     * @param Request $request
     * @param News    $news
     *
     * @return RedirectResponse
     */
    public function remove(Request $request, News $news)
    {
        $this->manager->remove($news)->store($news);
        $this->dispatchActionEvent($news, TargetActionEvent::DELETED);

        return $this->redirectToRoute('profile.news.index');
    }

    /**
     * @Route(
     *     path="{_locale}/profile/news/publish/{id}",
     *     name="profile.news.publish",
     *     requirements={"_locale": "%route.available_locales%", "id": "\d+"},
     * )
     * @Security("is_granted('CAN_PUBLISH_NEWS', news)")
     * @param Request $request
     * @param News    $news
     *
     * @return RedirectResponse
     */
    public function publish(Request $request, News $news)
    {
        $this->manager->publish($news)->store($news);
        $this->dispatchActionEvent($news, TargetActionEvent::PUBLISHED);

        return $this->redirectToRoute('profile.news.index');
    }

    /**
     * @Route(
     *     path="{_locale}/profile/news/moderate/{id}",
     *     name="profile.news.moderate",
     *     requirements={"_locale": "%route.available_locales%", "id": "\d+"},
     * )
     * @Security("is_granted('CAN_EDIT_UNIVERSITY_NEWS', news)")
     * @param Request $request
     * @param News    $news
     *
     * @return RedirectResponse
     */
    public function moderate(Request $request, News $news)
    {
        $this->manager->moderate($news)->store($news);
        $this->dispatchActionEvent($news, TargetActionEvent::MODERATION);

        return $this->redirectToRoute('profile.news.index');
    }

    /**
     * @Route(
     *     path="{_locale}/profile/news/decline/{id}",
     *     name="profile.news.decline",
     *     requirements={"_locale": "%route.available_locales%", "id": "\d+"},
     * )
     * @Security("is_granted('CAN_PUBLISH_NEWS', news)")
     * @param Request $request
     * @param News    $news
     *
     * @return RedirectResponse
     */
    public function decline(Request $request, News $news)
    {
        $this->manager->decline($news)->store($news);
        $this->dispatchActionEvent($news, TargetActionEvent::DECLINED);

        return $this->redirectToRoute('profile.news.index');
    }

    /**
     * @param News $news
     * @param Form $form
     *
     * @return RedirectResponse
     */
    private function chooseResponse(News $news, Form $form)
    {
        $response = $this->redirectToRoute('profile.news.edit', ['id' => $news->getId()]);
        if ($form->has('submitAndModerate') && $form->get('submitAndModerate')->isClicked()) {
            $response = $this->redirectToRoute('profile.news.moderate', ['id' => $news->getId()]);
        }

        if ($form->has('submitAndPublish') && $form->get('submitAndPublish')->isClicked()) {
            $response = $this->redirectToRoute('profile.news.publish', ['id' => $news->getId()]);
        }

        return $response;
    }
}
