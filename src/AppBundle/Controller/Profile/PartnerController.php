<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\Partner;
use AppBundle\Event\TargetActionEvent;
use AppBundle\Filter\PartnerFilter;
use AppBundle\Form\Partner\{PartnerType, PartnerFilterType};
use AppBundle\Manager\PartnerManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Security("is_granted('CAN_ACCESS_PARTNER')")
 */
class PartnerController extends BaseProfileController
{
    /**
     * @var PartnerManager
     */
    private $manager;

    public function __construct(PartnerManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/partner/",
     *     requirements={"_locale": "%route.available_locales%"},
     *     name="profile.partner.index"
     * )
     * @Template("@App/Profile/Partner/list.html.twig")
     */
    public function indexAction(Request $request)
    {
        $page = $request->get('page', 1);
        $perPage = $request->get('perPage', 12);
        $filter = new PartnerFilter();
        $filterForm = $this->createForm(PartnerFilterType::class, $filter);

        $filterForm->handleRequest($request);

        if ($this->isGranted('CAN_CREATE_PARTNER')) {
            $partners = $this->manager->findAllPartners($filter);
        } else {
            throw new NotFoundHttpException();
        }

        $paginatedPartners = $this->get('knp_paginator')->paginate($partners, $page, $perPage);

        return $this->response([
            'partners' => $paginatedPartners,
            'searchForm' => $filterForm->createView(),
        ]);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/partner/create",
     *     name="profile.partner.add",
     *     requirements={"_locale": "%route.available_locales%"}
     *)
     * @Template("@App/Profile/Partner/edit.html.twig")
     */
    public function createAction(Request $request)
    {
        $partner = $this->manager->create();

        return $this->editAction($request, $partner);
    }

    /**
     * @Route(
     *     path="{_locale}/profile/partner/edit/{id}",
     *     name="profile.partner.edit",
     *     requirements={"_locale": "%route.available_locales%", "id": "\d+"},
     * )
     * @Template("@App/Profile/Partner/edit.html.twig")
     * @Security("is_granted('CAN_EDIT_PARTNER', partner)")
     *
     * @param Request $request
     * @param Partner $partner
     *
     * @return array|Response
     */
    public function editAction(Request $request, Partner $partner)
    {
        $dispatchAction = $this->identifyAction($partner);
        $form = $this->createForm(PartnerType::class, $partner);
        $form->handleRequest($request);
        if($this->checkFormErrors($form)){
            $this->dispatchActionEvent($partner, TargetActionEvent::FORM_ERROR);

            return $this->response(['form' => $form->createView()]);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->has('submitAndModerate') && $form->get('submitAndModerate')->isClicked()) {
                $this->manager->moderate($partner)->store($partner);
                $this->dispatchActionEvent($partner, TargetActionEvent::MODERATION);

                return $this->redirectToRoute('profile.partner.index');

            } elseif ($form->has('submitAndPublish') && $form->get('submitAndPublish')->isClicked()) {
                $this->manager->update($partner)->store($partner);

                return $this->redirectToRoute('profile.partner.publish', ['id' => $partner->getId()]);
            }
            $this->manager->update($partner)->store($partner);

            $this->dispatchActionEvent($partner, $dispatchAction);

            return $this->redirectToRoute('profile.partner.edit', ['id' => $partner->getId()]);
        }

        return $this->response([
            'form' => $form->createView(),
            'partner' => $partner,
        ]);
    }

    /**
     * @Route(
     *     path="{_locale}/profile/partner/remove/{id}",
     *     name="profile.partner.remove",
     *     requirements={"_locale": "%route.available_locales%", "id": "\d+"},
     * )
     * @Security("is_granted('CAN_REMOVE_PARTNER', partner)")
     *
     * @param Request $request
     * @param Partner $partner
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Request $request, Partner $partner)
    {
        $this->manager->remove($partner)->store($partner);

        $this->dispatchActionEvent($partner, TargetActionEvent::DELETED);

        return $this->redirectToRoute('profile.partner.index');
    }

    /**
     * @Route(
     *     path="{_locale}/profile/partner/publish/{id}",
     *     name="profile.partner.publish",
     *     requirements={"_locale": "%route.available_locales%", "id": "\d+"},
     * )
     * @Security("is_granted('CAN_PUBLISH_PARTNER', partner)")
     *
     * @param Request $request
     * @param Partner $partner
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function publishAction(Request $request, Partner $partner)
    {
        $this->manager->publish($partner)->store($partner);

        $this->dispatchActionEvent($partner, TargetActionEvent::PUBLISHED);

        return $this->redirectToRoute('profile.partner.index');
    }

    /**
     * @Route(
     *     path="{_locale}/profile/partner/decline/{id}",
     *     name="profile.partner.decline",
     *     requirements={"_locale": "%route.available_locales%", "id": "\d+"},
     * )
     * @Security("is_granted('CAN_PUBLISH_PARTNER', partner)")
     *
     * @param Request $request
     * @param Partner $partner
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function declineAction(Request $request, Partner $partner)
    {
        $this->manager->decline($partner)->store($partner);

        $this->dispatchActionEvent($partner, TargetActionEvent::UNPUBLISHED);

        return $this->redirectToRoute('profile.partner.index');
    }
}
