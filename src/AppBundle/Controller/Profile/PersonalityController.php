<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\Personality;
use AppBundle\Event\TargetActionEvent;
use AppBundle\Filter\PersonalityFilter;
use AppBundle\Form\Personality\PersonalityProfileFilterType;
use AppBundle\Form\Personality\PersonalityType;
use AppBundle\Manager\PersonalityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route(
 *     path="{_locale}/profile/personality",
 *     requirements={"_locale": "%route.available_locales%"}
 * )
 * @Security("is_granted('CAN_ACCESS_PERSONALITY')")
 */
class PersonalityController extends BaseProfileController
{
    private $manager;

    /**
     * PersonalityController constructor.
     *
     * @param $manager
     */
    public function __construct(PersonalityManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route(
     *     path="/",
     *     name="profile.personality.index"
     * )
     * @Template("@App/Profile/Personality/list.html.twig")
     */
    public function indexAction(Request $request)
    {
        $filter = new PersonalityFilter();
        $searchForm = $this->createForm(PersonalityProfileFilterType::class, $filter, ['locale' => $request->getLocale()]);
        $searchForm->handleRequest($request);

        $personalities = $this->manager->findAll($filter);

        $page = $request->get('page', 1);
        $perPage = $request->get('perPage', 12);
        $paginator = $this->get('knp_paginator');

        $result = $paginator->paginate($personalities, $page, $perPage, ['wrap-queries' => true, 'distinct' => false]);

        return $this->response([
            'personalities' => $result,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    /**
     * @Route(
     *     path="/create",
     *     name="profile.personality.create"
     * )
     * @Template("@App/Profile/Personality/edit.html.twig")
     */
    public function createAction(Request $request)
    {
        $personality = $this->manager->create();

        return $this->editAction($request, $personality);
    }

    /**
     * @Route(
     *     path="/edit/{id}",
     *     name="profile.personality.edit"
     * )
     * @Template("@App/Profile/Personality/edit.html.twig")
     */
    public function editAction(Request $request, Personality $personality)
    {
        $form = $this->createForm(PersonalityType::class, $personality);
        $form->handleRequest($request);

        $dispatchAction = $this->identifyAction($personality);

        if ($this->checkFormErrors($form)) {
            $this->dispatchActionEvent($personality, TargetActionEvent::FORM_ERROR);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->update($personality)->store($personality);

            $this->dispatchActionEvent($personality, $dispatchAction);

            return $this->chooseResponse($personality, $form);
        }

        return $this->response([
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route(
     *     path="/delete/{id}",
     *     name="profile.personality.delete"
     * )
     */
    public function deleteAction(Request $request, Personality $personality)
    {
        $this->manager->remove($personality)->store($personality);
        $this->dispatchActionEvent($personality, TargetActionEvent::DELETED);

        return $this->redirectToRoute('profile.personality.index');
    }

    /**
     * @Route(
     *     path="/invite/{id}",
     *     name="profile.personality.invite"
     * )
     */
    public function invite(Request $request, Personality $personality)
    {
        $this->manager->invite($personality);

        return $this->redirectToRoute('profile.personality.index');
    }

    private function chooseResponse(Personality $personality, Form $form)
    {
        $response = $this->redirectToRoute('profile.personality.index');

        if ($form->has('submitAndInvite') && $form->get('submitAndInvite')->isClicked()) {
            $response = $this->redirectToRoute('profile.personality.invite', ['id' => $personality->getId()]);
        }

        return $response;
    }
}
