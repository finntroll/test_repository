<?php

namespace AppBundle\Controller\Profile;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProfileController
 *
 * @Route(path="/{_locale}/profile", requirements={"_locale": "%route.available_locales%" })
 */
class ProfileController extends BaseProfileController
{
    /**
     * @Route(path="/", name="profile.index")
     * @Route(name="fos_user_profile_show")
     * @Template("@App/Profile/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        return $this->response();
    }

}
