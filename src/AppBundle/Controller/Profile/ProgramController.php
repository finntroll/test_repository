<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\Program;
use AppBundle\Event\TargetActionEvent;
use AppBundle\Filter\ProgramFilter;
use AppBundle\Form\Program\ProgramProfileFilterType;
use AppBundle\Form\Program\ProgramType;
use AppBundle\Manager\ProgramManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("is_granted('CAN_ACCESS_PROGRAM')")
 */
class ProgramController extends BaseProfileController
{
    /**
     * @var ProgramManager
     */
    private $programManager;

    public function __construct(ProgramManager $manager)
    {
        $this->programManager = $manager;
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/program/",
     *     requirements={"_locale": "%route.available_locales%"},
     *     name="profile.program.index"
     * )
     * @Template("@App/Profile/Program/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $filter = new ProgramFilter();
        $filterForm = $this->createForm(ProgramProfileFilterType::class, $filter, ['locale' => $request->getLocale(), 'user' => $this->getUser()]);
        $filterForm->handleRequest($request);

        if ($this->isGranted(['ROLE_SITE_ADMIN', 'ROLE_GLOBAL_UNIVERSITY_ADMIN'])) {
            $programs = $this->programManager->findPrograms($filter);
        } else {
            $user = $this->getUser();
            $programs = $this->programManager->findUniversityPrograms($user->getUniversity(), $filter);
        }

        $page = $request->get('page', 1);
        $perPage = $request->get('perPage', 12);
        $paginator = $this->get('knp_paginator');

        $result = $paginator->paginate($programs, $page, $perPage);

        return $this->response(['programs' => $result, 'filterForm' => $filterForm->createView()]);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/program/create",
     *     requirements={"_locale": "%route.available_locales%"},
     *     name="profile.program.create"
     * )
     * @Template("@App/Profile/Program/edit.html.twig")
     */
    public function createAction(Request $request)
    {
        $program = $this->programManager->create(['user' => $this->getUser()]);

        return $this->editAction($request, $program);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/program/edit/{id}",
     *     requirements={"_locale": "%route.available_locales%"},
     *     name="profile.program.edit"
     * )
     * @Template
     */
    public function editAction(Request $request, Program $program)
    {
        $dispatchAction = $this->identifyAction($program);
        $form = $this->createForm(ProgramType::class, $program, ['locale' => $request->getLocale(), 'user' => $this->getUser()]);
        $form->handleRequest($request);
        if($this->checkFormErrors($form)) {
            $this->dispatchActionEvent($program, TargetActionEvent::FORM_ERROR);

            return $this->response(['form' => $form->createView()]);
        }

        if ($form->isSubmitted() && $form->isValid()) {

            if ($form->get('submitAndPublish')->isClicked()) {
                $this->dispatchActionEvent($program, TargetActionEvent::PUBLISHED);
                $this->programManager->publish($program);
            }

            $this->programManager->update($program)->store($program);

            $this->dispatchActionEvent($program, $dispatchAction);

            return $this->redirectToRoute('profile.program.index');
        }

        return $this->response(['form' => $form->createView()]);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/program/delete/{id}",
     *     requirements={"_locale": "%route.available_locales%"},
     *     name="profile.program.delete"
     * )
     */
    public function deleteAction(Request $request, Program $program)
    {
        $this->programManager->remove($program)->store($program);

        $this->dispatchActionEvent($program, TargetActionEvent::DELETED);

        return $this->redirectToRoute('profile.program.index');
    }
}
