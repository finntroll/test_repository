<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Form\User\UserDataType;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SettingsController
 */
class SettingsController extends BaseProfileController
{
    /**
     * @Route(
     *     path="/{_locale}/profile/settings/",
     *     name="profile.settings.index",
     *     requirements={ "_locale": "%route.available_locales%" }
     * )
     * @Template("@App/Profile/Settings/edit.html.twig")
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_INITIALIZE, new GetResponseUserEvent($this->getUser()));

        $form = $this->createForm(UserDataType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_SUCCESS, new FormEvent($form, $request));

            $manager = $this->get('fos_user.user_manager');
            $manager->updateUser($user, true);

            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_COMPLETED);

            return $this->redirectToRoute('profile.settings.index');
        }

        return $this->response([
            'settingsForm' => $form->createView(),
            'user' => $user,
        ]);
    }

}
