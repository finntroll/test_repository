<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\StaticPageData;
use AppBundle\Entity\StaticPageType\AboutAssociationPage;
use AppBundle\Event\TargetActionEvent;
use AppBundle\Form\StaticPage\StaticPageDataType;
use AppBundle\Manager\StaticPageDataManager;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("is_granted('CAN_ACCESS_STATIC_PAGES')")
 */
class StaticPageDataController extends BaseProfileController
{
    private $manager;

    /**
     * StaticDataController constructor.
     *
     * @param $manager
     */
    public function __construct(StaticPageDataManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/static/",
     *     name="profile.static.index",
     *     requirements={ "_locale": "%route.available_locales%" }
     * )
     * @Template("@App/Profile/StaticPageData/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $staticPages = $this->manager->findAll();

        $page = $request->get('page', 1);
        $perPage = $request->get('perPage', 12);
        $paginator = $this->get('knp_paginator');

        $result = $paginator->paginate($staticPages, $page, $perPage);

        return $this->response([
            'staticPages' => $result,
        ]);
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/static/edit/{id}",
     *     name="profile.static.edit",
     *     requirements={ "_locale": "%route.available_locales%" }
     * )
     * @Template("@App/Profile/StaticPageData/edit.html.twig")
     */
    public function editAction(Request $request, StaticPageData $staticPage)
    {
        $form = $this->createForm(StaticPageDataType::class, $staticPage);
        $dispatchAction = $this->identifyAction($staticPage);

        $em = $this->getDoctrine()->getManager();

        if ($staticPage instanceof AboutAssociationPage) {
            $originalDocuments = new ArrayCollection();
            // Must be collected before handleRequest call!
            foreach ($staticPage->getUploadedDocuments() as $doc) {
                $originalDocuments->add($doc);
            }
        }

        $form->handleRequest($request);

        if ($this->checkFormErrors($form)) {
            $this->dispatchActionEvent($staticPage, TargetActionEvent::FORM_ERROR);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            if ($staticPage instanceof AboutAssociationPage) {
                /** @var ArrayCollection $originalDocuments */
                foreach ($originalDocuments as $doc) {
                    if (false === $staticPage->getUploadedDocuments()->contains($doc)) {
                        $em->remove($doc); // flush in store;
                    }
                }
            }

            $this->manager->update($staticPage)->store($staticPage);
            $this->dispatchActionEvent($staticPage, $dispatchAction);
        }

        return $this->response([
            'static_page' => $staticPage,
            'form' => $form->createView(),
        ]);
    }
}
