<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\User;
use AppBundle\Service\StatisticReportCreator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class StatsController
 *
 * @Route(path="/{_locale}/profile/stats", requirements={"_locale": "%route.available_locales%" })
 * @Security("is_granted('CAN_ACCESS_STATS')")
 */
class StatsController extends BaseProfileController
{
    private $statisticReportCreator;

    public function __construct(StatisticReportCreator $statisticReportCreator)
    {
        $this->statisticReportCreator = $statisticReportCreator;
    }

    /**
     * @Route(path="/", name="profile.stats.index")
     *
     * @Template("@App/Profile/Stats/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        return $this->response();
    }

    /**
     * @Route(path="/get", name="profile.stats.get")
     */
    public function getStatisticAction(Request $request)
    {
        $dateType = $request->query->get('date', 'day');

        /** @var User $user */
        $user = $this->getUser();

        $locale = $request->getLocale();

        $universityId = null;
        if (!$this->isGranted(['ROLE_SITE_ADMIN', 'ROLE_GLOBAL_UNIVERSITY_ADMIN'])){
            $university = $user->getUniversity();
            if ($university) {
                $universityId = $university->getId();
            } else {
                $errorMsg = $this->get('translator')->trans('error.university',[],'statistic', $locale);
                return new JsonResponse(['error' => $errorMsg],404);
            }
        }

        $this->statisticReportCreator->setLocale($locale);
        $res = $this->statisticReportCreator->getReport($dateType, $universityId);

        return new JsonResponse($res);
    }

}
