<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\Subscription;
use AppBundle\Form\Subscription\SubscriptionEmailType;
use AppBundle\Model\SubscriptionEmail;
use AppBundle\Service\SubscriptionMailer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route(path="/{_locale}/profile/subscription", requirements={"_locale": "%route.available_locales%" })
 * @Security("is_granted('CAN_ACCESS_SUBSCRIPTION')")
 */
class SubscriptionController extends BaseProfileController
{
    /**
     * @Route(
     *     path="/",
     *     name="profile.subscription.index"
     * )
     * @Template("AppBundle:Profile/Subscription:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        return $this->response();
    }

    /**
     * @Route(
     *     path="/mail/{lang}",
     *     name="profile.subscription.mail",
     *     requirements={"lang": "%route.available_locales%" }
     * )
     * @Security("is_granted('CAN_SEND_SUBSCRIPTION_EMAIL')")
     * @Template("AppBundle:Profile/Subscription:mail.html.twig")
     */
    public function mailAction(Request $request, $lang, SubscriptionMailer $mailer)
    {
        $subscriptionMail = new SubscriptionEmail();

        $form = $this->createForm(SubscriptionEmailType::class, $subscriptionMail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $subscription = $em->getRepository(Subscription::class)->findBy(['isConfirmed'=>true, 'locale' => $lang]);
            $mailer->sendSubscription($subscriptionMail, $subscription);

            return new RedirectResponse($this->generateUrl('profile.subscription.index'));
        }

        return $this->response([
            'form' => $form->createView(),
            'lang' => $lang,
        ]);
    }

    /**
     * @Route(
     *     path="/export",
     *     name="profile.subscription.export"
     * )
     * @Security("is_granted('CAN_SEND_SUBSCRIPTION_EMAIL')")
     * @Template("AppBundle:Profile/Subscription:export.csv.twig")
     */
    public function exportAction(Request $request)
    {
        $subscribers = $this->getDoctrine()->getRepository('AppBundle:Subscription')->findAll();

        $content = $this->renderView('AppBundle:Profile/Subscription:export.csv.twig', [
            'subscribers' => $subscribers
        ]);

        return new Response($content, 200, [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment;filename=subscribers.csv',
        ]);
    }
}
