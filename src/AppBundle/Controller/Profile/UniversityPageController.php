<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Event\TargetActionEvent;
use AppBundle\Form\University\UniversityType;
use AppBundle\Manager\UniversityManager;
use AppBundle\Manager\ProgramManager;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("is_granted('CAN_EDIT_UNIVERSITY')")
 */
class UniversityPageController extends BaseProfileController
{
    /**
     * @var UniversityManager
     */
    private $manager;

    /**
     * @var ProgramManager
     */
    private $programManager;

    public function __construct(
        UniversityManager $manager,
        ProgramManager $programManager
    ) {
        $this->manager = $manager;
        $this->programManager = $programManager;
    }

    /**
     * @Route(
     *     path="/{_locale}/profile/university/",
     *     name="profile.university.index",
     *     requirements={"_locale": "%route.available_locales%"}
     *     )
     * @Template("@App/Profile/Unversity/edit.html.twig")
     */
    public function editAction(Request $request)
    {
        $loadedUniversity = $this->manager->loadUniversity($this->getUser()->getUniversity()->getId());
        $dispatchAction = $this->identifyAction($loadedUniversity);

        $em = $this->getDoctrine()
            ->getManager();

        $originalBranches = new ArrayCollection();
        foreach ($loadedUniversity->getSubBranches() as $branch) {
            $originalBranches->add($branch);
        }
        $originalDocuments = new ArrayCollection();
        foreach ($loadedUniversity->getUploadedDocuments() as $doc) {
            $originalDocuments->add($doc);
        }

        $form = $this->createForm(UniversityType::class, $loadedUniversity, ['locale' => $request->getLocale()]);
        $form->handleRequest($request);
        if ($this->checkFormErrors($form)) {
            $this->dispatchActionEvent($loadedUniversity, TargetActionEvent::FORM_ERROR);
        }
        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($originalBranches AS $branch) {
                if (!$loadedUniversity->getSubBranches()->contains($branch)) {
                    if ($this->programManager->findBy(['universityBranch' => $branch->getId()])) {
                        $this->dispatchActionEvent($loadedUniversity, TargetActionEvent::UNDELETABLE_BRANCH);

                        return $this->redirectToRoute('profile.university.index');
                    }
                }
            }
            /** @var ArrayCollection $originalDocuments */
            foreach ($originalDocuments as $doc) {
                if (false === $loadedUniversity->getUploadedDocuments()->contains($doc)) {
                    $em->remove($doc); // flush in store;
                }
            }
            $this->manager->update($loadedUniversity, $originalBranches)->store($loadedUniversity);
            $this->dispatchActionEvent($loadedUniversity, $dispatchAction);

            return $this->redirectToRoute('profile.university.index');
        }

        return $this->response(['form' => $form->createView(), 'university' => $loadedUniversity]);
    }
}
