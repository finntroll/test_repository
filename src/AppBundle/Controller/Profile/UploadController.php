<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\UploadedDocuments\UploadedDocument;
use Oneup\UploaderBundle\Controller\AbstractController;
use Oneup\UploaderBundle\Uploader\File\FileInterface;
use Oneup\UploaderBundle\Uploader\File\FilesystemFile;
use Oneup\UploaderBundle\Uploader\Response\EmptyResponse;
use Oneup\UploaderBundle\Uploader\Response\ResponseInterface;
use Symfony\Component\HttpFoundation\File\Exception\UploadException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;

class UploadController extends AbstractController
{

    public function upload()
    {
        // get some basic stuff together
        $request = $this->container->get('request_stack')->getMasterRequest();
        $response = new EmptyResponse();

        $files = $this->getFiles($request->files);
        $statusCode = 200;
        foreach ($files as $file) {
            try {
                $this->handleUpload($file, $response, $request);
            } catch (UploadException $e) {
                $this->errorHandler->addException($response, $e);
                $translator = $this->container->get('translator');
                $message = $translator->trans($e->getMessage(), [], 'OneupUploaderBundle');
                $response = $this->createSupportedJsonResponse(['error' => $message ]);
                $response->setStatusCode(400);

                return $response;
            }
        }

        return $this->createSupportedJsonResponse($response->assemble(), $statusCode);
    }

    protected function handleUpload($file, ResponseInterface $response, Request $request)
    {
        // wrap the file if it is not done yet which can only happen
        // if it wasn't a chunked upload, in which case it is definitely
        // on the local filesystem.
        if (!($file instanceof FileInterface)) {
            $file = new FilesystemFile($file);
        }
        $this->validate($file);

        $this->dispatchPreUploadEvent($file, $response, $request);

        // no error happend, proceed
        $namer = $this->container->get($this->config['namer']);
        $name  = $namer->name($file);

        // perform the real upload
        /** @var File $uploaded */
        $uploaded = $this->storage->upload($file, $name);

        $em = $this->container->get('doctrine.orm.entity_manager');

        // cut file extension with length form 2 to 4
        $withoutExt = \preg_replace(
            '/\\.[^.\\s]{2,4}$/',
            '',
            $uploaded->getFilename()
        );
        // cut file extension with length form 2 to 4
        $originalWithoutExt = \preg_replace(
            '/\\.[^.\\s]{2,4}$/',
            '',
            $file->getClientOriginalName()
        );

        $uploadedDocument = new UploadedDocument();
        $uploadedDocument->setExtension($uploaded->getExtension());
        $uploadedDocument->setSize($uploaded->getSize());
        $uploadedDocument->setName($originalWithoutExt);
        $uploadedDocument->setInternalName($withoutExt);
        $uploadedDocument->setMimeType($uploaded->getMimeType());
        $uploadedDocument->setPath($uploaded->getPath());

        $em->persist($uploadedDocument);
        $em->flush();

        /** @var \ArrayAccess $response */
        $response['id'] = $uploadedDocument->getId();
        $response['extension'] = $uploadedDocument->getExtension();
        $response['name'] = $uploadedDocument->getName();

        $this->dispatchPostEvents($uploaded, $response, $request);
    }
}
