<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AcademicMobility;
use AppBundle\Entity\BestPractice;
use AppBundle\Entity\Contest;
use AppBundle\Entity\News;
use AppBundle\Entity\Program;
use AppBundle\Entity\Statistic;
use AppBundle\Entity\University;
use AppBundle\Entity\UniversityBranch;
use AppBundle\Manager\StatisticManager;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableMethods;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/redirect")
 */
class RedirectController extends Controller
{

    const CLASS_MAP = [
        'academicMobility' => AcademicMobility::class,
        'bestPractice' => BestPractice::class,
        'program' => Program::class,
        'contest' => Contest::class,
        'university' => University::class,
        'universityBranch' => UniversityBranch::class,
        'news' => News::class
    ];

    private $statisticManager;

    /**
     * RedirectController constructor.
     * @param StatisticManager $statisticManager
     */
    public function __construct(StatisticManager $statisticManager)
    {
        $this->statisticManager = $statisticManager;
    }

    /**
     * @Route(
     *     path="/external",
     *     name="redirect.external"
     * )
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function externalAction(Request $request): RedirectResponse
    {
        $objectClass = $request->query->get('ob');
        $objectId = $request->query->get('id');
        $universityId = $request->query->get('un',null);

        if (!$objectClass && !$objectId) {
            throw new NotFoundHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        $repository = null;
        foreach (self::CLASS_MAP as $key => $value) {
            if ($objectClass === $key) {
                $repository = $em->getRepository($value);
                break;
            }
        }

        if (!$repository) {
            throw new NotFoundHttpException();
        }

        $object = $repository->find($objectId);

        if (!$object) {
            throw new NotFoundHttpException();
        }

        if ($object instanceof University) {
            $object = $object->getMainBranch();
        }

        $url = $this->getObjectUrl($object);

        if ($object instanceof UniversityBranch) {
            $objectClass = 'university';
            $objectId = $object->getUniversity()->getId();
        }

        if ($url === '') {
            throw new NotFoundHttpException();
        }

        $session = $this->get('session')->getId();

        $statistic = $this->statisticManager->create([
            'object' => $objectClass,
            'objectId' => $objectId,
            'session' => $session,
            'universityId' => $universityId,
            'type' => Statistic::EVENT_EXTERNAL,
        ]);

        $this->statisticManager->update($statistic)->store($statistic);

        return new RedirectResponse($url);
    }

    /**
     * @param TranslatableMethods $object
     * @return string
     */
    private function getObjectUrl($object)
    {
        $url = '';
        if (\method_exists($object->translate(), 'getUrl')) {
            $url = $object->translate()->getUrl();
        } elseif (\method_exists($object->translate(), 'getPageUrl')) {
            $url = $object->translate()->getPageUrl();
        } elseif (\method_exists($object->translate(), 'getSourceLink')) {
            $url = $object->translate()->getSourceLink();
        }

        return $url;
    }

}
