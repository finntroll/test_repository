<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Personality;
use AppBundle\Entity\User;
use AppBundle\Form\Personality\PersonalityRegistrationType;
use AppBundle\Manager\PersonalityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class RegistrationController extends Controller
{
    private $manager;

    /**
     * RegistrationController constructor.
     *
     * @param $manager
     */
    public function __construct(PersonalityManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route(
     *     path="/{_locale}/register/{hash}",
     *     requirements={"_locale": "%route.available_locales%"},
     *     name="register"
     * )
     * @Template("@App/default/registration.html.twig")
     */
    public function registerAction(Request $request, $hash)
    {
        $personality = $this->manager->findByHash($hash);

        if (!$personality instanceof Personality) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(PersonalityRegistrationType::class, $personality, [
            'validation_groups' => 'registration',
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager = $this->get('fos_user.user_manager');
            $manager->updateUser($personality, false);

            $this->manager->register($personality)->store($personality);

            $this->autoAuthUser($personality, $request);

            return $this->redirectToRoute('profile.index');
        }

        return [
            'form' => $form->createView(),
            'personality' => $personality,
        ];
    }

    private function autoAuthUser(User $user, Request $request)
    {
        $token = new UsernamePasswordToken($user, $user->getPassword(), "main", $user->getRoles());
        $this->get("security.token_storage")->setToken($token);

        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
    }
}
