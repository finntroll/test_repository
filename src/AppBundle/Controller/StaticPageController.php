<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AcademicMobilityType;
use AppBundle\Entity\Audience;
use AppBundle\Entity\ContestType;
use AppBundle\Service\StaticCounter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class StaticPageController extends Controller
{
    /**
     * @var StaticCounter
     */
    private $staticCounter;

    /**
     * StaticPageController constructor.
     * @param StaticCounter $staticCounter
     */
    public function __construct(StaticCounter $staticCounter)
    {
        $this->staticCounter = $staticCounter;
    }

    /**
     * @Route(
     *     path="/{_locale}/students/",
     *     name="main.static.students",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     */
    public function studentsAction()
    {
        $viewTypes = [
            ContestType::CONTEST_NAME__NIOKR => [
                'type' => StaticCounter::TYPE_CONTEST,
                'data' => [ContestType::CONTEST_TYPE__NIOKR],
                'audience' => [
                    Audience::AUDIENCE_TYPE__BACHELORS,
                    Audience::AUDIENCE_TYPE__MASTERS,
                ],
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SCHOOLS => [
                'type' => StaticCounter::TYPE_ACADEMIC,
                'data' => [AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SCHOOLS],
                'audience' => [
                    Audience::AUDIENCE_TYPE__BACHELORS,
                    Audience::AUDIENCE_TYPE__MASTERS,
                ],
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE => [
                'type' => StaticCounter::TYPE_ACADEMIC,
                'data' => [AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE],
                'audience' => [
                    Audience::AUDIENCE_TYPE__BACHELORS,
                    Audience::AUDIENCE_TYPE__MASTERS,
                ],
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE => [
                'type' => StaticCounter::TYPE_ACADEMIC,
                'data' => [
                    AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE,
                    AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__MASTERCLASS,
                ],
                'audience' => [
                    Audience::AUDIENCE_TYPE__BACHELORS,
                    Audience::AUDIENCE_TYPE__MASTERS,
                ],
            ],
        ];

        $counterData = $this->staticCounter
            ->getCountersByTypes($viewTypes);

        $views = [
            ContestType::CONTEST_NAME__NIOKR => [
                'translate' => 'middle-menu.rd',
                'count' => $counterData[ContestType::CONTEST_NAME__NIOKR],
                'link' => $this->generateUrl('main.contest.list', [
                    'audience' => Audience::AUDIENCE_MAP[Audience::AUDIENCE_TYPE__BACHELORS],
                    'contestType' => ContestType::CONTEST_TYPE_MAP[ContestType::CONTEST_TYPE__NIOKR]
                ])
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SCHOOLS => [
                'translate' => 'middle-menu.summerschools',
                'count' => $counterData[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SCHOOLS],
                'link' => $this->generateUrl('main.academic_mobility.list', [
                    'audiences' => Audience::AUDIENCE_MAP[Audience::AUDIENCE_TYPE__BACHELORS],
                    'mobilityType' => AcademicMobilityType::ACADEMIC_MOBILITY_MAP[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SCHOOLS]
                ])
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE => [
                'translate' => 'middle-menu.conferences',
                'count' => $counterData[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE],
                'link' => $this->generateUrl('main.academic_mobility.list', [
                    'audiences' => Audience::AUDIENCE_MAP[Audience::AUDIENCE_TYPE__BACHELORS],
                    'mobilityType' => AcademicMobilityType::ACADEMIC_MOBILITY_MAP[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE]
                ])
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE => [
                'translate' => 'middle-menu.lectures_class',
                'count' => $counterData[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE],
                'link' => $this->generateUrl('main.academic_mobility.list', [
                    'audiences' => Audience::AUDIENCE_MAP[Audience::AUDIENCE_TYPE__BACHELORS],
                    'mobilityType' => AcademicMobilityType::ACADEMIC_MOBILITY_MAP[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE]
                ])
            ],
        ];

        return $this->render('AppBundle:StaticPage:students.html.twig', ['views' => $views]);
    }

    /**
     * @Route(
     *     path="/{_locale}/undergraduate/",
     *     name="main.static.scholar",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     */
    public function scholarAction()
    {

        // Интересующие типы
        $viewTypes = [
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SEMINAR => [
                'type' => StaticCounter::TYPE_ACADEMIC,
                'data' => [
                    AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SEMINAR,
                    AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__MASTERCLASS
                ],
                'audience' => [Audience::AUDIENCE_TYPE__SCHOLAR],
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE => [
                'type' => StaticCounter::TYPE_ACADEMIC,
                'data' => [AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE],
                'audience' => [Audience::AUDIENCE_TYPE__SCHOLAR],
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE => [
                'type' => StaticCounter::TYPE_ACADEMIC,
                'data' => [AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE],
                'audience' => [Audience::AUDIENCE_TYPE__SCHOLAR],
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__ONLINE => [
                'type' => StaticCounter::TYPE_ACADEMIC,
                'data' => [AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__ONLINE],
                'audience' => [Audience::AUDIENCE_TYPE__SCHOLAR],
            ],
        ];

        $counterData = $this->staticCounter
            ->getCountersByTypes($viewTypes);

        $views = [
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SEMINAR => [
                'translate' => 'middle-menu.seminars',
                'count' => $counterData[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SEMINAR],
                'link' => $this->generateUrl('main.academic_mobility.list', [
                    'audiences' => Audience::AUDIENCE_MAP[Audience::AUDIENCE_TYPE__SCHOLAR],
                    'mobilityType' => AcademicMobilityType::ACADEMIC_MOBILITY_MAP[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SEMINAR]
                ])
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE => [
                'translate' => 'middle-menu.conferences',
                'count' => $counterData[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE],
                'link' => $this->generateUrl('main.academic_mobility.list', [
                    'audiences' => Audience::AUDIENCE_MAP[Audience::AUDIENCE_TYPE__SCHOLAR],
                    'mobilityType' => AcademicMobilityType::ACADEMIC_MOBILITY_MAP[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE]
                ])
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE => [
                'translate' => 'middle-menu.lectures-meetings',
                'count' => $counterData[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE],
                'link' => $this->generateUrl('main.academic_mobility.list', [
                    'audiences' => Audience::AUDIENCE_MAP[Audience::AUDIENCE_TYPE__SCHOLAR],
                    'mobilityType' => AcademicMobilityType::ACADEMIC_MOBILITY_MAP[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE]
                ])
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__ONLINE => [
                'translate' => 'middle-menu.webinars',
                'count' => $counterData[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__ONLINE],
                'link' => $this->generateUrl('main.academic_mobility.list', [
                    'audiences' => Audience::AUDIENCE_MAP[Audience::AUDIENCE_TYPE__SCHOLAR],
                    'mobilityType' => AcademicMobilityType::ACADEMIC_MOBILITY_MAP[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__ONLINE]
                ])
            ],
        ];

        return $this->render('AppBundle:StaticPage:scholar.html.twig', ['views' => $views]);
    }

    /**
     * @Route(
     *     path="/{_locale}/scientist/",
     *     name="main.static.scientist",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     */
    public function scientistAction()
    {

        $viewTypes = [
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE => [
                'type' => StaticCounter::TYPE_ACADEMIC,
                'data' => [AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE],
                'audience' => [Audience::AUDIENCE_TYPE__RESEARCHERS],
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE => [
                'type' => StaticCounter::TYPE_ACADEMIC,
                'data' => [AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE],
                'audience' => [Audience::AUDIENCE_TYPE__RESEARCHERS],
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__TRAINING => [
                'type' => StaticCounter::TYPE_ACADEMIC,
                'data' => [AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__TRAINING],
                'audience' => [Audience::AUDIENCE_TYPE__RESEARCHERS],
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SEMINAR => [
                'type' => StaticCounter::TYPE_ACADEMIC,
                'data' => [
                    AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SEMINAR,
                    AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__MASTERCLASS,
                ],
                'audience' => [Audience::AUDIENCE_TYPE__RESEARCHERS],
            ],
        ];

        $counterData = $this->staticCounter
            ->getCountersByTypes($viewTypes);

        $views = [
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE => [
                'translate' => 'middle-menu.conferences',
                'count' => $counterData[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE],
                'link' => $this->generateUrl('main.academic_mobility.list', [
                    'audiences' => Audience::AUDIENCE_MAP[Audience::AUDIENCE_TYPE__RESEARCHERS],
                    'mobilityType' => AcademicMobilityType::ACADEMIC_MOBILITY_MAP[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE]
                ])
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE => [
                'translate' => 'middle-menu.lectures-meetings',
                'count' => $counterData[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE],
                'link' => $this->generateUrl('main.academic_mobility.list', [
                    'audiences' => Audience::AUDIENCE_MAP[Audience::AUDIENCE_TYPE__RESEARCHERS],
                    'mobilityType' => AcademicMobilityType::ACADEMIC_MOBILITY_MAP[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE]
                ])
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__TRAINING => [
                'translate' => 'middle-menu.qualification',
                'count' => $counterData[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__TRAINING],
                'link' => $this->generateUrl('main.academic_mobility.list', [
                    'audiences' => Audience::AUDIENCE_MAP[Audience::AUDIENCE_TYPE__RESEARCHERS],
                    'mobilityType' => AcademicMobilityType::ACADEMIC_MOBILITY_MAP[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__TRAINING]
                ])
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SEMINAR => [
                'translate' => 'middle-menu.seminars',
                'count' => $counterData[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SEMINAR],
                'link' => $this->generateUrl('main.academic_mobility.list', [
                    'audiences' => Audience::AUDIENCE_MAP[Audience::AUDIENCE_TYPE__RESEARCHERS],
                    'mobilityType' => AcademicMobilityType::ACADEMIC_MOBILITY_MAP[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SEMINAR]
                ])
            ],
        ];

        return $this->render('AppBundle:StaticPage:scientist.html.twig', ['views' => $views]);
    }

    /**
     * @Route(
     *     path="/{_locale}/phd/",
     *     name="main.static.phd",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     */
    public function phdAction()
    {
        $viewTypes = [
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE => [
                'type' => StaticCounter::TYPE_ACADEMIC,
                'data' => [AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE],
                'audience' => [Audience::AUDIENCE_TYPE__PHDS],
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE => [
                'type' => StaticCounter::TYPE_ACADEMIC,
                'data' => [AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE],
                'audience' => [Audience::AUDIENCE_TYPE__PHDS],
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__TRAINING => [
                'type' => StaticCounter::TYPE_ACADEMIC,
                'data' => [AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__TRAINING],
                'audience' => [Audience::AUDIENCE_TYPE__PHDS],
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SCHOOLS => [
                'type' => StaticCounter::TYPE_ACADEMIC,
                'data' => [AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SCHOOLS],
                'audience' => [Audience::AUDIENCE_TYPE__PHDS],
            ],
        ];

        $counterData = $this->staticCounter
            ->getCountersByTypes($viewTypes);

        $views = [
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE => [
                'translate' => 'middle-menu.conferences',
                'count' => $counterData[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE],
                'link' => $this->generateUrl('main.academic_mobility.list', [
                    'audiences' => Audience::AUDIENCE_MAP[Audience::AUDIENCE_TYPE__PHDS],
                    'mobilityType' => AcademicMobilityType::ACADEMIC_MOBILITY_MAP[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__CONFERENCE]
                ])
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE => [
                'translate' => 'middle-menu.lectures-meetings',
                'count' => $counterData[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE],
                'link' => $this->generateUrl('main.academic_mobility.list', [
                    'audiences' => Audience::AUDIENCE_MAP[Audience::AUDIENCE_TYPE__PHDS],
                    'mobilityType' => AcademicMobilityType::ACADEMIC_MOBILITY_MAP[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__LECTURE]
                ])
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__TRAINING => [
                'translate' => 'middle-menu.qualification',
                'count' => $counterData[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__TRAINING],
                'link' => $this->generateUrl('main.academic_mobility.list', [
                    'audiences' => Audience::AUDIENCE_MAP[Audience::AUDIENCE_TYPE__PHDS],
                    'mobilityType' => AcademicMobilityType::ACADEMIC_MOBILITY_MAP[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__TRAINING]
                ])
            ],
            AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SCHOOLS => [
                'translate' => 'middle-menu.summerschools',
                'count' => $counterData[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SCHOOLS],
                'link' => $this->generateUrl('main.academic_mobility.list', [
                    'audiences' => Audience::AUDIENCE_MAP[Audience::AUDIENCE_TYPE__PHDS],
                    'mobilityType' => AcademicMobilityType::ACADEMIC_MOBILITY_MAP[AcademicMobilityType::ACADEMIC_MOBILITY_TYPE__SCHOOLS]
                ])
            ],
        ];

        return $this->render('AppBundle:StaticPage:phd.html.twig', ['views' => $views]);
    }

    /**
     * @Route(
     *     path="/{_locale}/members/",
     *     name="main.static.members",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     */
    public function membersAction()
    {

        $universities = $this->getDoctrine()->getRepository('AppBundle:University')->findForSlider();
        return $this->render('AppBundle:StaticPage:members.html.twig', array(
            'universities' => $universities,
        ));
    }

    /**
     * @Route(
     *     path="/{_locale}/about",
     *     name="main.static.about",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     */
    public function aboutAction(Request $request)
    {
        $associationNews = $this->getDoctrine()->getRepository('AppBundle:News')->findAssociationNews($request->getLocale());

        return $this->render('AppBundle:StaticPage:about.html.twig', array(
            'associationNews' => $associationNews,
        ));
    }

    /**
     *
     * @Route(
     *     path="/{_locale}/council/",
     *     name="main.static.council",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     */
    public function councilAction()
    {

        return $this->render('AppBundle:StaticPage:council.html.twig', array());
    }
    /**
     *
     * @Route(
     *     path="/{_locale}/commettee/",
     *     name="main.static.commettee",
     *     requirements={"_locale": "%route.available_locales%"}
     * )
     */
    public function commetteeAction()
    {

        return $this->render('AppBundle:StaticPage:revision_commettee.html.twig', array());
    }
}
