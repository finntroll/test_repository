<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Subscription;
use AppBundle\Form\Subscription\SubscriptionType;
use AppBundle\Manager\SubscriptionManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SubscriptionController extends Controller
{
    private $manager;

    /**
     * SubscriptionController constructor.
     *
     * @param SubscriptionManager $manager
     */
    public function __construct(SubscriptionManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Template("@App/subscription_form.html.twig")
     */
    public function renderFormAction()
    {
        $subscription = $this->manager->create();
        $formAction = $this->generateUrl('subscription.create');
        $form = $this->createForm(SubscriptionType::class, $subscription, ['action' => $formAction]);

        return ['form' => $form->createView()];
    }

    /**
     * @Route(
     *     path="/{_locale}/subscribe",
     *     name="subscription.create",
     *     requirements={"_locale": "%route.available_locales%"}
     *     )
     * @Template("@App/default/Subscription/create.html.twig")
     */
    public function createAction(Request $request)
    {
        $subscription = $this->manager->create();
        $form = $this->createForm(SubscriptionType::class, $subscription);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->preConfirm($subscription, $request->getLocale());
        }

        return ['form' => $form->createView(),];
    }

    /**
     * @Route(
     *     path="/{_locale}/subscribe/confirm/{hash}/{email}",
     *     name="subscription.confirm",
     *     requirements={"_locale": "%route.available_locales%"}
     *     )
     * @Template("@App/default/Subscription/result.html.twig")
     */
    public function confirmAction(Request $request, $hash, $email)
    {
        $subscription = $this->manager->findByHashAndEmail($hash, $email);

        if ($subscription && $subscription->getIsConfirmed()) {
            $this->addFlash('warning', 'subscription.email_already_was_confirmed');

            return [];
        }

        if ($this->manager->checkHash($hash, $email)) {
            $this->manager->confirm($email, $request->getLocale());
            $this->addFlash('success', 'subscription.email_confirmed');

            return [];
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route(
     *     path="/{_locale}/subscribe/revoke/{hash}",
     *     name="subscription.revoke",
     *     requirements={"_locale": "%route.available_locales%"}
     *     )
     *
     * @Template("@App/default/Subscription/result.html.twig")
     */
    public function revokeAction(Request $request, $hash)
    {
        $subscription = $this->manager->findByHash($hash);

        if (!$subscription instanceof Subscription) {
            throw new NotFoundHttpException();
        }

        $this->manager->revoke($subscription, $request->getLocale());
        $this->addFlash('success', 'subscription.email_revoked');

        return [];
    }
}
