<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * AcademicMobilityTranslation
 *
 * @ORM\Table(name="academic_mobility_translation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AcademicMobilityTranslationRepository")
 */
class AcademicMobilityTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=155)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="page_url", type="string", length=255)
     */
    private $pageUrl;

    /**
     * Set title
     *
     * @param string $title
     *
     * @return AcademicMobilityTranslation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set pageUrl
     *
     * @param string $pageUrl
     *
     * @return AcademicMobilityTranslation
     */
    public function setPageUrl($pageUrl)
    {
        $this->pageUrl = $pageUrl;

        return $this;
    }

    /**
     * Get pageUrl
     *
     * @return string
     */
    public function getPageUrl()
    {
        return $this->pageUrl;
    }
}
