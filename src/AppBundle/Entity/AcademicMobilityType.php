<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcademicMobilityType
 *
 * @ORM\Table(name="academic_mobility_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AcademicMobilityTypeRepository")
 */
class AcademicMobilityType
{
    const ACADEMIC_MOBILITY_TYPE__INTERNSHIP  = 'internship';
    const ACADEMIC_MOBILITY_TYPE__POSTDOC     = 'postdoc';
    const ACADEMIC_MOBILITY_TYPE__TRAINING    = 'training';
    const ACADEMIC_MOBILITY_TYPE__SCHOOLS     = 'schools';
    const ACADEMIC_MOBILITY_TYPE__CONFERENCE  = 'conference';
    const ACADEMIC_MOBILITY_TYPE__SEMINAR     = 'seminar';
    const ACADEMIC_MOBILITY_TYPE__LECTURE     = 'lecture';
    const ACADEMIC_MOBILITY_TYPE__MASTERCLASS = 'master_class';
    const ACADEMIC_MOBILITY_TYPE__ONLINE      = 'online';

    //Type for native query
    const ACADEMIC_MOBILITY_MAP = [
        self::ACADEMIC_MOBILITY_TYPE__INTERNSHIP  => 1,
        self::ACADEMIC_MOBILITY_TYPE__POSTDOC     => 2,
        self::ACADEMIC_MOBILITY_TYPE__TRAINING    => 3,
        self::ACADEMIC_MOBILITY_TYPE__SCHOOLS     => 4,
        self::ACADEMIC_MOBILITY_TYPE__CONFERENCE  => 5,
        self::ACADEMIC_MOBILITY_TYPE__SEMINAR     => 6,
        self::ACADEMIC_MOBILITY_TYPE__LECTURE     => 7,
        self::ACADEMIC_MOBILITY_TYPE__MASTERCLASS => 8,
        self::ACADEMIC_MOBILITY_TYPE__ONLINE      => 9,
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="trans_key", type="string", length=255)
     */
    private $transKey;

    public function __toString()
    {
        return $this->getTransKey();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set transKey
     *
     * @param string $transKey
     *
     * @return AcademicMobilityType
     */
    public function setTransKey($transKey)
    {
        $this->transKey = $transKey;

        return $this;
    }

    /**
     * Get transKey
     *
     * @return string
     */
    public function getTransKey()
    {
        return $this->transKey;
    }
}
