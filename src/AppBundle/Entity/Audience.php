<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="audience")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AudienceRepository")
 */
class Audience
{

    const AUDIENCE_TYPE__SCHOLAR     = 'scholar';
    const AUDIENCE_TYPE__BACHELORS   = 'bachelors';
    const AUDIENCE_TYPE__MASTERS     = 'masters';
    const AUDIENCE_TYPE__PHDS        = 'phds';
    const AUDIENCE_TYPE__TEACHERS    = 'teachers';
    const AUDIENCE_TYPE__SPECIALISTS = 'specialists';
    const AUDIENCE_TYPE__RESEARCHERS = 'researchers';

    const AUDIENCE_MAP = [
        Audience::AUDIENCE_TYPE__SCHOLAR     => 1,
        Audience::AUDIENCE_TYPE__BACHELORS   => 2,
        Audience::AUDIENCE_TYPE__MASTERS     => 3,
        Audience::AUDIENCE_TYPE__PHDS        => 4,
        Audience::AUDIENCE_TYPE__TEACHERS    => 5,
        Audience::AUDIENCE_TYPE__SPECIALISTS => 6,
        Audience::AUDIENCE_TYPE__RESEARCHERS => 7,
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", length=11)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="trans_key", type="string", length=255)
     */
    private $transKey;

    public function __toString()
    {
        return $this->getTransKey();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set transKey
     *
     * @param string $transKey
     *
     * @return Audience
     */
    public function setTransKey($transKey)
    {
        $this->transKey = $transKey;

        return $this;
    }

    /**
     * Get transKey
     *
     * @return string
     */
    public function getTransKey()
    {
        return $this->transKey;
    }

    public static function getIdByType($type)
    {
        return static::AUDIENCE_MAP[$type];
    }

    public static function getIdByTypes($types) {
        $result = [];

        if (!empty($types)) {
            array_walk($types, function ($type) use (&$result) {
                $result[] = static::getIdByType($type);
            });
        }

        return $result;
    }

}
