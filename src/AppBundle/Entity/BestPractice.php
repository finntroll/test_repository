<?php

namespace AppBundle\Entity;

use AppBundle\Entity\UploadedDocuments\HasUploadedDocumentsInterface;
use AppBundle\Entity\UploadedDocuments\HasUploadedDocumentsTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * BestPractice
 *
 * @ORM\Table(name="best_practice")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BestPracticeRepository")
 */
class BestPractice implements EntityInterface, HasImageInterface, TimestampableInterface, HasUploadedDocumentsInterface
{
    use ORMBehaviors\Translatable\Translatable;
    use Timestampable;
    use HasUploadedDocumentsTrait;
    
    const STATUS_DRAFT      = 1;
    const STATUS_MODERATING = 2;
    const STATUS_PUBLISHED  = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="smallint", nullable=false, options={"default": 1})
     */
    private $status = self::STATUS_DRAFT;

    /**
     * @var Scope
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Scope")
     */
    private $scope;

    /**
     * @var \AppBundle\Entity\University
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\University")
     */
    private $university;

    /**
     * @var \AppBundle\Entity\Image
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Image", cascade={"persist", "refresh", "remove"})
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $image;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->uploadedDocuments = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return BestPractice
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set scope
     *
     * @param \AppBundle\Entity\Scope $scope
     *
     * @return BestPractice
     */
    public function setScope(\AppBundle\Entity\Scope $scope = null)
    {
        $this->scope = $scope;

        return $this;
    }

    /**
     * Get scope
     *
     * @return \AppBundle\Entity\Scope
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @return \AppBundle\Entity\University
     */
    public function getUniversity()
    {
        return $this->university;
    }

    /**
     * @param \AppBundle\Entity\University $university
     */
    public function setUniversity(\AppBundle\Entity\University $university = null)
    {
        $this->university = $university;
    }

    /**
     * @return Image|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return '/images/' . $this->getImage()->getFileName();
    }

    public function getImageFolder()
    {
        return 'practice';
    }

    /**
     * @param Image|null $image
     */
    public function setImage(Image $image = null)
    {
        $this->image = $image;
    }
    
    public function isDraft()
    {
        return $this->getStatus() === self::STATUS_DRAFT;
    }

    public function isModerating()
    {
        return $this->getStatus() === self::STATUS_MODERATING;
    }

    public function isPublished()
    {
        return $this->getStatus() === self::STATUS_PUBLISHED;
    }

    public function hasTranslation($locale)
    {
        return $this->translate($locale, false)->getId() !== null;
    }
}
