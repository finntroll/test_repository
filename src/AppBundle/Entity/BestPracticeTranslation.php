<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * BestPracticeTranslation
 *
 * @ORM\Table(name="best_practice_translation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BestPracticeTranslationRepository")
 */
class BestPracticeTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=155)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", length=8000)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="lead_text", type="text", length=400)
     */
    private $leadText;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return BestPracticeTranslation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return BestPracticeTranslation
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getLeadText()
    {
        return $this->leadText;
    }

    /**
     * @param string $leadText
     */
    public function setLeadText($leadText)
    {
        $this->leadText = $leadText;
    }
}
