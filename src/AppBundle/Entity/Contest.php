<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Contest
 *
 * @ORM\Table(name="contest")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContestRepository")
 */
class Contest implements EntityInterface, TimestampableInterface
{
    use ORMBehaviors\Translatable\Translatable;
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="date_begin", type="datetime", nullable=true)
     */
    private $dateBegin;

    /**
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    private $dateEnd;

    /**
     * @ORM\Column(name="date_reception_end", type="datetime", nullable=true)
     */
    private $dateReceptionEnd;

    /**
     * @ORM\Column(name="is_published", type="boolean")
     */
    private $isPublished;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ContestType")
     */
    private $contestType;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\City")
     */
    private $city;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Language")
     */
    private $languages;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\University")
     */
    private $university;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\KnowledgeAreaItem")
     */
    private $knowledgeAreaItems;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Audience")
     */
    private $audiences;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->knowledgeAreaItems = new \Doctrine\Common\Collections\ArrayCollection();
        $this->audiences = new \Doctrine\Common\Collections\ArrayCollection();
        $this->languages = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function __toString()
    {
        return $this->translate()->getTitle();
    }

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateBegin
     * @param \DateTime $dateBegin
     * @return Contest
     */
    public function setDateBegin($dateBegin)
    {
        $this->dateBegin = $dateBegin;

        return $this;
    }

    /**
     * Get dateBegin
     * @return \DateTime
     */
    public function getDateBegin()
    {
        return $this->dateBegin;
    }

    /**
     * Set dateEnd
     * @param \DateTime $dateEnd
     * @return Contest
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set dateReceptionEnd
     * @param \DateTime $dateReceptionEnd
     * @return Contest
     */
    public function setDateReceptionEnd($dateReceptionEnd)
    {
        $this->dateReceptionEnd = $dateReceptionEnd;

        return $this;
    }

    /**
     * Get dateReceptionEnd
     * @return \DateTime
     */
    public function getDateReceptionEnd()
    {
        return $this->dateReceptionEnd;
    }

    /**
     * Set isPublished
     * @param boolean $isPublished
     * @return Contest
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * Set university
     * @param \AppBundle\Entity\University $university
     * @return Contest
     */
    public function setUniversity(\AppBundle\Entity\University $university = null)
    {
        $this->university = $university;

        return $this;
    }

    /**
     * Get university
     * @return \AppBundle\Entity\University
     */
    public function getUniversity()
    {
        return $this->university;
    }

    /**
     * Add knowledgeAreaItem
     * @param \AppBundle\Entity\KnowledgeAreaItem $knowledgeAreaItem
     * @return Contest
     */
    public function addKnowledgeAreaItem(\AppBundle\Entity\KnowledgeAreaItem $knowledgeAreaItem)
    {
        $this->knowledgeAreaItems[] = $knowledgeAreaItem;

        return $this;
    }

    /**
     * Remove knowledgeAreaItem
     * @param \AppBundle\Entity\KnowledgeAreaItem $knowledgeAreaItem
     */
    public function removeKnowledgeAreaItem(\AppBundle\Entity\KnowledgeAreaItem $knowledgeAreaItem)
    {
        $this->knowledgeAreaItems->removeElement($knowledgeAreaItem);
    }

    /**
     * Get knowledgeAreaItem
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKnowledgeAreaItems()
    {
        return $this->knowledgeAreaItems;
    }

    /**
     * Add audience
     * @param \AppBundle\Entity\Audience $audience
     * @return Contest
     */
    public function addAudience(\AppBundle\Entity\Audience $audience)
    {
        $this->audiences[] = $audience;

        return $this;
    }

    /**
     * Remove audience
     * @param \AppBundle\Entity\Audience $audience
     */
    public function removeAudience(\AppBundle\Entity\Audience $audience)
    {
        $this->audiences->removeElement($audience);
    }

    /**
     * Get audience
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAudiences()
    {
        return $this->audiences;
    }

    /**
     * Set contestType
     * @param string $contestType
     * @return Contest
     */
    public function setContestType($contestType)
    {
        $this->contestType = $contestType;

        return $this;
    }

    /**
     * Get contestType
     * @return string
     */
    public function getContestType()
    {
        return $this->contestType;
    }

    /**
     * Set city
     * @param \AppBundle\Entity\City $city
     * @return Contest
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Add language
     *
     * @param \AppBundle\Entity\Language $language
     *
     * @return Contest
     */
    public function addLanguage(\AppBundle\Entity\Language $language)
    {
        $this->languages[] = $language;

        return $this;
    }

    /**
     * Remove language
     *
     * @param \AppBundle\Entity\Language $language
     */
    public function removeLanguage(\AppBundle\Entity\Language $language)
    {
        $this->languages->removeElement($language);
    }

    /**
     * Get languages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLanguages()
    {
        return $this->languages;
    }
}
