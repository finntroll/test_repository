<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectType
 *
 * @ORM\Table(name="contest_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContestTypeRepository")
 */
class ContestType
{

    const CONTEST_NAME__GRANTS = 'grants';
    const CONTEST_NAME__INTERNATIONAL = 'international';
    const CONTEST_NAME__NIOKR = 'niokr';

    const CONTEST_TYPE__GRANTS        = 'contest_type.contest_grants';
    const CONTEST_TYPE__INTERNATIONAL = 'contest_type.contest_international';
    const CONTEST_TYPE__NIOKR         = 'contest_type.contest_niokr';

    const CONTEST_TYPE_MAP = [
        self::CONTEST_TYPE__GRANTS        => 1,
        self::CONTEST_TYPE__INTERNATIONAL => 2,
        self::CONTEST_TYPE__NIOKR         => 3,
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="transKey", type="string", length=255, unique=true)
     */
    private $transKey;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->getTransKey();
    }

    /**
     * Set transKey
     *
     * @param string $transKey
     *
     * @return ContestType
     */
    public function setTransKey($transKey)
    {
        $this->transKey = $transKey;

        return $this;
    }

    /**
     * Get transKey
     *
     * @return string
     */
    public function getTransKey()
    {
        return $this->transKey;
    }
}
