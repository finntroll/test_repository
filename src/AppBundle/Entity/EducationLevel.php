<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EducationLevel
 *
 * @ORM\Table(name="education_level")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EducationLevelRepository")
 */
class EducationLevel implements EntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="trans_key", type="string", length=255, unique=true)
     */
    private $transKey;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\EntranceTest", inversedBy="educationLevels")
     */
    private $entranceTests;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->entranceTests = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->getTransKey();
    }

    /**
     * Set transKey
     *
     * @param string $transKey
     *
     * @return EducationLevel
     */
    public function setTransKey($transKey)
    {
        $this->transKey = $transKey;

        return $this;
    }

    /**
     * Get transKey
     *
     * @return string
     */
    public function getTransKey()
    {
        return $this->transKey;
    }

    /**
     * Add entranceTest
     *
     * @param \AppBundle\Entity\EntranceTest $entranceTest
     *
     * @return EducationLevel
     */
    public function addEntranceTest(\AppBundle\Entity\EntranceTest $entranceTest)
    {
        $this->entranceTests[] = $entranceTest;

        return $this;
    }

    /**
     * Remove entranceTest
     *
     * @param \AppBundle\Entity\EntranceTest $entranceTest
     */
    public function removeEntranceTest(\AppBundle\Entity\EntranceTest $entranceTest)
    {
        $this->entranceTests->removeElement($entranceTest);
    }

    /**
     * Get entranceTests
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getEntranceTests()
    {
        return $this->entranceTests;
    }
}
