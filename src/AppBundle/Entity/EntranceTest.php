<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntranceTest
 *
 * @ORM\Table(name="entrance_test")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EntranceTestRepository")
 */
class EntranceTest implements EntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="trans_key", type="string", length=255)
     */
    private $transKey;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\EducationLevel", mappedBy="entranceTests")
     */
    private $educationLevels;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTransKey();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set transKey
     *
     * @param string $transKey
     *
     * @return EntranceTest
     */
    public function setTransKey($transKey)
    {
        $this->transKey = $transKey;

        return $this;
    }

    /**
     * Get transKey
     *
     * @return string
     */
    public function getTransKey()
    {
        return $this->transKey;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->educationLevels = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add educationLevel
     *
     * @param \AppBundle\Entity\EducationLevel $educationLevel
     *
     * @return EntranceTest
     */
    public function addEducationLevel(\AppBundle\Entity\EducationLevel $educationLevel)
    {
        $this->educationLevels[] = $educationLevel;

        return $this;
    }

    /**
     * Remove educationLevel
     *
     * @param \AppBundle\Entity\EducationLevel $educationLevel
     */
    public function removeEducationLevel(\AppBundle\Entity\EducationLevel $educationLevel)
    {
        $this->educationLevels->removeElement($educationLevel);
    }

    /**
     * Get educationLevels
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEducationLevels()
    {
        return $this->educationLevels;
    }
}
