<?php

namespace AppBundle\Entity;

interface HasImageInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return Image
     */
    public function getImage();

    /**
     * @return string
     */
    public function getImagePath();

    /**
     * @return string
     */
    public function getImageFolder();
}
