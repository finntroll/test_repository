<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * KnowledgeArea
 *
 * @ORM\Table(name="knowledge_area")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\KnowledgeAreaRepository")
 */
class KnowledgeArea implements EntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="trans_key", type="string", length=100, unique=true)
     */
    private $transKey;

    /**
     * @ORM\OneToMany(targetEntity="KnowledgeAreaItem", mappedBy="area")
     */
    private $knowledgeAreaItems;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->knowledgeAreaItems = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->getTransKey();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set transKey
     *
     * @param string $transKey
     *
     * @return KnowledgeArea
     */
    public function setTransKey($transKey)
    {
        $this->transKey = $transKey;

        return $this;
    }

    /**
     * Get transKey
     *
     * @return string
     */
    public function getTransKey()
    {
        return $this->transKey;
    }

    /**
     * Add knowledgeAreaItem
     *
     * @param \AppBundle\Entity\KnowledgeAreaItem $knowledgeAreaItem
     *
     * @return KnowledgeArea
     */
    public function addKnowledgeAreaItem(\AppBundle\Entity\KnowledgeAreaItem $knowledgeAreaItem)
    {
        $this->knowledgeAreaItems[] = $knowledgeAreaItem;

        return $this;
    }

    /**
     * Remove knowledgeAreaItem
     *
     * @param \AppBundle\Entity\KnowledgeAreaItem $knowledgeAreaItem
     */
    public function removeKnowledgeAreaItem(\AppBundle\Entity\KnowledgeAreaItem $knowledgeAreaItem)
    {
        $this->knowledgeAreaItems->removeElement($knowledgeAreaItem);
    }

    /**
     * Get knowledgeAreaItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKnowledgeAreaItems()
    {
        return $this->knowledgeAreaItems;
    }
}
