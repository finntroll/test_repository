<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * KnowledgeAreaItem
 *
 * @ORM\Table(name="knowledge_area_item")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\KnowledgeAreaItemRepository")
 */
class KnowledgeAreaItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="trans_key", type="string")
     */
    private $transKey;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\KnowledgeArea", inversedBy="knowledgeAreaItems")
     */
    private $area;

    public function __toString()
    {
        return $this->getTransKey();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set area
     *
     * @param \AppBundle\Entity\KnowledgeArea $area
     *
     * @return KnowledgeAreaItem
     */
    public function setArea(\AppBundle\Entity\KnowledgeArea $area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return \AppBundle\Entity\KnowledgeArea
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set transKey
     *
     * @param string $transKey
     *
     * @return KnowledgeAreaItem
     */
    public function setTransKey($transKey)
    {
        $this->transKey = $transKey;

        return $this;
    }

    /**
     * Get transKey
     *
     * @return string
     */
    public function getTransKey()
    {
        return $this->transKey;
    }
}
