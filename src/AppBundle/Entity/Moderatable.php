<?php

namespace AppBundle\Entity;

trait Moderatable
{
    public function isDraft()
    {
        return $this->status === static::STATUS_DRAFT;
    }

    public function isModerating()
    {
        return $this->status === static::STATUS_MODERATING;
    }

    public function isPublished()
    {
        return $this->status === static::STATUS_PUBLISHED;
    }
}
