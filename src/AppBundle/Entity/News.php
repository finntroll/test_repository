<?php

namespace AppBundle\Entity;

use AppBundle\Entity\UploadedDocuments\HasUploadedDocumentsInterface;
use AppBundle\Entity\UploadedDocuments\HasUploadedDocumentsTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * News
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NewsRepository")
 */
class News implements EntityInterface, HasImageInterface, TimestampableInterface, HasUploadedDocumentsInterface
{
    use ORMBehaviors\Translatable\Translatable;
    use Moderatable;
    use Timestampable;
    use HasUploadedDocumentsTrait;

    CONST STATUS_DRAFT = 1;
    CONST STATUS_MODERATING = 2;
    CONST STATUS_PUBLISHED = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var NewsType
     * @ORM\ManyToOne(targetEntity="NewsType")
     */
    private $type;

    /**
     * @var Rubric
     * @ORM\ManyToOne(targetEntity="Rubric")
     */
    private $rubric;

    /**
     * @ORM\ManyToMany(targetEntity="Keyword", cascade={"persist"})
     */
    private $keywords;

    /**
     * @var int
     * @ORM\Column(name="status", type="integer", nullable=false, options={"default" = 1})
     */
    private $status;

    /**
     * @var University
     * @ORM\ManyToOne(targetEntity="University")
     */
    private $university;

    /**
     * @var \DateTime
     * @ORM\Column(name="published_at", type="datetime", nullable=true)
     */
    private $publishedAt;

    /**
     * @var bool
     * @ORM\Column(name="is_important", type="boolean", nullable=false, options={"default" = false})
     */
    private $isImportant;

    /**
     * @var \AppBundle\Entity\Image
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Image", cascade={"persist", "refresh", "remove"})
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $image;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->keywords = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->uploadedDocuments = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Set publishedAt
     *
     * @param \DateTime $publishedAt
     *
     * @return News
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Add keyword
     *
     * @param \AppBundle\Entity\Keyword $keyword
     *
     * @return News
     */
    public function addKeyword(\AppBundle\Entity\Keyword $keyword)
    {
        $this->keywords[] = $keyword;

        return $this;
    }

    /**
     * Remove keyword
     *
     * @param \AppBundle\Entity\Keyword $keyword
     */
    public function removeKeyword(\AppBundle\Entity\Keyword $keyword)
    {
        $this->keywords->removeElement($keyword);
    }

    /**
     * Get keywords
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set isImportant
     *
     * @param boolean $isImportant
     *
     * @return News
     */
    public function setIsImportant($isImportant)
    {
        $this->isImportant = $isImportant;

        return $this;
    }

    /**
     * Get isImportant
     *
     * @return boolean
     */
    public function getIsImportant()
    {
        return $this->isImportant;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\NewsType $type
     *
     * @return News
     */
    public function setType(\AppBundle\Entity\NewsType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\NewsType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set rubric
     *
     * @param \AppBundle\Entity\Rubric $rubric
     *
     * @return News
     */
    public function setRubric(\AppBundle\Entity\Rubric $rubric = null)
    {
        $this->rubric = $rubric;

        return $this;
    }

    /**
     * Get rubric
     *
     * @return \AppBundle\Entity\Rubric
     */
    public function getRubric()
    {
        return $this->rubric;
    }

    /**
     * Set university
     *
     * @param \AppBundle\Entity\University $university
     *
     * @return News
     */
    public function setUniversity(\AppBundle\Entity\University $university = null)
    {
        $this->university = $university;

        return $this;
    }

    /**
     * Get university
     *
     * @return \AppBundle\Entity\University
     */
    public function getUniversity()
    {
        return $this->university;
    }

    /**
     * Set image
     *
     * @param \AppBundle\Entity\Image $image
     *
     * @return News
     */
    public function setImage(\AppBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \AppBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return '/images/'.$this->image->getFileName();
    }

    /**
     * @return string
     */
    public function getImageFolder()
    {
        return 'news';
    }
}
