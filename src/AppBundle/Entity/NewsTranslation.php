<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * NewsTranslation
 *
 * @ORM\Table(name="news_translation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NewsTranslationRepository")
 */
class NewsTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=155)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="lead", type="text", length=400)
     */
    private $lead;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=8000)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="videoCode", type="text", length=2000, nullable=true)
     */
    private $videoCode;

    /**
     * @var string
     *
     * @ORM\Column(name="sourceLink", type="string", length=2000, nullable=true)
     */
    private $sourceLink;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return NewsTranslation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set lead
     *
     * @param string $lead
     *
     * @return NewsTranslation
     */
    public function setLead($lead)
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * Get lead
     *
     * @return string
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return NewsTranslation
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set videoCode
     *
     * @param string $videoCode
     *
     * @return NewsTranslation
     */
    public function setVideoCode($videoCode)
    {
        $this->videoCode = $videoCode;

        return $this;
    }

    /**
     * Get videoCode
     *
     * @return string
     */
    public function getVideoCode()
    {
        return $this->videoCode;
    }

    /**
     * Set sourceLink
     *
     * @param string $sourceLink
     *
     * @return NewsTranslation
     */
    public function setSourceLink($sourceLink)
    {
        $this->sourceLink = $sourceLink;

        return $this;
    }

    /**
     * Get sourceLink
     *
     * @return string
     */
    public function getSourceLink()
    {
        return $this->sourceLink;
    }
}
