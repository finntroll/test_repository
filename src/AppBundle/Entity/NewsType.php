<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NewsType
 *
 * @ORM\Table(name="news_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NewsTypeRepository")
 */
class NewsType
{
    const UNIVERSITY_NEWS = 1;
    const ASSOCIATION_NEWS = 2;
    const MASS_MEDIA = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="trans_key", type="string", length=255, unique=true)
     */
    private $transKey;

    public function __toString()
    {
        return $this->getTransKey();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set transKey
     *
     * @param string $transKey
     *
     * @return NewsType
     */
    public function setTransKey($transKey)
    {
        $this->transKey = $transKey;

        return $this;
    }

    /**
     * Get transKey
     *
     * @return string
     */
    public function getTransKey()
    {
        return $this->transKey;
    }
}
