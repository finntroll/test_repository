<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Partner
 *
 * @ORM\Table(name="partner")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PartnerRepository")
 */
class Partner implements EntityInterface, TimestampableInterface, HasImageInterface
{
    use ORMBehaviors\Translatable\Translatable;
    use Moderatable;
    use Timestampable;

    const STATUS_DRAFT      = 1;
    const STATUS_MODERATING = 2;
    const STATUS_PUBLISHED  = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Image
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Image", cascade={"persist", "refresh", "remove"})
     * @ORM\JoinColumn(name="logo_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $logo;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_foreign", type="boolean")
     */
    private $isForeign;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Country")
     */
    private $country;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\University", mappedBy="partners")
     */
    private $universities;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="smallint", nullable=false, options={"default": 1})
     */
    private $status = self::STATUS_DRAFT;

    public function __toString()
    {
        return $this->translate()->getName().' ('.$this->translate()->getShortName().', '.$this->getCountry()->translate()->getName().')';
    }

    public function getName()
    {
        return $this->translate()->getName();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set logo
     *
     * @param \AppBundle\Entity\Image $logo
     *
     * @return Partner
     */
    public function setLogo(\AppBundle\Entity\Image $logo = null)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return \AppBundle\Entity\Image
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->universities = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    /**
     * Add university
     *
     * @param \AppBundle\Entity\University $university
     *
     * @return Partner
     */
    public function addUniversity(\AppBundle\Entity\University $university)
    {
        $this->universities[] = $university;

        return $this;
    }

    /**
     * Remove university
     *
     * @param \AppBundle\Entity\University $university
     */
    public function removeUniversity(\AppBundle\Entity\University $university)
    {
        $this->universities->removeElement($university);
    }

    /**
     * Get universities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUniversities()
    {
        return $this->universities;
    }

    /**
     * Set isForeign
     *
     * @param boolean $isForeign
     *
     * @return Partner
     */
    public function setIsForeign($isForeign)
    {
        $this->isForeign = $isForeign;

        return $this;
    }

    /**
     * Get isForeign
     *
     * @return boolean
     */
    public function isForeign()
    {
        return $this->isForeign;
    }

    /**
     * Get isForeign
     *
     * @return boolean
     */
    public function getIsForeign()
    {
        return $this->isForeign;
    }

    /**
     * Set country
     *
     * @param \AppBundle\Entity\Country $country
     *
     * @return Partner
     */
    public function setCountry(\AppBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \AppBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Partner
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getImage()
    {
        return $this->getLogo();
    }

    public function setImage(Image $image = null)
    {
        $this->setLogo($image);
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return '/images/' . $this->getImage()->getFileName();
    }

    /**
     * @return string
     */
    public function getImageFolder()
    {
        return 'partner';
    }
}
