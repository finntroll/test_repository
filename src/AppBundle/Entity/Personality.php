<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonalityRepository")
 */
class Personality extends User implements HasImageInterface, EntityInterface
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Image", cascade={"persist", "refresh", "remove"})
     * @ORM\JoinColumn(name="photo_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $photo;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AssociationPosition")
     */
    private $associationPosition;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $phone;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Partner")
     */
    private $workPlace;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $registrationToken;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $link;

    /**
     * @return Image|null
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param Image $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    /**
     * @return AssociationPosition|null
     */
    public function getAssociationPosition()
    {
        return $this->associationPosition;
    }

    /**
     * @param AssociationPosition $associationPosition
     */
    public function setAssociationPosition($associationPosition)
    {
        $this->associationPosition = $associationPosition;
    }

    /**
     * @return string
     */
    public function getProfessionalPosition($locale = null)
    {
        return $this->translate($locale)->getProfessionalPosition();
    }

    /**
     * @return Image
     */
    public function getImage()
    {
        return $this->getPhoto();
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->getImage() ? '/images/' . $this->getImage()->getFileName() : null;
    }

    /**
     * @return string
     */
    public function getImageFolder()
    {
        return 'personality';
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return Partner
     */
    public function getWorkPlace()
    {
        return $this->workPlace;
    }

    /**
     * @param Partner $workPlace
     */
    public function setWorkPlace($workPlace)
    {
        $this->workPlace = $workPlace;
    }

    /**
     * @return string
     */
    public function getRegistrationToken()
    {
        return $this->registrationToken;
    }

    /**
     * @param string $registrationToken
     */
    public function setRegistrationToken($registrationToken)
    {
        $this->registrationToken = $registrationToken;
    }

    /**
     * @return string
     */
    public function getFullNameWithWorkPlace()
    {
        return $this->getWorkPlace() ? $this->getWorkPlace()->translate()->getShortName().
            ', '.
            $this->getFullName() : $this->getFullName();
    }

    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }
}
