<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class PersonalityTranslation extends UserTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $professionalPosition;

    /**
     * @return string
     */
    public function getProfessionalPosition()
    {
        return $this->professionalPosition;
    }

    /**
     * @param string $professionalPosition
     */
    public function setProfessionalPosition($professionalPosition)
    {
        $this->professionalPosition = $professionalPosition;
    }
}
