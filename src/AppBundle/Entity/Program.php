<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Program
 *
 * @ORM\Table(name="program")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProgramRepository")
 */
class Program implements EntityInterface, TimestampableInterface
{
    use ORMBehaviors\Translatable\Translatable;
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\University")
     */
    private $university;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\UniversityBranch")
     */
    private $universityBranch;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Partner")
     */
    private $partners;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Language")
     */
    private $languages;

    /**
     * @var int
     *
     * @ORM\Column(name="budget_places", type="integer", nullable=true)
     */
    private $budgetPlaces;

    /**
     * @var int
     *
     * @ORM\Column(name="entrance_score", type="integer", nullable=true)
     */
    private $entranceScore;

    /**
     * @var int
     *
     * @ORM\Column(name="contract_places", type="integer", nullable=true)
     */
    private $contractPlaces;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="foreign_price", type="float", nullable=true)
     */
    private $foreignPrice;

    /**
     * @var int
     *
     * @ORM\Column(name="foreign_places", type="integer", nullable=true)
     */
    private $foreignPlaces;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EducationForm")
     */
    private $educationForm;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EducationLevel")
     */
    private $educationLevel;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\EntranceTest")
     */
    private $entranceTests;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\KnowledgeArea")
     */
    private $knowledgeArea;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\KnowledgeAreaItem")
     */
    private $knowledgeAreaItem;

    /**
     * @var bool
     *
     * @ORM\Column(name="military_department", type="boolean")
     */
    private $hasMilitaryDepartment;

    /**
     * @var bool
     *
     * @ORM\Column(name="exchange_study", type="boolean")
     */
    private $exchangeStudy;

    /**
     * @var bool
     *
     * @ORM\Column(name="double_diploma", type="boolean")
     */
    private $doubleDiploma;

    /**
     * @var int
     *
     * @ORM\Column(name="duration", type="integer")
     */
    private $duration;

    /**
     * @ORM\Column(name="is_published", type="boolean")
     */
    private $isPublished;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->entranceTests = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->partners = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Program
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set budgetPlaces
     *
     * @param integer $budgetPlaces
     *
     * @return Program
     */
    public function setBudgetPlaces($budgetPlaces)
    {
        $this->budgetPlaces = $budgetPlaces;

        return $this;
    }

    /**
     * Get budgetPlaces
     *
     * @return integer
     */
    public function getBudgetPlaces()
    {
        return $this->budgetPlaces;
    }

    /**
     * Set contractPlaces
     *
     * @param integer $contractPlaces
     *
     * @return Program
     */
    public function setContractPlaces($contractPlaces)
    {
        $this->contractPlaces = $contractPlaces;

        return $this;
    }

    /**
     * Get contractPlaces
     *
     * @return integer
     */
    public function getContractPlaces()
    {
        return $this->contractPlaces;
    }

    /**
     * Set educationForm
     *
     * @param \AppBundle\Entity\EducationForm $educationForm
     *
     * @return Program
     */
    public function setEducationForm(\AppBundle\Entity\EducationForm $educationForm = null)
    {
        $this->educationForm = $educationForm;

        return $this;
    }

    /**
     * Get educationForm
     *
     * @return \AppBundle\Entity\EducationForm
     */
    public function getEducationForm()
    {
        return $this->educationForm;
    }

    /**
     * Add entranceTest
     *
     * @param \AppBundle\Entity\EntranceTest $entranceTest
     *
     * @return Program
     */
    public function addEntranceTest(\AppBundle\Entity\EntranceTest $entranceTest)
    {
        $this->entranceTests[] = $entranceTest;

        return $this;
    }

    /**
     * Remove entranceTest
     *
     * @param \AppBundle\Entity\EntranceTest $entranceTest
     */
    public function removeEntranceTest(\AppBundle\Entity\EntranceTest $entranceTest)
    {
        $this->entranceTests->removeElement($entranceTest);
    }

    /**
     * Get entranceTests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntranceTests()
    {
        return $this->entranceTests;
    }

    /**
     * Set knowledgeArea
     *
     * @param \AppBundle\Entity\KnowledgeArea $knowledgeArea
     *
     * @return Program
     */
    public function setKnowledgeArea(\AppBundle\Entity\KnowledgeArea $knowledgeArea = null)
    {
        $this->knowledgeArea = $knowledgeArea;

        return $this;
    }

    /**
     * Get knowledgeArea
     *
     * @return \AppBundle\Entity\KnowledgeArea
     */
    public function getKnowledgeArea()
    {
        return $this->knowledgeArea;
    }

    /**
     * Add language
     *
     * @param \AppBundle\Entity\Language $language
     *
     * @return Program
     */
    public function addLanguage(\AppBundle\Entity\Language $language)
    {
        $this->languages[] = $language;

        return $this;
    }

    /**
     * Remove language
     *
     * @param \AppBundle\Entity\Language $language
     */
    public function removeLanguage(\AppBundle\Entity\Language $language)
    {
        $this->languages->removeElement($language);
    }

    /**
     * Get languages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * Set hasMilitaryDepartment
     *
     * @param boolean $hasMilitaryDepartment
     *
     * @return Program
     */
    public function setHasMilitaryDepartment($hasMilitaryDepartment)
    {
        $this->hasMilitaryDepartment = $hasMilitaryDepartment;

        return $this;
    }

    /**
     * Get hasMilitaryDepartment
     *
     * @return boolean
     */
    public function getHasMilitaryDepartment()
    {
        return $this->hasMilitaryDepartment;
    }

    /**
     * Set universityBranch
     *
     * @param \AppBundle\Entity\UniversityBranch $universityBranch
     *
     * @return Program
     */
    public function setUniversityBranch(\AppBundle\Entity\UniversityBranch $universityBranch = null)
    {
        $this->universityBranch = $universityBranch;

        return $this;
    }

    /**
     * Get universityBranch
     *
     * @return \AppBundle\Entity\UniversityBranch
     */
    public function getUniversityBranch()
    {
        return $this->universityBranch;
    }

    /**
     * Set educationLevel
     *
     * @param \AppBundle\Entity\EducationLevel $educationLevel
     *
     * @return Program
     */
    public function setEducationLevel(\AppBundle\Entity\EducationLevel $educationLevel = null)
    {
        $this->educationLevel = $educationLevel;

        return $this;
    }

    /**
     * Get educationLevel
     *
     * @return \AppBundle\Entity\EducationLevel
     */
    public function getEducationLevel()
    {
        return $this->educationLevel;
    }

    /**
     * Set knowledgeAreaItem
     *
     * @param \AppBundle\Entity\KnowledgeAreaItem $knowledgeAreaItem
     *
     * @return Program
     */
    public function setKnowledgeAreaItem(\AppBundle\Entity\KnowledgeAreaItem $knowledgeAreaItem = null)
    {
        $this->knowledgeAreaItem = $knowledgeAreaItem;

        return $this;
    }

    /**
     * Get knowledgeAreaItem
     *
     * @return \AppBundle\Entity\KnowledgeAreaItem
     */
    public function getKnowledgeAreaItem()
    {
        return $this->knowledgeAreaItem;
    }

    /**
     * Set isPublished
     *
     * @param boolean $isPublished
     *
     * @return Program
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished
     *
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * Set exchangeStudy
     *
     * @param boolean $exchangeStudy
     *
     * @return Program
     */
    public function setExchangeStudy($exchangeStudy)
    {
        $this->exchangeStudy = $exchangeStudy;

        return $this;
    }

    /**
     * Get exchangeStudy
     *
     * @return boolean
     */
    public function getExchangeStudy()
    {
        return $this->exchangeStudy;
    }

    /**
     * Set entranceScore
     *
     * @param integer $entranceScore
     *
     * @return Program
     */
    public function setEntranceScore($entranceScore)
    {
        $this->entranceScore = $entranceScore;

        return $this;
    }

    /**
     * Get entranceScore
     *
     * @return integer
     */
    public function getEntranceScore()
    {
        return $this->entranceScore;
    }

    /**
     * Set doubleDiploma
     *
     * @param boolean $doubleDiploma
     *
     * @return Program
     */
    public function setDoubleDiploma($doubleDiploma)
    {
        $this->doubleDiploma = $doubleDiploma;

        return $this;
    }

    /**
     * Get doubleDiploma
     *
     * @return boolean
     */
    public function getDoubleDiploma()
    {
        return $this->doubleDiploma;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     *
     * @return Program
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Add partner
     *
     * @param \AppBundle\Entity\Partner $partner
     *
     * @return Program
     */
    public function addPartner(\AppBundle\Entity\Partner $partner)
    {
        $this->partners[] = $partner;

        return $this;
    }

    /**
     * Remove partner
     *
     * @param \AppBundle\Entity\Partner $partner
     */
    public function removePartner(\AppBundle\Entity\Partner $partner)
    {
        $this->partners->removeElement($partner);
    }

    /**
     * Get partners
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPartners()
    {
        return $this->partners;
    }

    /**
     * Set university
     *
     * @param \AppBundle\Entity\University $university
     *
     * @return Program
     */
    public function setUniversity(\AppBundle\Entity\University $university = null)
    {
        $this->university = $university;

        return $this;
    }

    /**
     * Get university
     *
     * @return \AppBundle\Entity\University
     */
    public function getUniversity()
    {
        return $this->university;
    }

    /**
     * Set foreignPrice
     *
     * @param float $foreignPrice
     *
     * @return Program
     */
    public function setForeignPrice($foreignPrice)
    {
        $this->foreignPrice = $foreignPrice;

        return $this;
    }

    /**
     * Get foreignPrice
     *
     * @return float
     */
    public function getForeignPrice()
    {
        return $this->foreignPrice;
    }

    /**
     * Set foreignPlaces
     *
     * @param integer $foreignPlaces
     *
     * @return Program
     */
    public function setForeignPlaces($foreignPlaces)
    {
        $this->foreignPlaces = $foreignPlaces;

        return $this;
    }

    /**
     * Get foreignPlaces
     *
     * @return integer
     */
    public function getForeignPlaces()
    {
        return $this->foreignPlaces;
    }
}
