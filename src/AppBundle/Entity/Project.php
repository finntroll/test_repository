<?php

namespace AppBundle\Entity;

use AppBundle\Entity\UploadedDocuments\HasUploadedDocumentsInterface;
use AppBundle\Entity\UploadedDocuments\HasUploadedDocumentsTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectRepository")
 */
class Project implements EntityInterface, TimestampableInterface, HasUploadedDocumentsInterface
{
    use ORMBehaviors\Translatable\Translatable;
    use Timestampable;
    use HasUploadedDocumentsTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Personality")
     */
    private $head;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Personality")
     */
    private $manager;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProjectMember", mappedBy="project", cascade={"persist", "refresh", "remove"}, orphanRemoval=true)
     */
    private $projectMembers;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ProjectPassport", cascade={"persist", "refresh", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $passport;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ProjectOutlay", cascade={"persist", "refresh", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $outlay;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\University")
     */
    private $university;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateStart;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateEnd;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPublished = false;

    /**
     * Project constructor.
     */
    public function __construct()
    {
        $this->projectMembers = new ArrayCollection();
        $this->uploadedDocuments = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set head
     *
     * @param Personality|null $head
     *
     * @return Project
     */
    public function setHead($head)
    {
        $this->head = $head;

        return $this;
    }

    /**
     * Get head
     *
     * @return string
     */
    public function getHead()
    {
        return $this->head;
    }

    /**
     * Set members
     *
     * @param array $projectMembers
     *
     * @return Project
     */
    public function setProjectMembers($projectMembers)
    {
        foreach ($projectMembers as $member) {
            if ($member instanceof ProjectMember) {
                $this->addProjectMember($member);
            }
        }

        return $this;
    }

    /**
     * Get members
     *
     * @return ArrayCollection
     */
    public function getProjectMembers()
    {
        return $this->projectMembers;
    }

    public function addProjectMember(ProjectMember $member)
    {
        if ($this->projectMembers->contains($member)) {
            return;
        }

        $member->setProject($this);
        $this->projectMembers->add($member);
    }

    public function removeProjectMember(ProjectMember $member)
    {
        $this->projectMembers->removeElement($member);
    }

    /**
     * @return Personality|null
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @param Personality|null $manager
     */
    public function setManager(Personality $manager = null)
    {
        $this->manager = $manager;
    }

    /**
     * @return ProjectPassport|null
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @param ProjectPassport $passport
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;
    }

    /**
     * @return ProjectOutlay|null
     */
    public function getOutlay()
    {
        return $this->outlay;
    }

    /**
     * @param ProjectOutlay $outlay
     */
    public function setOutlay($outlay)
    {
        $this->outlay = $outlay;
    }

    /**
     * @return University|null
     */
    public function getUniversity()
    {
        return $this->university;
    }

    /**
     * @param University $university
     */
    public function setUniversity($university)
    {
        $this->university = $university;
    }

    /**
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * @param \DateTime $dateStart
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;
    }

    /**
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @param \DateTime $dateEnd
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;
    }

    public function isOpen()
    {
        return $this->dateEnd === null || (new \DateTime()) <= $this->dateEnd;
    }

    /**
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * @param boolean $isPublished
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;
    }

    public function getArchive()
    {
        $archive = $this->getArchivedDocuments();
        $passportArchive = $this->getPassport() ? $this->getPassport()->getArchivedDocuments()->toArray() : [];
        $outlayArchive = $this->getOutlay() ? $this->getOutlay()->getArchivedDocuments()->toArray() : [];

        return new ArrayCollection(array_merge($archive->toArray(), $passportArchive, $outlayArchive));
    }

    public function hasTranslation($locale)
    {
        return $this->translate($locale, false)->getId() !== null;
    }
}
