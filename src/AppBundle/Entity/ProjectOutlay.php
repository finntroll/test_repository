<?php

namespace AppBundle\Entity;

use AppBundle\Entity\UploadedDocuments\HasUploadedDocumentsInterface;
use AppBundle\Entity\UploadedDocuments\HasUploadedDocumentsTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="project_outlay")
 */
class ProjectOutlay implements HasUploadedDocumentsInterface
{
    use HasUploadedDocumentsTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * ProjectOutlay constructor.
     */
    public function __construct()
    {
        $this->uploadedDocuments = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}
