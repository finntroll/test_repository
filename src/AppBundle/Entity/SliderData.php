<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SliderData
 *
 * @ORM\Table(name="slider_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SliderDataRepository")
 */
class SliderData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="students", type="integer")
     */
    private $students;

    /**
     * @var int
     *
     * @ORM\Column(name="staff", type="integer")
     */
    private $staff;

    /**
     * @var int
     *
     * @ORM\Column(name="foreign_students", type="integer")
     */
    private $foreignStudents;

    /**
     * @var int
     *
     * @ORM\Column(name="edu_programs", type="integer")
     */
    private $educationPrograms;

    /**
     * @var int
     *
     * @ORM\Column(name="edu_programs_foreign", type="integer")
     */
    private $eduProgramsForeign;

    /**
     * @var int
     *
     * @ORM\Column(name="international_laboratories", type="integer")
     */
    private $internationalLaboratories;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\University", mappedBy="sliderData")
     */
    private $university;

    /**
     * @ORM\Column(name="slide", type="string")
     *
     * @var string
     */
    private $slide;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set students
     *
     * @param integer $students
     *
     * @return SliderData
     */
    public function setStudents($students)
    {
        $this->students = $students;

        return $this;
    }

    /**
     * Get students
     *
     * @return integer
     */
    public function getStudents()
    {
        return $this->students;
    }

    /**
     * Set staff
     *
     * @param integer $staff
     *
     * @return SliderData
     */
    public function setStaff($staff)
    {
        $this->staff = $staff;

        return $this;
    }

    /**
     * Get staff
     *
     * @return integer
     */
    public function getStaff()
    {
        return $this->staff;
    }

    /**
     * Set foreignStudents
     *
     * @param integer $foreignStudents
     *
     * @return SliderData
     */
    public function setForeignStudents($foreignStudents)
    {
        $this->foreignStudents = $foreignStudents;

        return $this;
    }

    /**
     * Get foreignStudents
     *
     * @return integer
     */
    public function getForeignStudents()
    {
        return $this->foreignStudents;
    }

    /**
     * Set educationPrograms
     *
     * @param integer $educationPrograms
     *
     * @return SliderData
     */
    public function setEducationPrograms($educationPrograms)
    {
        $this->educationPrograms = $educationPrograms;

        return $this;
    }

    /**
     * Get educationPrograms
     *
     * @return integer
     */
    public function getEducationPrograms()
    {
        return $this->educationPrograms;
    }

    /**
     * Set eduProgramsForeign
     *
     * @param integer $eduProgramsForeign
     *
     * @return SliderData
     */
    public function setEduProgramsForeign($eduProgramsForeign)
    {
        $this->eduProgramsForeign = $eduProgramsForeign;

        return $this;
    }

    /**
     * Get eduProgramsForeign
     *
     * @return integer
     */
    public function getEduProgramsForeign()
    {
        return $this->eduProgramsForeign;
    }

    /**
     * Set internationalLaboratories
     *
     * @param integer $internationalLaboratories
     *
     * @return SliderData
     */
    public function setInternationalLaboratories($internationalLaboratories)
    {
        $this->internationalLaboratories = $internationalLaboratories;

        return $this;
    }

    /**
     * Get internationalLaboratories
     *
     * @return integer
     */
    public function getInternationalLaboratories()
    {
        return $this->internationalLaboratories;
    }

    /**
     * Set university
     *
     * @param \AppBundle\Entity\University $university
     *
     * @return SliderData
     */
    public function setUniversity(\AppBundle\Entity\University $university = null)
    {
        $this->university = $university;

        return $this;
    }

    /**
     * Get university
     *
     * @return \AppBundle\Entity\University
     */
    public function getUniversity()
    {
        return $this->university;
    }

    /**
     * Set slide
     *
     * @param string $slide
     *
     * @return SliderData
     */
    public function setSlide($slide)
    {
        $this->slide = $slide;

        return $this;
    }

    /**
     * Get slide
     *
     * @return string
     */
    public function getSlide()
    {
        return $this->slide;
    }
}
