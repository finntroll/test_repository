<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * StaticPageData
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StaticPageDataRepository")
 * @ORM\Table(name="static_page_data")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="page_type", type="string", length=20)
 * @ORM\DiscriminatorMap({
 *     "page" = "StaticPageData",
 *     "audience" = "AppBundle\Entity\StaticPageType\AudiencePage",
 *     "about_association" = "AppBundle\Entity\StaticPageType\AboutAssociationPage",
 *     "council" = "AppBundle\Entity\StaticPageType\CouncilPage"})
 */
class StaticPageData implements EntityInterface
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=25, unique=true)
     */
    private $tag;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tag
     *
     * @param string $tag
     *
     * @return StaticPageData
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }
}

