<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * StaticPageDataTranslation
 *
 * @ORM\Table(name="static_page_data_translation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StaticPageDataTranslationRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="page_type", type="string", length=20)
 * @ORM\DiscriminatorMap({
 *     "about_association" = "AppBundle\Entity\StaticPageType\AboutAssociationPageTranslation",
 *     "audience" = "AppBundle\Entity\StaticPageType\AudiencePageTranslation",
 *     "page" = "StaticPageDataTranslation",
 *     "council" = "AppBundle\Entity\StaticPageType\CouncilPageTranslation"})
 */
class StaticPageDataTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    protected $text;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return StaticPageDataTranslation
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
}

