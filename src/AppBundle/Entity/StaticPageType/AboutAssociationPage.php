<?php

namespace AppBundle\Entity\StaticPageType;

use AppBundle\Entity\StaticPageData;
use AppBundle\Entity\UploadedDocuments\HasUploadedDocumentsInterface;
use AppBundle\Entity\UploadedDocuments\HasUploadedDocumentsTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class AboutAssociationPage extends StaticPageData implements HasUploadedDocumentsInterface
{
    use HasUploadedDocumentsTrait;
    use ORMBehaviors\Translatable\Translatable;

    /**
     * AboutAssociationPage constructor.
     */
    public function __construct()
    {
        $this->uploadedDocuments = new ArrayCollection();
    }
}
