<?php

namespace AppBundle\Entity\StaticPageType;

use AppBundle\Entity\HasImageInterface;
use AppBundle\Entity\Image;
use AppBundle\Entity\StaticPageDataTranslation;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class AboutAssociationPageTranslation extends StaticPageDataTranslation implements HasImageInterface
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $leftBlock;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Image", cascade={"all"})
     * @ORM\JoinColumn(name="logo_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $logo;

    /**
     * @return string
     */
    public function getLeftBlock()
    {
        return $this->leftBlock;
    }

    /**
     * @param string $leftBlock
     */
    public function setLeftBlock($leftBlock)
    {
        $this->leftBlock = $leftBlock;
    }

    /**
     * @return Image|null
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param Image $logo
     */
    public function setLogo(Image $logo)
    {
        $this->logo = $logo;
    }

    /**
     * Get image
     *
     * @return Image|null
     */
    public function getImage()
    {
        return $this->getLogo();
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->getImage() instanceof Image ? '/images/'.$this->getImage()->getFileName() : null;
    }

    /**
     * @return string
     */
    public function getImageFolder()
    {
        return 'about_association_page';
    }
}
