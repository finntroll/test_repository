<?php

namespace AppBundle\Entity\StaticPageType;

use AppBundle\Entity\StaticPageDataTranslation;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class AudiencePageTranslation extends StaticPageDataTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @ORM\Column(type="json_array")
     */
    private $highlights;

    /**
     * @return array
     */
    public function getHighlights()
    {
        return $this->highlights;
    }

    /**
     * @param mixed array
     */
    public function setHighlights($highlights)
    {
        $this->highlights = $highlights;
    }
}
