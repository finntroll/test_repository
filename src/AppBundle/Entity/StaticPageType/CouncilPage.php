<?php

namespace AppBundle\Entity\StaticPageType;

use AppBundle\Entity\StaticPageData;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class CouncilPage extends StaticPageData
{
    use ORMBehaviors\Translatable\Translatable;
}
