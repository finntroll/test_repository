<?php

namespace AppBundle\Entity\StaticPageType;

use AppBundle\Entity\StaticPageDataTranslation;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class CouncilPageTranslation extends StaticPageDataTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @ORM\Column(type="text")
     */
    private $competenceOfChairman;

    /**
     * @ORM\Column(type="text")
     */
    private $competenceOfExecutive;

    /**
     * @ORM\Column(type="text")
     */
    private $honoraryMembersText;

    /**
     * @return string
     */
    public function getCompetenceOfChairman()
    {
        return $this->competenceOfChairman;
    }

    /**
     * @param string $competenceOfChairman
     */
    public function setCompetenceOfChairman($competenceOfChairman)
    {
        $this->competenceOfChairman = $competenceOfChairman;
    }

    /**
     * @return string
     */
    public function getCompetenceOfExecutive()
    {
        return $this->competenceOfExecutive;
    }

    /**
     * @param string $competenceOfExecutive
     */
    public function setCompetenceOfExecutive($competenceOfExecutive)
    {
        $this->competenceOfExecutive = $competenceOfExecutive;
    }

    /**
     * @return string
     */
    public function getHonoraryMembersText()
    {
        return $this->honoraryMembersText;
    }

    /**
     * @param string $honoraryMembersText
     */
    public function setHonoraryMembersText($honoraryMembersText)
    {
        $this->honoraryMembersText = $honoraryMembersText;
    }
}
