<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * News
 *
 * @ORM\Table(name="statistic", indexes={
 *     @ORM\Index(name="search_statistic_date", columns={"event_date"}),
 *     @ORM\Index(name="search_statistic_date_university", columns={"event_date", "university_id"}),
 *     @ORM\Index(name="search_statistic_university", columns={"university_id"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StatisticRepository")
 */
class Statistic implements EntityInterface
{

    /**
     * Constants is for user events
     * When user show page using internal
     * When user go to other site by redirect controller using external
     */
    const EVENT_INTERNAL = 'internal';
    const EVENT_EXTERNAL = 'external';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="session",type="string")
     */
    private $session;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="event_date", type="datetime")
     */
    private $eventDate;

    /**
     * @var string
     *
     * @ORM\Column(name="object", type="string")
     */
    private $object;

    /**
     * @var integer
     *
     * @ORM\Column(name="object_id", type="integer")
     */
    private $objectId;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string")
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="university_id", type="integer", nullable=true)
     */
    private $universityId;

    /**
     * Statistic constructor.
     *
     * Set event date
     */
    public function __construct()
    {
        $this->eventDate = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSession(): string
    {
        return $this->session;
    }

    /**
     * @param string $session
     * @return $this
     */
    public function setSession(string $session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEventDate(): \DateTime
    {
        return $this->eventDate;
    }

    /**
     * @param \DateTime $eventDate
     * @return $this
     */
    public function setEventDate(\DateTime $eventDate)
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getObject(): string
    {
        return $this->object;
    }

    /**
     * @param string $object
     * @return $this
     */
    public function setObject(string $object)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * @return int
     */
    public function getObjectId(): int
    {
        return $this->objectId;
    }

    /**
     * @param int $objectId
     * @return $this
     */
    public function setObjectId(int $objectId)
    {
        $this->objectId = $objectId;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getUniversityId(): int
    {
        return $this->universityId;
    }

    /**
     * @param int|null $universityId
     * @return $this
     */
    public function setUniversityId($universityId = null)
    {
        $this->universityId = $universityId;

        return $this;
    }
}
