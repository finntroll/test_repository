<?php

namespace AppBundle\Entity;

Interface TimestampableInterface
{
    public function setUpdatedAt($updatedAt);

    public function setCreatedAt($createdAt);

    public function getCreatedAt();

    public function getUpdatedAt();
}
