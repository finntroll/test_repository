<?php

namespace AppBundle\Entity;

use AppBundle\Entity\UploadedDocuments\HasUploadedDocumentsInterface;
use AppBundle\Entity\UploadedDocuments\HasUploadedDocumentsTrait;
use AppBundle\Extension\Doctrine\Point;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * University
 *
 * @ORM\Table(name="university")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UniversityRepository")
 */
class University implements EntityInterface, HasUploadedDocumentsInterface
{
    use ORMBehaviors\Translatable\Translatable;
    use HasUploadedDocumentsTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\UniversityBranch")
     */
    private $mainBranch;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UniversityBranch", mappedBy="university")
     */
    private $branches;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Partner", inversedBy="universities")
     */
    private $partners;

    /**
     * @var SliderData
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\SliderData", inversedBy="university", cascade={"persist"})
     */
    private $sliderData;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\SocialLinks", cascade={"persist"})
     */
    private $socialLinks;

    /**
     * @ORM\Column(type="point", nullable=true)
     *
     * @var Point
     */
    private $locationPoint;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->branches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->partners = new \Doctrine\Common\Collections\ArrayCollection();
        $this->uploadedDocuments = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->translate()->getName();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return University
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set mainBranch
     *
     * @param \AppBundle\Entity\UniversityBranch $mainBranch
     *
     * @return University
     */
    public function setMainBranch(\AppBundle\Entity\UniversityBranch $mainBranch = null)
    {
        $this->mainBranch = $mainBranch;

        return $this;
    }

    /**
     * Get mainBranch
     *
     * @return \AppBundle\Entity\UniversityBranch
     */
    public function getMainBranch()
    {
        return $this->mainBranch;
    }

    public function getSubBranches()
    {
        $mainBranch = $this->mainBranch;
        return $this->branches->filter(function ($branch) use ($mainBranch) {
            return $branch !== $mainBranch;
        });
    }

    public function addSubBranch(UniversityBranch $branch)
    {
        $this->addBranch($branch);

        return $this;
    }

    public function removeSubBranch(UniversityBranch $branch)
    {
        if (!$branch->isMainBranch()) {
            $this->removeBranch($branch);
        }

        return $this;
    }

    /**
     * Add branch
     *
     * @param \AppBundle\Entity\UniversityBranch $branch
     *
     * @return University
     */
    public function addBranch(\AppBundle\Entity\UniversityBranch $branch)
    {
        $this->branches[] = $branch;

        return $this;
    }

    /**
     * Remove branch
     *
     * @param \AppBundle\Entity\UniversityBranch $branch
     */
    public function removeBranch(\AppBundle\Entity\UniversityBranch $branch)
    {
        $this->branches->removeElement($branch);
    }

    /**
     * Get branches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBranches()
    {
        return $this->branches;
    }

    /**
     * Add partner
     *
     * @param \AppBundle\Entity\Partner $partner
     *
     * @return University
     */
    public function addPartner(\AppBundle\Entity\Partner $partner)
    {
        $this->partners[] = $partner;

        return $this;
    }

    /**
     * Remove partner
     *
     * @param \AppBundle\Entity\Partner $partner
     */
    public function removePartner(\AppBundle\Entity\Partner $partner)
    {
        $this->partners->removeElement($partner);
    }

    /**
     * Get partners
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPartners()
    {
        return $this->partners;
    }

    public function getRussianPartners()
    {
        return $this->partners->filter(function($entry) {
            return ! $entry->isForeign();
        });
    }

    public function getForeignPartners()
    {
        return $this->partners->filter(function($entry) {
            return $entry->isForeign();
        });
    }

    /**
     * Set sliderData
     *
     * @param \AppBundle\Entity\SliderData $sliderData
     *
     * @return University
     */
    public function setSliderData(\AppBundle\Entity\SliderData $sliderData = null)
    {
        $this->sliderData = $sliderData;

        return $this;
    }

    /**
     * Get sliderData
     *
     * @return \AppBundle\Entity\SliderData
     */
    public function getSliderData()
    {
        return $this->sliderData;
    }

    /**
     * Set socialLinks
     *
     * @param \AppBundle\Entity\SocialLinks $socialLinks
     *
     * @return University
     */
    public function setSocialLinks(\AppBundle\Entity\SocialLinks $socialLinks = null)
    {
        $this->socialLinks = $socialLinks;

        return $this;
    }

    /**
     * Get socialLinks
     *
     * @return \AppBundle\Entity\SocialLinks
     */
    public function getSocialLinks()
    {
        return $this->socialLinks;
    }

    /**
     * Set locationPoint
     *
     * @param Point $locationPoint
     *
     * @return University
     */
    public function setLocationPoint($locationPoint)
    {
        $this->locationPoint = $locationPoint;

        return $this;
    }

    /**
     * Get locationPoint
     *
     * @return Point
     */
    public function getLocationPoint()
    {
        return $this->locationPoint;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return University
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }


}
