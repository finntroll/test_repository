<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * UniversityBranch
 *
 * @ORM\Table(name="university_branch", uniqueConstraints={@UniqueConstraint(name="universityBranch_city", columns={"city_id", "university_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UniversityBranchRepository")
 */
class UniversityBranch
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="working_hours", type="string", length=100, nullable=true)
     */
    private $workingHours;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\University", inversedBy="branches")
     */
    private $university;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\City")
     */
    private $city;

    public function __toString()
    {
        return $this->getCity()->getName();
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Show that the branch is main
     *
     * @return bool
     */
    public function isMainBranch()
    {
        $mainBranch = $this->getUniversity()->getMainBranch();

        return $mainBranch->getId() === $this->id;
    }

    /**
     * Set workingHours
     *
     * @param string $workingHours
     *
     * @return UniversityBranch
     */
    public function setWorkingHours($workingHours)
    {
        $this->workingHours = $workingHours;

        return $this;
    }

    /**
     * Get workingHours
     *
     * @return string
     */
    public function getWorkingHours()
    {
        return $this->workingHours;
    }

    /**
     * Set university
     *
     * @param \AppBundle\Entity\University $university
     *
     * @return UniversityBranch
     */
    public function setUniversity(\AppBundle\Entity\University $university = null)
    {
        $this->university = $university;

        return $this;
    }

    /**
     * Get university
     *
     * @return \AppBundle\Entity\University
     */
    public function getUniversity()
    {
        return $this->university;
    }

    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     *
     * @return UniversityBranch
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }
}
