<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * UniversityBranchTranslation
 *
 * @ORM\Table(name="university_branch_translation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UniversityBranchTranslationRepository")
 */
class UniversityBranchTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="reference_phone", type="string", length=20)
     */
    private $referencePhone;

    /**
     * @var string
     *
     * @ORM\Column(name="reference_mail", type="string", length=50)
     */
    private $referenceMail;

    /**
     * @var string
     *
     * @ORM\Column(name="selection_committee_phone", type="string", length=20)
     */
    private $selectionCommitteePhone;

    /**
     * @var string
     *
     * @ORM\Column(name="selection_committee_mail", type="string", length=50)
     */
    private $selectionCommitteeMail;

    /**
     * Set address
     *
     * @param string $address
     *
     * @return UniversityBranchTranslation
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return UniversityBranchTranslation
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set referencePhone
     *
     * @param string $referencePhone
     *
     * @return UniversityBranchTranslation
     */
    public function setReferencePhone($referencePhone)
    {
        $this->referencePhone = $referencePhone;

        return $this;
    }

    /**
     * Get referencePhone
     *
     * @return string
     */
    public function getReferencePhone()
    {
        return $this->referencePhone;
    }

    /**
     * Set referenceMail
     *
     * @param string $referenceMail
     *
     * @return UniversityBranchTranslation
     */
    public function setReferenceMail($referenceMail)
    {
        $this->referenceMail = $referenceMail;

        return $this;
    }

    /**
     * Get referenceMail
     *
     * @return string
     */
    public function getReferenceMail()
    {
        return $this->referenceMail;
    }

    /**
     * Set selectionCommitteePhone
     *
     * @param string $selectionCommitteePhone
     *
     * @return UniversityBranchTranslation
     */
    public function setSelectionCommitteePhone($selectionCommitteePhone)
    {
        $this->selectionCommitteePhone = $selectionCommitteePhone;

        return $this;
    }

    /**
     * Get selectionCommitteePhone
     *
     * @return string
     */
    public function getSelectionCommitteePhone()
    {
        return $this->selectionCommitteePhone;
    }

    /**
     * Set selectionCommitteeMail
     *
     * @param string $selectionCommitteeMail
     *
     * @return UniversityBranchTranslation
     */
    public function setSelectionCommitteeMail($selectionCommitteeMail)
    {
        $this->selectionCommitteeMail = $selectionCommitteeMail;

        return $this;
    }

    /**
     * Get selectionCommitteeMail
     *
     * @return string
     */
    public function getSelectionCommitteeMail()
    {
        return $this->selectionCommitteeMail;
    }
}
