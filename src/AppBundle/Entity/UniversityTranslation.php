<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * UniversityTranslation
 *
 * @ORM\Table(name="university_translation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UniversityTranslationRepository")
 */
class UniversityTranslation implements HasImageInterface
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", length=255)
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="short_name", type="string", length=30)
     */
    private $shortName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="promo_video", type="text", length=2000, nullable=true)
     */
    private $promoVideo;

    /**
     * @var \AppBundle\Entity\Image
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Image", cascade={"persist", "refresh", "remove"})
     * @ORM\JoinColumn(name="logo_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $image;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return UniversityTranslation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return UniversityTranslation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set promoVideo
     *
     * @param string $promoVideo
     *
     * @return UniversityTranslation
     */
    public function setPromoVideo($promoVideo)
    {
        $this->promoVideo = $promoVideo;

        return $this;
    }

    /**
     * Get promoVideo
     *
     * @return string
     */
    public function getPromoVideo()
    {
        return $this->promoVideo;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     *
     * @return UniversityTranslation
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return UniversityTranslation
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set image
     *
     * @param \AppBundle\Entity\Image $image
     *
     * @return UniversityTranslation
     */
    public function setImage(\AppBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \AppBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return '/images/'.$this->image->getFileName();
    }

    /**
     * @return string
     */
    public function getImageFolder()
    {
        return 'university';
    }
}
