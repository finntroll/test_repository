<?php

namespace AppBundle\Entity\UploadedDocuments;

use Doctrine\Common\Collections\ArrayCollection;

interface HasUploadedDocumentsInterface
{
    public function addUploadedDocument($uploadedDocument);

    public function removeUploadedDocument(UploadedDocument $uploadedDocument);

    public function getUploadedDocuments();

    public function setUploadedDocuments($uploadedDocuments = null);

    public function getNewDocuments(): ArrayCollection;
}
