<?php

namespace AppBundle\Entity\UploadedDocuments;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

trait HasUploadedDocumentsTrait
{
    /**
     * @var ArrayCollection
     */
    protected $uploadedDocuments;

    public function addUploadedDocument($uploadedDocument)
    {
        $this->uploadedDocuments->add($uploadedDocument);

        return $this;
    }

    public function removeUploadedDocument(UploadedDocument $uploadedDocument)
    {
        $this->uploadedDocuments->removeElement($uploadedDocument);
    }

    /**
     * @return UploadedDocument[]|ArrayCollection
     */
    public function getUploadedDocuments()
    {
        $result = $this->uploadedDocuments->filter(function(UploadedDocument $doc) {
            return !$doc->isArchived();
        });

        $result = new ArrayCollection($result->getValues());

        return $result;
    }

    public function setUploadedDocuments($uploadedDocuments = null)
    {
        $this->uploadedDocuments = $uploadedDocuments;

        return $this;
    }

    public function getNewDocuments(): ArrayCollection
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq('status', UploadedDocument::STATUS_NEW));

        return $this->getUploadedDocuments()->matching($criteria);
    }

    public function getArchivedDocuments()
    {
        $result = $this->uploadedDocuments->filter(function(UploadedDocument $doc) {
            return $doc->isArchived();
        });

        $result = new ArrayCollection($result->getValues());

        return $result;
    }

    public function getApprovedDocuments()
    {
        $result = $this->uploadedDocuments->filter(function(UploadedDocument $doc) {
            return $doc->isApproved();
        });

        $result = new ArrayCollection($result->getValues());

        return $result;
    }

    public function getAllDocuments()
    {
        return $this->uploadedDocuments;
    }
}
