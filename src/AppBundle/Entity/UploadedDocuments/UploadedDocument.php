<?php

namespace AppBundle\Entity\UploadedDocuments;

use Doctrine\ORM\Mapping as ORM;

/**
 * UploadedDocument
 *
 * @ORM\Table(name="uploaded_document")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UploadedDocumentRepository")
 */
class UploadedDocument
{
    const STATUS_NEW = 0;
    const STATUS_MOVED = 1;

    const CONTENT_STATUS_PROJECT  = 10;
    const CONTENT_STATUS_APPROVED = 11;
    const CONTENT_STATUS_ARCHIVED = 12;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="integer")
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="mime_type", type="string")
     */
    private $mimeType;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string")
     */
    private $extension;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", options={"default": 0})
     */
    private $status = self::STATUS_NEW;

    /**
     * @var string
     *
     * @ORM\Column(name="internal_name", type="string")
     */
    private $internalName;

    /**
     * @ORM\Column(name="content_status", type="integer", nullable=true)
     */
    private $contentStatus;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $archivedAt;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return UploadedDocument
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return UploadedDocument
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set mimeType
     *
     * @param string $mimeType
     *
     * @return UploadedDocument
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Get mimeType
     *
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * Set extension
     *
     * @param string $extension
     *
     * @return UploadedDocument
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set size
     *
     * @param integer $size
     *
     * @return UploadedDocument
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getInternalName(): string
    {
        return $this->internalName;
    }

    /**
     * @param string $internalName
     * @return $this
     */
    public function setInternalName(string $internalName)
    {
        $this->internalName = $internalName;

        return $this;
    }

    public function getRelativePath(): string
    {
        $fullPath = $this->getPath() .
            \DIRECTORY_SEPARATOR .
            $this->getInternalName() .
            '.' .
            $this->getExtension();

        return $fullPath;
    }

    public function getDownloadName(): string
    {
        return $this->getName() . '.' . $this->getExtension();
    }

    public function getViewName(): string
    {
        return $this->getName() . '.' . \strtoupper($this->getExtension());
    }

    public function getFormattedBytes()
    {
        $bytes = $this->getSize();

        if ($bytes >= 1073741824) {
            $bytes = \number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = \number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = \number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes .= ' bytes';
        } elseif ($bytes == 1) {
            $bytes .= ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    /**
     * @return int
     */
    public function getContentStatus()
    {
        return $this->contentStatus;
    }

    /**
     * @param int $contentStatus
     */
    public function setContentStatus($contentStatus)
    {
        $this->contentStatus = $contentStatus;
    }

    public function isProject()
    {
        return $this->getContentStatus() === self::CONTENT_STATUS_PROJECT;
    }

    public function isApproved()
    {
        return $this->getContentStatus() === self::CONTENT_STATUS_APPROVED;
    }

    public function isArchived()
    {
        return $this->getContentStatus() === self::CONTENT_STATUS_ARCHIVED;
    }

    public function getContentStatusTransKey()
    {
        switch ($this->getContentStatus()) {
            case self::CONTENT_STATUS_PROJECT:
                return 'uploaded_documents.content_status.in_project';
            case self::CONTENT_STATUS_APPROVED:
                return 'uploaded_documents.content_status.approved';
            case self::CONTENT_STATUS_ARCHIVED:
                return 'uploaded_documents.content_status.archived';
        }
    }

    /**
     * @return \DateTime|null
     */
    public function getArchivedAt()
    {
        return $this->archivedAt;
    }

    /**
     * @param \DateTime|null $archivedAt
     */
    public function setArchivedAt($archivedAt)
    {
        $this->archivedAt = $archivedAt;
    }
}
