<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Class User
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="agu_user")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="class_type", type="string")
 * @ORM\DiscriminatorMap({"user" = "User", "personality" = "Personality"})
 */
class User extends BaseUser
{
    use ORMBehaviors\Translatable\Translatable;

    const ROLE_SITE_ADMIN                 = 'role_site_admin';
    const ROLE_UNIVERSITY_NEWS_USER       = 'role_university_news_user';
    const ROLE_GLOBAL_UNIVERSITY_ADMIN    = 'role_global_university_admin';
    const ROLE_UNIVERSITY_ADMIN           = 'role_university_admin';
    const ROLE_UNIVERSITY_EDITOR          = 'role_university_editor';
    const ROLE_UNIVERSITY_EVENTS_EDITOR   = 'role_university_events_editor';
    const ROLE_NEWS_EDITOR                = 'role_news_editor';
    const ROLE_UNIVERSITY_PROGRAM_EDITOR  = 'role_university_program_editor';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\University")
     */
    private $university;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $newEmail;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDeleted = false;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return University
     */
    public function getUniversity()
    {
        return $this->university;
    }

    /**
     * @param University $university
     *
     * @return $this
     */
    public function setUniversity(University $university = null)
    {
        $this->university = $university;

        return $this;
    }

    public function getFirstName($locale = null)
    {
        return $this->translate($locale)->getFirstName();
    }

    public function getSurname($locale = null)
    {
        return $this->translate($locale)->getSurname();
    }

    public function getPatronymic($locale = null)
    {
        return $this->translate($locale)->getPatronymic();
    }

    public function getFullName($locale = null)
    {
        $fullName = $this->getSurname($locale).' '.$this->getFirstName($locale);
        $fullName .= empty($this->getPatronymic($locale)) ? '' : ' '.$this->getPatronymic($locale);

        return $fullName;
    }

    /**
     * Set newEmail
     *
     * @param string $newEmail
     *
     * @return User
     */
    public function setNewEmail($newEmail)
    {
        $this->newEmail = $newEmail;

        return $this;
    }

    /**
     * Get newEmail
     *
     * @return string
     */
    public function getNewEmail()
    {
        return $this->newEmail;
    }

    /**
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param boolean $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }
}
