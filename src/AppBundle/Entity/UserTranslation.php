<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="class_type", type="string")
 * @ORM\DiscriminatorMap({"user" = "UserTranslation", "personality" = "PersonalityTranslation"})
 */
class UserTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    protected $surname;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    protected $patronymic;

    /**
     * @ORM\Column(type="string")
     *
     * It's a hack to be able to filter Personalities by professionalPosition. See https://github.com/doctrine/doctrine2/issues/6371
     */
    protected $professionalPosition;

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getPatronymic()
    {
        return $this->patronymic;
    }

    /**
     * @param string $patronymic
     */
    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;
    }

    /**
     * @return mixed
     */
    public function getProfessionalPosition()
    {
        return $this->professionalPosition;
    }

    /**
     * @param mixed $professionalPosition
     */
    public function setProfessionalPosition($professionalPosition)
    {
        $this->professionalPosition = $professionalPosition;
    }


}
