<?php

namespace AppBundle\Event\Listener;

use AppBundle\Entity\UploadedDocuments\HasUploadedDocumentsInterface;
use AppBundle\Entity\UploadedDocuments\UploadedDocument;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Filesystem\Filesystem;

class OrphanDocumentsRemoverListener
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * UploadedDocumentsRemover constructor.
     *
     * @param EntityManagerInterface $em
     * @param Filesystem             $fs
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fs)
    {
        $this->em = $em;
        $this->fs = $fs;
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        if ($object instanceof HasUploadedDocumentsInterface) {
            $this->removeDocumentsOfObject($object);
        }
    }

    private function removeDocumentsOfObject(HasUploadedDocumentsInterface $object)
    {
        $dir = "";
        $method = \method_exists($object, 'getAllDocuments') ? 'getAllDocuments' : 'getUploadedDocuments';
        /** @var UploadedDocument $uploadedDocument */
        foreach ($object->$method() as $uploadedDocument) {
            $dir = $uploadedDocument->getPath();
            // Document will be removed from disk by AppBundle\Event\Subscriber\UploadedDocumentMover
            $this->em->remove($uploadedDocument);
        }
        $this->em->flush();
        $this->fs->remove($dir);
    }
}
