<?php

namespace AppBundle\Event\Subscriber;

use AppBundle\Event\TargetActionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\TranslatorInterface;

class ActionSubscriber implements EventSubscriberInterface
{
    const UPDATE = 'updated.flash';
    const DELETE = 'deleted.flash';
    const CREATE = 'created.flash';
    const PUBLISHED = 'published.flash';
    const UNPUBLISHED = 'unpublished.flash';
    const DECLINED = 'declined.flash';
    const MODERATION = 'moderation.flash';
    const UNDELETABLE_BRANCH = 'undeletableBranch.flash';
    const FORM_ERROR = 'formError.flash';

    /**
     * @var Session
     */
    private $session;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(Session $session, TranslatorInterface $translator){
        $this->session = $session;
        $this->translator = $translator;
    }

    private static $flashMessages = [
        'AppBundle\Entity\BestPractice' => 'best.practice.flash',
        'AppBundle\Entity\News' => 'news.flash',
        'AppBundle\Entity\AcademicMobility' => 'academic.mobility.flash',
        'AppBundle\Entity\Contest' => 'projects.contests.flash',
        'AppBundle\Entity\Program' => 'educational.programs.flash',
        'AppBundle\Entity\University' => 'university.page.flash',
        'Proxies\__CG__\AppBundle\Entity\University' => 'university.page.flash',
        'AppBundle\Entity\Partner' => 'partners.flash',
        'BranchUndeletable' => 'undeletableBranch.flash',
        'FormError' => 'formError.flash',
        'AppBundle\Entity\StaticPageData' => 'static.flash',
        'AppBundle\Entity\StaticPageType\AudiencePage' => 'static.flash',
        'AppBundle\Entity\StaticPageType\AboutAssociationPage' => 'static.flash',
        'AppBundle\Entity\StaticPageType\CouncilPage' => 'static.flash',
        'AppBundle\Entity\Personality' => 'personality.flash',
        'AppBundle\Entity\Project' => 'project.flash',
    ];

    public static function getSubscribedEvents()
    {
        return array(
            TargetActionEvent::CREATED => 'onTargetCreated',
            TargetActionEvent::UPDATED => 'onTargetUpdated',
            TargetActionEvent::DELETED => 'onTargetDeleted',
            TargetActionEvent::PUBLISHED => 'onTargetPublished',
            TargetActionEvent::UNPUBLISHED => 'onTargetUnpublished',
            TargetActionEvent::DECLINED => 'onTargetDeclined',
            TargetActionEvent::MODERATION => 'onTargetModeration',
            TargetActionEvent::UNDELETABLE_BRANCH => 'onTargetUndeletable',
            TargetActionEvent::FORM_ERROR => 'onTargetFormError',
        );
    }

    public function onTargetCreated(TargetActionEvent $event)
    {
        $this->addSuccessFlash(get_class($event->getTarget()), self::CREATE);
    }

    public function onTargetUpdated(TargetActionEvent $event)
    {
        $this->addSuccessFlash(get_class($event->getTarget()), self::UPDATE);
    }

    public function onTargetDeleted(TargetActionEvent $event)
    {
        $this->addSuccessFlash(get_class($event->getTarget()), self::DELETE);
    }

    public function onTargetPublished(TargetActionEvent $event)
    {
        $this->addSuccessFlash(get_class($event->getTarget()), self::PUBLISHED);
    }

    public function onTargetUnpublished(TargetActionEvent $event)
    {
        $this->addSuccessFlash(get_class($event->getTarget()), self::UNPUBLISHED);
    }

    public function onTargetDeclined(TargetActionEvent $event)
    {
        $this->addSuccessFlash(get_class($event->getTarget()), self::DECLINED);
    }

    public function onTargetModeration(TargetActionEvent $event)
    {
        $this->addSuccessFlash(get_class($event->getTarget()), self::MODERATION);
    }

    public function onTargetUndeletable(TargetActionEvent $event)
    {
        $this->addErrorFlash(get_class($event->getTarget()), self::UNDELETABLE_BRANCH);
    }

    public function onTargetFormError(TargetActionEvent $event)
    {
        $this->addErrorFlash(get_class($event->getTarget()), self::FORM_ERROR);
    }

    /**
     * @param string $eventName
     */
    public function addSuccessFlash($eventName, $action)
    {
        if (!isset(self::$flashMessages[$eventName])) {
            throw new \InvalidArgumentException('This event does not correspond to a known flash message');
        }

        $this->session->getFlashBag()->add('success', $this->trans(self::$flashMessages[$eventName]). ' ' .$this->trans($action));
    }

    /**
     * @param string $eventName
     */
    public function addErrorFlash($eventName, $action)
    {
        if (!isset(self::$flashMessages[$eventName])) {
            throw new \InvalidArgumentException('This event does not correspond to a known flash message');
        }

        $this->session->getFlashBag()->add('danger', $this->trans(self::$flashMessages[$eventName]). ' ' .$this->trans($action));
    }

    /**
     * @param string$message
     * @param array $params
     *
     * @return string
     */
    private function trans($message, array $params = array())
    {
        return $this->translator->trans($message, $params);
    }
}
