<?php

namespace AppBundle\Event\Subscriber;

use AppBundle\Entity\UploadedDocuments\UploadedDocument;
use AppBundle\Entity\UploadedDocuments\HasUploadedDocumentsInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;

class DynamicRelationSubscriber implements EventSubscriber
{
    const INTERFACE_FQNS = HasUploadedDocumentsInterface::class;

    /**
     * {@inheritDoc}
     */
    public function getSubscribedEvents()
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        // the $metadata is the whole mapping info for this class
        $metadata = $eventArgs->getClassMetadata();

        if (!in_array(self::INTERFACE_FQNS, class_implements($metadata->getName()))) {
            return;
        }

        $namingStrategy = $eventArgs->getEntityManager()->getConfiguration()->getNamingStrategy();

        $metadata->mapManyToMany([
            'targetEntity' => UploadedDocument::CLASS,
            'fieldName' => 'uploadedDocuments',
            'cascade' => ['persist'],
            'joinTable' => [
                'name' => strtolower($namingStrategy->classToTableName($metadata->getName())).'_document',
                'joinColumns' => [
                    [
                        'name' => $namingStrategy->joinKeyColumnName($metadata->getName()),
                        'referencedColumnName' => $namingStrategy->referenceColumnName(),
                        'onDelete' => 'CASCADE',
                        'onUpdate' => 'CASCADE',
                    ],
                ],
                'inverseJoinColumns' => [
                    [
                        'name' => 'document_id',
                        'referencedColumnName' => $namingStrategy->referenceColumnName(),
                        'onDelete' => 'CASCADE',
                        'onUpdate' => 'CASCADE',
                    ],
                ],
            ],
        ]);
    }
}
