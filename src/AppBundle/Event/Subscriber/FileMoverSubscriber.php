<?php

namespace AppBundle\Event\Subscriber;

use AppBundle\Entity\HasImageInterface;
use AppBundle\Entity\Image;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

class FileMoverSubscriber implements EventSubscriber
{
    /**
     * @var Filesystem
     */
    private $fs;
    private $imagesDir;
    private $imageMoved = false;

    private $toPersist = [];
    private $toRemove = [];

    /**
     * FileMover constructor.
     *
     * @param Filesystem             $fs
     * @param EntityManagerInterface $em
     * @param string                 $imagesDir
     */
    public function __construct(Filesystem $fs, string $imagesDir)
    {
        $this->fs = $fs;
        $this->imagesDir = $imagesDir;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
            'postFlush',
        ];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $object = $args->getObject();
        if ($object instanceof HasImageInterface) {
            return $this->moveNewImage($object);
        }
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $object = $args->getObject();
        if ($object instanceof Image) {
            return $this->moveExistingImage($object);
        }

        if ($object instanceof HasImageInterface) {
            return $this->moveNewImage($object);
        }
    }

    public function postFlush(PostFlushEventArgs $args)
    {
        if (!empty($this->toPersist) || !empty($this->toRemove)) {
            $em = $args->getEntityManager();

            foreach ($this->toPersist as $object) {
                $em->persist($object);
            }

            foreach ($this->toRemove as $object) {
                $em->remove($object);
            }

            $this->toPersist = [];
            $this->toRemove = [];

            $em->flush();
        }
    }

    private function moveNewImage(HasImageInterface $object)
    {
        $image = $object->getImage();
        if (!$image instanceof Image) {
            return;
        }

        if ($this->fs->exists($this->getFilePath($image)) && !empty($image->getFile())) {
            $image->setDir($this->getObjectDir($object));
            $newPath = $this->getNewFilePath($object);

            // This function ignores already existing directories.
            $this->fs->mkdir($newPath);

            $image->getFile()->move($newPath);
            $this->imageMoved = true;

            $this->toPersist[] = $image;
        }
    }

    /**
     * When user changes image in HasImage Entity Doctrine fires postUpdate for Image object not HasImage.
     * Since that we do not have access to the HasImage object and must operate with the Image only.
     * Therefore all relations between object and image made with onDelete SET NULL.
     * So we can just delete image if new image was not loaded.
     *
     * @param Image $originalImage
     */
    private function moveExistingImage(Image $originalImage)
    {
        $loadedImageFile = $originalImage->getFile();
        if (!$loadedImageFile instanceof File) {

            $this->toRemove[] = $originalImage;

            return;
        }
        // ignore second dispatching event when load image first time
        if (!$this->imageMoved) {
            $this->fs->copy($this->getFilePath($loadedImageFile), $this->getFilePath($originalImage));
            $this->fs->remove($this->getFilePath($loadedImageFile));
        }
    }

    /**
     * Get base directory path. This directory stores fresh uploaded images in root
     * and folders of objects, which have images.
     *
     * @return string
     */
    private function getBaseDir(): string
    {
        return $this->imagesDir.DIRECTORY_SEPARATOR;
    }

    /**
     * Get path for file or image. Method getFileName contains object directory so  don't need to add it.
     *
     * @param File|Image $file
     *
     * @return string
     */
    private function getFilePath($file): string
    {
        return $this->getBaseDir().$file->getFileName();
    }

    /**
     * Get image directory for object.
     *
     * @param HasImageInterface $object
     *
     * @return string
     */
    private function getObjectDir(HasImageInterface $object): string
    {
        return trim($object->getImageFolder(), DIRECTORY_SEPARATOR)
            .DIRECTORY_SEPARATOR
            .$object->getId()
            .DIRECTORY_SEPARATOR;
    }

    /**
     * Get full directory path for object
     *
     * @param $object
     *
     * @return string
     */
    private function getNewFilePath($object): string
    {
        return $this->getBaseDir().$this->getObjectDir($object);
    }
}
