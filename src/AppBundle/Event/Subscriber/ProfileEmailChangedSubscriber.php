<?php

namespace AppBundle\Event\Subscriber;

use AppBundle\Entity\User;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Mailer\Mailer;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ProfileEmailChangedSubscriber implements EventSubscriberInterface
{
    /**
     * Old email
     *
     * @var string $email
     */
    protected $email;

    protected $generator;

    protected $mailer;

    public function __construct(TokenGeneratorInterface $generator, Mailer $mailer)
    {
        $this->generator = $generator;
        $this->mailer = $mailer;
    }

    public function onProfileEditSuccess(FormEvent $event)
    {
        /**
         * @var UserInterface $user
         */
        $user = $event->getForm()->getData();

        if ($user->getEmail() !== $this->email) {
            // Don't disable user and not apply new email while it not confirmed.
            $user->setConfirmationToken($this->generator->generateToken());
            $this->mailer->sendConfirmationEmailMessage($user);

            // Old email is still active, new - awaits confirmation
            $user->setNewEmail($user->getEmail());
            $user->setEmail($this->email);
        }
    }

    /**
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::PROFILE_EDIT_SUCCESS => 'onProfileEditSuccess',
            FOSUserEvents::PROFILE_EDIT_INITIALIZE => 'onProfileEditInit',
            FOSUserEvents::REGISTRATION_CONFIRM => 'onEmailConfirmed',
        ];
    }

    public function onEmailConfirmed(GetResponseUserEvent $event)
    {
        /**
         * @var User $user
         */
        $user = $event->getUser();
        // apply new email
        $event->getUser()->setEmail($user->getNewEmail());
        $user->setNewEmail(null);
    }

    public function onProfileEditInit(GetResponseUserEvent $event)
    {
        // remember old email
        $this->email = $event->getUser()->getEmail();
    }
}
