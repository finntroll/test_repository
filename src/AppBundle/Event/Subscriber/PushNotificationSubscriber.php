<?php

namespace AppBundle\Event\Subscriber;

use AppBundle\Entity\BestPractice;
use AppBundle\Entity\News;
use AppBundle\Entity\Partner;
use AppBundle\Entity\University;
use AppBundle\Event\TargetActionEvent;
use AppBundle\Service\Notifications\NotificationStorage;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PushNotificationSubscriber implements EventSubscriberInterface
{
    private $storage;

    public function __construct(NotificationStorage $notificationStorage)
    {
        $this->storage = $notificationStorage;
    }

    public static function getSubscribedEvents()
    {
        return [
            TargetActionEvent::MODERATION => 'onModeration',
            TargetActionEvent::UPDATED => 'onUpdate',
        ];
    }

    public function onModeration(TargetActionEvent $event)
    {
        $object = $event->getTarget();
        if ($object instanceof News) {
            $data = $this->buildData($object);
            $this->storage->pushEventData('news_moderation', $data);
        }

        if ($object instanceof BestPractice) {
            $data = $this->buildData($object);
            $this->storage->pushEventData('best_practice_moderation', $data);
        }

        if ($object instanceof Partner) {
            $data = $this->buildData($object);
            $this->storage->pushEventData('partner_moderation', $data);
        }
    }

    public function onUpdate(TargetActionEvent $event)
    {
        $object = $event->getTarget();
        if ($object instanceof University) {
            $data = $this->buildData($object);
            $this->storage->pushEventData('university_updated', $data);
        }
    }

    private function buildData($object)
    {
        return get_class($object).':'.$object->getId();
    }
}
