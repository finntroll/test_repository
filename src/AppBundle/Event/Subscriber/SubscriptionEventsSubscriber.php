<?php

namespace AppBundle\Event\Subscriber;

use AppBundle\Event\SubscriptionEvent;
use AppBundle\Service\SubscriptionMailer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SubscriptionEventsSubscriber implements EventSubscriberInterface
{
    private $mailer;

    public function __construct(SubscriptionMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents()
    {
        return [
            SubscriptionEvent::SUBSCRIPTION_CREATED => 'onSubscriptionCreated',
        ];
    }

    public function onSubscriptionCreated(SubscriptionEvent $event)
    {
        $subscription = $event->getSubscription();
        $this->mailer->sendConfirmationMessage($subscription);
    }
}
