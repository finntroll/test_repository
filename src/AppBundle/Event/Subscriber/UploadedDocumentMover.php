<?php

namespace AppBundle\Event\Subscriber;

use AppBundle\Entity\UploadedDocuments\HasUploadedDocumentsInterface;
use AppBundle\Entity\UploadedDocuments\UploadedDocument;
use AppBundle\Service\ArchiveManager;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Only for HasUploadedDocumentsInterface
 * Class UploadedDocumentMover
 * @package AppBundle\Event\Subscriber
 */
class UploadedDocumentMover implements EventSubscriber
{
    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ArchiveManager
     */
    private $archive;

    /**
     * @var string
     */
    private $documentsDir;

    /**
     * @var string
     */
    private $webDir;

    /**
     * FileMover constructor.
     *
     * @param Filesystem             $fs
     * @param EntityManagerInterface $em
     * @param ArchiveManager         $archive
     * @param string                 $documentsDir
     * @param string                 $webDir
     */
    public function __construct(Filesystem $fs, EntityManagerInterface $em, ArchiveManager $archive, $documentsDir, $webDir)
    {
        $this->fs = $fs;
        $this->em = $em;
        $this->archive = $archive;
        $this->documentsDir = $documentsDir;
        $this->webDir = $webDir;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
            Events::postUpdate,
            Events::postRemove,
        ];
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $object = $args->getObject();
        /** @var $object UploadedDocument */
        if ($object instanceof UploadedDocument) {
            if ($object->isArchived()) {
                $this->archive->delete($object, false);
            } else {
                $this->fs->remove($this->webDir . $object->getRelativePath());
            }
        }
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        if ($object instanceof HasUploadedDocumentsInterface) {
            /** @var HasUploadedDocumentsInterface $object */
            $this->processUploadedFiles($object, true);
        }
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        if ($object instanceof HasUploadedDocumentsInterface) {
            /** @var HasUploadedDocumentsInterface $object */
            $this->processUploadedFiles($object);
        }
    }

    private function processUploadedFiles(HasUploadedDocumentsInterface $object, $isNewObject = false)
    {
        $files = $object->getNewDocuments();
        $newRelativePath = $this->getNewFilePath($object);

        $this->fs->mkdir($this->webDir.$newRelativePath);

        /** @var UploadedDocument $file */
        foreach ($files as $file) {
            $oldName = $file->getRelativePath();
            $newPath = $this->webDir . $newRelativePath . $file->getInternalName() . '.' . $file->getExtension();
            $this->fs->rename($oldName, $newPath);

            $file->setPath($newRelativePath);
            $file->setStatus(UploadedDocument::STATUS_MOVED);

            // Добавили, потому что не сохранялось новое имя файла при создании объектов, реализующих HasUploadedDocumentsInterface
            if ($isNewObject) {
                $this->em->getUnitOfWork()
                         ->recomputeSingleEntityChangeSet($this->em->getClassMetadata(UploadedDocument::class), $file);
            }

            $this->em->persist($file);
        }
        $this->em->flush();
    }

    private function getObjectDir(HasUploadedDocumentsInterface $object): string
    {
        $reflect = new \ReflectionClass($object);

        $className = \strtolower($reflect->getShortName());
        $id = $object->getId();

        return \trim($className, \DIRECTORY_SEPARATOR)
            . \DIRECTORY_SEPARATOR
            . $id
            . \DIRECTORY_SEPARATOR;
    }

    /**
     * Get base directory path. This directory stores fresh uploaded images in root
     * and folders of objects, which have images.
     *
     * @return string
     */
    private function getBaseDir(): string
    {
        return $this->documentsDir . \DIRECTORY_SEPARATOR;
    }

    /**
     * Get full directory path for object
     *
     * @param $object
     *
     * @return string
     */
    private function getNewFilePath($object): string
    {
        return $this->getBaseDir() . $this->getObjectDir($object);
    }
}
