<?php

namespace AppBundle\Event;

use AppBundle\Entity\Subscription;
use Symfony\Component\EventDispatcher\Event;

class SubscriptionEvent extends Event
{
    const SUBSCRIPTION_CREATED = 'subscription.created';
    const SUBSCRIPTION_CONFIRMED = 'subscription.confirmed';
    const SUBSCRIPTION_REVOKED = 'subscription.revoked';

    private $subscription;
    private $locale;

    /**
     * SubscriptionEvent constructor.
     *
     * @param Subscription $subscription
     * @param string       $locale
     */
    public function __construct(Subscription $subscription, string $locale)
    {
        $this->subscription = $subscription;
        $this->locale;
    }

    /**
     * @return Subscription
     */
    public function getSubscription(): Subscription
    {
        return $this->subscription;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }
}
