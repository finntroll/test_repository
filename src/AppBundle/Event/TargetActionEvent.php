<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class TargetActionEvent extends Event
{
    private $target;

    const UPDATED = 'target.updated';
    const DELETED = 'target.deleted';
    const CREATED = 'target.created';
    const PUBLISHED = 'target.published';
    const UNPUBLISHED = 'target.unpublished';
    const DECLINED = 'target.declined';
    const MODERATION = 'target.moderation';
    const UNDELETABLE_BRANCH = 'target.undeletableBranch';
    const FORM_ERROR = 'target.formError';

    public function getTarget()
    {
       return $this->target;
    }

    public function setTarget($target)
    {
       $this->target = $target;
    }
}
