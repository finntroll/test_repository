<?php

namespace AppBundle\EventListener;

use AppBundle\Configuration\Statistic;
use AppBundle\Entity\AcademicMobility;
use AppBundle\Entity\BestPractice;
use AppBundle\Entity\Contest;
use AppBundle\Entity\News;
use AppBundle\Entity\Program;
use AppBundle\Entity\University;
use AppBundle\Entity\UniversityBranch;
use AppBundle\Manager\StatisticManager;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

/**
 * Class StatisticListener
 * @package AppBundle\EventListener
 */
class StatisticListener
{
    const CLASS_MAP = [
        'academicMobility' => AcademicMobility::class,
        'bestPractice' => BestPractice::class,
        'program' => Program::class,
        'contest' => Contest::class,
        'university' => University::class,
        'universityBranch' => UniversityBranch::class,
        'news' => News::class
    ];

    /**
     * @var Reader
     */
    protected $reader;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var Request
     */
    private $currentRequest;

    /**
     * @var StatisticManager
     */
    private $statisticManager;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * StatisticListener constructor.
     *
     * @param Reader $reader
     * @param RequestStack $requestStack
     * @param StatisticManager $statisticManager
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        Reader $reader,
        RequestStack $requestStack,
        StatisticManager $statisticManager,
        EntityManagerInterface $entityManager
    ) {
        $this->reader = $reader;
        $this->requestStack = $requestStack;
        $this->currentRequest = $this->requestStack->getMasterRequest();
        $this->statisticManager = $statisticManager;
        $this->em = $entityManager;
    }

    /**
     * @param FilterControllerEvent $event
     * @throws \RuntimeException
     */
    public function onKernelController(FilterControllerEvent $event): void
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        list($controllerObject, $methodName) = $controller;

        $controllerReflectionObject = new \ReflectionObject($controllerObject);
        $reflectionMethod = $controllerReflectionObject->getMethod($methodName);

        foreach ($this->reader->getMethodAnnotations($reflectionMethod) as $configuration) {
            if ($configuration instanceof Statistic) {

                $objectName = $configuration->getObjectName();
                $objectId = $this->currentRequest->attributes->get('id', 0);

                $class = self::CLASS_MAP[$objectName];

                if (!$class) {
                    throw new \RuntimeException('Class not found');
                }

                $universityId = $objectId;

                if ($class !== University::class) {
                    $universityId = null;
                    $repo = $this->em->getRepository($class);
                    $object = $repo->find($objectId);
                    $university = $object->getUniversity();
                    if ($university) {
                        $universityId = $university->getId();
                    }
                }

                $sessionId = 'anon';

                if ($session = $this->currentRequest->getSession()) {
                    $sessionId = $session->getId();
                }

                $data = [
                    'object' => $objectName,
                    'objectId' => $objectId,
                    'session' => $sessionId,
                    'universityId' => $universityId,
                ];

                $statistic = $this->statisticManager->create($data);
                $this->statisticManager->update($statistic)->store($statistic);
            }
        }
    }
}
