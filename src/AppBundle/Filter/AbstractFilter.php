<?php

namespace AppBundle\Filter;

use AppBundle\Entity\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyAccess\Exception\NoSuchPropertyException;

abstract class AbstractFilter implements FilterInterface
{
    /**
     * @var array $data
     */
    protected $data = [];

    public function __construct()
    {
        $this->buildFilter();
    }

    public function add(string $field, string $applicant, string $transformer = null)
    {
        $this->data[$field] = [
            'applicant' => $applicant,
            'transformer' => $transformer];

        return $this;
    }

    /**
     * Build filter by using add() method
     *
     */
    abstract public function buildFilter();

    /**
     * Fill filter fields from Request
     *
     * @param Request $request
     */
    public function fillFromRequest(Request $request)
    {
        $fields = $this->getFieldList();

        foreach ($fields as $field) {
            $this->$field = $request->query->get($field, null);
        }
    }

    /**
     * Get fields from data
     *
     * @return array
     */
    public function getFieldList(): array
    {
        return array_keys($this->data) ?? [];
    }

    public function __get($name)
    {
        $fields = $this->getFieldList();

        if (!in_array($name, $fields)) {
            throw new NoSuchPropertyException(
                sprintf('Neither the property "%s" is not in allowed fields in class "'.get_class($this).'".', $name)
            );
        }
    }

    public function __set($name, $value)
    {
        if (!$this->hasField($name)) {
            return;
        }

        $this->$name = $value;
    }

    /**
     * Check that the data contains field
     *
     * @param string $field
     * @return bool
     */
    public function hasField(string $field): bool
    {
        return isset($this->data[$field]);
    }

    /**
     * Check that the fields has transformer callback.
     *
     * @param string $field
     * @return mixed
     */
    public function hasTransformer(string $field): bool
    {
        $data = $this->getFieldData($field);

        return !empty($data['transformer']);
    }

    /**
     * Get field data
     *
     * @param string $field
     * @return array
     * @throws \Exception
     */
    public function getFieldData(string $field): array
    {
        if (!$this->hasField($field)) {
            throw new \Exception(sprintf('Field \'%s\' is not defined!', $field));
        }

        return $this->data[$field];
    }

    /**
     * Call all transformers for fields
     */
    public function transformFields()
    {
        foreach ($this->getFieldList() as $field) {
            $this->transform($field);
        }
    }

    /**
     * Call callback which transform filter data to repository applicant data.
     *
     * @param string $field
     * @throws \Exception
     */
    public function transform(string $field)
    {
        if (!$this->hasTransformer($field)) {
            return;
        }

        if (!is_callable([$this, $this->getFieldData($field)['transformer']])) {
            throw new \Exception(
                sprintf(
                    'Transformer callback for field \'%s\' named \'%s\' is not found!',
                    $field,
                    $this->getFieldData($field)['transformer']
                )
            );
        }

        $this->$field = call_user_func([$this, $this->getFieldData($field)['transformer']], $this->$field);
    }

    /**
     * Get method name in repository, which apply coming data.
     *
     * @param string $field
     * @return mixed
     * @throws \Exception
     */
    public function getFilterApplicant(string $field): string
    {
        if (!isset($this->getFieldData($field)['applicant'])) {
            throw new \Exception('Applicant method is required for each field!');
        }

        return $this->getFieldData($field)['applicant'];
    }

    /**
     * Call appliers method in repository
     *
     * @param callable     $applicant
     * @param QueryBuilder $query
     * @param string       $field
     * @param string       $alias
     * @throws \Exception
     */
    public function callFilterApplicant(callable $applicant, QueryBuilder $query, string $field, string $alias = null)
    {
        $method = $this->getFilterApplicant($field);

        if(!is_callable($applicant)) {
            throw new \Exception(sprintf('Can\'t call applicant %s for %s!', $method, $field));
        }

        call_user_func($applicant, $query, $this->$field, $alias);
    }

    /**
     * Call all applicants for fields in received context.
     *
     * @param              $context
     * @param QueryBuilder $query
     * @param string       $alias
     * @throws \Exception
     */
    public function callApplicants(EntityRepository $context, QueryBuilder $query, string $alias = null)
    {
        $fields  = $this->getFieldList();

        foreach($fields as $field) {
            if (is_null($this->$field)) {
                continue;
            }

            $applicant = [$context, $this->getFilterApplicant($field)];
            $this->callFilterApplicant($applicant, $query, $field, $alias);
        }
    }

    /**
     * Transform value to integer.
     * Return null if value is not numeric or empty.
     *
     * @param $value
     * @return int|null
     */
    public function toIntegerTransformer($value)
    {
        if ($value instanceof EntityInterface) {
            return $value->getId();
        }

        return (empty($value) || !is_numeric($value)) ? null : (int) $value;
    }

    /**
     * Transform value to boolean. False value is one of ['false', 'no', 0, '0', 'n', false], otherwise - true.
     * Return null when the value is empty string or null.
     *
     * @param $value
     * @return bool|null
     */
    public function toBooleanTransformer($value)
    {
        if (is_null($value) || $value === '') {
            return null;
        }

        $falseArray = ['false', 'no', 0, '0', 'n', false];

        if (is_string($value)) {
            $value = strtolower($value);
        }

        return in_array($value, $falseArray, true) ? false : true;
    }

    /**
     * Transform value to DateTime object.
     *
     * @param $value
     * @return \DateTime|null
     */
    public function toDateTransformer($value)
    {
        if (empty($value)) {
            return null;
        }

        // It isn't necessary if filter was created by form.
        if ($value instanceof \DateTime) {
            return $value;
        }

        return \DateTime::createFromFormat('Y-m-d', $value);
    }

    public function toInArrayTransformer($value)
    {
        if (is_array($value) && empty($value)) {
            return null;
        }

        if ($value instanceof ArrayCollection && $value->isEmpty()) {
            return null;
        }

        return $value;
    }
}
