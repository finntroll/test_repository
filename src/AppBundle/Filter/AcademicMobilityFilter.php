<?php

namespace AppBundle\Filter;

class AcademicMobilityFilter extends AbstractFilter
{
    /**
     * Build filter by using add() method
     *
     */
    public function buildFilter()
    {
        $this
             ->add('title', 'queryByTitle')
             ->add('dateBegin', 'queryByDateBegin', 'toDateTransformer')
             ->add('dateEnd', 'queryByDateEnd', 'toDateTransformer')
             ->add('dateReceptionEnd', 'queryByDateReceptionEnd', 'toDateTransformer')
             ->add('university', 'queryByUniversities', 'toInArrayTransformer')
             ->add('mobilityType', 'queryByMobilityType', 'toInArrayTransformer')
             ->add('city', 'queryByCities', 'toInArrayTransformer')
             ->add('languages', 'queryByLanguages', 'toInArrayTransformer')
             ->add('knowledgeAreaItems', 'queryByKnowledgeAreaItems', 'toInArrayTransformer')
             ->add('audiences', 'queryByAudiences', 'toInArrayTransformer')
             ->add('isPublished', 'queryByStatus', 'toBooleanTransformer');
    }
}
