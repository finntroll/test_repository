<?php

namespace AppBundle\Filter;

class BestPracticeFilter extends AbstractFilter
{
    public function buildFilter()
    {
        $this->add('title', 'queryByTitle')
             ->add('status', 'queryByStatus', 'toIntegerTransformer')
             ->add('scope', 'queryByScopes', 'toInArrayTransformer')
             ->add('university', 'queryByUniversities', 'toInArrayTransformer');
    }
}
