<?php

namespace AppBundle\Filter;

class ContestFilter extends AbstractFilter
{
    /**
     * Build filter by using add() method
     *
     */
    public function buildFilter()
    {
        $this
            ->add('title', 'queryBySearch')
            ->add('isPublished', 'queryIsPublished','toBooleanTransformer')
            ->add('knowledgeAreaItems', 'queryByKnowledgeAreaItems', 'toInArrayTransformer')
            ->add('audiences', 'queryByAudience', 'toInArrayTransformer')
            ->add('dateBegin', 'queryByDateBegin', 'toDateTransformer')
            ->add('dateEnd', 'queryByDateEnd', 'toDateTransformer')
            ->add('dateReceptionEnd', 'queryByDateReceptionEnd', 'toDateTransformer')
            ->add('university', 'queryByUniversity', 'toInArrayTransformer')
            ->add('contestType', 'queryByContestType', 'toInArrayTransformer')
            ->add('city', 'queryByCities', 'toInArrayTransformer')
            ->add('languages', 'queryByLanguages', 'toInArrayTransformer')
        ;
    }
}
