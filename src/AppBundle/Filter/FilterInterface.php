<?php

namespace AppBundle\Filter;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

interface FilterInterface
{
    public function getFieldList(): array;

    public function hasField(string $field): bool;

    public function hasTransformer(string $field): bool;

    public function getFilterApplicant(string $field): string;

    public function buildFilter();

    public function getFieldData(string $field);

    public function transform(string $field);

    public function transformFields();

    public function callFilterApplicant(callable $applicant, QueryBuilder $query, string $field, string $alias = null);

    public function callApplicants(EntityRepository $context, QueryBuilder $query, string $alias = null);

    public function add(string $field, string $applicant, string $transformer = null);
}
