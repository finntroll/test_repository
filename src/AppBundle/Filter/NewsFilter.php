<?php

namespace AppBundle\Filter;

class NewsFilter extends AbstractFilter
{

    public function buildFilter()
    {
        $this->add('title', 'queryTitle')
             ->add('status', 'queryStatus', 'toIntegerTransformer')
             ->add('type', 'queryTypes', 'toInArrayTransformer')
             ->add('rubric', 'queryRubrics','toInArrayTransformer')
             ->add('university', 'queryUniversity', 'toInArrayTransformer');
    }
}
