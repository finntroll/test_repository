<?php

namespace AppBundle\Filter;

class NewsSearchFilter extends AbstractFilter
{
    public function buildFilter()
    {
        $this->add('keywords', 'queryKeywords', 'toInArrayTransformer')
             ->add('rubrics', 'queryRubrics', 'toInArrayTransformer')
             ->add('universities', 'queryUniversities', 'toInArrayTransformer')
             ->add('newsType', 'queryType', 'toInArrayTransformer')
             ->add('publishedFrom', 'queryPublishedFrom', 'toDateTransformer')
             ->add('publishedTo', 'queryPublishedTo', 'toDateTransformer');
    }
}
