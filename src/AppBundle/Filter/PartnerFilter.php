<?php

namespace AppBundle\Filter;

class PartnerFilter extends AbstractFilter
{

    public function buildFilter()
    {
        $this->add('name', 'queryName')
            ->add('status', 'queryStatus', 'toIntegerTransformer');
    }
}
