<?php

namespace AppBundle\Filter;

class PersonalityFilter extends AbstractFilter
{
    /**
     * Build filter by using add() method
     *
     */
    public function buildFilter()
    {
        $this->add('nameOrPosition', 'queryByNameOrPosition')
             ->add('workPlace', 'queryByWorkPlace', 'toInArrayTransformer');
    }
}
