<?php

namespace AppBundle\Filter;

class ProgramFilter extends AbstractFilter
{
    /**
     * Build filter by using add() method
     */
    public function buildFilter()
    {
        $this->add('name', 'queryName')
             ->add('educationLevel', 'queryEducationLevel', 'toInArrayTransformer')
             ->add('educationForm', 'queryEducationForm', 'toInArrayTransformer')
             ->add('entranceTests', 'queryEntranceTests', 'toInArrayTransformer')
             ->add('knowledgeArea', 'queryKnowledgeArea', 'toIntegerTransformer')
             ->add('knowledgeAreaItem', 'queryKnowledgeAreaItem', 'toInArrayTransformer')
             ->add('university', 'queryUniversities', 'toInArrayTransformer')
             ->add('hasBudgetPlaces', 'queryBudgetPlaces', 'toBooleanTransformer')
             ->add('hasForeignPlaces', 'queryForeignPlaces', 'toBooleanTransformer')
             ->add('hasDoubleGraduate', 'queryDoubleDiploma', 'toBooleanTransformer')
             ->add('hasMilitaryDepartment', 'queryMilitaryDepartment', 'toBooleanTransformer')
             ->add('priceFrom', 'queryPriceFrom', 'toIntegerTransformer')
             ->add('priceTo', 'queryPriceTo', 'toIntegerTransformer')
             ->add('languages', 'queryLanguages', 'toInArrayTransformer')
             ->add('branchCities', 'queryBranchCities', 'toInArrayTransformer')
             ->add('isPublished', 'queryStatus', 'toBooleanTransformer');
    }
}
