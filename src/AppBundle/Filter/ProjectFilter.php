<?php

namespace AppBundle\Filter;

class ProjectFilter extends AbstractFilter
{
    /**
     * Build filter by using add() method
     *
     */
    public function buildFilter()
    {
        $this->add('name', 'queryByName')
             ->add('head', 'queryByHead', 'toInArrayTransformer')
             ->add('manager', 'queryByManager','toInArrayTransformer')
             ->add('members', 'queryByMembers', 'toInArrayTransformer')
             ->add('university', 'queryByUniversities', 'toInArrayTransformer');
    }
}
