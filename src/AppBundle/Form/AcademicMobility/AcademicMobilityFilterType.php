<?php

namespace AppBundle\Form\AcademicMobility;

use AppBundle\Entity\AcademicMobilityType;
use AppBundle\Form\Common\{FilterType, RelatedWithTakenCitiesType};
use AppBundle\Form\ReusableType\{UniversityEntityType, KnowledgeAreaItemEntityType, AudienceEntityType, EventDatesType, LanguageEntityType};
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class AcademicMobilityFilterType extends FilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('title', null, [
                'translation_domain' => 'dictionary',
            ])
            ->add('mobilityType', EntityType::class, [
                'class' => AcademicMobilityType::class,
                'label' => 'Mobility type',
                'translation_domain' => 'dictionary',
                'choice_translation_domain' => 'dictionary',
                'placeholder' => 'none',
            ])
            ->add('university', UniversityEntityType::class, [
                'locale' => $options['locale'],
                'multiple' => true,
            ])
            ->add('knowledgeAreaItems', KnowledgeAreaItemEntityType::class, [
                'required' => false,
                'multiple' => true,
                'load_all_when_empty' => true,
            ])
            ->add('audiences', AudienceEntityType::class, [
                'multiple' => false,
            ])
            ->add('eventDates', EventDatesType::class)
            ->add('city', RelatedWithTakenCitiesType::class, [
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->findWhereMobilitiesExist($options['locale']);
                },
            ])
            ->add('languages', LanguageEntityType::class, [
                'required' => true,
            ]);
    }
}
