<?php

namespace AppBundle\Form\AcademicMobility;

use AppBundle\Entity\AcademicMobilityType;
use AppBundle\Form\Common\{FilterType, RelatedWithTakenCitiesType};
use AppBundle\Form\ReusableType\{UniversityEntityType, KnowledgeAreaItemEntityType, AudienceEntityType, EventDatesType, LanguageEntityType};
use AppBundle\Form\Selection\MobilityStatusSelection;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AcademicMobilityProfileFilterType extends FilterType
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    public function __construct(AuthorizationCheckerInterface $authChecker)
    {
        $this->authChecker = $authChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        if ($this->authChecker->isGranted(['ROLE_SITE_ADMIN', 'ROLE_GLOBAL_UNIVERSITY_ADMIN'])) {
            $builder->add('university', UniversityEntityType::class, [
                'locale' => $options['locale'],
                'multiple' => true,
            ]);
        }

        $builder->add('title', null, [
                'translation_domain' => 'dictionary',
            ])
            ->add('isPublished', ChoiceType::class, [
                'choices' => MobilityStatusSelection::choices(),
                'translation_domain' => 'dictionary',
                'choice_translation_domain' => 'dictionary',
                'placeholder' => 'all_choices',
                'label' => 'Status'
            ])
            ->add('mobilityType', EntityType::class, [
                'class' => AcademicMobilityType::class,
                'label' => 'Mobility type',
                'translation_domain' => 'dictionary',
                'choice_translation_domain' => 'dictionary',
                'placeholder' => 'none',
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('knowledgeAreaItems', KnowledgeAreaItemEntityType::class, [
                'required' => false,
                'multiple' => true,
                'load_all_when_empty' => true,
            ])
            ->add('audiences', AudienceEntityType::class, [
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('eventDates', EventDatesType::class)

            ->add('city', RelatedWithTakenCitiesType::class, [
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->findWhereMobilitiesExist($options['locale']);
                },
            ])
            ->add('languages', LanguageEntityType::class, [
                'required' => false,
            ]);
    }
}
