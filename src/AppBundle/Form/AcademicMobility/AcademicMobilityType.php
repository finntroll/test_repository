<?php

namespace AppBundle\Form\AcademicMobility;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Entity\AcademicMobility;
use AppBundle\Form\ReusableType\{KnowledgeAreaItemEntityType,LanguageEntityType, AudienceEntityType, EventDatesType, UniversityEntityType};
use AppBundle\Form\Common\RelatedWithDynamicLoadedCitiesType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AcademicMobilityType extends AbstractType
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    public function __construct(AuthorizationCheckerInterface $authChecker)
    {
        $this->authChecker = $authChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($this->authChecker->isGranted(['ROLE_SITE_ADMIN', 'ROLE_GLOBAL_UNIVERSITY_ADMIN'])) {
            $builder->add('university', UniversityEntityType::class, [
                'locale' => $options['locale'],
                'required' => true,
            ]);
        }

        $builder
            ->add('translations', TranslationsType::class, [
                'translation_domain' => 'dictionary',
                'label' => false,
            ])
            ->add('mobilityType', null, [
                'placeholder' => 'none',
                'translation_domain' => 'dictionary',
                'choice_translation_domain' => 'dictionary',
            ])
            ->add('knowledgeAreaItems', KnowledgeAreaItemEntityType::class, [
                'required' => true,
                'load_all_when_empty' => true,
                'multiple' => true,
            ])
            ->add('audiences', AudienceEntityType::class, [
                'expanded' => true,
                'required' => true,
            ])
            ->add('eventDates', EventDatesType::class, [
                'has_duration' => true,
            ])
            ->add('city', RelatedWithDynamicLoadedCitiesType::class)
            ->add('languages', LanguageEntityType::class, [
                'required' => true,
                'expanded' => true,
            ])
            ->add('submitAndPublish', SubmitType::class)
            ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AcademicMobility::class,
            'user' => null,
            'attr' => ['class'=>'app-form-field-mapping'],
            'locale' => 'ru',
        ]);
    }
}
