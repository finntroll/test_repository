<?php

namespace AppBundle\Form\AssociationProject;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use AppBundle\Entity\{Project, University};
use AppBundle\Form\Common\{DatePickerType, UploadedDocumentCollectionType};
use AppBundle\Form\ReusableType\PersonalityEntityType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AssociationProjectType extends AbstractType
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    public function __construct(AuthorizationCheckerInterface $authChecker)
    {
        $this->authChecker = $authChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $project = $builder->getData();

        if (!$this->authChecker->isGranted('CAN_CREATE_PROJECT')
            && !$this->authChecker->isGranted('CAN_EDIT_PROJECT', $project)) {
            throw new AccessDeniedException();
        }

        $builder->add('translations', TranslationsType::class, [
            'fields' => [
                'name' => [
                    'label' => 'project.name',
                    'translation_domain' => 'dictionary',
                ],
                'description' => [
                    'field_type' => CKEditorType::class,
                    'label' => 'project.description',
                    'translation_domain' => 'dictionary',
                    'required' => false,
                ],
                'purpose' => [
                    'required' => false,
                    'translation_domain' => 'dictionary',
                    'label' => 'project.purpose',
                ],
            ],
            'label' => false,
            'translation_domain' => 'dictionary',
        ])
        ->add('university', EntityType::class, [
            'class' => University::class,
            'placeholder' => 'none',
            'translation_domain' => 'dictionary',
            'required' => false,
            'label' => 'project.university',
        ])
        ->add('dateStart', DatePickerType::class, [
            'attr' => ['class' => 'date-start'],
            'required' => false,
            'translation_domain' => 'dictionary',
            'label' => 'project.date_start',
            'error_bubbling' => false,
        ])
        ->add('dateEnd', DatePickerType::class, [
            'attr' => ['class' => 'date-end'],
            'required' => false,
            'translation_domain' => 'dictionary',
            'label' => 'project.date_end',
            'error_bubbling' => false,
        ])
        ->add('passport', ProjectPassportType::class, [
            'label' => 'project.passport',
            'translation_domain' => 'dictionary',
            'required' => false,
            'error_bubbling' => false,
        ])
        ->add('outlay', ProjectOutlayType::class, [
            'label' => 'project.outlay',
            'translation_domain' => 'dictionary',
            'required' => false,
            'error_bubbling' => false,
        ])
        ->add('uploadedDocuments', UploadedDocumentCollectionType::class, [
            'label' => 'project.public_docs',
            'required' => false,
            'translation_domain' => 'dictionary',
            'entry_options' => [
                'need_status_field' => true,
            ],
        ]);

        if ($this->authChecker->isGranted('CAN_ASSIGN_PROJECT_HEAD', $project)) {
            $builder->add('head', PersonalityEntityType::class, [
                'label' => 'project.head',
                'translation_domain' => 'dictionary',
            ]);
        }

        if ($this->authChecker->isGranted('CAN_ASSIGN_PROJECT_MANAGER', $project)) {
            $builder->add('manager', PersonalityEntityType::class, [
                'label' => 'project.manager',
                'translation_domain' => 'dictionary',
            ]);
        }

        if ($this->authChecker->isGranted('CAN_INVITE_PROJECT_MEMBERS', $project)) {
            $builder->add('projectMembers', CollectionType::class, [
                'entry_type' => ProjectMemberType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_options' => [
                    'attr' => [
                        'class' => 'project-member-item bordered',
                    ],
                    'label' => 'project.member',
                    'error_bubbling' => false,
                ],
                'label' => 'project.members',
                'by_reference' => false,
                'error_bubbling' => false,
            ]);
        }

        $builder->add('submit', SubmitType::class, [
            'translation_domain' => 'dictionary',
            'attr' => [
                'class' => 'side-button side-button blue-button default-button',
            ],
            'validation_groups' => $project->getIsPublished() ? 'publication' : 'draft',
        ]);

        if ($this->authChecker->isGranted('CAN_PUBLISH_PROJECT', $project)) {
            $builder->add('submitAndPublish', SubmitType::class, [
                'translation_domain' => 'dictionary',
                'attr' => [
                    'class' => 'side-button side-button pink-button default-button',
                ],
                'validation_groups' => 'publication',
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
        ]);
    }
}
