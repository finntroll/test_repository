<?php

namespace AppBundle\Form\AssociationProject;

use AppBundle\Form\Common\FilterType;
use AppBundle\Form\ReusableType\UniversityEntityType;
use Symfony\Component\Form\FormBuilderInterface;

class ProjectFilterFormType extends FilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('name', null, [
            'label' => 'project.name',
            'translation_domain' => 'dictionary',
        ])
        ->add('university', UniversityEntityType::class, [
            'multiple' => true,
            'required' => false,
            'label' => 'project.university',
            'translation_domain' => 'dictionary',
        ]);
    }
}
