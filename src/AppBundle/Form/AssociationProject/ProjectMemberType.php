<?php

namespace AppBundle\Form\AssociationProject;

use AppBundle\Entity\ProjectMember;
use AppBundle\Form\ReusableType\PersonalityEntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\{ButtonType, TextareaType};
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectMemberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('personality', PersonalityEntityType::class, [
                'label' => 'project.members.personality',
            ])
            ->add('cause', TextareaType::class, [
                'label' => 'project.members.cause',
                'required' => false,
                'attr' => [
                    'maxlength' => 255,
                ],
            ])
            ->add('delete', ButtonType::class, [
                'attr' => [
                    'class' => 'delete-member feed-remove tooltip-toggle',
                ],
                'label' => ' '
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProjectMember::class,
            'translation_domain' => 'dictionary',
        ]);
    }
}
