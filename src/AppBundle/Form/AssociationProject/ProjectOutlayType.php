<?php

namespace AppBundle\Form\AssociationProject;

use AppBundle\Entity\ProjectOutlay;
use AppBundle\Form\Common\UploadedDocumentCollectionType;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectOutlayType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('uploadedDocuments', UploadedDocumentCollectionType::class, [
            'label' => false,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProjectOutlay::class,
        ]);
    }
}
