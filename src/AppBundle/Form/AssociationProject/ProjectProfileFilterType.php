<?php

namespace AppBundle\Form\AssociationProject;

use AppBundle\Form\Common\FilterType;
use AppBundle\Form\ReusableType\{PersonalityEntityType, UniversityEntityType};
use Symfony\Component\Form\FormBuilderInterface;

class ProjectProfileFilterType extends FilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('name', null, [
            'required' => false,
        ])
        ->add('head', PersonalityEntityType::class, [
            'label' => 'project.head',
            'multiple' => true,
        ])
        ->add('manager', PersonalityEntityType::class, [
            'label' => 'project.manager',
            'multiple' => true,
        ])
        ->add('members', PersonalityEntityType::class, [
            'label' => 'project.members',
            'multiple' => true,
        ])
        ->add('university', UniversityEntityType::class, [
            'multiple' => true,
            'required' => false,
            'label' => 'project.university',
            'translation_domain' => 'dictionary',
        ]);
    }
}
