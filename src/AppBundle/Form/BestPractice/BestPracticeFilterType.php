<?php

namespace AppBundle\Form\BestPractice;

use AppBundle\Entity\Scope;
use AppBundle\Form\Common\FilterType;
use AppBundle\Form\ReusableType\UniversityEntityType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class BestPracticeFilterType extends FilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('title', null, [
                'translation_domain' => 'dictionary',
            ])
            ->add('scope', EntityType::class, [
                'class' => Scope::class,
                'translation_domain' => 'dictionary',
                'choice_translation_domain' => 'dictionary',
                'placeholder' => 'none',
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('university', UniversityEntityType::class, [
                'required' => false,
                'multiple' => true,
                'locale' => $options['locale'],
            ]);
    }
}
