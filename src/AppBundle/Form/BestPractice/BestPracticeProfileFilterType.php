<?php

namespace AppBundle\Form\BestPractice;

use AppBundle\Entity\Scope;
use AppBundle\Form\Common\FilterType;
use AppBundle\Form\ReusableType\UniversityEntityType;
use AppBundle\Form\Selection\BestPracticeStatusSelection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class BestPracticeProfileFilterType extends FilterType
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    public function __construct(AuthorizationCheckerInterface $authChecker)
    {
        $this->authChecker = $authChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('title', null, [
            'required' => false,
            'translation_domain' => 'dictionary',
        ])
        ->add('status', ChoiceType::class, [
            'required' => false,
            'placeholder' => 'all_choices',
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
            'choices' => BestPracticeStatusSelection::choices(),
        ])
        ->add('scope', EntityType::class, [
            'class' => Scope::class,
            'required' => false,
            'multiple' => true,
            'expanded' => true,
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
        ]);

        if ($this->authChecker->isGranted('CAN_CREATE_BEST_PRACTICE')) {
            $builder->add('university', UniversityEntityType::class, [
                'required' => false,
                'multiple' => true,
                'locale' => $options['locale'],
            ]);
        }
    }
}
