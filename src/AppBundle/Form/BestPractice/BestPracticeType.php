<?php

namespace AppBundle\Form\BestPractice;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use AppBundle\Entity\BestPractice;
use AppBundle\Form\ReusableType\UniversityEntityType;
use AppBundle\Form\Common\{ImageCropperType, UploadedDocumentCollectionType};
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class BestPracticeType extends AbstractType
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    public function __construct(AuthorizationCheckerInterface $authChecker)
    {
        $this->authChecker = $authChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('translations', TranslationsType::class, [
            'translation_domain' => 'dictionary',
            'label' => false,
            'fields' => [
                'title' => [],
                'leadText' => [],
                'text' => [
                    'field_type' => CKEditorType::class,
                ],
            ],
        ])
        ->add('scope', null, [
            'placeholder' => 'none',
            'choice_translation_domain' => 'dictionary',
            'translation_domain' => 'dictionary',
        ])
        ->add('image', ImageCropperType::class, [
            'required' => false,
            'error_bubbling' => false,
        ])
        ->add('submitForModeration', SubmitType::class, [
            'translation_domain' => 'dictionary',
            'attr' => [
                'class' => 'side-button pink-button default-button',
            ],
        ])
        ->add('uploadedDocuments', UploadedDocumentCollectionType::class)
        ->add('submit', SubmitType::class, [
            'translation_domain' => 'dictionary',
            'attr' => [
                'class' => 'side-button side-button blue-button default-button',
            ],
        ]);

        if ($this->authChecker->isGranted('CAN_CREATE_BEST_PRACTICE')) {
            $builder->add('university', UniversityEntityType::class, [
                'locale' => $options['locale'],
                'required' => false,
            ]);
        }

        $object = $builder->getData();

        if ($this->authChecker->isGranted('CAN_EDIT_BEST_PRACTICE', $object)) {
            if (!$object->isDraft()) {
                $builder->remove('submitForModeration');
            }
        }

        if ($this->authChecker->isGranted('CAN_PUBLISH_BEST_PRACTICE', $object)) {
            $builder->add('submitAndPublish', SubmitType::class, [
                'translation_domain' => 'dictionary',
                'attr' => [
                    'class' => 'side-button pink-button default-button',
                ],
            ]);

            if ($object->isModerating()) {
                $builder->add('submitAndSaveAsDraft', SubmitType::class, [
                    'translation_domain' => 'dictionary',
                    'attr' => [
                        'class' => 'side-button pink-button default-button',
                    ],
                ]);
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BestPractice::class,
            'user' => null,
            'allow_extra_fields' => true,
            'locale' => 'ru',
        ]);
    }
}
