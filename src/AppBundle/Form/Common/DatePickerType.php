<?php

namespace AppBundle\Form\Common;

use AppBundle\Form\Transformer\DatePickerTransformer;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DatePickerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('date', TextType::class);
        $builder->addViewTransformer(new DatePickerTransformer());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'date_format' => 'Y-m-d',
            ]
        );
    }
}
