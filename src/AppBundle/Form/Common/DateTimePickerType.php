<?php

namespace AppBundle\Form\Common;

use AppBundle\Form\Transformer\DateTimePickerTransformer;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DateTimePickerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('date', TextType::class)
                ->add('time', TextType::class);
        $builder->addViewTransformer(new DateTimePickerTransformer());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'date_format' => 'Y-m-d',
                'time_format' => 'H:i',
            ]
        );
    }
}
