<?php

namespace AppBundle\Form\Common;

use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->setMethod('GET');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'csrf_protection' => false,
            'required' => false,
            'attr' => [
                'class' => 'filters-form app-form-field-mapping',
            ],
            'allow_extra_fields' => true,
            'locale' => 'ru',
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
