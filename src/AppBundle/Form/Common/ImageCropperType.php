<?php

namespace AppBundle\Form\Common;

use AppBundle\Entity\Image;
use Presta\ImageBundle\Form\Type\ImageType;
use Presta\ImageBundle\Model\AspectRatio;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImageCropperType extends AbstractType
{
    /**
     * @var string $imagesDir
     */
    private $imagesDir;

    public function __construct($imagesDir)
    {
        $this->imagesDir = $imagesDir;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', ImageType::class, [
            'aspect_ratios' => $options['aspect_ratios'],
            'preview_height' => $options['preview_height'],
            'preview_width' => $options['preview_width'],
            'max_width' => $options['max_width'],
            'max_height' => $options['max_height'],
            'delete_label' => $options['delete_label'],
            'translation_domain' => $options['translation_domain'],
            'label' => $options['disable_label'] ? false : $options['label'],
            'enable_remote' => $options['enable_remote'],
            'cropper_options' => $options['cropper_options'],
            'upload_button_class' => $options['upload_button_class'],
            'upload_button_icon' => $options['upload_button_icon'],
            'cancel_button_class' => $options['cancel_button_class'],
            'save_button_class' => $options['save_button_class'],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Image::class,
            'aspect_ratios' => [new AspectRatio(1.0, 'square')],
            'preview_height' => 300,
            'preview_width' => 300,
            'max_width' => 1200,
            'max_height' => 1200,
            'delete_label' => 'Remove an image',
            'translation_domain' => 'messages',
            'label' => false,
            'disable_label' => false,
            'enable_remote' => false,
            'cropper_options' => [
                'cropBoxResizable' => true,
                'minCropBoxWidth' => 150,
                'minCropBoxHeight' => 150,
            ],
            'upload_button_class' => 'blue-button default-button',
            'upload_button_icon' => '',
            'cancel_button_class' => 'default-button pink-button',
            'save_button_class' => 'default-button blue-button save-button',
        ]);
    }
}
