<?php

namespace AppBundle\Form\Common;

use AppBundle\Entity\City;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class RelatedWithDynamicLoadedCitiesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('city', Select2EntityType::class, [
            'translation_domain' => 'dictionary',
            'remote_route' => 'cities',
            'primary_key' => 'id',
            'required' => $options['required'],
            'class' => City::class,
            'attr' => [
                'class' => 'select2-apply',
            ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'inherit_data' => true,
            'multiple' => false,
            'label' => false,
        ]);
    }
}
