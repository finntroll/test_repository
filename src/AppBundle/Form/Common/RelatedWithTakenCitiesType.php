<?php

namespace AppBundle\Form\Common;

use AppBundle\Entity\City;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;

class RelatedWithTakenCitiesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('city', EntityType::class, [
            'class' => City::class,
            'placeholder' => 'all_choices',
            'required' => false,
            'multiple' => true,
            'attr' => ['class' => 'select2-apply'],
            'translation_domain' => 'dictionary',
            'query_builder' => $options['query_builder'],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'inherit_data' => true,
            'label' => false,
            'query_builder' => null,
        ]);

        $resolver->setRequired('query_builder');
    }
}
