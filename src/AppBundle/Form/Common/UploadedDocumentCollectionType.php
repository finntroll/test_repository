<?php

namespace AppBundle\Form\Common;

use AppBundle\Form\Transformer\UploadedDocumentTransformer;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UploadedDocumentCollectionType extends CollectionType
{
    /**
     * @var UploadedDocumentTransformer
     */
    private $transformer;

    public function __construct(UploadedDocumentTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->addModelTransformer($this->transformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'entry_type' => UploadedDocumentType::class,
            'translation_domain' => 'dictionary',
            'allow_add' => true,
            'allow_delete' => true,
            'error_bubbling' => false,
            'attr' => ['class' => 'uploaded_documents'],
            'label' => 'uploaded_documents.main.label',
            'required' => false,
        ]);
    }
}
