<?php

namespace AppBundle\Form\Common;

use AppBundle\Entity\UploadedDocuments\UploadedDocument;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\{ChoiceType, HiddenType, TextType};
use Symfony\Component\Form\{FormEvent, FormEvents};
use Symfony\Component\OptionsResolver\OptionsResolver;

class UploadedDocumentType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => false,
        ]);

        $builder->add('id', HiddenType::class); // for saving
        $builder->add('extension', HiddenType::class); // for view;

        if ($options['need_status_field']) {

            $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
                $form = $event->getForm();
                $data = $event->getData();
                $contentStatusOptions = [
                    'choices' => [
                        'uploaded_documents.content_status.in_project' => UploadedDocument::CONTENT_STATUS_PROJECT,
                        'uploaded_documents.content_status.approved' => UploadedDocument::CONTENT_STATUS_APPROVED,
                    ],
                    'translation_domain' => 'dictionary',
                    'label' => 'uploaded_documents.content_status.label',
                    'disabled' => isset($data['contentStatus']) && $data['contentStatus'] === UploadedDocument::CONTENT_STATUS_APPROVED,
                ];

                $form->add('contentStatus', ChoiceType::class, $contentStatusOptions);
            });
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'label' => false,
            'need_status_field' => false,
        ]);
    }
}
