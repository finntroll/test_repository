<?php

namespace AppBundle\Form\Contest;

use AppBundle\Entity\{Audience, ContestType};
use AppBundle\Form\Common\{FilterType, RelatedWithTakenCitiesType};
use AppBundle\Form\ReusableType\{UniversityEntityType, KnowledgeAreaItemEntityType, AudienceEntityType, EventDatesType, LanguageEntityType};
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class ContestFilterType extends FilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('title', null, [
            'translation_domain' => 'dictionary',
        ])
        ->add('contestType', EntityType::class, [
            'class' => ContestType::class,
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
            'placeholder' => 'none',
            'label' => 'Contest type',
        ])
        ->add('university', UniversityEntityType::class, [
            'locale' => $options['locale'],
            'multiple' => true,
        ])
        ->add('knowledgeAreaItems', KnowledgeAreaItemEntityType::class, [
            'required' => false,
            'multiple' => true,
            'load_all_when_empty' => true,
        ])
        ->add('audiences', AudienceEntityType::class, [
            'required' => false,
            'multiple' => false,
            'exclude' => [Audience::AUDIENCE_TYPE__SCHOLAR],
        ])
        ->add('eventDates', EventDatesType::class)
        ->add('city', RelatedWithTakenCitiesType::class, [
            'query_builder' => function (EntityRepository $er) use ($options) {
                return $er->findWhereContestsExist($options['locale']);
            },
        ])
        ->add('languages', LanguageEntityType::class, [
            'required' => false,
            'expanded' => false,
        ]);
    }
}
