<?php

namespace AppBundle\Form\Contest;

use AppBundle\Entity\{Audience, ContestType};
use AppBundle\Form\Common\{RelatedWithTakenCitiesType, FilterType};
use AppBundle\Form\ReusableType\{UniversityEntityType, KnowledgeAreaItemEntityType, AudienceEntityType, EventDatesType, LanguageEntityType};
use AppBundle\Form\Selection\ContestStatusSelection;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ContestProfileFilterType extends FilterType
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    public function __construct(AuthorizationCheckerInterface $authChecker)
    {
        $this->authChecker = $authChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        if ($this->authChecker->isGranted(['ROLE_SITE_ADMIN', 'ROLE_GLOBAL_UNIVERSITY_ADMIN'])) {
            $builder->add('university', UniversityEntityType::class, [
                'multiple' => true,
                'locale' => $options['locale'],
            ]);
        }

        $builder->add('title', null, [
                'translation_domain' => 'dictionary',
            ])
            ->add('isPublished', ChoiceType::class, [
                'choices' => ContestStatusSelection::choices(),
                'translation_domain' => 'dictionary',
                'choice_translation_domain' => 'dictionary',
                'placeholder' => 'all_choices',
                'label' => 'Status'
            ])
            ->add('contestType', EntityType::class, [
                'class' => ContestType::class,
                'translation_domain' => 'dictionary',
                'choice_translation_domain' => 'dictionary',
                'placeholder' => 'none',
                'label' => 'Contest type',
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('knowledgeAreaItems', KnowledgeAreaItemEntityType::class, [
                'required' => false,
                'multiple' => true,
                'load_all_when_empty' => true,
            ])
            ->add('audiences', AudienceEntityType::class, [
                'expanded' => true,
                'required' => false,
                'exclude' => [Audience::AUDIENCE_TYPE__SCHOLAR],
            ])
            ->add('eventDates', EventDatesType::class, [
                'has_reception_date' => false,
            ])
            ->add('city', RelatedWithTakenCitiesType::class, [
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->findWhereContestsExist($options['locale']);
                },
            ])
            ->add('languages', LanguageEntityType::class, [
                'required' => false,
                'expanded' => true,
            ]);
    }
}
