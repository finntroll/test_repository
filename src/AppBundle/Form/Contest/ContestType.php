<?php

namespace AppBundle\Form\Contest;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Entity\{Audience, Contest};
use AppBundle\Form\Common\RelatedWithDynamicLoadedCitiesType;
use AppBundle\Form\ReusableType\{UniversityEntityType, KnowledgeAreaItemEntityType, AudienceEntityType, EventDatesType, LanguageEntityType};
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ContestType extends AbstractType
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    public function __construct(AuthorizationCheckerInterface $authChecker)
    {
        $this->authChecker = $authChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($this->authChecker->isGranted(['ROLE_SITE_ADMIN', 'ROLE_GLOBAL_UNIVERSITY_ADMIN'])) {
            $builder->add('university', UniversityEntityType::class, [
                'locale' => $options['locale'],
            ]);
        }

        $builder
            ->add('translations', TranslationsType::class, [
                'translation_domain' => 'dictionary',
                'label' => false,
            ])
            ->add('contestType', null, [
                'placeholder' => 'none',
                'choice_translation_domain' => 'dictionary',
                'translation_domain' => 'dictionary',
            ])
            ->add('knowledgeAreaItems', KnowledgeAreaItemEntityType::class, [
                'required' => true,
                'load_all_when_empty' => true,
                'multiple' => true,
            ])
            ->add('audiences', AudienceEntityType::class, [
                'expanded' => true,
                'required' => true,
                'exclude' => [Audience::AUDIENCE_TYPE__SCHOLAR],
            ])
            ->add('eventDates', EventDatesType::class)
            ->add('city', RelatedWithDynamicLoadedCitiesType::class)
            ->add('languages', LanguageEntityType::class, [
                'required' => true,
            ])
            ->add('submitAndPublish', SubmitType::class)
            ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contest::class,
            'user' => null,
            'attr' => ['class'=>'app-form-field-mapping'],
            'locale' => 'ru',
        ]);
    }
}
