<?php

namespace AppBundle\Form\Event;

use AppBundle\Form\ReusableType\UniversityBranchCitiesType;
use Symfony\Component\Form\Form;

class AddBranchCitiesFieldSubscriber extends AddDependentFieldSubscriber
{
    protected $propertyPath = 'university';

    /**
     * Add dependent field to form with values based on controlParameter
     *
     * @param Form  $form
     * @param mixed $controlParameter
     */
    protected function addDependentField(Form $form, $controlParameter)
    {
        if ($form->has('branchCities')) {
            $form->remove('branchCities');
        }

        $form->add('branchCities', UniversityBranchCitiesType::class, [
            'attr' => [
                'data-dependency-field-id' => 'university',
                'request-url-key' => 'university_branch_cities',
            ],
            'multiple' => $this->options['multiple'] ?? false,
            'required' => $this->options['required'] ?? true,
            'locale' => $this->options['locale'] ?? 'ru',
            'load_all_when_empty' => $this->options['load_all_when_empty'] ?? false ,
            'university' => $controlParameter,
        ]);
    }
}
