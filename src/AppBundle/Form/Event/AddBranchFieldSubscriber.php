<?php

namespace AppBundle\Form\Event;

use AppBundle\Form\ReusableType\UniversityBranchEntityType;
use Symfony\Component\Form\Form;

class AddBranchFieldSubscriber extends AddDependentFieldSubscriber
{
    protected $propertyPath = 'university';

    /**
     * Add dependent field to form with values based on controlParameter
     *
     * @param Form  $form
     * @param mixed $controlParameter
     */
    protected function addDependentField(Form $form, $controlParameter)
    {
        if ($form->has('universityBranch')) {
            $form->remove('universityBranch');
        }

        $form->add('universityBranch', UniversityBranchEntityType::class, [
            'attr' => [
                'data-dependency-field-id' => 'university',
                'request-url-key' => 'university_branches',
            ],
            'multiple' => $this->options['multiple'] ?? false,
            'required' => $this->options['required'] ?? true,
            'load_all_when_empty' => $this->options['load_all_when_empty'] ?? false ,
            'university' => $controlParameter,
        ]);
    }
}
