<?php

namespace AppBundle\Form\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\{Form, FormEvent, FormEvents};
use Symfony\Component\PropertyAccess\PropertyAccess;

abstract class AddDependentFieldSubscriber implements EventSubscriberInterface
{
    /**
     * Property path for field which have dependent field, i.e. controller
     *
     * @var string
     */
    protected $propertyPath;

    /**
     * Array with options for form field
     *
     * @var array
     */
    protected $options;

    public function configureOptions(array $options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    public function preSetData(FormEvent $event)
    {
        list($form, $data) = $this->extractData($event);

        if ($data === null) {
            return;
        }

        $accessor = PropertyAccess::createPropertyAccessor();
        $controlParameter = $accessor->getValue($data, $this->propertyPath);
        $this->addDependentField($form, $controlParameter);
    }

    public function preSubmit(FormEvent $event)
    {
        list($form, $data) = $this->extractData($event);
        $controlParameter = array_key_exists($this->propertyPath, $data) ? $data[$this->propertyPath] : null;
        $this->addDependentField($form, $controlParameter);
    }

    /**
     * Extract from event form and data [$form, $data]
     *
     * @param FormEvent $event
     *
     * @return array
     */
    private function extractData(FormEvent $event)
    {
        return [$event->getForm(), $event->getData()];
    }

    /**
     * Add dependent field to form with values based on controlParameter
     *
     * @param Form  $form
     * @param mixed $controlParameter
     */
    protected abstract function addDependentField(Form $form, $controlParameter);
}
