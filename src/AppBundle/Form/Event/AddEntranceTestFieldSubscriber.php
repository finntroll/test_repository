<?php

namespace AppBundle\Form\Event;

use AppBundle\Entity\EntranceTest;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Form;

class AddEntranceTestFieldSubscriber extends AddDependentFieldSubscriber
{
    protected $propertyPath = 'educationLevel';

    /**
     * {@inheritdoc}
     */
    protected function addDependentField(Form $form, $controlParameter)
    {
        $options = [
            'placeholder' => 'none',
            'class' => EntranceTest::class,
            'expanded' => $this->options['expanded'] ?? false,
            'multiple' => $this->options['multiple'] ?? false,
            'required' => $this->options['required'] ?? true,
            'attr' => [
                'data-dependency-field-id' => 'educationLevel',
                'request-url-key' => 'entrance_tests',
            ],
            'choice_translation_domain' => 'dictionary',
            'translation_domain' => 'dictionary',
            'query_builder' => function (EntityRepository $repository) use ($controlParameter) {
                $qb = $repository->createQueryBuilder('test')
                                 ->where(':educationLevel MEMBER OF test.educationLevels')
                                 ->setParameter('educationLevel', $controlParameter);

                return $qb;
            }
        ];

        $form->add('entranceTests', EntityType::class, $options);
    }
}
