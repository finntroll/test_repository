<?php

namespace AppBundle\Form\Event;

use AppBundle\Form\ReusableType\KnowledgeAreaItemEntityType;
use Symfony\Component\Form\Form;

class AddKnowledgeAreaItemFieldSubscriber extends AddDependentFieldSubscriber
{
    protected $propertyPath = 'knowledgeArea';

    /**
     * {@inheritdoc}
     */
    protected function addDependentField(Form $form, $controlParameter)
    {
        if ($form->has('knowledgeAreaItem')) {
            $form->remove('knowledgeAreaItem');
        }

        $form->add('knowledgeAreaItem', KnowledgeAreaItemEntityType::class, [
            'attr' => [
                'data-dependency-field-id' => 'knowledgeArea',
                'request-url-key' => 'knowledge_area_items',
            ],
            'knowledge_area' => $controlParameter,
            'load_all_when_empty' => true,
            'multiple' => $this->options['multiple'] ?? false,
            'required' => $this->options['required'] ?? true,
        ]);
    }
}
