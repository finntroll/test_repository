<?php

namespace AppBundle\Form\Event;

use AppBundle\Form\ReusableType\PartnerEntityType;
use Symfony\Component\Form\Form;

class AddPartnerFieldSubscriber extends AddDependentFieldSubscriber
{
    protected $propertyPath = 'university';

    /**
     * Add dependent field to form with values based on controlParameter
     *
     * @param Form  $form
     * @param mixed $controlParameter
     */
    protected function addDependentField(Form $form, $controlParameter)
    {
        if ($form->has('partners')) {
            $form->remove('partners');
        }
        $form->add('partners', PartnerEntityType::class, [
            'attr' => [
                'data-dependency-field-id' => 'university',
                'request-url-key' => 'university_partners',
                'class' => 'select2-apply',
            ],
            'university' => $controlParameter,
            'multiple' => $this->options['multiple'] ?? false,
            'required' => $this->options['required'] ?? true,
        ]);
    }
}
