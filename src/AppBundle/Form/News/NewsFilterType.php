<?php

namespace AppBundle\Form\News;

use AppBundle\Entity\{Keyword, Rubric};
use AppBundle\Form\Common\FilterType;
use AppBundle\Form\ReusableType\UniversityEntityType;
use AppBundle\Form\Selection\NewsTypeSelection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class NewsFilterType extends FilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('keywords', EntityType::class, [
            'attr' => ['class' => 'select2-apply'],
            'translation_domain' => 'dictionary',
            'multiple' => true,
            'class' => Keyword::class,
            'required' => false,
        ])
        ->add('rubrics', EntityType::class, [
            'placeholder' => 'all_choices',
            'attr' => ['class' => 'select2-apply'],
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
            'multiple' => true,
            'class' => Rubric::class,
            'required' => false,
        ])
        ->add('universities', UniversityEntityType::class, [
            'multiple' => true,
            'required' => false,
            'locale' => $options['locale'],
        ])
        ->add('newsType', ChoiceType::class, [
            'placeholder' => 'all_choices',
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
            'choices' => NewsTypeSelection::choices(),
            'required' => false,
        ])
        ->add('publishedFrom', null, [
            'translation_domain' => 'dictionary',
            'attr' => ['class' => 'date-start datepicker'],
            'required' => false,
        ])
        ->add('publishedTo', null, [
            'translation_domain' => 'dictionary',
            'attr' => ['class' => 'date-end datepicker'],
            'required' => false,
        ]);
    }
}
