<?php

namespace AppBundle\Form\News;

use AppBundle\Form\Common\FilterType;
use AppBundle\Form\ReusableType\UniversityEntityType;
use AppBundle\Form\Selection\{NewsStatusSelection, NewsTypeSelection, RubricSelection};
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class NewsProfileFilterType extends FilterType
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authChecker = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('title', null, [
            'required' => false,
            'translation_domain' => 'dictionary',
        ])
        ->add('status', ChoiceType::class, [
            'required' => false,
            'placeholder' => 'all_choices',
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
            'choices' => NewsStatusSelection::choices(),
        ])
        ->add('type', ChoiceType::class, [
            'required' => false,
            'expanded' => true,
            'multiple' => true,
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
            'choices' => NewsTypeSelection::choices(),
        ])
        ->add('rubric', ChoiceType::class, [
            'required' => false,
            'expanded' => true,
            'multiple' => true,
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
            'choices' => RubricSelection::choices(),
        ]);

        if ($this->authChecker->isGranted("ROLE_NEWS_EDITOR")) {
            $builder->add('university', UniversityEntityType::class, [
                'required' => false,
                'multiple' => false,
                'locale' => $options['locale'],
            ]);
        }
    }
}
