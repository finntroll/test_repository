<?php

namespace AppBundle\Form\News;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use AppBundle\Entity\{Keyword, News, NewsType as NType};
use AppBundle\Form\Common\{ImageCropperType, UploadedDocumentCollectionType};
use AppBundle\Form\ReusableType\UniversityEntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class NewsType extends AbstractType
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $checker;

    public function __construct(AuthorizationCheckerInterface $checker)
    {
        $this->checker = $checker;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('translations', TranslationsType::class, [
            'translation_domain' => 'dictionary',
            'fields' => [
                'title' => [],
                'lead' => [],
                'content' => [
                    'field_type' => CKEditorType::class,
                ],
                'videoCode' => [
                    'required' => false
                ],
                'sourceLink' => [
                    'required' => false
                ]
            ],
        ])
        ->add('rubric', null, [
            'placeholder' => 'none',
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
        ])
        ->add('keywords', Select2EntityType::class, [
            'translation_domain' => 'dictionary',
            'multiple' => true,
            'remote_route' => 'keywords',
            'class' => Keyword::class,
            'primary_key' => 'id',
            'required' => true,
            'text_property' => 'name',
            'allow_add' => [
                'new_tag_text' => ' (NEW)',
                'new_tag_prefix' => '__',
                'enabled' => true,
                'tag_separators' => '[","]',
            ],
        ])
        ->add('image', ImageCropperType::class, [
            'required' => false,
            'error_bubbling' => false,
        ])
        ->add('uploadedDocuments', UploadedDocumentCollectionType::class)
        ->add('submit', SubmitType::class);

        if ($this->checker->isGranted('CAN_CREATE_NEWS')) {
            $builder->add('type', EntityType::class, [
                'placeholder' => 'none',
                'class' => NType::class,
                'translation_domain' => 'dictionary',
                'choice_translation_domain' => 'dictionary',
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('n')
                        ->where('n.id != :type')
                        ->setParameter('type', NType::MASS_MEDIA);
                }
            ]);
            $builder->add('university', UniversityEntityType::class, [
                'required' => true,
                'multiple' => false,
                'locale' => $options['locale'],
            ]);
        }

        if ($builder->getData()->isDraft() && !$this->checker->isGranted('CAN_PUBLISH_NEWS', $builder->getData())) {

            $builder->add('submitAndModerate', SubmitType::class, [
                'translation_domain' => 'dictionary',
            ]);
        }

        if (!$builder->getData()->isPublished() && $this->checker->isGranted('CAN_PUBLISH_NEWS', $builder->getData())) {
            $builder->add('submitAndPublish', SubmitType::class);
        }

        if ($this->checker->isGranted('ROLE_NEWS_EDITOR', $builder->getData())) {
            $builder->add('isImportant', null, [
                'translation_domain' => 'dictionary',
                'required' => false,
                'label' => 'news.is_important',
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
            'locale' => 'ru',
        ]);
    }
}
