<?php

namespace AppBundle\Form\Partner;

use AppBundle\Form\Common\FilterType;
use AppBundle\Form\Selection\PartnerStatusSelection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class PartnerFilterType extends FilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('name', null, [
            'required' => false,
            'translation_domain' => 'dictionary',
            'label' => 'partner.name',
        ])
        ->add('status', ChoiceType::class, [
            'required' => false,
            'placeholder' => 'all_choices',
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
            'choices' => PartnerStatusSelection::choices(),
        ]);
    }
}
