<?php

namespace AppBundle\Form\Partner;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Entity\Partner;
use AppBundle\Form\Common\ImageCropperType;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class PartnerType extends AbstractType
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $checker;

    public function __construct(AuthorizationCheckerInterface $checker)
    {
        $this->checker = $checker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('translations', TranslationsType::class, [
            'translation_domain' => 'dictionary',
            'label' => false,
            'fields' => [
                'name' => [
                    'label' => 'partner.name'
                ],
                'shortName' => [
                    'label' => 'partner.short_name'
                ],
                'url' => [
                    'label' => 'partner.url'
                ],
            ],
        ])
        ->add('isForeign', null, [
           'translation_domain' => 'dictionary'
        ])
        ->add('country', null, [
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary'
        ])
        ->add('logo', ImageCropperType::class, [
            'required' => false,
            'error_bubbling' => false,
        ])
        ->add('submit', SubmitType::class, [
            'translation_domain' => 'dictionary',
            'attr' => [
                'class' => 'side-button blue-button default-button',
            ],
        ]);

        if ($builder->getData()->isDraft() && !$this->checker->isGranted('CAN_PUBLISH_PARTNER', $builder->getData())) {

            $builder->add('submitAndModerate', SubmitType::class, [
                'translation_domain' => 'dictionary',
            ]);
        }

        if (!$builder->getData()->isPublished() && $this->checker->isGranted('CAN_PUBLISH_PARTNER', $builder->getData())) {
            $builder->add('submitAndPublish', SubmitType::class, [
                'translation_domain' => 'dictionary',
                'attr' => [
                    'class' => 'side-button pink-button default-button'
                ]
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Partner::class
        ]);
    }
}
