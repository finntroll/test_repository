<?php

namespace AppBundle\Form\Personality;

use AppBundle\Form\Common\FilterType;
use AppBundle\Form\ReusableType\PartnerEntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class PersonalityProfileFilterType extends FilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('nameOrPosition', TextType::class, [
            'label' => 'profile.personality.name_or_position',
            'translation_domain' => 'dictionary',
            'attr' => [
                'placeholder' => 'profile.personality.enter_name_or_position',
            ],
        ])
        ->add('workPlace', PartnerEntityType::class, [
            'multiple' => true,
            'load_all_when_empty' => true,
            'required' => false,
        ]);
    }
}
