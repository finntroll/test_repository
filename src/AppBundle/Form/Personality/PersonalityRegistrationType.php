<?php

namespace AppBundle\Form\Personality;

use Symfony\Component\Form\Extension\Core\Type\{PasswordType, RepeatedType};
use Symfony\Component\Form\FormBuilderInterface;

class PersonalityRegistrationType extends PersonalityType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->remove('email')
            ->remove('submitAndInvite')
            ->remove('associationPosition');

        $builder->add('plainPassword', RepeatedType::class, [
            'type' => PasswordType::class,
            'required' => true,
            'invalid_message' => 'profile.settings.password_not_equals',
            'first_options'  => array('label' => 'New password'),
            'second_options' => array('label' => 'Repeat new password'),
        ]);
    }
}
