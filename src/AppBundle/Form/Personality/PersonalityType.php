<?php

namespace AppBundle\Form\Personality;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Presta\ImageBundle\Model\AspectRatio;
use AppBundle\Entity\{AssociationPosition, Personality};
use AppBundle\Form\Common\ImageCropperType;
use AppBundle\Form\ReusableType\PartnerEntityType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonalityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, [
            'translation_domain' => 'dictionary',
            'required' => true,
        ])
        ->add('associationPosition', EntityType::class, [
            'placeholder' => 'none',
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
            'class' => AssociationPosition::class,
            'required' => true,
        ])
        ->add('translations', TranslationsType::class, [
            'fields' => [
                'firstName' => [
                    'required' => true,
                ],
                'surname' => [
                    'required' => true,
                ],
                'patronymic' => [
                    'required' => false,
                ],
                'professionalPosition' => [
                    'required' => false,
                ],
            ],
            'translation_domain' => 'dictionary',
            'label' => false,
        ])
        ->add('photo', ImageCropperType::class, [
            'aspect_ratios' => [new AspectRatio('0.83', 'photo')],
        ])
        ->add('workPlace', PartnerEntityType::class, [
            'multiple' => false,
            'load_all_when_empty' => true,
            'required' => false,
        ])
        ->add('phone', TextType::class, [
            'translation_domain' => 'dictionary',
            'attr' => ['custom-phone-mask-input' => 'on'],
            'required' => false,
        ])
        ->add('link', UrlType::class, [
            'translation_domain' => 'dictionary',
            'label' => 'personality.link',
            'required' => false,
        ])
        ->add('submit', SubmitType::class, [
            'translation_domain' => 'dictionary',
            'attr' => [
                'class' => 'side-button submit-button pink-button default-button',
            ],
        ]);

        $personality = $builder->getData();

        if ($personality instanceof Personality && !$personality->isEnabled()) {
            $builder->add('submitAndInvite', SubmitType::class, [
                'label' => 'profile.personality.submit_and_invite',
                'attr' => [
                    'class' => 'side-button submit-button blue-button default-button',
                ],
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Personality::class,
        ]);
    }
}
