<?php

namespace AppBundle\Form\Program;

use AppBundle\Entity\{EducationForm, EducationLevel, KnowledgeArea};
use AppBundle\Form\Common\FilterType;
use AppBundle\Form\Event\{AddBranchCitiesFieldSubscriber, AddEntranceTestFieldSubscriber, AddKnowledgeAreaItemFieldSubscriber};
use AppBundle\Form\ReusableType\{UniversityEntityType, LanguageEntityType};
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;

class ProgramFilterType extends FilterType
{
    /**
     * @var AddKnowledgeAreaItemFieldSubscriber
     */
    private $kaiSubscriber;

    /**
     * @var AddEntranceTestFieldSubscriber
     */
    private $testSubscriber;

    /**
     * @var AddBranchCitiesFieldSubscriber
     */
    private $branchSubscriber;

    public function __construct(AddKnowledgeAreaItemFieldSubscriber $kaiSubscriber, AddEntranceTestFieldSubscriber $testSubscriber, AddBranchCitiesFieldSubscriber $branchSubscriber)
    {
        $this->kaiSubscriber = $kaiSubscriber;
        $this->testSubscriber = $testSubscriber;
        $this->branchSubscriber = $branchSubscriber;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('name', null, [
            'required' => false,
        ])
        ->add('knowledgeArea', EntityType::class, [
            'class' => KnowledgeArea::class,
            'placeholder' => 'all_choices',
            'required' => false,
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
        ])
        ->add('educationForm', EntityType::class, [
            'class' => EducationForm::class,
            'placeholder' => 'all_choices',
            'required' => false,
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
        ])
        ->add('educationLevel', EntityType::class, [
            'class' => EducationLevel::class,
            'placeholder' => 'all_choices',
            'required' => false,
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
        ])
        ->add('university', UniversityEntityType::class, [
            'required' => false,
            'multiple' => true,
            'locale' => $options['locale'],
        ])
        ->addEventSubscriber($this->branchSubscriber->configureOptions([
            'required' => false,
            'multiple' => true,
            'load_all_when_empty' => true,
            'locale' => $options['locale']
            ]))
        ->add('hasBudgetPlaces', CheckboxType::class, [
            'required' => false,
            'label' => 'Has budget places',
            'translation_domain' => 'dictionary',
        ])
        ->add('hasForeignPlaces', CheckboxType::class, [
            'required' => false,
            'label' => 'Has foreign places',
            'translation_domain' => 'dictionary',
        ])
        ->add('hasDoubleGraduate', CheckboxType::class, [
            'required' => false,
            'label' => 'Double diploma',
            'translation_domain' => 'dictionary'
        ])
        ->add('hasMilitaryDepartment', CheckboxType::class, [
            'required' => false,
            'translation_domain' => 'dictionary'
        ])
        ->add('priceFrom', null, ['required' => false,])
        ->add('priceTo', null, ['required' => false,])
        ->add('languages', LanguageEntityType::class, [
            'required' => false,
            'multiple' => true,
        ]);

        $builder->addEventSubscriber($this->kaiSubscriber->configureOptions(['multiple' => true, 'required' => false]))
                ->addEventSubscriber($this->testSubscriber->configureOptions(['multiple' => true, 'required' => false]));
    }
}
