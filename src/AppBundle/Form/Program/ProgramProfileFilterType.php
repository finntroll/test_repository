<?php

namespace AppBundle\Form\Program;

use AppBundle\Entity\{EducationForm, EducationLevel, KnowledgeArea};
use AppBundle\Form\Event\{AddBranchCitiesFieldSubscriber, AddKnowledgeAreaItemFieldSubscriber};
use AppBundle\Form\Common\FilterType;
use AppBundle\Form\ReusableType\{LanguageEntityType, UniversityBranchCitiesType, UniversityEntityType};
use AppBundle\Form\Selection\ProgramStatusSelection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\{CheckboxType, ChoiceType};
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ProgramProfileFilterType extends FilterType
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    /**
     * @var AddKnowledgeAreaItemFieldSubscriber
     */
    private $kaiSubscriber;

    /**
     * @var AddBranchCitiesFieldSubscriber
     */
    private $branchSubscriber;

    public function __construct(AuthorizationCheckerInterface $authChecker, AddKnowledgeAreaItemFieldSubscriber $kaiSubscriber, AddBranchCitiesFieldSubscriber $branchSubscriber)
    {
        $this->authChecker = $authChecker;
        $this->kaiSubscriber = $kaiSubscriber;
        $this->branchSubscriber = $branchSubscriber;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('branchCities', UniversityBranchCitiesType::class, [
            'multiple' => true,
            'required' => false,
            'attr' => ['class' => 'select2-apply'],
            'university' => $options['user']->getUniversity(),
            'load_all_when_empty' => true,
            'locale' => $options['locale'],
        ])
        ->add('name', null, ['required' => false,])
        ->add('isPublished', ChoiceType::class, [
            'label' => 'Status',
            'placeholder' => 'all_choices',
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
            'choices' => ProgramStatusSelection::choices(),
        ])
        ->add('knowledgeArea',EntityType::class, [
            'class' => KnowledgeArea::class,
            'placeholder' => 'all_choices',
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
        ])
        ->add('educationForm', EntityType::class, [
            'class' => EducationForm::class,
            'multiple' => true,
            'expanded' => true,
            'placeholder' => 'all_choices',
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
        ])
        ->add('educationLevel', EntityType::class, [
            'class' => EducationLevel::class,
            'multiple' => true,
            'expanded' => true,
            'placeholder' => 'all_choices',
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
        ])
        ->add('hasBudgetPlaces', CheckboxType::class, [
            'label' => 'Has budget places',
            'translation_domain' => 'dictionary',
        ])
        ->add('hasDoubleGraduate', CheckboxType::class, [
            'label' => 'Double diploma',
            'translation_domain' => 'dictionary'
        ])
        ->add('hasMilitaryDepartment', CheckboxType::class, [
            'translation_domain' => 'dictionary'
        ])
        ->add('priceFrom')
        ->add('priceTo')
        ->add('languages', LanguageEntityType::class, [
            'expanded' => true,
            'required' => false,
        ]);

        if ($this->authChecker->isGranted(['ROLE_SITE_ADMIN', 'ROLE_GLOBAL_UNIVERSITY_ADMIN'])) {
            $builder->add('university', UniversityEntityType::class, [
                'locale' => $options['locale'],
                'multiple' => true,
            ]);

            $builder->addEventSubscriber($this->branchSubscriber->configureOptions([
                'required' => false,
                'multiple' => true,
                'load_all_when_empty' => true,
                'locale' => $options['locale'],
            ]));
        }

        $builder->addEventSubscriber($this->kaiSubscriber->configureOptions(['multiple' => true, 'required' => false]));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setRequired(['user']);
    }
}
