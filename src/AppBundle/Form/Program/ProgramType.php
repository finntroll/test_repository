<?php

namespace AppBundle\Form\Program;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Entity\{EducationForm, EducationLevel, KnowledgeArea, Program, User};
use AppBundle\Form\Event\{AddBranchFieldSubscriber, AddEntranceTestFieldSubscriber, AddKnowledgeAreaItemFieldSubscriber, AddPartnerFieldSubscriber};
use AppBundle\Form\ReusableType\{LanguageEntityType, PartnerEntityType, UniversityBranchEntityType, UniversityEntityType};
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\{SubmitType, TextType};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ProgramType extends AbstractType
{
    /**
     * @var AddKnowledgeAreaItemFieldSubscriber
     */
    private $kaiSubscriber;

    /**
     * @var AddEntranceTestFieldSubscriber
     */
    private $testSubscriber;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    /**
     * @var AddBranchFieldSubscriber
     */
    private $branchSubscriber;

    /**
     * @var AddPartnerFieldSubscriber
     */
    private $partnerSubscriber;

    public function __construct(AuthorizationCheckerInterface $authChecker, AddKnowledgeAreaItemFieldSubscriber $kaiSubscriber, AddEntranceTestFieldSubscriber $testSubscriber, AddBranchFieldSubscriber $branchSubscriber, AddPartnerFieldSubscriber $partnerSubscriber)
    {
        $this->authChecker = $authChecker;
        $this->kaiSubscriber = $kaiSubscriber;
        $this->testSubscriber = $testSubscriber;
        $this->branchSubscriber = $branchSubscriber;
        $this->partnerSubscriber = $partnerSubscriber;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /**
         * @var User $user
         */
        $user = $options['user'];

        if ($this->authChecker->isGranted(['ROLE_SITE_ADMIN', 'ROLE_GLOBAL_UNIVERSITY_ADMIN'])) {
            $builder->add('university', UniversityEntityType::class, [
                'locale' => $options['locale'],
            ]);

            $builder->addEventSubscriber($this->branchSubscriber->configureOptions(['required' => true]));
            $builder->addEventSubscriber($this->partnerSubscriber->configureOptions(['multiple' => true, 'required' => false]));
        }

        $builder->add('translations', TranslationsType::class, [
            'translation_domain' => 'dictionary',
            'label' => false,
        ])
        ->add('universityBranch', UniversityBranchEntityType::class, [
            'required' => true,
            'university' => $user->getUniversity(),
        ])
        ->add('partners', PartnerEntityType::class, [
            'required' => false,
            'university' => $user->getUniversity(),
        ])
        ->add('educationForm', EntityType::class, [
            'class' => EducationForm::class,
            'placeholder' => 'none',
            'choice_translation_domain' => 'dictionary',
            'translation_domain' => 'dictionary',
        ])
        ->add('educationLevel', EntityType::class, [
            'class' => EducationLevel::class,
            'placeholder' => 'none',
            'choice_translation_domain' => 'dictionary',
            'translation_domain' => 'dictionary',
        ])
        ->add('duration', TextType::class, [
            'translation_domain' => 'dictionary',
        ])
        ->add('knowledgeArea', EntityType::class, [
            'class' => KnowledgeArea::class,
            'placeholder' => 'none',
            'multiple' => false,
            'choice_translation_domain' => 'dictionary',
            'translation_domain' => 'dictionary',
        ])
        ->add('budgetPlaces', TextType::class, [
            'required' => false,
            'translation_domain' => 'dictionary',
        ])
        ->add('entranceScore', TextType::class, [
            'required' => false,
            'translation_domain' => 'dictionary',
        ])
        ->add('contractPlaces', TextType::class, [
            'required' => false,
            'translation_domain' => 'dictionary',
        ])
        ->add('price', TextType::class, [
            'required' => false,
            'translation_domain' => 'dictionary',
        ])
        ->add('foreignPlaces', TextType::class, [
            'required' => false,
            'translation_domain' => 'dictionary',
        ])
        ->add('foreignPrice', TextType::class, [
            'required' => false,
            'translation_domain' => 'dictionary',
        ])
        ->add('languages', LanguageEntityType::class, [
            'required' => true,
            'expanded' => true,
        ])
        ->add('doubleDiploma', null, [
            'translation_domain' => 'dictionary',
        ])
        ->add('exchangeStudy', null, [
            'translation_domain' => 'dictionary',
        ])
        ->add('hasMilitaryDepartment', null, [
            'translation_domain' => 'dictionary',
        ])
        ->add('submitAndPublish', SubmitType::class)
        ->add('submit', SubmitType::class);

        $builder->addEventSubscriber($this->kaiSubscriber->configureOptions(['required' => true]))
                ->addEventSubscriber($this->testSubscriber->configureOptions(['multiple' => true, 'expanded' => true]));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Program::class,
            'user' => null,
            'locale' => 'ru',
            'attr' => ['class'=>'app-form-field-mapping'],
        ]);
    }
}
