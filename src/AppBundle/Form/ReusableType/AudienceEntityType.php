<?php

namespace AppBundle\Form\ReusableType;

use AppBundle\Entity\Audience;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\{Options, OptionsResolver};

class AudienceEntityType extends EntityType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'class' => Audience::class,
            'placeholder' => 'none',
            'label' => 'Audiences',
            'choice_translation_domain' => 'dictionary',
            'translation_domain' => 'dictionary',
            'expanded' => false,
            'multiple' => true,
            'required' => false,
            'exclude' => null,
            'query_builder' => function (Options $options) {
                return $this->registry->getRepository('AppBundle:Audience')->createQueryBuilder('a')->andWhere('a.id NOT IN (:ids)')
                    ->setParameter('ids', implode(',', Audience::getIdByTypes($options['exclude'])));
            },
        ]);
    }

}
