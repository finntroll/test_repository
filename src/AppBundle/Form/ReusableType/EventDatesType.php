<?php

namespace AppBundle\Form\ReusableType;

use AppBundle\Form\Common\DatePickerType;
use AppBundle\Form\Selection\PeriodSelection;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface, FormInterface, FormView};
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventDatesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateBegin', DatePickerType::class, [
                'attr' => ['class' => 'date-start'],
                'required' => false,
                'translation_domain' => 'dictionary',
            ])
            ->add('dateEnd', DatePickerType::class, [
                'attr' => ['class' => 'date-end'],
                'required' => false,
                'translation_domain' => 'dictionary',
            ]);

        // Optional fields
        if ($options['has_reception_date']) {
            $builder->add('dateReceptionEnd', DatePickerType::class, [
                'attr' => ['class' => 'date-before-start'],
                'required' => false,
                'translation_domain' => 'dictionary',
            ]);
        }

        if ($options['has_duration']) {
            $builder->add('duration', null, [
                    'required' => false,
                ])
                ->add('durationType', ChoiceType::class, [
                    'choices' => PeriodSelection::choices(),
                    'placeholder' => 'none',
                    'label' => false,
                    'required' => false,
                    'choice_translation_domain' => 'dictionary',
                    'translation_domain' => 'dictionary',
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'inherit_data' => true,
            'label' => false,
            'has_reception_date' => true,
            'has_duration' => false,
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars['has_duration'] = $options['has_duration'];
        $view->vars['has_reception_date'] = $options['has_reception_date'];
    }
}
