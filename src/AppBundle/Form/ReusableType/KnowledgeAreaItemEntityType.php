<?php

namespace AppBundle\Form\ReusableType;

use AppBundle\Entity\KnowledgeAreaItem;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\{FormInterface, FormView};
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class KnowledgeAreaItemEntityType extends EntityType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'class' => KnowledgeAreaItem::class,
            'placeholder' => function (Options $options) {
                return $options['required'] ? 'none' : 'all_choices';
            },
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
            'attr' => ['class' => 'select2-apply'],
            'multiple' => false,
            'load_all_when_empty' => false,
            'knowledge_area' => null,
            'query_builder' => function (Options $options) {
                return $this->registry->getRepository('AppBundle:KnowledgeAreaItem')->findByKnowledgeArea($options['knowledge_area'], $options['load_all_when_empty']);
            }
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars['load_all_when_empty'] = $options['load_all_when_empty'];
    }
}
