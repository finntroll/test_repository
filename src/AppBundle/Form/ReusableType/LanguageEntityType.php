<?php

namespace AppBundle\Form\ReusableType;

use AppBundle\Entity\Language;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\{Options, OptionsResolver};

class LanguageEntityType extends EntityType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'class' => Language::class,
            'multiple' => true,
            'translation_domain' => 'dictionary',
            'choice_translation_domain' => 'dictionary',
            'query_builder' => $this->registry->getRepository('AppBundle:Language')->loadChoices(),
        ]);

        $resolver->setDefault('attr', function (Options $options) {
            return !$options['expanded'] ? ['class' => 'select2-apply'] : [];
        });
    }
}
