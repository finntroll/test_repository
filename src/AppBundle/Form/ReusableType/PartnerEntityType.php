<?php

namespace AppBundle\Form\ReusableType;

use AppBundle\Entity\{Partner, University};
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\{FormInterface, FormView};
use Symfony\Component\OptionsResolver\{Options, OptionsResolver};

class PartnerEntityType extends EntityType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'attr' => [
                'class' => 'select2-apply',
            ],
            'class' => Partner::class,
            'placeholder' => 'none',
            'university' => null,
            'translation_domain' => 'dictionary',
            'locale' => 'ru',
            'multiple' => true,
            'load_all_when_empty' => false,
            'query_builder' => function (Options $options) {
                return $this->registry->getRepository('AppBundle:Partner')
                    ->findByUniversity($options['university'], $options['locale'], $options['load_all_when_empty']);
            }
        ]);

        $resolver->setAllowedTypes('university', ['null', University::class, 'numeric']);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars['load_all_when_empty'] = $options['load_all_when_empty'];
    }
}
