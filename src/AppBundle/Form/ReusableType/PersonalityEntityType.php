<?php

namespace AppBundle\Form\ReusableType;

use AppBundle\Entity\Personality;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class PersonalityEntityType extends Select2EntityType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'class' => Personality::class,
            'placeholder' => 'none',
            'remote_route' => 'personalities',
            'primary_key' => 'id',
            'text_property' => 'fullNameWithWorkPlace',
            'autostart' => true,
            'attr' => [
                'class' => 'select2-apply',
            ],
            'translation_domain' => 'dictionary',
        ]);
    }
}
