<?php

namespace AppBundle\Form\ReusableType;

use AppBundle\Entity\{City, University};
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\{FormInterface, FormView};
use Symfony\Component\OptionsResolver\{Options, OptionsResolver};

class UniversityBranchCitiesType extends EntityType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'class' => City::class,
            'placeholder' => 'all_choices',
            'translation_domain' => 'dictionary',
            'locale' => 'ru',
            'university' => null,
            'load_all_when_empty' => false,
            'query_builder' => function (Options $options) {
                return $this->registry->getRepository('AppBundle:City')->findByUniversity($options['university'], $options['locale'], $options['load_all_when_empty']);
            },
        ]);

        $resolver->setAllowedTypes('university', ['null', 'array', University::class, 'numeric']);
        $resolver->setAllowedTypes('load_all_when_empty', 'bool');
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars['load_all_when_empty'] = $options['load_all_when_empty'];
    }
}
