<?php

namespace AppBundle\Form\ReusableType;

use AppBundle\Entity\{University, UniversityBranch};
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\{FormInterface, FormView};
use Symfony\Component\OptionsResolver\{Options, OptionsResolver};

class UniversityBranchEntityType extends EntityType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'choice_label' => 'getCity',
            'translation_domain' => 'dictionary',
            'placeholder' => 'none',
            'university' => null,
            'load_all_when_empty' => false,
            'class' => UniversityBranch::class,
            'query_builder' => function (Options $options) {
                return $this->registry
                    ->getRepository('AppBundle:UniversityBranch')
                    ->findByUniversity($options['university'], $options['load_all_when_empty']);
            }
        ]);

        $resolver->setAllowedTypes('university', ['null', 'array', University::class, 'numeric']);
        $resolver->setAllowedTypes('load_all_when_empty', ['bool']);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars['load_all_when_empty'] = $options['load_all_when_empty'];
    }

}
