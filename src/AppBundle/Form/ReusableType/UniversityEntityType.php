<?php

namespace AppBundle\Form\ReusableType;

use AppBundle\Entity\University;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\{Options, OptionsResolver};

class UniversityEntityType extends EntityType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'class' => University::class,
            'placeholder' => 'all_choices',
            'translation_domain' => 'dictionary',
            'attr' => ['class' => 'select2-apply'],
            'locale' => 'ru',
            'query_builder' => function (Options $options) {
                // we must return callable
                return $this->registry->getRepository('AppBundle:University')->loadChoices(['locale' => $options['locale']]);
            }
        ]);

        $resolver->setRequired(['locale']);
    }
}
