<?php

namespace AppBundle\Form\Selection;

abstract class AbstractSelection
{
    /**
     * Array with all possible selections
     *
     * @return array
     */
    abstract public static function all();

    /**
     * Array with translations for label
     *
     * @return array
     */
    abstract public static function trans();

    /**
     * Array with values. Default return translations as values
     *
     * @return array
     */
    public static function values()
    {
        return self::trans();
    }

    /**
     * Choices for usage in forms
     *
     * @return array|bool
     */
    public static function choices()
    {
        return array_flip(static::trans());
    }

    /**
     * Get value (translation by default) for choice
     *
     * @param $choice
     * @return mixed
     */
    public static function getValue($choice)
    {
        $values = static::values();
        if(!isset($values[$choice])) {
            return null;
        } else {
            return $values[$choice];
        }
    }
}
