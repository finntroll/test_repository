<?php

namespace AppBundle\Form\Selection;

class BestPracticeStatusSelection extends AbstractSelection
{
    const DRAFT = 1;
    const MODERATING = 2;
    const PUBLISHED = 3;

    /**
     * Array with all possible selections
     *
     * @return array
     */
    public static function all()
    {
        return [
            self::DRAFT,
            self::MODERATING,
            self::PUBLISHED
        ];
    }

    /**
     * Array with translations for label
     *
     * @return array
     */
    public static function trans()
    {
        return [
            self::DRAFT => 'best_practice.status.draft',
            self::MODERATING => 'best_practice.status.moderating',
            self::PUBLISHED => 'best_practice.status.published',
        ];
    }

    public static function values()
    {
        return [
            self::DRAFT => 1,
            self::MODERATING => 2,
            self::PUBLISHED => 3,
        ];
    }
}
