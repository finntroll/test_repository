<?php

namespace AppBundle\Form\Selection;

class MobilitySelection extends AbstractSelection
{
    const INTERNSHIPS = 'internships';
    const POSTDOC_PROGRAMS = 'postdoc';
    const TRAINING = 'training';
    const SUMMER_AND_WINTER_SCHOOLS = 'schools';
    const CONFERENCES = 'conferences';
    const SEMINARS = 'seminars';
    const LECTURES_AND_MEETINGS = 'lectures';
    const MASTER_CLASSES = 'master_class';
    const WEBINAR_AND_ONLINE_BROADCASTS = 'online';

    public static function all()
    {
        return [
            static::INTERNSHIPS,
            static::POSTDOC_PROGRAMS,
            static::TRAINING,
            static::SUMMER_AND_WINTER_SCHOOLS,
            static::CONFERENCES,
            static::SEMINARS,
            static::LECTURES_AND_MEETINGS,
            static::MASTER_CLASSES,
            static::WEBINAR_AND_ONLINE_BROADCASTS,
        ];
    }

    public static function trans()
    {
        return [
            static::INTERNSHIPS => 'profile.mobility.internship',
            static::POSTDOC_PROGRAMS => 'profile.mobility.postdoc',
            static::TRAINING => 'profile.mobility.training',
            static::SUMMER_AND_WINTER_SCHOOLS => 'profile.mobility.schools',
            static::CONFERENCES => 'profile.mobility.conferences',
            static::SEMINARS => 'profile.mobility.seminars',
            static::LECTURES_AND_MEETINGS => 'profile.mobility.lectures',
            static::MASTER_CLASSES => 'profile.mobility.master_classes',
            static::WEBINAR_AND_ONLINE_BROADCASTS => 'profile.mobility.online',
        ];
    }

    public static function values()
    {
        return [
            static::INTERNSHIPS => 1,
            static::POSTDOC_PROGRAMS => 2,
            static::TRAINING => 3,
            static::SUMMER_AND_WINTER_SCHOOLS => 4,
            static::CONFERENCES => 5,
            static::SEMINARS => 6,
            static::LECTURES_AND_MEETINGS => 7,
            static::MASTER_CLASSES => 8,
            static::WEBINAR_AND_ONLINE_BROADCASTS => 9,
        ];
    }
}
