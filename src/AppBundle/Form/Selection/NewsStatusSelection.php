<?php

namespace AppBundle\Form\Selection;

class NewsStatusSelection extends AbstractSelection
{
    const DRAFT = 1;
    const MODERATING = 2;
    const PUBLISHED = 3;

    /**
     * Array with all possible selections
     *
     * @return array
     */
    public static function all()
    {
        return [
            self::DRAFT,
            self::MODERATING,
            self::PUBLISHED
        ];
    }

    /**
     * Array with translations for label
     *
     * @return array
     */
    public static function trans()
    {
        return [
            self::DRAFT => 'news.status.draft',
            self::MODERATING => 'news.status.moderating',
            self::PUBLISHED => 'news.status.published',
        ];
    }

    public static function values()
    {
        return [
            self::DRAFT => 1,
            self::MODERATING => 2,
            self::PUBLISHED => 3,
        ];
    }
}
