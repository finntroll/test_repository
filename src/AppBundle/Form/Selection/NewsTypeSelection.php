<?php

namespace AppBundle\Form\Selection;

class NewsTypeSelection extends AbstractSelection
{
    const TYPE_UNIVERSITY = 1;
    const TYPE_UNIVERSITY_TITLE = 'news_type.university_news';
    const TYPE_ASSOCIATION = 2;
    const TYPE_ASSOCIATION_TITLE = 'news_type.association_news';
    const TYPE_MASS_MEDIA = 3;
    const TYPE_MASS_MEDIA_TITLE = 'news_type.in_mass_media_news';

    /**
     * Array with all possible selections
     *
     * @return array
     */
    public static function all()
    {
        return [
            static::TYPE_UNIVERSITY,
            static::TYPE_ASSOCIATION,
            //static::TYPE_MASS_MEDIA,
        ];
    }

    /**
     * Array with translations for label
     *
     * @return array
     */
    public static function trans()
    {
        return [
            static::TYPE_UNIVERSITY => static::TYPE_UNIVERSITY_TITLE,
            static::TYPE_ASSOCIATION => static::TYPE_ASSOCIATION_TITLE,
            //static::TYPE_MASS_MEDIA => static::TYPE_MASS_MEDIA_TITLE,
        ];
    }
}
