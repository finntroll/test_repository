<?php

namespace AppBundle\Form\Selection;

class PeriodSelection extends AbstractSelection
{
    const DAY = 'day';
    const WEEK = 'week';
    const MONTH = 'month';

    public static function all()
    {
        return [self::DAY, self::WEEK, self::MONTH];
    }

    public static function trans()
    {
        return [
            self::DAY => 'day',
            self::WEEK => 'week',
            self::MONTH => 'month',
        ];
    }

    public static function values()
    {
        return [
            self::DAY => 1,
            self::WEEK => 2,
            self::MONTH => 3,
        ];
    }
}
