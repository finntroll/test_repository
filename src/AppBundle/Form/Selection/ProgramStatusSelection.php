<?php

namespace AppBundle\Form\Selection;

class ProgramStatusSelection extends AbstractSelection
{
    const DRAFT = 0;
    const PUBLISHED = 1;

    /**
     * Array with all possible selections
     *
     * @return array
     */
    public static function all()
    {
        return [
            self::DRAFT,
            self::PUBLISHED
        ];
    }

    /**
     * Array with translations for label
     *
     * @return array
     */
    public static function trans()
    {
        return [
            self::DRAFT => 'program.status.draft',
            self::PUBLISHED => 'program.status.published',
        ];
    }

    public static function values()
    {
        return [
            self::DRAFT => 0,
            self::PUBLISHED => 1,
        ];
    }
}
