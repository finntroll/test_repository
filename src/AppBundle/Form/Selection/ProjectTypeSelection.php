<?php

namespace AppBundle\Form\Selection;

class ProjectTypeSelection extends AbstractSelection
{
    const GRANTS = 'grants';
    const CONTEST_INTERNATIONAL = 'contest_international';
    const CONTEST_NIOKR = 'contest_niokr';

    public static function all()
    {
        return [
            self::GRANTS,
            self::CONTEST_INTERNATIONAL,
            self::CONTEST_NIOKR,
        ];
    }

    public static function trans()
    {
        return [
            self::GRANTS => 'contest_type.contest_grants',
            self::CONTEST_INTERNATIONAL => 'contest_type.contest_international',
            self::CONTEST_NIOKR => 'contest_type.contest_niokr',
        ];
    }

    public static function values()
    {
        return [
            self::GRANTS => 1,
            self::CONTEST_INTERNATIONAL => 2,
            self::CONTEST_NIOKR => 3,
        ];
    }
}
