<?php

namespace AppBundle\Form\Selection;

class RubricSelection extends AbstractSelection
{
    const RUBRIC_SCIENCE         = 1;
    const RUBRIC_SCIENCE_TITLE   = 'rubric.science';
    const RUBRIC_STARTUPS        = 2;
    const RUBRIC_STARTUPS_TITLE  = 'rubric.startup_and_business';
    const RUBRIC_IT              = 3;
    const RUBRIC_IT_TITLE        = 'rubric.it';
    const RUBRIC_EDUCATION       = 4;
    const RUBRIC_EDUCATION_TITLE = 'rubric.education';

    /**
     * Array with all possible selections
     *
     * @return array
     */
    public static function all()
    {
        return [static::RUBRIC_SCIENCE, static::RUBRIC_STARTUPS, static::RUBRIC_IT, static::RUBRIC_EDUCATION];
    }

    /**
     * Array with translations for label
     *
     * @return array
     */
    public static function trans()
    {
        return [
            static::RUBRIC_SCIENCE => static::RUBRIC_SCIENCE_TITLE,
            static::RUBRIC_STARTUPS => static::RUBRIC_STARTUPS_TITLE,
            static::RUBRIC_IT => static::RUBRIC_IT_TITLE,
            static::RUBRIC_EDUCATION => static::RUBRIC_EDUCATION_TITLE,
        ];
    }

}
