<?php

namespace AppBundle\Form\Selection;

class UserRolesSelection extends AbstractSelection
{
    /**
     * Array with all possible selections
     *
     * @return array
     */
    public static function all()
    {
        return [
            'ROLE_SITE_ADMIN',
            'ROLE_UNIVERSITY_NEWS_USER',
            'ROLE_GLOBAL_UNIVERSITY_ADMIN',
            'ROLE_UNIVERSITY_ADMIN',
            'ROLE_UNIVERSITY_EDITOR',
            'ROLE_UNIVERSITY_EVENTS_EDITOR',
            'ROLE_NEWS_EDITOR',
            'ROLE_UNIVERSITY_PROGRAM_EDITOR',
        ];
    }

    /**
     * Array with translations for label
     *
     * @return array
     */
    public static function trans()
    {
        return self::values();
    }

    /**
     * @return array
     */
    public static function values()
    {
        return [
            'ROLE_SITE_ADMIN'                 => 'role_site_admin',
            'ROLE_UNIVERSITY_NEWS_USER'       => 'role_university_news_user',
            'ROLE_GLOBAL_UNIVERSITY_ADMIN'    => 'role_global_university_admin',
            'ROLE_UNIVERSITY_ADMIN'           => 'role_university_admin',
            'ROLE_UNIVERSITY_EDITOR'          => 'role_university_editor',
            'ROLE_UNIVERSITY_EVENTS_EDITOR'   => 'role_university_events_editor',
            'ROLE_NEWS_EDITOR'                => 'role_news_editor',
            'ROLE_UNIVERSITY_PROGRAM_EDITOR'  => 'role_university_program_editor',
        ];
    }
}
