<?php

namespace AppBundle\Form\StaticPage;

use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class HighlightType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('number', null, [
            'label' => 'static.highlights.number',
            'translation_domain' => 'dictionary',
        ])
        ->add('description', TextareaType::class, [
            'label' => 'static.highlights.description',
            'translation_domain' => 'dictionary',
        ])
        ->add('delete', ButtonType::class, [
            'attr' => [
                'class' => 'highlight-delete pink-button default-button btn ladda-button',
            ],
            'label' => 'static.highlights.delete',
            'translation_domain' => 'dictionary',
        ]);
    }
}
