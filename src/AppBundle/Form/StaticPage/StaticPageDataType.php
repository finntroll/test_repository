<?php

namespace AppBundle\Form\StaticPage;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use AppBundle\Entity\StaticPageType\{AboutAssociationPage, AudiencePage, CouncilPage};
use AppBundle\Form\Common\{UploadedDocumentCollectionType, ImageCropperType};
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\{CollectionType, SubmitType};

class StaticPageDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $object = $builder->getData();

        $builder->add('translations', TranslationsType::class, [
            'label' => false,
            'fields' => [
                'text' => [
                    'field_type' => CKEditorType::class,
                    'config' => [
                        'allowedContent' => true,
                    ],
                    'translation_domain' => 'dictionary',
                ],
                'title' => [
                    'display' => false,
                ],
            ],
            'translation_domain' => 'dictionary',
        ]);

        $objectClass = get_class($object);

        switch ($objectClass) {
            case AboutAssociationPage::class:
                $builder->add('translations', TranslationsType::class, [
                    'fields' => [
                        'title' => [
                            'display' => false,
                        ],
                        'text' => [
                            'field_type' => CKEditorType::class,
                            'config' => [
                                'allowedContent' => true,
                            ],
                            'translation_domain' => 'dictionary',
                        ],
                        'leftBlock' => [
                            'field_type' => CKEditorType::class,
                            'config' => [
                                'allowedContent' => true,
                            ],
                            'label' => 'static.about.left_block',
                            'translation_domain' => 'dictionary',
                        ],
                        'logo' => [
                            'field_type' => ImageCropperType::class,
                            'allow_extra_fields' => true,
                            'error_bubbling' => false,
                            'required' => false,
                            'aspect_ratios' => null,
                        ],
                    ],
                    'label' => false,
                ]);
                $builder->add('uploadedDocuments', UploadedDocumentCollectionType::class);
                break;
            case CouncilPage::class:
                $builder->add('translations', TranslationsType::class, [
                    'fields' => [
                        'title' => [
                            'display' => false,
                        ],
                        'text' => [
                            'field_type' => CKEditorType::class,
                            'config' => [
                                'allowedContent' => true,
                            ],
                            'translation_domain' => 'dictionary',
                        ],
                        'competenceOfChairman' => [
                            'field_type' => CKEditorType::class,
                            'config' => [
                                'allowedContent' => true,
                            ],
                            'label' => 'static.council.competence_of_chairman',
                            'translation_domain' => 'dictionary',
                        ],
                        'competenceOfExecutive' => [
                            'field_type' => CKEditorType::class,
                            'config' => [
                                'allowedContent' => true,
                            ],
                            'label' => 'static.council.competence_of_executive',
                            'translation_domain' => 'dictionary',
                        ],
                        'honoraryMembersText' => [
                            'field_type' => CKEditorType::class,
                            'config' => [
                                'allowedContent' => true,
                            ],
                            'label' => 'static.council.honorary_members_text',
                            'translation_domain' => 'dictionary',
                        ],
                    ],
                    'label' => false,
                ]);
                break;
            case AudiencePage::class:
                $builder->add('translations', TranslationsType::class, [
                    'label' => false,
                    'fields' => [
                        'title' => [
                            'display' => false,
                        ],
                        'text' => [
                            'field_type' => CKEditorType::class,
                            'config' => [
                                'allowedContent' => true,
                            ],
                            'translation_domain' => 'dictionary',
                        ],
                        'highlights' => [
                            'field_type' => CollectionType::class,
                            'entry_type' => HighlightType::class,
                            'allow_add' => true,
                            'allow_delete' => true,
                            'required' => false,
                            'entry_options' => [
                                'label' => false,
                                'attr' => [
                                    'class' => 'highlight-item',
                                ],
                                'label_attr' => [
                                    'class' => 'h3',
                                ],
                            ],
                            'label' => 'static.highlights',
                            'translation_domain' => 'dictionary',
                        ],
                    ],
                ]);
                break;
        }

        $builder->add('submit', SubmitType::class, [
            'translation_domain' => 'dictionary',
            'attr' => [
                'class' => 'side-button side-button blue-button default-button',
            ],
        ]);
    }
}
