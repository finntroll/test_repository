<?php

namespace AppBundle\Form\Subscription;

use AppBundle\Model\SubscriptionEmail;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\{CollectionType, FileType, SubmitType, TextType};
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubscriptionEmailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('subject', TextType::class, [
            'translation_domain' => 'dictionary',
            'label' => 'subscription_mail.subject',
            'required' => true,
        ])
        ->add('message', CKEditorType::class, [
            'translation_domain' => 'dictionary',
            'label' => 'subscription_mail.text',
            'required' => true,
        ])
        ->add('files', CollectionType::class, [
            'translation_domain' => 'dictionary',
            'required' => false,
            'entry_type' => FileType::class,
            'label' => 'subscription_mail.attachment',
            'allow_add' => true,
            'allow_delete' => true,
        ])
        ->add('submit', SubmitType::class, [
            'translation_domain' => 'dictionary',
            'label' => 'subscription_mail.send',
            'attr' => [
                'class' => 'side-button blue-button default-button'
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => SubscriptionEmail::class]);
    }
}
