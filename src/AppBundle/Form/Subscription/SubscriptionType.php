<?php

namespace AppBundle\Form\Subscription;

use AppBundle\Entity\Subscription;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\{EmailType, SubmitType};
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, [
            'label' => 'footer_subscribe',
            'translation_domain' => 'messages',
            'attr' => [
                'placeholder' => 'email-placeholder',
                'class' => 'default-input masked-email',
            ],
            'label_attr' => [
                'class' => 'footer-title',
            ],
            'required' => false,
        ])
        ->add('subscribe', SubmitType::class, [
            'label' => 'send-button',
            'translation_domain' => 'messages',
            'attr' => [
                'class' => 'send-button pink-button default-button',
            ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Subscription::class,
        ]);
    }
}
