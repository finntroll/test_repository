<?php

namespace AppBundle\Form\Transformer;

use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\DataTransformer\BaseDateTimeTransformer;

class DatePickerTransformer extends BaseDateTimeTransformer
{
    private $date_format;

    public function __construct($inputTimezone = null, $outputTimezone = null, $date_format = 'Y-m-d')
    {
        $this->date_format = $date_format;

        parent::__construct($inputTimezone, $outputTimezone);
    }

    public function transform($dateTime)
    {
        if (empty($dateTime)) {
            return ['date' => null];
        }

        if (!$dateTime instanceof \DateTimeInterface) {
            throw new TransformationFailedException('Invalid date!');
        }

        $date = $dateTime->format($this->date_format);

        if ($date === false) {
            return null;
        }

        return ['date' => $date];
    }

    public function reverseTransform($date)
    {
        if ($date === null) {
            return null;
        }

        if (!is_array($date)) {
            throw new TransformationFailedException('Array Expected!');
        }

        $stringDate = $date['date'];

        if ($stringDate === '') {
            return null;
        }

        try {
            $result = \DateTime::createFromFormat($this->date_format, $stringDate);
        } catch (\Exception $e) {
            throw new TransformationFailedException($e->getMessage(), $e->getCode());
        }

        if ($result === false) {
            return null;
        }

        return $result;
    }
}
