<?php

namespace AppBundle\Form\Transformer;

use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\DataTransformer\BaseDateTimeTransformer;

class DateTimePickerTransformer extends BaseDateTimeTransformer
{
    private $date_format;

    private $time_format;

    public function __construct($inputTimezone = null, $outputTimezone = null, $date_format = 'Y-m-d', $time_format = 'H:i')
    {
        $this->date_format = $date_format;
        $this->time_format = $time_format;

        parent::__construct($inputTimezone, $outputTimezone);
    }

    public function transform($dateTime)
    {
        if ($dateTime === null) {
            return null;
        }

        if (!$dateTime instanceof \DateTimeInterface) {
            throw new TransformationFailedException('Invalid date!');
        }

        $date = $dateTime->format($this->date_format);
        $time = $dateTime->format($this->time_format);

        return ['date' => $date, 'time' => $time];
    }

    public function reverseTransform($dateTimePickerValue)
    {
        if (is_null($dateTimePickerValue)) {
            return null;
        }

        if (!is_array($dateTimePickerValue)) {
            throw new TransformationFailedException('Array expected!');
        }

        if (empty($dateTimePickerValue['date'])) {
            return null;
        }

        $dateTimeString = $dateTimePickerValue['date'].' '.$dateTimePickerValue['time'];

        try {
            $dateTime = \DateTime::createFromFormat($this->date_format.' '.$this->time_format, $dateTimeString);
        } catch (\Exception $e) {
            throw new TransformationFailedException($e->getMessage(), $e->getCode());
        }

        return $dateTime;
    }
}
