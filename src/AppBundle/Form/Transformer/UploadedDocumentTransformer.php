<?php

namespace AppBundle\Form\Transformer;

use AppBundle\Entity\UploadedDocuments\UploadedDocument;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\{EntityManagerInterface, PersistentCollection};
use Symfony\Component\Form\DataTransformerInterface;

class UploadedDocumentTransformer implements DataTransformerInterface
{
    protected $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Transform Collection to array
     *
     * @param PersistentCollection $value
     *
     * @return array
     */
    public function transform($value): array
    {
        $result = [];
        /** @var UploadedDocument $item */
        if (!$value){
            return $result;
        }
        foreach ($value as $item) {
            $result [] = [
                'name' => $item->getName(),
                'id' => $item->getId(),
                'extension' => $item->getExtension(),
                'contentStatus' => $item->getContentStatus(),
            ];
        }

        return $result;
    }

    /**
     * Transform a Collection to Another Collection
     *
     * @param PersistentCollection $value
     *
     * @return ArrayCollection
     */
    public function reverseTransform($value): ArrayCollection
    {
        $result = new ArrayCollection();
        if (!$value){
            return $result;
        }
        $repository = $this->em->getRepository(UploadedDocument::class);

        /** @var ArrayCollection $item */
        foreach ($value as $item) {
            if (!$item['id']) {
                continue;
            }
            $document = $repository->find($item['id']);
            if ($document) {
                $document->setName($item['name']);
                if (isset($item['contentStatus'])) {
                    $document->setContentStatus($item['contentStatus']);
                }
                $result->add($document);
            }
        }

        return $result;
    }
}
