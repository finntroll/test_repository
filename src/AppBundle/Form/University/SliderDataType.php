<?php

namespace AppBundle\Form\University;

use AppBundle\Entity\SliderData;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;

class SliderDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('students')
            ->add('staff')
            ->add('foreignStudents')
            ->add('educationPrograms')
            ->add('eduProgramsForeign')
            ->add('internationalLaboratories');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SliderData::class,
        ]);
    }
}
