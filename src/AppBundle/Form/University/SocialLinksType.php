<?php

namespace AppBundle\Form\University;

use AppBundle\Entity\SocialLinks;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;

class SocialLinksType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('vkontakte', null, ['required' => false])
            ->add('instagram', null, ['required' => false])
            ->add('facebook', null, ['required' => false])
            ->add('twitter', null, ['required' => false])
            ->add('youtube', null, ['required' => false])
            ->add('telegram', null, ['required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SocialLinks::class,
        ]);
    }
}
