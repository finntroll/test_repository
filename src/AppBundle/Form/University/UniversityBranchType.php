<?php

namespace AppBundle\Form\University;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Form\Common\RelatedWithDynamicLoadedCitiesType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use AppBundle\Entity\{City, UniversityBranch};
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;

class UniversityBranchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('translations', TranslationsType::class, [
            'translation_domain' => 'dictionary',
            'label' => false,
            'fields' => [
                'address' => [],
                'url' => [
                    'label' => 'university_branch.official_site',
                    'translation_domain' => 'messages'
                ],
                'referencePhone' => [
                    'locale_options' => [
                        'ru' => ['attr' => ['custom-phone-mask-input' => 'on']],
                        'en' => ['attr' => ['custom-phone-mask-input' => 'on']],
                    ]
                ],
                'referenceMail' => [],
                'selectionCommitteePhone' => [
                    'locale_options' => [
                        'ru' => ['attr' => ['custom-phone-mask-input' => 'on']],
                        'en' => ['attr' => ['custom-phone-mask-input' => 'on']],
                    ]
                ],
                'selectionCommitteeMail' => [],
            ],
        ])
        ->add('city', RelatedWithDynamicLoadedCitiesType::class, [
            'required' => true,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UniversityBranch::class,
            'label' => false,
            'locale' => 'ru',
        ]);
    }
}
