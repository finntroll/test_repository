<?php

namespace AppBundle\Form\University;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Entity\{Partner, SocialLinks};
use AppBundle\Form\Common\{ImageCropperType, UploadedDocumentCollectionType};
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\{CollectionType, SubmitType};
use Symfony\Component\OptionsResolver\OptionsResolver;

class UniversityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('translations', TranslationsType::class, [
            'fields' => [
                'fullName' => [],
                'name' => [],
                'shortName' => [],
                'description' => [],
                'promoVideo' => [
                    'required' => false,
                ],
                'image' => [
                    'field_type' => ImageCropperType::class,
                    'error_bubbling' => false,
                    'required' => false,
                    'allow_extra_fields' => true,
                    'label' => 'logo_upload',
                    'translation_domain' => 'messages',
                    'disable_label' => true,
                ],

            ],
        ])
        ->add('mainBranch', UniversityBranchType::class,[
              'label' => false,
              'locale' => $options['locale'],
        ])
        ->add('socialLinks', SocialLinksType::class, [
            'data_class' => SocialLinks::class,
            'required' => false,
            'label' => false,
        ])
        ->add('subBranches', CollectionType::class, [
            'entry_type' => UniversityBranchType::class,
            'allow_add' => true,
            'allow_delete' => true,
            'label' => false,
            'entry_options' => [
                'locale' => $options['locale'],
            ]
        ])
        ->add('sliderData', SliderDataType::class )
        ->add('partners', EntityType::class, [
            'multiple' => true,
            'required' => false,
            'class' => Partner::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->findAllWithTrans();
            }
        ])
        ->add('uploadedDocuments', UploadedDocumentCollectionType::class)
        ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'locale' => 'ru',
        ]);
    }
}
