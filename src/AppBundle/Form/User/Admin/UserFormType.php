<?php

namespace AppBundle\Form\User\Admin;

use AppBundle\Form\Selection\UserRolesSelection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', null, [
                'required' => false
            ])
            ->add('username', null, [
                'required' => false
            ])
            ->add('plainPassword', null, [
                'required' => false
            ])
            ->add('roles', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', [
                'choices'  => UserRolesSelection::choices(),
                'multiple' => true,
            ])
            ->add('enabled')
            ->add('university', EntityType::class, [
                'class'        => 'AppBundle\Entity\University',
                'choice_label' => 'logo',
                'placeholder'  => 'None',
                'required'     => false,
                'multiple'     => false,
            ])
            ->add('submit', SubmitType::class)
        ;;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\User'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }

}
