<?php

namespace AppBundle\Form\User;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\{PasswordType, RepeatedType};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class UserDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translations', TranslationsType::class, [
                'fields' => [
                        'firstName' => [],
                        'surname' => [],
                        'patronymic' => [
                            'required' => false,
                        ],
                    ],
                'translation_domain' => 'dictionary',
            ])
            ->add('email')
            ->add('oldPassword', PasswordType::class, [
                'mapped' => false,
                'constraints' => [
                    new UserPassword()
                ]
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => false,
                'invalid_message' => 'profile.settings.password_not_equals',
                'first_options'  => array('label' => 'New password'),
                'second_options' => array('label' => 'Repeat new password'),
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true,
        ]);
    }
}
