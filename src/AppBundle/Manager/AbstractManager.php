<?php

namespace AppBundle\Manager;

use AppBundle\Entity\EntityInterface;
use AppBundle\Entity\TimestampableInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

abstract class AbstractManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * Create new entity object and set initial data passed throw $options
     *
     * @param array $options
     *
     * @return EntityInterface
     */
    abstract public function create(array $options = []);

    /**
     * Remove object from database. Call store() after this method.
     *
     * @param EntityInterface $object
     *
     * @return $this
     */
    public function remove(EntityInterface $object)
    {
        $this->em->remove($object);

        return $this;
    }

    /**
     * Persist new object for update. Call store() after this method.
     *
     * @param EntityInterface $object
     *
     * @return $this
     */
    public function update(EntityInterface $object)
    {
        $this->em->persist($object);

        return $this;
    }

    /**
     * Call flush(). If object have translations, they would be saved.
     *
     * @param EntityInterface $object
     */
    public function store(EntityInterface $object)
    {
        if (method_exists($object, 'translate')) {
            $object->mergeNewTranslations();
        }
        if($object instanceof TimestampableInterface){
            $object->setUpdatedAt(new \DateTime());
        }

        $this->em->flush();
    }

    /**
     * @param array $criteria
     *
     * @return array
     */
    public function findBy(array $criteria)
    {
        return $this->repository->findBy($criteria);
    }

    /**
     *
     * Set isPublished to true to object if it possible.
     *
     * @param EntityInterface $object
     *
     * @return $this
     */
    public function publish(EntityInterface $object)
    {
        // @TODO Probably, it would be better to create a publishable trait. Think about this when you add the publishedAt timestamp.
        if (method_exists($object, 'setIsPublished')) {
            $object->setIsPublished(true);
        }

        return $this;
    }
}

