<?php

namespace AppBundle\Manager;

use AppBundle\Entity\BestPractice;
use AppBundle\Entity\EntityInterface;
use AppBundle\Entity\University;
use AppBundle\Repository\BestPracticeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class BestPracticeManager extends AbstractManager
{
    protected $em;
    protected $repository;
    protected $authChecker;
    protected $requestStack;

    public function __construct(EntityManagerInterface $manager,
                                BestPracticeRepository $repository,
                                AuthorizationCheckerInterface $authChecker,
                                RequestStack $requestStack)
    {
        $this->em = $manager;
        $this->repository = $repository;
        $this->authChecker = $authChecker;
        $this->requestStack = $requestStack;
    }

    public function findAllBestPractices($filter = null)
    {
        $alias = 'bp';
        $qb = $this->repository->createQueryBuilderRelated($alias);
        $qb = $this->repository->orderQuery($alias, $qb);

        return $this->repository->applyFilter($qb, $filter, $alias);
    }

    public function findUniversityBestPractices(University $university, $filter = null)
    {
        $alias = 'bp';
        $qb = $this->repository->createQueryBuilderRelated($alias);
        $qb = $this->repository->orderQuery($alias, $qb);

        $qb = $this->repository->applyFilter($qb, $filter, $alias);

        $query = $qb->andWhere('bp.university = :university')
                    ->setParameter('university', $university);

        return $query;
    }

    /**
     * @param null $filter
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findPublishedBestPractices($filter = null)
    {
        $alias = 'p';
        $locale = $this->requestStack->getCurrentRequest()->getLocale();
        $qb = $this->repository->createQueryBuilderRelated($alias, $locale);

        $qb->andWhere($alias.'.status = :status')
            ->orderBy($alias.'.id', 'DESC')
            ->setParameter('status', BestPractice::STATUS_PUBLISHED);

        $query = $this->repository->applyFilter($qb, $filter, $alias);

        return $query;
    }

    public function findLimitedPublishedBestPractices($limit)
    {
        $locale = $this->requestStack->getCurrentRequest()->getLocale();

        return $this->repository->findPublished($limit, $locale);
    }

    public function create(array $options = [])
    {
        $bestPractice = new BestPractice();

        $userCanChooseUniversity = $this->authChecker->isGranted('CAN_CREATE_BEST_PRACTICE');
        $userHasUniversity = $options['user']->getUniversity() instanceof University;
        if (!$userCanChooseUniversity && $userHasUniversity) {
            $bestPractice->setUniversity($options['user']->getUniversity());
        }

        return $bestPractice;
    }

    public function moderate(BestPractice $bestPractice)
    {
        if ($bestPractice->isPublished()) {
            throw new \LogicException('Moderation after publication is meaningless');
        }

        $bestPractice->setStatus(BestPractice::STATUS_MODERATING);
        $this->update($bestPractice);

        return $this;
    }

    public function publish(EntityInterface $bestPractice)
    {
        $bestPractice->setStatus(BestPractice::STATUS_PUBLISHED);

        return parent::publish($bestPractice);
    }

    public function decline(EntityInterface $bestPractice)
    {
        $bestPractice->setStatus(BestPractice::STATUS_DRAFT);

        return $this;
    }
}
