<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Contest;
use AppBundle\Entity\User;
use AppBundle\Repository\ContestRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ContestManager extends AbstractManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var ContestRepository
     */
    protected $repository;

    protected $requestStack;

    public function __construct(EntityManagerInterface $manager, ContestRepository $repository, RequestStack $requestStack)
    {
        $this->repository = $repository;
        $this->em = $manager;
        $this->requestStack = $requestStack;
    }

    public function create(array $options = [])
    {
        $contest = new Contest();
        $contest->setIsPublished($options['isPublished'] ?? false);
        $contest->setUniversity($options['user']->getUniversity());

        return $contest;
    }

    public function findContests(User $user)
    {
        return $this->repository->findBy(['university' => $user->getUniversity()],['id' => "DESC"]);
    }

    public function findContestsFilter($filter)
    {
        $alias = 'c';
        $qb = $this->repository->createQueryBuilderRelated($alias);
        $qb = $this->repository->orderQueryByStatusAndId($alias, $qb);

        return $this->repository
            ->applyFilter($qb, $filter, $alias);
    }

    public function findUniversityContests($university, $filter)
    {
        $alias = 'c';
        $qb = $this->repository->createQueryBuilderRelated($alias);
        $qb = $this->repository->applyFilter($qb, $filter, $alias);
        $qb = $this->repository->orderQueryByStatusAndId($alias, $qb);

        $query = $qb
            ->andWhere($alias.'.university = :university')
            ->setParameter('university', $university);

        return $query;
    }

    public function findPublishedContests($filter)
    {
        $alias = 'c';
        $locale = $this->requestStack->getCurrentRequest()->getLocale();
        $qb = $this->repository->createQueryBuilderRelated($alias, $locale);
        $qb->andWhere($alias.'.isPublished = TRUE')
            ->orderBy($alias.'.id', 'DESC');

        $qb = $this->repository->applyFilter($qb, $filter, $alias);

        return $qb;
    }
}
