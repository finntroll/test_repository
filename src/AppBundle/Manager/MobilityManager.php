<?php

namespace AppBundle\Manager;

use AppBundle\Entity\AcademicMobility;
use AppBundle\Entity\University;
use AppBundle\Repository\AcademicMobilityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class MobilityManager extends AbstractManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var AcademicMobilityRepository
     */
    protected $repository;

    protected $requestStack;

    public function __construct(EntityManagerInterface $manager, AcademicMobilityRepository $repository, RequestStack $requestStack)
    {
        $this->em = $manager;
        $this->repository = $repository;
        $this->requestStack = $requestStack;
    }

    public function create(array $options = [])
    {
        $mobility = new AcademicMobility();
        $mobility->setIsPublished($options['isPublished'] ?? false);
        $mobility->setUniversity(isset($options['user']) ? $options['user']->getUniversity() : null);

        return $mobility;
    }

    public function findMobilities($filter = null)
    {
        $alias = 'am';
        $qb = $this->repository->createQueryBuilderRelated($alias);
        $qb = $this->repository->orderQueryByStatusAndId($alias, $qb);

        $query = $this->repository->applyFilter($qb, $filter, $alias);

        return $query;
    }

    public function findUniversityMobilities(University $university, $filter)
    {
        $alias = 'am';
        $qb = $this->repository->createQueryBuilderRelated($alias);
        $qb = $this->repository->applyFilter($qb, $filter, $alias);
        $qb = $this->repository->orderQueryByStatusAndId($alias, $qb);

        $query = $qb->andWhere($alias.'.university = :university')
                    ->setParameter('university', $university);

        return $query;
    }

    public function findPublishedMobilities($filter = null)
    {
        $alias = 'am';
        $locale = $this->requestStack->getCurrentRequest()->getLocale();
        $qb = $this->repository->createQueryBuilderRelated($alias, $locale);

        $qb->andWhere($alias.'.isPublished = TRUE');
        $qb->orderBy($alias.'.id', 'DESC');

        $query = $this->repository->applyFilter($qb, $filter, $alias);

        return $query;
    }
}
