<?php

namespace AppBundle\Manager;

use AppBundle\Entity\EntityInterface;
use AppBundle\Entity\News;
use AppBundle\Entity\University;
use AppBundle\Filter\NewsSearchFilter;
use AppBundle\Repository\NewsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class NewsManager extends AbstractManager
{
    protected $requestStack;

    public function __construct(EntityManagerInterface $em, NewsRepository $repository, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->repository = $repository;
        $this->requestStack = $requestStack;
    }

    public function findUniversityNews($filter = null, University $university)
    {
        $qb = $this->repository->findUniversityNews($filter, $university);
        $qb = $this->repository->orderQuery('n', $qb);

        return $qb;
    }

    public function findAllNews($filter = null)
    {
        $qb = $this->repository->createQueryBuilderRelated('n');
        $qb = $this->repository->orderQuery('n', $qb);

        $qb = $this->repository->applyFilter($qb, $filter);

        return $qb;
    }

    /**
     * Create new entity object and set initial data passed throw $options
     *
     * @param array $options
     *
     * @return News
     */
    public function create(array $options = [])
    {
        $news = new News();
        $news->setStatus($options['status'] ?? News::STATUS_DRAFT);
        $news->setIsImportant($options['importance'] ?? false);
        $news->setUniversity($options['university'] ?? null);
        $news->setType($options['type'] ?? null);

        return $news;
    }

    public function moderate(EntityInterface $news)
    {
        if ($news->getStatus() === News::STATUS_PUBLISHED) {
            throw new \LogicException('Moderation after publication is meaningless');
        }

        $news->setStatus(News::STATUS_MODERATING);
        $this->update($news);

        return $this;
    }

    public function publish(EntityInterface $news)
    {
        $news->setStatus(News::STATUS_PUBLISHED);
        $news->setPublishedAt(new \DateTime('now'));

        return parent::publish($news);
    }

    public function decline(EntityInterface $news)
    {
        $news->setStatus(News::STATUS_DRAFT);

        return $this;
    }

    public function findImportantNews()
    {
        return $this->repository->findImportantNews($this->getLocale());
    }

    public function findAssociationNews()
    {
        return $this->repository->findAssociationNews($this->getLocale());
    }

    public function findAllUniversitiesNews()
    {
        return $this->repository->findAllUniversitiesNews($this->getLocale());
    }

    public function findMassMediaNews()
    {
        return $this->repository->findMassMediaNews($this->getLocale());
    }

    public function findByFilter(NewsSearchFilter $filter)
    {
        $alias = 'n';
        $locale = $this->requestStack->getCurrentRequest()->getLocale();
        $qb = $this->repository->createQueryBuilderRelated($alias, $locale);
        $qb = $this->repository->queryOrdered($qb, $alias);

        $filteredQuery = $this->repository->applyFilter($qb, $filter, $alias);

        return $this->repository->queryPublished($filteredQuery, $alias);
    }

    private function getLocale()
    {
        return $this->requestStack->getCurrentRequest()->getLocale();
    }
}
