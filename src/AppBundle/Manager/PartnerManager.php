<?php

namespace AppBundle\Manager;

use AppBundle\Entity\EntityInterface;
use AppBundle\Entity\Partner;
use AppBundle\Repository\PartnerRepository;
use Doctrine\ORM\EntityManagerInterface;

class PartnerManager extends AbstractManager
{

    public function __construct(EntityManagerInterface $manager, PartnerRepository $repository)
    {
        $this->em = $manager;
        $this->repository = $repository;
    }

    /**
     * Create new entity object and set initial data passed throw $options
     *
     * @param array $options
     *
     * @return Partner
     */
    public function create(array $options = [])
    {
        $partner = new Partner();
        $partner->setStatus($options['status'] ?? Partner::STATUS_DRAFT);

        return $partner;
    }

    public function findAllPartners($filter = null)
    {
        $qb = $this->repository->createQueryBuilderRelated('p');
        $qb = $this->repository->orderQuery('p', $qb);

        return $this->repository->applyFilter($qb, $filter);
    }

    public function moderate(Partner $partner)
    {
        if ($partner->getStatus() === Partner::STATUS_PUBLISHED) {
            throw new \LogicException('Moderation after publication is meaningless');
        }

        $partner->setStatus(Partner::STATUS_MODERATING);
        $this->update($partner);

        return $this;
    }

    public function publish(EntityInterface $partner)
    {
        $partner->setStatus(Partner::STATUS_PUBLISHED);

        return parent::publish($partner);
    }

    public function decline(EntityInterface $partner)
    {
        $partner->setStatus(Partner::STATUS_DRAFT);

        return $this;
    }

}
