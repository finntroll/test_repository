<?php

namespace AppBundle\Manager;

use AppBundle\Entity\EntityInterface;
use AppBundle\Entity\Personality;
use AppBundle\Repository\PersonalityRepository;
use AppBundle\Service\Notifications\NotificationsMailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PersonalityManager extends AbstractManager
{
    protected $em;
    protected $repository;
    private $mailer;
    private $emailTemplate;
    private $router;
    private $salt;

    /**
     * PersonalityManager constructor.
     *
     * @param $repository
     */
    public function __construct(EntityManagerInterface $em, PersonalityRepository $repository, NotificationsMailer $mailer, string $emailTemplate, UrlGeneratorInterface $router, $salt)
    {
        $this->em = $em;
        $this->repository = $repository;
        $this->mailer = $mailer;
        $this->emailTemplate = $emailTemplate;
        $this->router = $router;
        $this->salt = $salt;
    }

    /**
     * Create new entity object and set initial data passed throw $options
     *
     * @param array $options
     *
     * @return Personality
     */
    public function create(array $options = [])
    {
        return new Personality();
    }

    /**
     * @param Personality $object
     *
     * @return AbstractManager
     */
    public function update(EntityInterface $personality)
    {
        /** @var Personality $personality */
        if (is_null($personality->getUsername())) {
            $personality->setUsername($personality->getEmail());
        }

        if (is_null($personality->getPassword())) {
            $personality->setPassword("");
        }

        return parent::update($personality);
    }

    public function findAll($filter = null)
    {
        $alias = 'p';
        $qb = $this->repository->createQueryBuilderRelated($alias);
        $qb = $this->repository->orderQueryByStatusAndId($alias, $qb);

        return $this->repository->applyFilter($qb, $filter, $alias);
    }

    /**
     * @param Personality $personality
     *
     * @return $this
     */
    public function remove(EntityInterface $personality)
    {
        $personality->setEnabled(false);
        $personality->setIsDeleted(true);
        $this->update($personality);

        return $this;
    }

    public function invite(Personality $personality)
    {
        $personality->setRegistrationToken($this->generateHash($personality));
        $this->update($personality)->store($personality);

        $link = $this->router->generate('register', ['hash' => $personality->getRegistrationToken()], UrlGeneratorInterface::ABSOLUTE_URL);
        $this->mailer->sendInvitationToRegistration($this->emailTemplate, $personality, $link);
    }

    public function generateHash(Personality $personality)
    {
        return hash('sha256', $personality->getEmail().$this->salt.time());
    }

    public function findByHash($hash)
    {
        return $this->repository->findOneBy(['registrationToken' => $hash]);
    }

    public function register(Personality $personality)
    {
        $personality->setRegistrationToken(null);
        $personality->setEnabled(true);
        
        return $this->update($personality);
    }
}
