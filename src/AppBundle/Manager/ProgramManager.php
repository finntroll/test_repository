<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Program;
use AppBundle\Entity\University;
use AppBundle\Repository\ProgramRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ProgramManager extends AbstractManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var ProgramRepository
     */
    protected $repository;

    protected $requestStack;

    public function __construct(EntityManagerInterface $manager, ProgramRepository $repository, RequestStack $requestStack)
    {
        $this->em = $manager;
        $this->repository = $repository;
        $this->requestStack = $requestStack;
    }

    public function findPrograms($filter = null)
    {
        $alias = 'p';
        $qb = $this->repository->createQueryBuilderRelated($alias);
        $qb = $this->repository->orderQueryByStatusAndId($alias, $qb);

        return $this->repository->applyFilter($qb, $filter, $alias);
    }

    public function findUniversityPrograms(University $university, $filter = null)
    {
        $alias = 'p';
        $qb = $this->repository->createQueryBuilderRelated($alias);
        $qb = $this->repository->orderQueryByStatusAndId($alias, $qb);

        $qb = $this->repository->applyFilter($qb, $filter, $alias);
        $query = $qb->andWhere('p.university = :university')
                    ->setParameter('university', $university);

        return $query;
    }

    public function findPublishedPrograms($filter = null)
    {
        $alias = 'p';
        $locale = $this->requestStack->getCurrentRequest()->getLocale();
        $qb = $this->repository->createQueryBuilderRelated($alias, $locale);

        $qb->andWhere($alias.'.isPublished = TRUE');
        $qb->orderBy($alias.'.id', 'DESC');

        $query = $this->repository->applyFilter($qb, $filter, $alias);

        return $query;
    }

    public function create(array $options = [])
    {
        $program = new Program();
        $program->setIsPublished($options['isPublished'] ?? false);
        $program->setUniversity($options['user']->getUniversity());

        return $program;
    }
}
