<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Personality;
use AppBundle\Entity\Project;
use AppBundle\Repository\ProjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ProjectManager extends AbstractManager
{
    protected $requestStack;

    public function __construct(EntityManagerInterface $em, ProjectRepository $repository, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->repository = $repository;
        $this->requestStack = $requestStack;
    }

    /**
     * Create new entity object and set initial data passed throw $options
     *
     * @param array $options
     *
     * @return Project
     */
    public function create(array $options = [])
    {
        $project = new Project();
        $project->setCreatedAt(new \DateTime());

        return $project;
    }

    public function findPersonalityProjects(Personality $personality, $filter = null)
    {
        $alias = 'p';
        $qb = $this->repository->createQueryBuilder($alias);
        $qb = $this->repository->queryByHeadManagerOrMember($qb, $personality, $alias);
        $qb = $this->repository->queryPublished($qb, $alias);

        return $this->repository->applyFilter($qb, $filter, $alias);
    }

    public function findAllProjects($filter = null)
    {
        $alias = 'p';
        $qb = $this->repository->createQueryBuilder($alias);

        return  $this->repository->applyFilter($qb, $filter, $alias);
    }

    public function findRelated(Project $project)
    {
        return $this->repository->findRelated($project);
    }

    public function findPublishedProjects($filter = null)
    {
        $alias = 'p';
        $locale = $this->requestStack->getCurrentRequest()->getLocale();
        $qb = $this->repository->createQueryBuilderRelated($alias, $locale);
        $qb = $this->repository->queryPublished($qb, $alias);

        return $this->repository->applyFilter($qb, $filter, $alias);
    }
}
