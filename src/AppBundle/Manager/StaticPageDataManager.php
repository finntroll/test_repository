<?php

namespace AppBundle\Manager;

use AppBundle\Entity\EntityInterface;
use AppBundle\Entity\StaticPageData;
use AppBundle\Repository\StaticPageDataRepository;
use Doctrine\ORM\EntityManagerInterface;

class StaticPageDataManager extends AbstractManager
{
    /**
     * StaticPageDataManager constructor.
     */
    public function __construct(EntityManagerInterface $em, StaticPageDataRepository $repository)
    {
        $this->em = $em;
        $this->repository = $repository;
    }

    /**
     * Create new entity object and set initial data passed throw $options
     *
     * @param array $options
     *
     * @return EntityInterface
     */
    public function create(array $options = [])
    {
        return new StaticPageData();
    }

    public function findAll()
    {
        return $this->repository->findAll();
    }
}
