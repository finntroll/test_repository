<?php

namespace AppBundle\Manager;

use AppBundle\Entity\EntityInterface;
use AppBundle\Entity\Statistic;
use AppBundle\Repository\StatisticRepository;
use Doctrine\ORM\EntityManagerInterface;

class StatisticManager extends AbstractManager
{
    public function __construct(EntityManagerInterface $em, StatisticRepository $repository)
    {
        $this->em = $em;
        $this->repository = $repository;
    }

    /**
     * Create new entity object and set initial data passed throw $options
     *
     * @param array $options
     *
     * @return EntityInterface|Statistic
     * @throws \InvalidArgumentException
     */
    public function create(array $options = [])
    {
        if (!\array_key_exists('object', $options)
            && !\array_key_exists('objectId', $options)
            && !\array_key_exists('session', $options)
        ) {
            throw new \InvalidArgumentException('Arguments required = [object, objectId, session, universityId]');
        }
        $statistic = new Statistic();

        $statistic->setObject($options['object'])
            ->setObjectId($options['objectId'])
            ->setSession($options['session'])
            ->setUniversityId($options['universityId'] ?? null)
            ->setType($options['type'] ?? Statistic::EVENT_INTERNAL);

        return $statistic;
    }
}
