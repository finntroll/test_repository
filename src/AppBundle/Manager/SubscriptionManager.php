<?php

namespace AppBundle\Manager;

use AppBundle\Entity\EntityInterface;
use AppBundle\Entity\Subscription;
use AppBundle\Event\SubscriptionEvent;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class SubscriptionManager extends AbstractManager
{
    private $salt;

    private $dispatcher;

    protected $em;

    protected $repository;

    /**
     * SubscriptionManager constructor.
     *
     * @param string $salt
     */
    public function __construct($salt, EventDispatcherInterface $dispatcher, EntityManagerInterface $em, ObjectRepository $repo)
    {
        $this->salt = $salt;
        $this->dispatcher = $dispatcher;
        $this->em = $em;
        $this->repository = $repo;
    }

    /**
     * @param $hash
     * @param $email
     *
     * @return Subscription|null
     */
    public function findByHashAndEmail($hash, $email)
    {
        return $this->repository->findByHashAndEmail($hash, $email);
    }

    public function findByHash($hash)
    {
        return $this->repository->findOneBy(['hash' => $hash]);
    }

    public function checkHash($hash, $email)
    {
        return $hash === $this->generateHash($email);
    }

    protected function generateHash($email)
    {
        return hash('sha256', $this->salt . $email);
    }

    public function create(array $options = [])
    {
        $subscription = new Subscription();
        $subscription->setCreatedAt(new \DateTime());

        return $subscription;
    }

    public function preConfirm(Subscription $subscription, $locale)
    {
        $hash = $this->generateHash($subscription->getEmail());
        $subscription->setHash($hash);

        $this->dispatcher->dispatch(SubscriptionEvent::SUBSCRIPTION_CREATED,
                                    new SubscriptionEvent($subscription, $locale));

        return $subscription;
    }

    public function confirm($email, $locale)
    {
        $subscription = $this->create();
        $subscription->setEmail($email);
        $subscription->setIsConfirmed(true);
        $subscription->setHash($this->generateHash($email));
        $subscription->setLocale($locale);
        $this->update($subscription)->store($subscription);

        $this->dispatcher->dispatch(SubscriptionEvent::SUBSCRIPTION_CONFIRMED,
                                    new SubscriptionEvent($subscription, $locale));
    }

    public function revoke(Subscription $subscription, $locale)
    {
        $this->dispatcher->dispatch(SubscriptionEvent::SUBSCRIPTION_REVOKED,
                                    new SubscriptionEvent($subscription, $locale));

        $this->remove($subscription)->store($subscription);
    }
}
