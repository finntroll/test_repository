<?php

namespace AppBundle\Manager;

use AppBundle\Entity\EntityInterface;
use AppBundle\Entity\University;
use AppBundle\Entity\User;
use AppBundle\Repository\UniversityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class UniversityManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var UniversityRepository
     */
    protected $repository;

    public function __construct(
        EntityManagerInterface $manager,
        UniversityRepository $repository
    ) {
        $this->em = $manager;
        $this->repository = $repository;
    }

    /**
     * Create new entity object and set initial data passed throw $options
     *
     * @param array $options
     *
     * @return EntityInterface
     */
    public function create(array $options = [])
    {
        return new University();
    }

    public function update(EntityInterface $university, ArrayCollection $originalBranches)
    {
        // Remove branches
        foreach ($originalBranches as $branch) {
            /**
             * @var University $university
             */
            if (!$university->getSubBranches()->contains($branch)) {
                $this->em->remove($branch);
            }
        }

        // Attach added branches
        foreach ($university->getSubBranches() as $branch) {
            if (!$this->em->contains($branch)) {
                $branch->setUniversity($university);
                $this->em->persist($branch);
            }
        }

        $this->em->persist($university);

        return $this;
    }

    public function store(University $university = null)
    {
        $this->em->flush();
    }

    public function loadUniversity(int $id)
    {
        return $this->repository->findUniversity($id);
    }
}
