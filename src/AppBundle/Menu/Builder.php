<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class Builder
{
    const PROFILE_ROOT = 'profile_root';
    const PROJECT_ROOT = 'project_root';

    private $factory;
    private $router;
    private $authChecker;
    private $requestStack;

    /**
     * Builder constructor.
     *
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory, UrlGeneratorInterface $router, AuthorizationCheckerInterface $authorizationChecker, RequestStack $requestStack)
    {
        $this->factory = $factory;
        $this->router = $router;
        $this->authChecker = $authorizationChecker;
        $this->requestStack = $requestStack;
    }

    public function profileMenu(array $options)
    {
        $menu = $this->factory->createItem(self::PROFILE_ROOT);

        if ($this->isGranted('CAN_ACCESS_NEWS')) {
            $menu->addChild('profile_menu.news.title', [
                'route' => 'profile.news.index',
            ])
            ->setLinkAttributes([
                 'class' => 'news-ico',
            ]);
        }

        if ($this->isGranted('CAN_ACCESS_ACADEMIC_MOBILITY')) {
            $menu->addChild('profile_menu.academic_mobility.title', [
                'route' => 'profile.academic.index',
            ])
            ->setLinkAttributes([
                         'class' => 'academic-ico',
            ]);
        }

        if ($this->isGranted('CAN_ACCESS_CONTEST')) {
            $menu->addChild('profile_main.projects.title', [
                'route' => 'profile.contest.index',
            ])
            ->setLinkAttributes([
               'class' => 'contests-ico',
            ]);
        }

        if ($this->isGranted('CAN_ACCESS_PROGRAM')) {
            $menu->addChild('profile_main.edu_programms.title', [
                'route' => 'profile.program.index',
            ])
            ->setLinkAttributes([
                'class' => 'program-ico',
            ]);
        }

        if ($this->isGranted('CAN_ACCESS_BEST_PRACTICE')) {
            $menu->addChild('profile_main.best_pract.title', [
                'route' => 'profile.practice.index',
            ])
            ->setLinkAttributes([
                'class' => 'practice-ico',
            ]);
        }

        if ($this->isGranted('CAN_ACCESS_STATS')) {
            $menu->addChild('profile_main.stats.title', [
                'route' => 'profile.stats.index',
            ])
            ->setLinkAttributes([
                 'class' => 'statistics-ico',
            ]);
        }

        if ($this->isGranted('CAN_EDIT_UNIVERSITY')) {
            $menu->addChild('profile_main.page.university.title', [
                'route' => 'profile.university.index',
            ])
            ->setLinkAttributes([
                  'class' => 'university-ico',
            ]);
        }

        if ($this->isGranted('CAN_ACCESS_PARTNER')) {
            $menu->addChild('profile_menu.partner.title', [
                'route' => 'profile.partner.index',
            ])
            ->setLinkAttributes([
                  'class' => 'partner-ico',
            ]);
        }

        if ($this->isGranted('CAN_ACCESS_SUBSCRIPTION')) {
            $menu->addChild('profile_menu.subscription.title', [
                'route' => 'profile.subscription.index',
            ])
             ->setLinkAttributes([
                'class' => 'subscription-ico',
             ]);
        }

        if ($this->isGranted('CAN_ACCESS_STATIC_PAGES')) {
            $menu->addChild('profile_menu.static_pages.title', [
                'route' => 'profile.static.index',
            ])
             ->setLinkAttributes([
                'class' => 'static-ico',
             ]);
        }

        if ($this->isGranted('CAN_ACCESS_PERSONALITY')) {
            $menu->addChild('profile_main.personality.title', [
                'route' => 'profile.personality.index',
            ])
            ->setLinkAttributes([
                 'class' => 'personality-ico',
            ]);
        }

        if ($this->isGranted('CAN_ACCESS_PROJECT')) {
            $menu->addChild('profile_menu.project.title', [
                'route' => 'profile.project.index',
            ])
            ->setLinkAttributes([
                'class' => 'folder-ico',
            ]);
        }

        $menu->addChild('profile_main.settings.title', [
            'route' => 'profile.settings.index',
        ])
        ->setLinkAttributes([
             'class' => 'settings-ico',
        ]);

        $menu
            ->addChild('profile_main.logout', [
                'uri' => '#logout-modal',
            ])
            ->setLinkAttributes([
                'class' => 'logout-confirm',
                'data-action' => $this->router->generate('fos_user_security_logout'),
                'data-toggle' => 'modal',
                'href' => '#',
                'id' => 'logout-link',
        ]);

        return $menu;
    }

    public function projectMenu(array $options)
    {
        $projectId = $this->requestStack->getCurrentRequest()->get('id');

        $menu = $this->factory->createItem(self::PROJECT_ROOT);
        $menu->addChild('project.main', [
           'route' => 'profile.communication.project.main',
           'routeParameters' => ['id' => $projectId],
        ]);
        $menu->addChild('project.public_docs', [
            'route' => 'profile.communication.project.docs',
            'routeParameters' => ['id' => $projectId],
        ]);
        $menu->addChild('project.archive', [
            'route' => 'profile.communication.project.archive',
            'routeParameters' => ['id' => $projectId],
        ]);
        $menu->addChild('project.members', [
            'route' => 'profile.communication.project.members',
            'routeParameters' => ['id' => $projectId],
        ]);
        $menu->addChild('profile_main.return', [
            'route' => 'profile.project.index',
        ]);

        return $menu;
    }

    private function isGranted($attribute)
    {
        return $this->authChecker->isGranted($attribute);
    }
}
