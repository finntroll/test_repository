<?php

namespace AppBundle\Menu;

use Knp\Menu\ItemInterface;
use Knp\Menu\Matcher\Voter\RegexVoter;
use Knp\Menu\Matcher\Voter\VoterInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ExtendedRegexVoter implements VoterInterface
{
    private $requestStack;

    /**
     * RegexpVoter constructor.
     *
     * @param $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @inheritdoc
     */
    public function matchItem(ItemInterface $item)
    {
        switch ($item->getRoot()->getName()) {
            case Builder::PROFILE_ROOT:
                return $this->matchProfileItem($item);
            case Builder::PROJECT_ROOT:
                return $this->mathProjectItem($item);
        }

        return null;
    }

    private function matchProfileItem(ItemInterface $item)
    {
        $route = $this->requestStack->getCurrentRequest()->getRequestUri();
        $chunks = explode('/', $route);
        $mainPart = $chunks[3] ?? "";

        if ($mainPart !== "") {
            $baseVoter = new RegexVoter('/\/{0,1}[a-z]{2}\/profile\/'.$mainPart.'[\S]+/');

            return $baseVoter->matchItem($item);
        }

        return null;
    }

    private function mathProjectItem(ItemInterface $item)
    {
        $route = $this->requestStack->getCurrentRequest()->getRequestUri();
        $chunks = explode('/', $route);
        $mainPart = $chunks[5] ?? "";

        if ($mainPart !== "") {
            $baseVoter = new RegexVoter('/\/{0,1}[a-z]{2}\/profile\/communication\/project\/'.$mainPart.'\/[\d]+/');

            return $baseVoter->matchItem($item);
        }

        return null;
    }
}
