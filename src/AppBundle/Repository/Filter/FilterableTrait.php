<?php

namespace AppBundle\Repository\Filter;

use AppBundle\Filter\FilterInterface;
use Doctrine\ORM\QueryBuilder;

trait FilterableTrait
{
    public function applyFilter(QueryBuilder $query, FilterInterface $filter, string $alias = null)
    {
        $filter->transformFields();
        $filter->callApplicants($this, $query, $alias);

        return $query;
    }
}
