<?php

namespace AppBundle\Repository;

use AppBundle\Repository\Filter\FilterableTrait;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class PersonalityRepository extends EntityRepository
{
    use FilterableTrait;

    public function queryByNameOrPosition(QueryBuilder $queryBuilder, $search, $alias = null)
    {
        $queryBuilder->orWhere('pt.firstName LIKE :search')
            ->orWhere('pt.surname LIKE :search')
            ->orWhere('pt.patronymic LIKE :search')
            ->orWhere('pt.professionalPosition LIKE :search')
            ->setParameter('search', '%'.$search.'%');

        return $queryBuilder;
    }

    public function queryByWorkPlace(QueryBuilder $queryBuilder, $partner, $alias = null)
    {
        $queryBuilder->andWhere('wp.id IN (:partner)')
                     ->setParameter('partner', $partner);

        return $queryBuilder;
    }

    public function createQueryBuilderRelated($alias = 'p')
    {
        return $this->createQueryBuilder($alias)
                    ->leftJoin($alias.'.translations', 'pt')
                    ->leftJoin($alias.'.workPlace', 'wp');
    }

    public function orderQueryByStatusAndId($alias, QueryBuilder $queryBuilder)
    {
        $queryBuilder->addOrderBy('pt.surname', 'ASC');

        return $queryBuilder;
    }

    public function findByNameWithWorkPlace($name, $locale, $limit)
    {
        $qb = $this->createQueryBuilder('p')
                   ->select("DISTINCT p.id, CONCAT(COALESCE(CONCAT(wt.shortName, ', '), ''), COALESCE(pt.surname, ''), ' ', COALESCE(pt.firstName, ''), ' ', COALESCE(pt.patronymic, '')) AS text, wt.shortName, pt.surname")
                   ->leftJoin('p.translations', 'pt')
                   ->leftJoin('p.workPlace', 'w')
                   ->leftJoin('w.translations', 'wt')
                   ->orWhere('pt.surname LIKE :name')
                   ->orWhere('pt.firstName LIKE :name')
                   ->orWhere('wt.shortName LIKE :name')
                   ->orWhere('wt.name LIKE :name')
                   ->andWhere('pt.locale = :locale')
                   ->andWhere('wt.locale = :locale')
                   ->setParameters([
                       'name' => $name.'%',
                       'locale' => $locale,
                   ])
                   ->setMaxResults($limit)
                   ->addOrderBy('wt.shortName', 'ASC')
                   ->addOrderBy('pt.surname', 'ASC');

        return $qb->getQuery()->getArrayResult();
    }

    public function findByAssociationPositionKey($position, $limit = null)
    {
        $qb = $this->createQueryBuilder('p')
                    ->join('p.associationPosition', 'ap')
                    ->andWhere('ap.transKey = :position')
                    ->setParameter('position', $position);

        if (!is_null($limit)) {
            $qb->setMaxResults($limit);
        }

        $result = $qb->getQuery()->getResult();

        return $limit === 1 ? $result[0] : $result;
    }
}
