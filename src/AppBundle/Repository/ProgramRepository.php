<?php

namespace AppBundle\Repository;

use AppBundle\Repository\Filter\FilterableInterface;
use AppBundle\Repository\Filter\FilterableTrait;
use Doctrine\ORM\QueryBuilder;

/**
 * ProgramRepository
 */
class ProgramRepository extends \Doctrine\ORM\EntityRepository implements FilterableInterface
{
    use FilterableTrait;

    public function queryName(QueryBuilder $queryBuilder, string $name, $alias = null)
    {
        $queryBuilder->andWhere('trans.name LIKE :name')
                     ->setParameter('name', '%'.$name.'%');

        return $queryBuilder;
    }

    public function queryEducationLevel(QueryBuilder $queryBuilder, $levels, $alias = null)
    {
        $queryBuilder->andWhere('edl IN (:levels)')
                     ->setParameter('levels', $levels);

        return $queryBuilder;
    }

    public function queryEducationForm(QueryBuilder $queryBuilder, $forms, $alias = null)
    {
        $queryBuilder->andWhere('edf IN (:forms)')
                     ->setParameter('forms', $forms);

        return $queryBuilder;
    }

    public function queryEntranceTests(QueryBuilder $queryBuilder, $tests, $alias = null)
    {
        foreach ($tests as $key => $test) {
            $queryBuilder->join($alias.'.entranceTests', 'et'.$key, 'WITH', 'et'.$key.' = :test'.$key)
                ->setParameter('test'.$key, $test);
        }

        return $queryBuilder;
    }

    public function queryKnowledgeArea(QueryBuilder $queryBuilder, $areaId, $alias = null)
    {
        $queryBuilder->andWhere('ka = :areaId')->setParameter('areaId', $areaId);

        return $queryBuilder;
    }

    public function queryKnowledgeAreaItem(QueryBuilder $queryBuilder, $items, $alias = null)
    {
        $queryBuilder->andWhere('kai IN (:items)')->setParameter('items', $items);

        return $queryBuilder;
    }

    public function queryBudgetPlaces(QueryBuilder $queryBuilder, $value = true, $alias = null)
    {
        if ($value) {
            $queryBuilder->andWhere($alias.'.budgetPlaces IS NOT NULL AND '.$alias.'.budgetPlaces > 0');
        }

        return $queryBuilder;
    }

    public function queryForeignPlaces(QueryBuilder $queryBuilder, $value = true, $alias = null)
    {
        if ($value) {
            $queryBuilder->andWhere($alias.'.foreignPlaces IS NOT NULL AND '.$alias.'.foreignPlaces > 0');
        }

        return $queryBuilder;
    }

    public function queryDoubleDiploma(QueryBuilder $queryBuilder, $value = true, $alias = null)
    {
        if ($value) {
            $queryBuilder->andWhere($alias.'.doubleDiploma = :value')->setParameter('value', $value);
        }

        return $queryBuilder;
    }

    public function queryMilitaryDepartment(QueryBuilder $queryBuilder, $value = true, $alias = null)
    {
        if ($value) {
            $queryBuilder->andWhere($alias.'.hasMilitaryDepartment = :value')->setParameter('value', $value);
        }

        return $queryBuilder;
    }

    public function queryUniversities(QueryBuilder $queryBuilder, $universities, $alias = null)
    {
        $queryBuilder->andWhere('un IN (:universities)')
                     ->setParameter('universities', $universities);

        return $queryBuilder;
    }

    public function queryLanguages(QueryBuilder $queryBuilder, $languages, $alias = null)
    {
        foreach ($languages as $key => $language) {
            $queryBuilder->join($alias.'.languages', 'lang'.$key, 'WITH', 'lang'.$key.' = :language'.$key)
                ->setParameter('language'.$key, $language);
        }

        return $queryBuilder;
    }

    public function queryBranchCities(QueryBuilder $queryBuilder, $cities, $alias = null)
    {
        $queryBuilder->andWhere('unbc IN (:cities)')
                     ->setParameter('cities', $cities);

        return $queryBuilder;
    }

    public function queryPriceFrom(QueryBuilder $queryBuilder, $from, $alias = null)
    {
        $queryBuilder->andWhere($alias.'.price >= :from')
                     ->setParameter('from', $from);

        return $queryBuilder;
    }

    public function queryPriceTo(QueryBuilder $queryBuilder, $to, $alias = null)
    {
        $queryBuilder->andWhere($alias.'.price <= :to')
                     ->setParameter('to', $to);

        return $queryBuilder;
    }

    public function queryStatus(QueryBuilder $queryBuilder, $isPublished, $alias = null)
    {
        $queryBuilder->andWhere($alias.'.isPublished = :isPublished')
                     ->setParameter('isPublished', $isPublished);

        return $queryBuilder;
    }

    public function createQueryBuilderRelated($alias, $locale = null)
    {
        $qb = $this->createQueryBuilder($alias);

        $qb->select($alias.', edf, edl, ka, kai, partners, un, et, unb, unbc, country, lang, trans, utrans, unbtrans, unbctrans, ptrans, cotrans')
            ->leftJoin($alias.'.educationForm', 'edf')
            ->leftJoin($alias.'.educationLevel', 'edl')
            ->leftJoin($alias.'.knowledgeArea', 'ka')
            ->leftJoin($alias.'.knowledgeAreaItem', 'kai')
            ->leftJoin($alias.'.partners', 'partners')
            ->leftJoin($alias.'.university', 'un')
            ->leftJoin($alias.'.universityBranch', 'unb')
            ->leftJoin($alias.'.translations', 'trans')
            ->leftJoin('unb.city', 'unbc')
            ->leftJoin('unbc.translations', 'unbctrans')
            ->leftJoin('un.translations', 'utrans')
            ->leftJoin('unb.translations', 'unbtrans')
            ->leftJoin('partners.translations', 'ptrans')
            ->leftJoin('partners.country', 'country')
            ->leftJoin('country.translations', 'cotrans')
            ->leftJoin($alias.'.languages', 'lang')
            ->leftJoin($alias.'.entranceTests', 'et')
        ;

        if (!is_null($locale)) {
            $qb->andWhere('trans.locale = :locale')
               ->setParameter('locale', $locale);
        }

        return $qb;
    }

    public function orderQueryByStatusAndId($alias, QueryBuilder $queryBuilder)
    {
        $queryBuilder
            ->addOrderBy($alias.'.isPublished')
            ->addOrderBy($alias.'.id', 'DESC');

        return $queryBuilder;
    }
}
