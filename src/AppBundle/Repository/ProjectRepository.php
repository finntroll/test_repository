<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Personality;
use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use AppBundle\Repository\Filter\FilterableTrait;
use Doctrine\ORM\QueryBuilder;

class ProjectRepository extends \Doctrine\ORM\EntityRepository
{
    use FilterableTrait;

    public function findByHeadManagerOrMember(User $user)
    {
        $qb = $this->createQueryBuilder('p');
        $qb = $this->queryByHeadManagerOrMember($qb, $user, 'p');
        $query = $this->queryPublished($qb, 'p')->getQuery();

        return $query->getResult();
    }

    public function queryByHeadManagerOrMember(QueryBuilder $queryBuilder, Personality $personality, $alias = null)
    {
        $queryBuilder->join($alias.'.projectMembers', 'm')
                     ->andWhere()
                     ->orWhere('m.personality = :personality')
                     ->orWhere($alias.'.head = :personality')
                     ->orWhere($alias.'.manager = :personality')
                     ->setParameter('personality', $personality);

        return $queryBuilder;
    }

    public function queryAllProjects($alias = null)
    {
        return $this->createQueryBuilder($alias);
    }

    public function queryByName(QueryBuilder $queryBuilder, $search, $alias = null)
    {
        $queryBuilder->join($alias.'.translations', 't')
                     ->andWhere('t.name LIKE :search')
                     ->setParameter('search', '%'.$search.'%');

        return $queryBuilder;
    }

    public function queryByHead(QueryBuilder $queryBuilder, $head, $alias = null)
    {
        $queryBuilder
            ->andWhere($alias.'.head IN (:head)')
            ->setParameter('head', $head);

        return $queryBuilder;
    }

    public function queryByManager(QueryBuilder $queryBuilder, $managers, $alias = null)
    {
        $queryBuilder
            ->andWhere($alias.'.manager IN (:managers)')
            ->setParameter('managers', $managers);

        return $queryBuilder;
    }

    public function queryByMembers(QueryBuilder $queryBuilder, $members, $alias = null)
    {
        $queryBuilder->join($alias.'.projectMembers', 'members')
            ->andWhere('members.personality IN (:members)')
            ->setParameter('members', $members);

        return $queryBuilder;
    }

    public function queryByUniversities(QueryBuilder $queryBuilder, $universities, $alias = null)
    {
        $queryBuilder
            ->andWhere($alias.'.university IN (:universities)')
            ->setParameter('universities', $universities);

        return $queryBuilder;
    }

    public function findRelated(Project $project)
    {
        return $this->createQueryBuilder('p')
                    ->andWhere('p.isPublished = TRUE')
                    ->andWhere('p.id <> :current')
                    ->andWhere('p.dateEnd >= :now')
                    ->setParameters([
                            'now' => new \DateTime(),
                            'current' => $project->getId(),
                        ])
                    ->orderBy('p.createdAt', 'DESC')
                    ->setMaxResults(5)
                    ->getQuery()
                    ->execute();
    }

    public function queryPublished(QueryBuilder $queryBuilder, $alias)
    {
        return $queryBuilder->andWhere($alias.'.isPublished = :published')
                            ->setParameter('published', true);
    }

    public function createQueryBuilderRelated($alias, $locale = null)
    {
        $qb = $this->createQueryBuilder($alias);
        $qb->select($alias, 'trans');
        $qb->leftJoin($alias.'.translations', 'trans');

        if (!is_null($locale)) {
            $qb->andWhere('trans.locale = :locale')
               ->setParameter('locale', $locale);
        }

        return $qb;
    }
}
