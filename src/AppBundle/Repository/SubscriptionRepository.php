<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SubscriptionRepository extends EntityRepository
{
    public function findByHashAndEmail($hash, $email)
    {
        return $this->findOneBy(['hash' => $hash, 'email' => $email]);
    }
}
