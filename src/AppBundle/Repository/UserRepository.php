<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function findByRoles($roles)
    {
        if (!is_array($roles)) {
            $roles = [$roles];
        }
        $users = $this->findAll();

        return $this->filterUsersByRoles($users, $roles);
    }

    public function findByRolesAndUniversity($roles, $university)
    {
        if (!is_array($roles)) {
            $roles = [$roles];
        }

        $users = $this->findBy(['university' => $university]);

        return $this->filterUsersByRoles($users, $roles);
    }

    private function filterUsersByRoles($users, $roles)
    {
        return array_filter($users, function(User $user) use ($roles) {
            $userRoles = $user->getRoles();
            $intersect = array_intersect($userRoles, $roles);
            return count($intersect) > 0;
        });
    }
}

