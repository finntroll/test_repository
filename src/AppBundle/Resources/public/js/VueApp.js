var $ = require('jquery');
var Vue = require('vue');

var AguAPI = require('./api-requests');
$(document).ready(function () {
    Vue.component('select-items', {
        template: '#template-dependencyFieldSelectItems',
        data: function () {
            return {
                items: []
            }
        },
        props: [
            'changer', // field id which defines content of dependent components
            'name', // field name of current component
            'chosen', // chosen items
            'url', // key (NOT URL) for API, which gets url list (key => full_path)
            'requiredAttr', // just required attribute
            'placeholder',
            'multiple',
            'loadAllWhenEmpty'
        ],

        created: function () {
            if ($('#' + this.changer).size() === 0) {
                throw new Error("Changer field with name '" + this.changer + "' is not found!");
            }

            var vueApp = this.$parent; // main Vue instance

            vueApp.updateList(this);
            vueApp.configureEvent(this.$parent, this);
        },

        methods: {
            isChosen: function (itemId) {
                return this.chosen.indexOf(itemId) !== -1;
            }
        }
    });

    Vue.component('checkbox-items', {
        template: '#template-dependencyFieldCheckboxItems',

        data: function () {
            return {
                items: []
            }
        },

        props: [
            'changer',
            'name',
            'chosen',
            'url'
        ],

        created: function () {
            if ($('#' + this.changer).size() === 0) {
                throw new Error("Changer field with name '" + this.changer + "' is not found!");
            }

            var vueApp = this.$parent; // main Vue instance

            vueApp.updateList(this);
            vueApp.configureEvent(vueApp, this);
        },

        methods: {
            nameGenerator: function (itemId) {
                var appendix = itemId ? '['+itemId+']' : '[]';

                return this.name + appendix;
            },

            isChosen: function (itemId) {
                return this.chosen.indexOf(itemId) !== -1;
            }
        }
    });

    // Main instance must be initialized after all components
    var VueApp = new Vue({
        el: '.app-form-field-mapping',

        data: {
            components: {}
        },

        created: function () {
        },

        methods: {
            configureEvent: function (vue, vueComponent) {
                $(document).ready(function () {
                    $('#' + vueComponent.changer).on('change', function (e) {
                        vue.updateList(vueComponent);

                        // If used select2, reset and re-init
                        var $select = $(vueComponent.$el).find('.select2-apply');
                        if ($select) {
                            $select.val('').trigger('change');
                            $select.select2({});
                        }
                    });
                });
            },

            updateList: function (vueComponent) {
                AguAPI.request({ id: $('#' + vueComponent.changer).val(), l: vueComponent.loadAllWhenEmpty ? '1' : '0' }, vueComponent.url)
                    .then(function (loadedList) {
                        VueApp.insertValues(vueComponent, loadedList);
                });
            },

            insertValues: function (vueComponent, loadedList) {
                // clear old values
                vueComponent.items.splice(0, vueComponent.items.length);

                for (var key in loadedList) {
                    vueComponent.items.push(loadedList[key]);
                }
            }
        }
    });
});
