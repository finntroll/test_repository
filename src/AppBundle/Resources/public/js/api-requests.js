var AguAPI = function () {
    var self = this;
    self.urls = {};

    self.addUrl = function (name, url) {
        self.urls[name] = url;
    };

    self.getUrl = function (name) {
        if (!self.urls.hasOwnProperty(name)) {
            throw new Error("URI address not added to me!");
        }

        return  self.urls[name];
    };

    self.get = function (url, data, resolve, reject) {
        return {
            url: url,
            data: data,
            method: 'GET',
            success: function (data) {
                resolve(data);
            },
            fail: function (data) {
                console.log(['ERROR', data]);
                reject(data);
            }
        }
    };

    self.post = function (url, data, resolve, reject) {
        var basicPost = self.get(url, data, resolve, reject);
        basicPost.method = 'POST';
        return basicPost;
    };

    self.request = function(data, urlKey) {
        return new Promise(function(resolve, reject){
            $.ajax(self.get(self.getUrl(urlKey), data, resolve, reject));
        });
    };

    return self;
}();

module.exports = AguAPI;
