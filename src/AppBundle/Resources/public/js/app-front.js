var $ = window.$ = window.jQuery = require('jquery');

var AguAPI = window.AguAPI =  require('./api-requests');
var Ladda = require('ladda');
$('button[type="submit"]').attr('data-style', 'slide-up');
Ladda.bind('button[type="submit"]', { timeout: 4500 });

require('imports-loader?jQuery=jquery!./jquery.datetimepicker.full.min');
require('inputmask/dist/inputmask/jquery.inputmask');

require('bootstrap/dist/js/bootstrap');
require('select2');
require('select2/dist/js/i18n/ru');
require('select2/dist/js/i18n/en');

require('./applyDateTimePicker');
require('./VueApp');
require('./filters-form');
require('./scripts');
require('./selectAllTrigger');

$(document).ready(function() {

    $('.select2-apply')
        .css('margin-left', '0!important')
        .css('min-height', '')
        .css('height', '')
        .select2({
            language: locale,
            placeholder: {
                id: "",
                placeholder: ""
            }
        });

    $(document).on('change', '#per_page-trigger', function(event) {
        var prepared_location = $(event.target).find(":selected").data('query');
        jQuery(location).attr('href', prepared_location);
    });

    $('input[custom-phone-mask-input="on"]').inputmask({mask: ["+7 (999) 999-9999"]});
});

