require('./app-front');

require('chart.js/dist/Chart.min');
require('./chart/colors');
window.AGU = require('./chart/statistic');

require('cropper');
var Cropper = require('./prestaimage/js/cropper');

$(document).ready(function() {

    (function(w, $) {
        'use strict';
        $(function() {
            $('.cropper').each(function() {
                new Cropper($(this));
            });
        });
    })(window, window.jQuery);

});

require('./emailFilesCollection');
require('./highlights-form');
require('./subBranchesCollection');
require('./members-collection');
require('./uploaded-documents');
require('./remove-archived-docs');
