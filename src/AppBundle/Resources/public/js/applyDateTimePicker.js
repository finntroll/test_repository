(function () {
    $(document).ready(function () {
        $.datetimepicker.setLocale(locale);

        jQuery(function () {
            $('.date-start.datepicker').datetimepicker({
                onShow: function (ct) {
                    var maxDate = $('.date-end.datepicker').val() ? $('.date-end.datepicker').val() : false;
                    this.setOptions({
                        maxDate: maxDate,
                        formatDate: 'Y-m-d'
                    })
                },
                format: 'Y-m-d',
                timepicker: false,
                scrollInput: false
            });

            $('.date-end.datepicker').datetimepicker({
                onShow: function (ct) {
                    var minDate = $('.date-start.datepicker').val() ? $('.date-start.datepicker').val() : false;
                    this.setOptions({
                        minDate: minDate,
                        formatDate: 'Y-m-d'
                    })
                },
                format: 'Y-m-d',
                timepicker: false,
                scrollInput: false
            });

            $('.date-before-start.datepicker').datetimepicker({
                onShow: function (ct) {
                    var maxDate = $('.date-start.datepicker').val() ? $('.date-start.datepicker').val() : false;
                    this.setOptions({
                        maxDate: maxDate,
                        formatDate: 'Y-m-d'
                    })
                },
                format: 'Y-m-d',
                timepicker: false,
                scrollInput: false
            });

            $('.timepicker').datetimepicker({
                scrollInput: false,
                datepicker: false,
                timepicker: true,
                format: "H:i"
            });
        });
    });
}());
