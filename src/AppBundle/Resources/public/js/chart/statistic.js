"use strict";
var AGU = typeof window.AGU === 'undefined' ? {} : window.AGU;

AGU.StatsticManager = {
    _ctx: null,
    _link: null,
    chart: null,

    init: function (link) {
        this._link = link;
        this._ctx = document.getElementById("statistic-chart").getContext('2d');

        this._initChart();
        this._initEvents();
        this._clickDefault(0)
    },

    _clickDefault: function (elNum) {
        $($('.chart-control')[elNum]).trigger('click');
    },

    _initChart: function () {
        this.chart = new Chart(this._ctx, {
            type: 'line',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    },

    _initEvents: function () {
        $(document).on('click', '.chart-control', this._chartControl)
    },

    _chartControl: function () {
        var _self = AGU.StatsticManager;
        var $this = $(this);
        var data = $this.data('type');
        $this.removeClass('blue-button');

        $('.statistic-button')
            .find('.pink-button')
            .removeClass('pink-button')
            .addClass('blue-button')
        ;

        $this.addClass('pink-button');
        _self.loader(data);
    },

    _clearChart: function () {
        this.chart.data.labels = [];
        this.chart.data.datasets = [];
        this.chart.update();
    },

    _clearTable: function () {
        $('.statistic-table-data').html("-");
    },
    loader: function (type) {
        this._clearChart();
        this._clearTable();
        $.ajax({
            url: this._link,
            data: {
                'date': type
            },
            dataType: 'json',
            type: 'GET',
            success: this.updater,
            error: function (response) {
                var msg = response.responseJSON.error;
                var $view = $('.stats-data');
                $view.html('<h2>'+msg +'</h2>')
            }
        });
    },

    updater: function (response) {
        // Update Chart
        var _self = AGU.StatsticManager;
        _self.chart.data.labels = response.chart.labels;
        _self.chart.data.datasets = response.chart.data;
        _self.chart.update();
        // Update Table
        for (var index in response.table) {
            if (response.table.hasOwnProperty(index)) {
                var sub = response.table[index];
                for (var subIndex in sub) {
                    if (sub.hasOwnProperty(subIndex)) {
                        var data = sub[subIndex];
                        var cell = $("." + index + "-" + subIndex);
                        if (typeof data === "number") {
                            cell.html("<b>" + data + "</b>")
                        } else {
                            cell.html(data.all)
                        }
                    }
                }
            }
        }
    }
};

module.exports = AGU;
