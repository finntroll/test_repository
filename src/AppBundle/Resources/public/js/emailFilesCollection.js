$(document).ready(function () {
        if (typeof fileListClass !== 'undefined') {
            var $list = $(fileListClass);

            // Next code depends on HTML structure! If layout will change, the following code may not work.
            // Inside each branchContainer we are insert deleteLink and add onclick closure on it.
            $list.children('div').each(function () {
                addDeleteLink($(this));
            });

            $(document).on('click', '#add-file', function (e) {
                e.preventDefault();
                var $fileList = $(fileListClass);
                var newFile = $fileList.data('prototype');

                // delete unnecessary label
                newFile = newFile.replace('<label class="control-label" for="subscription_email_files___name__">__name__label__<\/label>', ' ');
                // change names in prepared prototype
                newFile = newFile.replace(/__name__/g, fileIndexName);


                // create and insert form into newBranchContainer then add deleteLink
                var newFileContainer = $('<div class="form-group"></div>').html(newFile);
                newFileContainer.appendTo($fileList);
                addDeleteLink(newFileContainer);

                fileIndexName++;
            });


        function addDeleteLink($fileContainer) {
            var $clearfix = $('<div class="clearfix"></div>');
            var $link = $('<a href="#" class="btn default-button pink-button pull-right">' + deleteLinkMessage + '</a>');
            $link.on('click', function (e) {
                e.preventDefault();
                $fileContainer.remove();
            });
            $fileContainer.append($clearfix);
            $fileContainer.append($link);
        }
    }
});
//"<div class="form-group"><label class="control-label" for="subscription_email_files_1">1label__</label><input type="file" id="subscription_email_files_1" name="subscription_email[files][1]" /></div>"
//"<div class="form-group"><label class="control-label" for="subscription_email_files___name__">__name__label__</label><input type="file" id="subscription_email_files___name__" name="subscription_email[files][__name__]" /></div>"
