$(document).on('submit', '.filters-form', function(e) {
    var q = new URLSearchParams(window.location.search);
    var perPage = parseInt(q.get('perPage'));
    if (perPage > 0) {
        var input = $('<input type="hidden" name="perPage" value=' + perPage + '>');
        $(this).append(input);
    }
});
