$(document).ready(function () {
    var addHighlightButtons = $('.add-button');
    var tabs = $('a[data-toggle="tab"]');

    tabs.click(function () {
        var target = $(this).data('target');
        addHighlightButtons.each(function (i) {
            var button = $(addHighlightButtons[i]);
            var locale = $(button).data('locale');
            if (target.indexOf('_' + locale + '_') > -1) {
                $(button).show();
            } else {
                $(button).hide();
            }
        });
    });

    tabs.each(function (i) {
        var tab = $(tabs[i]);
        if (tab.parent().hasClass('active')) {
            $(tab).trigger('click');
        }
    });

    addHighlightButtons.click(function (e) {
        e.preventDefault();
        var $container = $("#" + $(this).data('target'));
        var template = $($container).data('prototype');
        var counter = $(this).data('counter') + 1;

        var $newChild = $(template.replace(/__name__/g, counter));
        $($container).append($newChild);
        $(this).data('counter', counter);
    });

    $('.highlight-delete').click(function(e) {
        e.preventDefault();
        $(this).parent().parent().remove();
    });
});
