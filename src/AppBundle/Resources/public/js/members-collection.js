require('./tetranzselect2entity/select2entity');

$(document).ready(function () {
    var addMemberButton = $('.add-member-button');

    addMemberButton.click(function (e) {
        e.preventDefault();
        var $container = $("#" + $(this).data('target'));
        var template = $($container).data('prototype');
        var counter = $(this).data('counter') + 1;

        var $newChild = $(template.replace(/__name__/g, counter));
        $($container).append($newChild);
        $(this).data('counter', counter);

        var $select2 = $($newChild).find('.select2entity').last();
        $($select2).select2entity();
    });

    /* Adding 1 required member by default */
    /* If form will be remade this lines may be unnecessary */
    var memberItems = $('.project-member-item').length;
    if (memberItems<1) {
        addMemberButton.trigger('click');
    }


    $(document).on('click', '.delete-member', function(e) {
        e.preventDefault();
        $(this).parent().parent().parent().remove();
    });
});
