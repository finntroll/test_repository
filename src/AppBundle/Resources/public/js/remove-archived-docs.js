$(document).on('click', 'a[data-ajax-remove="true"]', function (e) {
    e.preventDefault();
    var modal = $(this).data('target');
    var deleteLink = $(this).data('link');
    var button = this;
    $(modal).on('shown.bs.modal', function() {
        $(modal).find('.delete-submit').on('click', function() {
            e.preventDefault();
            $.get(deleteLink, {}, function() {
                $(button).parent().parent().remove();
                $(modal).modal('hide');
            });
        });
    });
});
