$(document).ready(function() {
    /* Hide and open search */
    $('#search-open').click(function () {
        if (!$(this).hasClass('active')) {
            $('#search-additional-form').slideDown();
            var textNode=$(this).find('.search-close');
            $(this).addClass('active');
            $(textNode).html($(textNode).attr('data-closeText'));
        }
        else if ($(event.target).hasClass('search-close')) {
            var textNode=$(this).find('.search-close');
            $('#search-additional-form').slideUp();
            $('#search-open').removeClass('active');
            $(textNode).html($(textNode).attr('data-openText'));
        }
    });

    var chosenOptions = $('#search-open input:checked').length + $('#search-open option[selected="selected"]').length;

    if (chosenOptions > 0 && !$('#search-open').hasClass('active')) {
        $('#search-additional-form').slideDown();
        var textNode = $('#search-open .search-close');
        $('#search-open').addClass('active');
        $(textNode).html($(textNode).attr('data-closeText'));
    }

    /* Hide and open council competencies */
    $('.show-info').click(function () {
        var textNode=$(this).attr('data-text');
        if (!$(this).hasClass('active')) {
            $(this).prev('.person-addition-info').slideDown();
            $(this).attr('data-text', $(this).text());
            $(this).addClass('active');
            $(this).text(textNode);
        }
        else {
            $(this).prev('.person-addition-info').slideUp();
            $(this).removeClass('active');
            $(this).attr('data-text', $(this).text());
            $(this).text(textNode);
        }
    });

    $('#subscribe').click(function() {
        /* Checking if form is valid */
        if ($('.masked-email').inputmask('isComplete')) {
            $('#subscribe-modal').modal('show');
        }
    });

    $('#login-toggle').click(function() {
        if (!$(this).hasClass('active')) {
            $(this).next('.login-sub-menu').slideDown();
            $(this).addClass('active');
        }
        else {
            $(this).next('.login-sub-menu').slideUp();
            $(this).removeClass('active');
        }
    });

    $('.collapse-button').click(function(e) {
        e.preventDefault();
        $(this).hide();
        $(this).next('.collapse-text').slideDown();
    });

     $("#researches-search input").keyup(function(event) {
            if (event.keyCode === 13) {
                var link = $(this).next('p').find('.default-button:first');
                $(link).trigger('click');
                location.href = $(link).attr("href");
            }
    });

    /* Footer positioning */

    $(window).resize(function() {
        $('body').css('paddingBottom', $('#footer').outerHeight());
    });

    $('body').css('paddingBottom', $('#footer').outerHeight());

    /* Confirm changes */
    var warn_on_unload = false;

    $('form input, form select, form textarea').on('change', function () {
        warn_on_unload = true;
    });
    $('.edit-page header a, .edit-page .aside-nav a').click(function(e) {
       var url = $(this).attr('href');
       if (warn_on_unload && url !== '' && url !== '#') {
           e.preventDefault();
           $('#confirm-modal').modal('show');
           $('#confirm-modal .cancel-button, #confirm-modal .close-button').attr('href', url);
       }
    });
    $('#save-submit').click(function() {
        $('#confirm-modal').modal('hide');
        $('.submit-button').trigger('click');
    });

    /* Slider on main page */
    $('.carousel').carousel();

    $('#main-carousel').on('slide.bs.carousel', function (e) {
        var previousStep = $(this).find('.active').index();
        var elems = $('.carousel-indicators li');
        var nextStep = $(e.relatedTarget).index();
        $(elems).hide();
        if ($(elems).length - nextStep <= 3) {
            $(elems).slice(nextStep - (7 - ($(elems).length - nextStep)), nextStep).show();
            $(elems).slice(nextStep, $(elems).length).show();
        }
        else if (nextStep > 3) {
              $(elems).slice(nextStep - 3, nextStep).show();
              $(elems).slice(nextStep, nextStep + 4).show();
            }
            else {
            $(elems).slice(nextStep, nextStep + (7 - nextStep)).show();
            $(elems).slice(0, nextStep).show();
        }
    });

    /* Modal confirm */
    $('.delete-confirm').click(function () {
        $('#delete-modal').find('.delete-submit').attr('href', $(this).attr('data-action'));
    });

    /* Modal confirm */
    $('#logout-link, .logout-link').click(function (e) {
        e.preventDefault();
        $('#logout-modal').find('.logout-submit').attr('href', $(this).attr('data-action'));
    });

    /* Hide and open menu */
    $('.middle-menu a').click(function () {
        var innerNav = $(this).closest('li').find('.middle-menu-inner');
        if ($(innerNav).length>0 && !$(this).closest('li').hasClass('opened')) {
                $('.opened .middle-menu-inner').stop().slideUp();
                $('.middle-menu .opened').removeClass('opened');
                $(this).closest('li').addClass('opened');
                $(innerNav).stop().slideDown();

                $(document).one('click', function closeMenu (e){
                    if($('.middle-menu').has(e.target).length === 0){
                        $('.opened .middle-menu-inner').stop().slideUp();
                        $('.middle-menu .opened').removeClass('opened');

                    } else {
                        $(document).one('click', closeMenu);
                    }
            });
        }
        else {
            $('.opened .middle-menu-inner').stop().slideUp();
            $('.middle-menu .opened').removeClass('opened');
        }
    });

    /* Hide and open password */
    $('#show-password').click(function () {
        if (!$(this).hasClass('active')) {
            $('#password').attr('type', 'text');
            $(this).addClass('active');
        }
        else {
            $('#password').attr('type', 'password');
            $(this).removeClass('active');
        }
    });

    /* Hide and open sublists */
    $('.sub-list select').change(function () {
        if ($(this).val()!='') {
            $(this).addClass('opened');
        }
        else {
            $(this).removeClass('opened');
        }
    });

    /* Hide and open sublists */
    $('#education-level select').change(function () {
        if ($(this).val()!='') {
            $('.entrance-tests').show();
        }
        else {
            $('.entrance-tests').hide();
        }
    });

    /* Hide and open checkboxes */
    $('.toggle .control-label').click(function () {
        if (!$(this).hasClass('opened')) {
            $(this).addClass('opened');
        }
        else {
            $(this).removeClass('opened');
        }
    });

    $(document).on('change', 'input[type="checkbox"]', function (event) {
        var checkbox = event.target;
        var $label = $(checkbox).parent('label');

        if ($(checkbox).is(':checked')) {
            $label.addClass('checked-box');
        } else {
            $label.removeClass('checked-box');
        }
    });

    $(document).ready(function () {
        // activate custom checkboxes
        $('input[type="checkbox"]').each(function (k, checkbox) {
            var $label = $(checkbox).parent('label');
            if ($(checkbox).is(':checked')) {
                $label.addClass('checked-box');
            }
        })
    })
});
