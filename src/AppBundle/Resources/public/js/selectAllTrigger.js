(function () {
    $(document).on('click', '.select-all-trigger', function () {
        var checked = this.checked;
        $(this).siblings('.child-line').find('input[type="checkbox"]')
            .each(function (index, item) {
                item.checked = checked;
            });
    });

    $(document).ready(function () {
        var $triggers = $('.select-all-trigger');
        $triggers.each(function (index, trigger) {
            var $trigger = $(trigger);
            var $inputs = $trigger.siblings('.child-line').find('input[type="checkbox"]');
            handleListClick($trigger, $inputs);
        });
    });

    $(document).on('click', '.select-all .child-line input[type="checkbox"]', function () {
        var $trigger = $(this).parent().siblings('.select-all-trigger');
        var $inputs = $trigger.siblings('.child-line').find('input[type="checkbox"]');
        handleListClick($trigger, $inputs);
    });

    var handleListClick = function ($trigger, $list) {
        var has_unchecked = false;
        $list.each(function (index, item) {
            if(!item.checked) {
                has_unchecked = true;
            }
        });
        $trigger.prop('checked', !has_unchecked);
    }
}());
