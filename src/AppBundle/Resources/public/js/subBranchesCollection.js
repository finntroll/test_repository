require('./tetranzselect2entity/select2entity');

// #formName_collectionFieldName
var branchListClass = '#university_subBranches';
$(document).ready(function () {
    var $list = $(branchListClass);

    // Next code depends on HTML structure! If layout will change, the following code may not work.
    // Inside each branchContainer we are insert deleteLink and add onclick closure on it.
    $list.children('div').each(function () {
        addDeleteLink($(this));
    });

    $(document).on('click', '#add-branch', function (e) {
        e.preventDefault();
        var $branchList = $(branchListClass);
        var newBranch = $branchList.data('prototype');

        // delete unnecessary label
        newBranch = newBranch.replace('<label class="control-label required">__name__label__<\/label>', '');
        // change names in prepared prototype
        newBranch = newBranch.replace(/__name__/g, branchIndexName);


        // create and insert form into newBranchContainer then add deleteLink
        var newBranchContainer = $('<div class="form-group"></div>').html(newBranch);
        $(newBranchContainer).find('input[custom-phone-mask-input="on"]')
            .attr('pattern', "[\\+]\\d{1}\\s{1}[\\(]\\d{3}[\\)]\\s{1}\\d{3}[\\-]\\d{4}")
            .inputmask({mask: ["+7 (999) 999-9999"]});
        $(newBranchContainer).find('.select2entity').last().select2entity();
        newBranchContainer.appendTo($branchList);
        addDeleteLink(newBranchContainer);

        branchIndexName++;
    });
});

function addDeleteLink($branchContainer) {
    var $clearfix = $('<div class="clearfix"></div>');
    var $link = $('<a href="#" class="remove-button grey-button default-button pull-right">'+deleteLinkMessage+'</a>');
    $link.on('click', function (e) {
        e.preventDefault();
        $branchContainer.remove();
    });
    $branchContainer.append($clearfix);
    $branchContainer.append($link);
}
