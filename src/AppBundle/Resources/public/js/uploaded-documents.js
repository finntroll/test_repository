AGU = AGU || {};
if (AGU.UploadedDocuments !== undefined) {
    for (var i = 0; i < AGU.UploadedDocuments.length; i++) {
        (function() {
            var obj = AGU.UploadedDocuments[i];
            var prefix = obj.prefix;

            var $container = $("#" + (prefix === "" ? prefix : (prefix + "_")) + "uploadedDocuments");
            var $button = $("#" + (prefix === "" ? prefix : (prefix + "-")) + "fileinput-button");
            var $errorPath = $("#" + (prefix === "" ? prefix : (prefix + "-")) + "file-error-path");
            var $fileUpload = $("#" + (prefix === "" ? prefix : (prefix + "-")) + "fileupload");

            var name = '';
            if (prefix !== "") {
                name = "#" + prefix + "_uploadedDocuments_"
            } else {
                name = "#uploadedDocuments_"
            }

            $button.click(function (e) {
                e.preventDefault();
                $errorPath.html('');
                $fileUpload.trigger('click');
            });

            $container.children('div').each(function () {
                addDocumentDeleteLink($(this));
            });

            $fileUpload.change(function (e) {
                e.preventDefault();
                var files = this.files;
                var fd = new FormData();

                if (files.length > 0) {

                    fd.append('document', files[0]);

                    $.ajax({
                        url: obj.uploadPath,
                        data: fd,
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        type: 'POST',
                        success: function (data) {
                            var $template = $($container.data('prototype').replace(/__name__/g, obj.counter));

                            $container.append($template);

                            $container.find(name + obj.counter + '_name').val(data.name);
                            $container.find(name + obj.counter + '_id').val(data.id);
                            $container.find(name + obj.counter + '_extension').val(data.extension);
                            $container.find(name + obj.counter + '_extension_view').html('.' + data.extension);

                            addDocumentDeleteLink($template);

                            obj.counter++;
                        },
                        error: function () {
                            $errorPath.html('<div class="alert alert-warning">\n' +
                                '        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>\n' +
                                obj.errorMessage +
                                '    </div>');
                        },
                        complete: function () {
                            $fileUpload.val('');
                            $fileUpload.trigger('change');
                        }
                    });
                }
            });

            function addDocumentDeleteLink($branchContainer) {
                var $link = $('<a href="#" class="feed-remove" title="' + obj.deleteDocumentLinkMessage + '"></a>');
                $link.on('click', function (e) {
                    e.preventDefault();
                    $branchContainer.remove();
                });
                $branchContainer.find('.remove-form').append($link);
            }
        })();
    }
}
