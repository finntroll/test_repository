<?php

namespace AppBundle\Security\Voter;

use AppBundle\Entity\BestPractice;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class BestPracticeVoter extends Voter
{
    const CAN_CREATE_BEST_PRACTICE = 'CAN_CREATE_BEST_PRACTICE';
    const CAN_CREATE_UNIVERSITY_BEST_PRACTICE = 'CAN_CREATE_UNIVERSITY_BEST_PRACTICE';
    const CAN_EDIT_BEST_PRACTICE = 'CAN_EDIT_BEST_PRACTICE';
    const CAN_EDIT_UNIVERSITY_BEST_PRACTICE = 'CAN_EDIT_UNIVERSITY_BEST_PRACTICE';
    const CAN_REMOVE_BEST_PRACTICE = 'CAN_REMOVE_BEST_PRACTICE';
    const CAN_PUBLISH_BEST_PRACTICE = 'CAN_PUBLISH_BEST_PRACTICE';

    const FULL_ACCESS_ROLES = [
        'ROLE_SITE_ADMIN',
        'ROLE_NEWS_EDITOR',
        'ROLE_GLOBAL_UNIVERSITY_ADMIN',
    ];

    const RESTRICTED_ACCESS_ROLES = [
        'ROLE_UNIVERSITY_EDITOR',
        'ROLE_UNIVERSITY_ADMIN',
    ];

    const BEST_PRACTICE_PUBLISHER_ROLE = ['ROLE_NEWS_EDITOR'];

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }
    
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [
            self::CAN_CREATE_BEST_PRACTICE, 
            self::CAN_CREATE_UNIVERSITY_BEST_PRACTICE,
            self::CAN_EDIT_BEST_PRACTICE,
            self::CAN_EDIT_UNIVERSITY_BEST_PRACTICE,
            self::CAN_REMOVE_BEST_PRACTICE,
            self::CAN_PUBLISH_BEST_PRACTICE,
        ])) {
            return false;
        }

        if (!$subject instanceof BestPractice && !is_null($subject)) {
            return false;
        }

        return true;
    }
    
    protected function voteOnAttribute($attribute, $bestPractice, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case self::CAN_CREATE_BEST_PRACTICE:
                return $this->canCreateBestPractice($bestPractice, $token);
            case self::CAN_CREATE_UNIVERSITY_BEST_PRACTICE:
                return $this->canCreateUniversityBestPractice($bestPractice, $token);
            case self::CAN_EDIT_BEST_PRACTICE:
                return $this->canEditBestPractice($bestPractice, $token);
            case self::CAN_EDIT_UNIVERSITY_BEST_PRACTICE:
                return $this->canEditUniversityBestPractice($bestPractice, $token);
            case self::CAN_REMOVE_BEST_PRACTICE:
                return $this->canRemoveBestPractice($bestPractice, $token);
            case self::CAN_PUBLISH_BEST_PRACTICE:
                return $this->canPublishBestPractice($bestPractice, $token);
        }

        return false;
    }

    private function canCreateBestPractice(BestPractice $bestPractice = null, TokenInterface $token)
    {
        return $this->decisionManager->decide($token, self::FULL_ACCESS_ROLES);
    }

    private function canCreateUniversityBestPractice(BestPractice $bestPractice = null, TokenInterface $token)
    {
        return $this->canCreateBestPractice($bestPractice, $token) 
               || $this->decisionManager->decide($token, self::RESTRICTED_ACCESS_ROLES);
    }

    private function canEditBestPractice(BestPractice $bestPractice, TokenInterface $token)
    {
        return $this->decisionManager->decide($token, self::FULL_ACCESS_ROLES);
    }

    private function canEditUniversityBestPractice(BestPractice $bestPractice, TokenInterface $token)
    {
        if ($this->canEditBestPractice($bestPractice, $token)) {
            return true;
        }

        if ($this->decisionManager->decide($token, self::RESTRICTED_ACCESS_ROLES)) {
            return $this->sameUniversity($bestPractice, $token) && $bestPractice->isDraft();
        }
        
        return true;
    }

    private function sameUniversity(BestPractice $news, TokenInterface $token) {
        return $news->getUniversity() === $token->getUser()->getUniversity();
    }

    private function canRemoveBestPractice(BestPractice $bestPractice, TokenInterface $token)
    {
        return $this->canEditUniversityBestPractice($bestPractice, $token);
    }

    private function canPublishBestPractice(BestPractice $bestPractice, TokenInterface $token)
    {
        if ($this->decisionManager->decide($token, self::BEST_PRACTICE_PUBLISHER_ROLE)) {
            return $bestPractice->getStatus() !== BestPractice::STATUS_PUBLISHED;
        }

        return false;
    }
}
