<?php

namespace AppBundle\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class MailingVoter extends Voter
{
    const CAN_SEND_SUBSCRIPTION_EMAIL = 'CAN_SEND_SUBSCRIPTION_EMAIL';

    const SEND_MAIL_GROUPS = [
        'ROLE_SITE_ADMIN',
        'ROLE_NEWS_EDITOR',
        'ROLE_GLOBAL_UNIVERSITY_ADMIN'
    ];

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        if (self::CAN_SEND_SUBSCRIPTION_EMAIL !== $attribute) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        return $this->decisionManager->decide($token, self::SEND_MAIL_GROUPS);
    }
}
