<?php

namespace AppBundle\Security\Voter;

use AppBundle\Entity\News;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class NewsVoter extends Voter
{
    const CAN_CREATE_UNIVERSITY_NEWS = 'CAN_CREATE_UNIVERSITY_NEWS';
    const CAN_CREATE_NEWS = 'CAN_CREATE_NEWS';
    const CAN_EDIT_UNIVERSITY_NEWS = 'CAN_EDIT_UNIVERSITY_NEWS';
    const CAN_EDIT_NEWS = 'CAN_EDIT_NEWS';
    const CAN_REMOVE_NEWS = 'CAN_REMOVE_NEWS';
    const CAN_PUBLISH_NEWS = 'CAN_PUBLISH_NEWS';

    const FULL_ACCESS_ROLES = [
        'ROLE_SITE_ADMIN',
        'ROLE_NEWS_EDITOR',
        'ROLE_GLOBAL_UNIVERSITY_ADMIN',
    ];

    const RESTRICTED_ACCESS_ROLES = [
        'ROLE_UNIVERSITY_EDITOR',
        'ROLE_UNIVERSITY_NEWS_USER',
        'ROLE_UNIVERSITY_ADMIN',
    ];

    const NEWS_PUBLISHER_ROLE = ['ROLE_NEWS_EDITOR'];

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     * @inheritdoc
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [
            self::CAN_CREATE_UNIVERSITY_NEWS,
            self::CAN_CREATE_NEWS,
            self::CAN_EDIT_UNIVERSITY_NEWS,
            self::CAN_EDIT_NEWS,
            self::CAN_REMOVE_NEWS,
            self::CAN_PUBLISH_NEWS,
        ])) {
            return false;
        }

        if (!$subject instanceof News && !is_null($subject)) {
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /**
         * @var News $news
         */
        $news = $subject;

        switch ($attribute) {
            case self::CAN_CREATE_NEWS:
                return $this->canCreateNews($news, $token);
            case self::CAN_CREATE_UNIVERSITY_NEWS:
                return $this->canCreateUniversityNews($news, $token);
            case self::CAN_EDIT_NEWS:
                return $this->canEditNews($news, $token);
            case self::CAN_EDIT_UNIVERSITY_NEWS:
                return $this->canEditUniversityNews($news, $token);
            case self::CAN_REMOVE_NEWS:
                return $this->canRemoveNews($news, $token);
            case self::CAN_PUBLISH_NEWS:
                return $this->canPublishNews($news, $token);
        }

        return self::ACCESS_ABSTAIN;
    }

    private function canCreateNews(News $news = null, TokenInterface $token)
    {
        if ($this->decisionManager->decide($token, self::FULL_ACCESS_ROLES)) {
            return true;
        }

        return false;
    }

    private function canCreateUniversityNews(News $news = null, TokenInterface $token)
    {
        if ($this->canCreateNews($news, $token) || $this->decisionManager->decide($token, self::RESTRICTED_ACCESS_ROLES)) {
            return true;
        }

        return false;
    }

    private function canEditNews(News $news, TokenInterface $token)
    {

        if ($this->decisionManager->decide($token, self::FULL_ACCESS_ROLES)) {
            return true;
        }

        return false;
    }

    private function canEditUniversityNews(News $news, TokenInterface $token)
    {
        if ($this->canEditNews($news, $token)) {
            return true;
        }

        if ($this->decisionManager->decide($token, self::RESTRICTED_ACCESS_ROLES)) {
            return $this->sameUniversity($news, $token) && $news->isDraft();
        }

        return false;
    }

    private function sameUniversity(News $news, TokenInterface $token) {
        return $news->getUniversity() === $token->getUser()->getUniversity();
    }

    private function canRemoveNews(News $news, $token)
    {
        return $this->canEditUniversityNews($news, $token);
    }

    private function canPublishNews(News $news, $token)
    {
        if ($this->decisionManager->decide($token, self::NEWS_PUBLISHER_ROLE)) {
            return $news->getStatus() !== News::STATUS_PUBLISHED;
        }

        return false;
    }
}
