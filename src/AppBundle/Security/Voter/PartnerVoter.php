<?php

namespace AppBundle\Security\Voter;

use AppBundle\Entity\News;
use AppBundle\Entity\Partner;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PartnerVoter extends Voter
{
    const CAN_CREATE_PARTNER = 'CAN_CREATE_PARTNER';
    const CAN_EDIT_PARTNER= 'CAN_EDIT_PARTNER';
    const CAN_REMOVE_PARTNER = 'CAN_REMOVE_PARTNER';
    const CAN_PUBLISH_PARTNER = 'CAN_PUBLISH_PARTNER';

    const PARTNER_PUBLISHER_ROLE = ['ROLE_NEWS_EDITOR'];

    const CREATE_EDIT_ACCESS_ROLES = [
        'ROLE_NEWS_EDITOR',
        'ROLE_UNIVERSITY_ADMIN',
        'ROLE_UNIVERSITY_EDITOR',
        'ROLE_UNIVERSITY_NEWS_USER',
        'ROLE_UNIVERSITY_PARTNER',
        'ROLE_GLOBAL_UNIVERSITY_ADMIN',
        'ROLE_SITE_ADMIN',
    ];

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     * @inheritdoc
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [
            self::CAN_CREATE_PARTNER,
            self::CAN_EDIT_PARTNER,
            self::CAN_REMOVE_PARTNER,
            self::CAN_PUBLISH_PARTNER,
        ])) {
            return false;
        }

        if (!$subject instanceof Partner && !is_null($subject)) {
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /**
         * @var Partner $partner
         */
        $partner = $subject;

        switch ($attribute) {
            case self::CAN_CREATE_PARTNER:
                return $this->canCreatePartner($partner, $token);
            case self::CAN_EDIT_PARTNER:
                return $this->canEditPartner($partner, $token);
            case self::CAN_REMOVE_PARTNER:
                return $this->canRemovePartner($partner, $token);
            case self::CAN_PUBLISH_PARTNER:
                return $this->canPublishPartner($partner, $token);
        }

        return self::ACCESS_ABSTAIN;
    }

    private function canCreatePartner(Partner $partner = null, TokenInterface $token)
    {
        if ($this->decisionManager->decide($token, self::CREATE_EDIT_ACCESS_ROLES)) {
            return true;
        }

        return false;
    }

    private function canEditPartner(Partner $partner = null, TokenInterface $token)
    {

        if (!$partner->isPublished()) {
            if ($this->decisionManager->decide($token, self::CREATE_EDIT_ACCESS_ROLES)) {
                return true;
            }
        } else {
            if ($this->decisionManager->decide($token, self::PARTNER_PUBLISHER_ROLE)) {
                return true;
            }
        }

        return false;
    }

    private function canRemovePartner(Partner $partner = null, $token)
    {
        if ($this->decisionManager->decide($token, self::PARTNER_PUBLISHER_ROLE)) {
            return true;
        }

        return false;
    }

    private function canPublishPartner(Partner $partner, $token)
    {
        if ($this->decisionManager->decide($token, self::PARTNER_PUBLISHER_ROLE)) {
            return $partner->getStatus() !== Partner::STATUS_PUBLISHED;
        }

        return false;
    }
}
