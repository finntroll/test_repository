<?php

namespace AppBundle\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PersonalityVoter extends Voter
{
    const CAN_EDIT_PERSONALITY = 'CAN_EDIT_PERSONALITY';
    const CAN_DELETE_PERSONALITY = 'CAN_DELETE_PERSONALITY';

    const FULL_ACCESS_ROLES = [
        'ROLE_SITE_ADMIN',
        'ROLE_GLOBAL_UNIVERSITY_ADMIN',
    ];

    private $accessDecisionManager;

    /**
     * PersonalityVoter constructor.
     *
     * @param AccessDecisionManagerInterface $accessDecisionManager
     */
    public function __construct(AccessDecisionManagerInterface $accessDecisionManager)
    {
        $this->accessDecisionManager = $accessDecisionManager;
    }

    /**
     * @inheritdoc
     */
    protected function supports($attribute, $subject)
    {
        if (in_array($attribute, [
            self::CAN_EDIT_PERSONALITY,
            self::CAN_DELETE_PERSONALITY,
        ])) {
            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        return $this->accessDecisionManager->decide($token, self::FULL_ACCESS_ROLES);
    }
}
