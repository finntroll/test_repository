<?php

namespace AppBundle\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ProfileVoter extends Voter
{
    const ATTRIBUTES_ROLES = [
        'CAN_ACCESS_ACADEMIC_MOBILITY' => [
            'ROLE_UNIVERSITY_EDITOR',
            'ROLE_UNIVERSITY_ADMIN',
            'ROLE_GLOBAL_UNIVERSITY_ADMIN',
            'ROLE_SITE_ADMIN',
        ],
        'CAN_ACCESS_BEST_PRACTICE' => [
            'ROLE_NEWS_EDITOR',
            'ROLE_UNIVERSITY_EDITOR',
            'ROLE_UNIVERSITY_ADMIN',
            'ROLE_GLOBAL_UNIVERSITY_ADMIN',
            'ROLE_SITE_ADMIN',
        ],
        'CAN_ACCESS_CONTEST' => [
            'ROLE_UNIVERSITY_EDITOR',
            'ROLE_UNIVERSITY_ADMIN',
            'ROLE_GLOBAL_UNIVERSITY_ADMIN',
            'ROLE_SITE_ADMIN',
        ],
        'CAN_ACCESS_PROGRAM' => [
            'ROLE_UNIVERSITY_PROGRAM_EDITOR',
            'ROLE_UNIVERSITY_EDITOR',
            'ROLE_UNIVERSITY_ADMIN',
            'ROLE_GLOBAL_UNIVERSITY_ADMIN',
            'ROLE_SITE_ADMIN',
        ],
        'CAN_ACCESS_NEWS' => [
            'ROLE_UNIVERSITY_EDITOR',
            'ROLE_UNIVERSITY_ADMIN',
            'ROLE_UNIVERSITY_NEWS_USER',
            'ROLE_GLOBAL_UNIVERSITY_ADMIN',
            'ROLE_SITE_ADMIN',
            'ROLE_NEWS_EDITOR',
        ],
        'CAN_ACCESS_STATS' => [
            'ROLE_UNIVERSITY_ADMIN',
            'ROLE_GLOBAL_UNIVERSITY_ADMIN',
            'ROLE_SITE_ADMIN',
        ],
        'CAN_ACCESS_PARTNER' => [
            'ROLE_NEWS_EDITOR',
            'ROLE_UNIVERSITY_EDITOR',
            'ROLE_UNIVERSITY_ADMIN',
            'ROLE_GLOBAL_UNIVERSITY_ADMIN',
            'ROLE_SITE_ADMIN',
        ],
        'CAN_EDIT_UNIVERSITY' => [
            'ROLE_UNIVERSITY_EDITOR',
            'ROLE_UNIVERSITY_ADMIN',
            'ROLE_GLOBAL_UNIVERSITY_ADMIN',
            'ROLE_SITE_ADMIN',
        ],
        'CAN_ACCESS_SUBSCRIPTION' => [
            'ROLE_SITE_ADMIN',
            'ROLE_NEWS_EDITOR',
            'ROLE_GLOBAL_UNIVERSITY_ADMIN',
        ],
        'CAN_ACCESS_STATIC_PAGES' => [
            'ROLE_SITE_ADMIN',
            'ROLE_NEWS_EDITOR',
            'ROLE_GLOBAL_UNIVERSITY_ADMIN',
        ],
        'CAN_ACCESS_PERSONALITY' => [
            'ROLE_SITE_ADMIN',
            'ROLE_GLOBAL_UNIVERSITY_ADMIN',
        ],
        'CAN_ACCESS_PROJECT' => [
            'ROLE_SITE_ADMIN',
            'ROLE_GLOBAL_UNIVERSITY_ADMIN',
        ],
    ];

    private $accessDecisionManager;

    /**
     * @param AccessDecisionManagerInterface $accessDecisionManager
     */
    public function __construct(AccessDecisionManagerInterface $accessDecisionManager)
    {
        $this->accessDecisionManager = $accessDecisionManager;
    }

    /**
     * @inheritdoc
     */
    protected function supports($attribute, $subject)
    {
        if (in_array($attribute, array_keys(self::ATTRIBUTES_ROLES))) {
            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if ($attribute === 'CAN_EDIT_UNIVERSITY') {
            $hasUniversity = $token->getUser()->getUniversity() ? true : false;

            return $this->accessDecisionManager->decide($token, self::ATTRIBUTES_ROLES[$attribute]) && $hasUniversity;
        }

        return $this->accessDecisionManager->decide($token, self::ATTRIBUTES_ROLES[$attribute]);
    }
}
