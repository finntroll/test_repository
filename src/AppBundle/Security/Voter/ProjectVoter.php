<?php

namespace AppBundle\Security\Voter;

use AppBundle\Entity\Personality;
use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectMember;
use AppBundle\Repository\ProjectRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ProjectVoter extends Voter
{
    const CAN_ACCESS_PROJECT = 'CAN_ACCESS_PROJECT';

    const CAN_CREATE_PROJECT = 'CAN_CREATE_PROJECT';
    const CAN_EDIT_PROJECT = 'CAN_EDIT_PROJECT';
    const CAN_DELETE_PROJECT = 'CAN_DELETE_PROJECT';
    const CAN_BROWSE_PROJECT = 'CAN_BROWSE_PROJECT';
    const CAN_PUBLISH_PROJECT = 'CAN_PUBLISH_PROJECT';

    const CAN_ASSIGN_PROJECT_HEAD = 'CAN_ASSIGN_PROJECT_HEAD';
    const CAN_ASSIGN_PROJECT_MANAGER = 'CAN_ASSIGN_PROJECT_MANAGER';
    const CAN_INVITE_PROJECT_MEMBERS = 'CAN_INVITE_PROJECT_MEMBERS';

    const FULL_ACCESS_ROLES = [
        'ROLE_SITE_ADMIN',
        'ROLE_GLOBAL_UNIVERSITY_ADMIN',
    ];

    private $decisionManager;
    private $repository;

    public function __construct(AccessDecisionManagerInterface $decisionManager, ProjectRepository $repository)
    {
        $this->decisionManager = $decisionManager;
        $this->repository = $repository;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [
            self::CAN_ACCESS_PROJECT,
            self::CAN_CREATE_PROJECT,
            self::CAN_EDIT_PROJECT,
            self::CAN_DELETE_PROJECT,
            self::CAN_ASSIGN_PROJECT_HEAD,
            self::CAN_ASSIGN_PROJECT_MANAGER,
            self::CAN_INVITE_PROJECT_MEMBERS,
            self::CAN_BROWSE_PROJECT,
            self::CAN_PUBLISH_PROJECT,
        ])) {
            return false;
        }

        if (!$subject instanceof Project && !is_null($subject)) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $project, TokenInterface $token)
    {
        switch ($attribute) {
            case self::CAN_CREATE_PROJECT:
                return $this->canCreateProject($token);
            case self::CAN_PUBLISH_PROJECT:
                return $this->canPublishProject($project, $token);
            case self::CAN_DELETE_PROJECT:
                return $this->canDeleteProject($project, $token);
            case self::CAN_ASSIGN_PROJECT_HEAD:
                return $this->canAssignPersonalities($project, $token);
            case self::CAN_ASSIGN_PROJECT_MANAGER:
                return $this->canAssignPersonalities($project, $token);
            case self::CAN_INVITE_PROJECT_MEMBERS:
                return $this->canAssignPersonalities($project, $token);
            case self::CAN_EDIT_PROJECT:
                return $this->canEditProject($project, $token);
            case self::CAN_BROWSE_PROJECT:
                return $this->canBrowseProject($project, $token);
            case self::CAN_ACCESS_PROJECT:
                return $this->canAccessProject($token);
        }

        return false;
    }

    private function canCreateProject(TokenInterface $token)
    {
        return $this->isAdmin($token);
    }

    private function canPublishProject(Project $project, TokenInterface $token)
    {
        if ($project && !$project->getIsPublished() && $this->isAdmin($token)) {
            return true;
        }

        return false;
    }

    private function canDeleteProject(Project $project = null, TokenInterface $token)
    {
        return $this->isAdmin($token);
    }

    private function canAssignPersonalities(Project $project, $token)
    {
        return $this->isAdmin($token) && $project->isOpen();
    }

    private function canEditProject(Project $project, TokenInterface $token)
    {
        // Closed project cannot be edited by anyone
        if (!$project->isOpen() && $project->getIsPublished()) {
            return false;
        }

        // Admin can edit open project regardless publish status
        if ($this->isAdmin($token)) {
            return true;
        }

        // Manager and head can edit published and opened project
        if ($project->getIsPublished() && ($this->isHead($project, $token) || $this->isManager($project, $token))) {
            return true;
        }

        // Members and others cannot edit project
        return false;
    }

    private function canBrowseProject(Project $project, TokenInterface $token)
    {
        // Cannot browse project if it isn't published
        if (!$project->getIsPublished()) {
            return false;
        }

        // All participating personalities can browse published project
        if ($this->isAdmin($token)
            || $this->isManager($project, $token)
            || $this->isMember($project, $token)
            || $this->isHead($project, $token)
        ) {
            return true;
        }

        // Other cannot browse project
        return false;
    }

    /**
     * This method is responsible for displaying in the profile menu
     *
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canAccessProject(TokenInterface $token)
    {
        // Show always for admin (Be care! Admin is not Personality)
        if ($this->isAdmin($token)) {
            return true;
        }

        $user = $token->getUser();

        // Allowed only for Personalities
        if (!$user instanceof Personality) {
            return false;
        }

        // Show for personality if it has participating projects
        if (count($this->repository->findByHeadManagerOrMember($user)) > 0) {
            return true;
        }

        return false;
    }

    private function isAdmin(TokenInterface $token)
    {
        return $this->decisionManager->decide($token, self::FULL_ACCESS_ROLES);
    }

    private function isHead(Project $project, TokenInterface $token)
    {
        return $token->getUser() === $project->getHead();
    }

    private function isManager(Project $project, TokenInterface $token)
    {
        return $token->getUser() === $project->getManager();
    }

    private function isMember(Project $project, TokenInterface $token)
    {
        $user = $token->getUser();

        /** @var ProjectMember $member */
        foreach ($project->getProjectMembers() as $member) {
            if ($member->getPersonality() === $user) {
                return true;
            }
        }

        return false;
    }
}
