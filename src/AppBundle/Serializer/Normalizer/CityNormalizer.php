<?php

namespace AppBundle\Serializer\Normalizer;

use AppBundle\Entity\City;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CityNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'name' => (string) $object,
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof City;
    }
}
