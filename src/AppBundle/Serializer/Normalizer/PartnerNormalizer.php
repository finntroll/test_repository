<?php

namespace AppBundle\Serializer\Normalizer;

use AppBundle\Entity\Partner;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class PartnerNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'name' => (string) $object,
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Partner;
    }
}
