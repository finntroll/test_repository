<?php

namespace AppBundle\Serializer\Normalizer;

use AppBundle\Entity\EntranceTest;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Translation\TranslatorInterface;

class TransKeyObjectNormalizer implements NormalizerInterface
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'name' => $this->translator->trans($object->getTransKey(), [], 'dictionary'),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return method_exists($data, 'getTransKey');
    }
}
