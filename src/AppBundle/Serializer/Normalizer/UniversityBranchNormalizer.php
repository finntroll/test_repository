<?php

namespace AppBundle\Serializer\Normalizer;

use AppBundle\Entity\UniversityBranch;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UniversityBranchNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'name' => $object->getCity()->translate()->getName(),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof UniversityBranch;
    }
}
