<?php

namespace AppBundle\Service;

use AppBundle\Entity\UploadedDocuments\UploadedDocument;
use Aws\S3\S3Client;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;

class ArchiveManager
{
    /**
     * @var S3Client
     */
    private $s3;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @var string
     */
    private $bucket;

    /**
     * @var string
     */
    private $webDir;

    /**
     * ArchiveManager constructor.
     *
     * @param S3Client               $s3
     * @param EntityManagerInterface $em
     * @param string                 $bucket
     * @param string                 $webDir
     */
    public function __construct(S3Client $s3, EntityManagerInterface $em, Filesystem $fs, $bucket, $webDir)
    {
        $this->s3 = $s3;
        $this->em = $em;
        $this->fs = $fs;
        $this->bucket = $bucket;
        $this->webDir = $webDir;
    }

    public function archive(UploadedDocument $document, array $s3Options = [], $needFlush = true)
    {
        if ($document->isArchived()) {
            return null;
        }

        $key = $this->getKey($document);

        $s3Options = array_merge([
                'Key' => $key,
                'Bucket' => $this->bucket,
                'SourceFile' => $this->webDir . $document->getRelativePath(),
                'ContentDisposition' => 'attachment; filename="'.$document->getDownloadName().'"',
                'ACL' => 'public-read',
            ], $s3Options);

        $result = $this->s3->putObject($s3Options);
        $document->setContentStatus(UploadedDocument::CONTENT_STATUS_ARCHIVED);
        $document->setArchivedAt(new \DateTime());
        $this->fs->remove($this->webDir . $document->getRelativePath());

        $this->em->persist($document);

        if ($needFlush) {
            $this->em->flush();
        }

        return $result;
    }

    public function getUrl(UploadedDocument $document)
    {
        if (!$document->isArchived()) {
            return false;
        }

        return $this->s3->getObjectUrl($this->bucket, $this->getKey($document));
    }

    public function delete(UploadedDocument $document, $needFlush = true)
    {
        if (!$document->isArchived()) {
            return null;
        }

        $result = $this->s3->deleteObject([
            'Key' => $this->getKey($document),
            'Bucket' => $this->bucket,
        ]);

        $this->em->remove($document);

        if ($needFlush) {
            $this->em->flush();
        }

        return $result;
    }

    private function getKey(UploadedDocument $document)
    {
        return str_replace($this->webDir, '', $document->getPath()).$document->getInternalName();
    }
}
