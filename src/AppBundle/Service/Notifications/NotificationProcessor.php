<?php

namespace AppBundle\Service\Notifications;

use Doctrine\ORM\EntityManagerInterface;
use Monolog\Logger;

class NotificationProcessor
{
    private $storage;
    private $subscribersManager;
    private $notificator;
    private $em;
    private $logger;

    public function __construct(NotificationStorage $notificationStorage, SubscribersDataDistributor $subscribersManager, Notificator $notificator, EntityManagerInterface $em, Logger $logger)
    {
        $this->storage = $notificationStorage;
        $this->subscribersManager = $subscribersManager;
        $this->notificator = $notificator;
        $this->em = $em;
        $this->logger = $logger;
    }

    public function process()
    {
        foreach ($this->storage->getEventList() as $event) {
            $this->notifyAboutEvent($event);
        }
    }

    private function notifyAboutEvent(string $event)
    {
        try {
            $data = $this->storage->popEventData($event);
            $objects = $this->hydrateData($data);
            $usersObjectsMap = $this->subscribersManager->getSubscribersForEvent($event, $objects);

            foreach ($usersObjectsMap as $usersObjects) {
                $users = $usersObjects['users'];
                $objects = $usersObjects['objects'];

                if (empty($users) || empty($objects)) {
                    $this->storage->pushEventData($event, $data);

                    return;
                }

                if (!$this->notificator->notify($event, $objects, $users)) {
                    $this->storage->pushEventData($event, $data);
                    $this->logger->log('error', 'NOTIFICATIONS: '.$this->notificator->getErrorMessage());
                }
            }
        } catch (\Exception $e) {
            $this->logger->log('error', $e);
        }
    }

    private function hydrateData(array $data): array
    {
        $hydrated = array_map(function ($item) {
            list($class, $id) = explode(':', $item);
            $object = $this->em->getRepository($class)->find($id);

            return $object;
        }, $data);

        return array_filter($hydrated, function ($object) {
            return (bool) $object;
        });
    }
}
