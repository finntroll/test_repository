<?php

namespace AppBundle\Service\Notifications;

use Predis\Client as Predis;

class NotificationStorage
{
    const PREFIX = 'notification:';

    private $redis;

    public function __construct(Predis $redis)
    {
        $this->redis = $redis;
    }

    public function getEventList()
    {
        $keys = $this->redis->keys(self::PREFIX.'?*');
        $events = array_map(function($key) {
                return str_replace(self::PREFIX, '', $key);
            }, $keys);

        return $events;
    }

    public function popEventData(string $eventName): array
    {
        $data = [];
        $popped = $this->redis->spop($this->generateKey($eventName));

        while (!is_null($popped)) {
            $data[] = $popped;
            $popped = $this->redis->spop($this->generateKey($eventName));
        }

        return $data;
    }

    public function pushEventData(string $eventName, $data): bool
    {
        if (!is_array($data)) {
            $data = [$data];
        }

        $pushedCount = $this->redis->sadd($this->generateKey($eventName), $data);

        return $pushedCount === count($data);
    }

    private function generateKey($eventName)
    {
        return self::PREFIX.$eventName;
    }
}
