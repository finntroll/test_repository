<?php

namespace AppBundle\Service\Notifications;

use AppBundle\Entity\Personality;
use AppBundle\Entity\User;
use AppBundle\Service\SubscriptionMailer;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class NotificationsMailer extends SubscriptionMailer
{
    public function sendNotification(string $template, array $data, array $users)
    {
        $rendered = $this->templating->render($template, [
            'data' => $data,
        ]);

        $to = array_pop($users)->getEmail();
        $bcc = [];
        if  (count($users) > 0) {
            $bcc = array_map(function(User $user) {
                return $user->getEmail();
            }, $users);
        }

        return $this->sendMessage($rendered, $to, null, $bcc);
    }

    public function sendInvitationToRegistration(string $template, Personality $personality, string $registrationLink, array $data = [])
    {
        // First line of rendered template is the subject of message
        $rendered = $this->templating->render($template, [
            'data' => $data,
            'registration_link' => $registrationLink,
            'personality' => $personality,
        ]);

        $to = $personality->getEmail();

        return $this->sendMessage($rendered, $to);
    }
}
