<?php

namespace AppBundle\Service\Notifications;

class Notificator
{
    private $errorMessage = "";
    private $mailer;
    private $eventsMap;

    public function __construct(NotificationsMailer $mailer, array $eventsMap)
    {
        $this->mailer = $mailer;
        $this->eventsMap = $eventsMap;
    }

    public function notify($event, array $data, array $users): bool
    {
        $template = $this->eventsMap[$event]['template'];

        try {
            $this->mailer->sendNotification($template, $data, $users);
        } catch (\Exception $e) {
            $this->errorMessage = $e->getMessage();

            return false;
        }

        return true;
    }

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }
}
