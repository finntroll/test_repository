<?php

namespace AppBundle\Service\Notifications;

use AppBundle\Entity\University;
use Doctrine\ORM\EntityManagerInterface;

class SubscribersDataDistributor
{
    private $eventsMap;
    private $em;

    public function __construct(array $eventsMap, EntityManagerInterface $em)
    {
        $this->eventsMap = $eventsMap;
        $this->em = $em;
    }

    /**
     * @param string $eventName
     * @param array  $data
     *
     * @return array Format:
     *              [
     *                  ['users' => [], 'objects' => []],
     *                  ['users' => [], 'objects' => []],
     *                  ...
     *              ]
     * @throws \Exception
     */
    public function getSubscribersForEvent(string $eventName, array $data): array
    {
        $eventConfig = $this->eventsMap[$eventName];

        $roles = (array) $eventConfig['roles'];

        if (empty($roles)) {
            throw new \Exception('You must specify array of roles for event '.$eventName);
        }

        $type = $eventConfig['type'];

        switch ($type) {
            case 'university':
                $usersObjectsMap = $this->getSortedUniversitySubscribers($roles, $data);
                break;
            case 'common':
                $usersObjectsMap = $this->getSubscribersForCommonEvent($roles, $data);
                break;
            default:
                throw new \Exception('You have specified unavailable type "'.$type.'"" for event '.$eventName
                    .'. Available types are "university" and "common".');
        }

        return $usersObjectsMap;
    }

    private function getSortedUniversitySubscribers(array $roles, array $data)
    {
        $repo = $this->em->getRepository('AppBundle:User');

        $objectsByUniversity = [];
        foreach ($data as $object) {
            if ($object instanceof University) {
                $universityId = $object->getId();
            } else {
                $universityId = $object->getUniversity()->getId();
            }
            $objectsByUniversity[$universityId][] = $object;
        }

        $sorted = [];
        foreach ($objectsByUniversity as $universityId => $objects) {
            $sorted[] = [
                'users' => $repo->findByRolesAndUniversity($roles, $universityId),
                'objects' => $objects,
            ];
        }

        return $sorted;
    }

    private function getSubscribersForCommonEvent(array $roles, array $data)
    {
        $repo = $this->em->getRepository('AppBundle:User');

        return [
            [
                'users' => $repo->findByRoles($roles),
                'objects' => $data,
            ],
        ];
    }
}
