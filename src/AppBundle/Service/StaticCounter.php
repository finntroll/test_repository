<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class StaticCounter
 *
 * @package AppBundle\Service
 */
class StaticCounter
{

    const TYPE_CONTEST = 'contest';
    const TYPE_ACADEMIC = 'academic';

    private static $contestQuery = "(
    SELECT COUNT(DISTINCT(%table_asset%.id))
    FROM contest %table_asset%
        JOIN contest_type as ct_%table_asset% 
            ON (%table_asset%.contest_type_id = ct_%table_asset%.id)
        JOIN contest_audience ca_%table_asset% 
            ON (%table_asset%.id = ca_%table_asset%.contest_id)
        JOIN audience au_%table_asset% 
            ON (ca_%table_asset%.audience_id = au_%table_asset%.id)
    WHERE au_%table_asset%.trans_key IN (:audience%table_asset%)
        AND ct_%table_asset%.transKey IN (:data%table_asset%)
        AND %table_asset%.is_published = 1
  ) AS %table_asset%
";

    private static $academicQuery = "(
    SELECT COUNT(DISTINCT(%table_asset%.id))
    FROM academic_mobility %table_asset%
      JOIN academic_mobility_type as at_%table_asset% 
         ON (%table_asset%.mobility_type_id = at_%table_asset%.id)
      JOIN academic_mobility_audience ama_%table_asset% 
         ON (%table_asset%.id = ama_%table_asset%.academic_mobility_id)
      JOIN audience au_%table_asset%
         ON (ama_%table_asset%.audience_id = au_%table_asset%.id)
    WHERE au_%table_asset%.trans_key IN (:audience%table_asset%) 
            AND at_%table_asset%.trans_key IN (:data%table_asset%)
            AND %table_asset%.is_published = 1
  ) AS %table_asset%
";

    private $em;

    /**
     * StaticCounter constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     *
     * @param array $types
     * @return array
     */
    public function getCountersByTypes(array $types = [])
    {
        $query = 'SELECT';

        $numItems = count($types);
        $i = 0;
        foreach ($types as $key => $type) {
            switch ($type['type']) {
                case self::TYPE_ACADEMIC:
                    $template = self::$academicQuery;
                    break;
                case self::TYPE_CONTEST:
                    $template = self::$contestQuery;
                    break;
                default:
                    $template = '';
                    break;
            }
            $query .= str_replace(
                '%table_asset%',
                $key,
                $template
            );
            if (++$i !== $numItems) {
                $query .= ',';
            }
        }


        foreach ($types as $key => $type) {
            $query = str_replace(
                ':' . 'data' . $key,
                $this->formatArray($type['data']),
                $query
            );
            $query = str_replace(
                ':' . 'audience' . $key,
                $this->formatArray($type['audience']),
                $query
            );
        }

        $statement = $this->em
            ->getConnection()
            ->prepare($query);

        $statement->execute();

        $result = $statement->fetchAll();

        if (count($result) > 0) {
            return $result[0];
        }

        return null;
    }

    private function formatArray(array $array)
    {
        return '\'' . implode('\', \'', $array) . '\'';
    }
}
