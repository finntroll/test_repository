<?php

namespace AppBundle\Service;

use AppBundle\Entity\Statistic;
use Doctrine\ORM\EntityManagerInterface;
use Predis\Client as Predis;
use Symfony\Component\Cache\Simple\RedisCache;
use Symfony\Component\Translation\Translator;

class StatisticReportCreator
{
    /**
     * Кол-во точек для каждого графика
     * В месяце 30, т.к мы делаем отступ от текущей даты.
     */
    const POINTS_COUNT__DAY = 24;
    const POINTS_COUNT__WEEK = 7;
    const POINTS_COUNT__MONTH = 30;
    const POINTS_COUNT__QUARTER = 3;
    const POINTS_COUNT__YEAR = 12;

    const LIFETIME = 60 * 60; // 1 hour

    const SECONDS_IN_HOUR = 60 * 60;
    const SECONDS_IN_DAY = self::SECONDS_IN_HOUR * 24;

    const NAMESPACE = 'statistic';

    const COLORS_BY_TYPE = [
        'program' => 'rgb(255, 99, 132)',
        'academicMobility' => 'rgb(255, 159, 64)',
        'news' => 'rgb(255, 205, 86)',
        'contest' => 'rgb(75, 192, 192)',
        'bestPractice' => 'rgb(54, 162, 235)',
        'university' => 'rgb(153, 102, 255)',
        'all' => 'rgb(201, 203, 207)',
    ];

    /**
     * @var Predis
     */
    private $redis;

    /**
     * @var RedisCache
     */
    private $cache;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Translator
     */
    private $translator;

    private $locale = 'ru';

    /**
     * StatisticReportCreator constructor.
     * @param EntityManagerInterface $entityManager
     * @param Predis $redis
     * @param Translator $translator
     */
    public function __construct(EntityManagerInterface $entityManager, Predis $redis, Translator $translator)
    {
        $this->em = $entityManager;
        $this->redis = $redis;
        $this->translator = $translator;

        $this->cache = new RedisCache($redis, self::NAMESPACE, self::LIFETIME);
    }

    public function getReport($dateType, $universityId = null)
    {
        $key = $dateType . '.';
        if ($universityId) {
            $key .= $universityId;
        } else {
            $key .= 'all';
        }

        if (!$this->cache->has($key)) {
            $statistic = $this->performStatistic($dateType, $universityId);
            $this->cache->set($key, $statistic);
        }

        return $this->cache->get($key);
    }

    /**
     * BEWARE STRANGER!
     *
     * @param $dateType
     * @param $universityId
     *
     * @return array
     */
    private function performStatistic($dateType, $universityId): array
    {
        /*
         * Все даты устанавливаем в 0 0 0, начало дня.
         * Для того чтобы отрезки при растоновке точек были от начала дня
         * Дата начала будет выставлятся в зависимости от типа графика
         *
         * В некоторых случаях отступы по секундам указаны явно($stepSeconds)
         * Конечную дату ставим на "завтра"
         * Отображение lables на графике меняется в зависимости от типа($format)
         */
        $firstDate = new \DateTime();
        $firstDate->setTime(0, 0);

        $endDate = new \DateTime();
        $endDate->modify('+1 day'); // Финальная дата является "завтрашним" днём
        $endDate->setTime(0, 0);

        $format = 'd.m.Y';
        $stepSeconds = 0;

        switch ($dateType) {
            case 'day': //
                $points = self::POINTS_COUNT__DAY;
                $format = 'H:i';
                $endDate->modify('-1 second'); // Устанавливаем дату на конец дня
                $stepSeconds = self::SECONDS_IN_HOUR; // 1 hour
                break;
            case 'month':
                $firstDate->modify('-1 month'); // Отступаем на месяц
                $firstDate->modify('+1 day'); // И прибавляем 1 день (выравниваем график)
                $points = self::POINTS_COUNT__MONTH;
                $stepSeconds = self::SECONDS_IN_DAY; // 24 hours
                break;
            case 'quarter':
                // Тут всё указано явно чтобы не запутаться
                $firstDate->modify('-2 month'); // отступаем 2 месяца
                $firstDate->modify('first day of this month'); // Устанавливаем дату на начало этого(на который отступили) месяца
                $endDate->modify('first day of next month'); // устанавливаем дату конца на первый день след месяца(конец графика)
                $points = self::POINTS_COUNT__QUARTER;
                $stepSeconds = 1; // устанавливаем сдвиг на 1 секунду.
                $format = 'F';
                break;
            case 'year':
                $firstDate->modify('-1 year'); // Отступаем год
                $firstDate->modify('first day of next month'); // Ставим дату на начало месяца первого месяца
                $endDate->modify('first day of next month'); // Ставим дату на начало след месяца (конец графика)
                $points = self::POINTS_COUNT__YEAR;
                $stepSeconds = 1; // устанавливаем сдвиг на 1 секунду.
                $format = 'F';
                break;
            case 'week': // week is default
            default:
                $firstDate->modify('-1 week'); // Отступаем неделю
                $firstDate->modify('+1 day'); // Берём 7 дней с нашим включительно т.е. если вторник, то [Ср,Чт,Пят,Сб,Вс,Пн,Вт]
                $stepSeconds = self::SECONDS_IN_DAY;
                $format = 'l';
                $points = self::POINTS_COUNT__WEEK;
                break;
        }

        /*
         * Для того чтобы обойти долгую доктриновскую гидрацию данных, всё делаем на чистом SQL
         * Прирост значительный.
         *
         * TODO: стэйтменты не работали, может можно сменить
         */
        $sql = 'SELECT * FROM statistic ';

        $strDateStart = $firstDate->format('Y-m-d H:i:s');
        $strDateEnd = $endDate->format('Y-m-d H:i:s');
        $sql .= "WHERE (event_date BETWEEN '$strDateStart' AND '$strDateEnd') ";
        if ($universityId) {
            $sql .= 'AND university_id = ' . $universityId . ' ';
        }
        $sql .= 'GROUP BY id, object ORDER BY event_date ASC';


        $statement = $this->em->getConnection()->prepare($sql);
        $statement->execute();
        $rawData = $statement->fetchAll();

        /*
         * Если мы не указали шаг, то мы его вычисляем.
         *
         * Берём расстояние начала и конца, вычисляем разницу между ними.
         * смотрим кол-во требуемых точек на графике, отнимаем одну(последняя точка, это сегодня)
         * Получаем кол-во секунд между точками
         *
         * Функция для подсчёта интервалов понадобится если мы будем считать данные по датам
         * Например между самой первой точкой и самой последней.
         *
         */
        if ($stepSeconds !== 0) {
            $diffSeconds = $stepSeconds;
        } else {
            $seconds = \floor($endDate->getTimestamp() - $firstDate->getTimestamp());
            $diffSeconds = \floor($seconds / ($points - 1)); // -1 for last day today
        }

        /*
         * Создаём массив дат, в который добавляем даты(точки)
         * Т.к. в месяц имеет не фиксированное кол-во часов, то для квартала и года, мы считаем отдельно, по месяцам
         */
        $dateEnter = [];
        if ($dateType !== 'quarter' && $dateType !== 'year') {
            // Пока мы попадаем в интервал, мы заполняем массив точек.
            // Массив точек представляет собой массив таймстемпов.
            // Дата устанавливается на границу точки напр (00:59:59), след (01:59:59)
            while ($firstDate->getTimestamp() <= $endDate->getTimestamp() - $diffSeconds - 1) {
                $dateEnter[] = $firstDate->getTimestamp();
                $firstDate->modify('+' . $diffSeconds . ' second');
            }
            $dateEnter[] = $endDate->getTimestamp() - 1; // последняя дата устанавливатся на границу - 1 секунда
        } else {
            $firstDate->modify('last day of this month'); // После поиска мы должны сдвинуть начальную дату на границу точки
            $i = 0;
            while ($i++ < $points) {
                // устанавливаем точку на её границе напр (31.01.2017 23:59:59)
                $dateEnter[] = $firstDate->getTimestamp() + self::SECONDS_IN_DAY -1;
                // Сдвигаем дату на конец след месяца напр (31.01.2017 23:59:59 -> 28.02.2017 00:00:00)
                $firstDate->modify('last day of next month');

            }
        }

        /*
         * Пожалуйста не бейте
         * Тут собирается в кучу сырой результат, это бы тоже упростить
         *
         */
        $rawResult = [];
        $rawResult['all'][Statistic::EVENT_INTERNAL] = 0;
        $rawResult['all'][Statistic::EVENT_EXTERNAL] = 0;
        $rawResult['all']['color'] = self::COLORS_BY_TYPE['all'];
        foreach ($rawData as $data) {
            $rawResult[$data['object']][$data['type']]['all'] = 0;
            $rawResult[$data['object']][$data['type']]['color'] = self::COLORS_BY_TYPE[$data['object']];
            for ($i = 0; $i < $points; ++$i) {
                $rawResult[$data['object']][$data['type']]['data'][$i] = 0;
            }
        }

        /*
         * Тут бы упростить, но как?
         *
         * Мы обходим все данные, и по каждым данным мы смотрим попадания в дату
         * если попадание есть обрываем фильтрацию, т.к. она попадает и в след точки.
         *
         * Все данные отсортированны по датам, так что повторных попаданий нету.(из за break;)
         *
         * Также мы агрегируем общие данные по типу (внутренний/внешний переход)
         *
         * А ещё мы собираем все данные по всем типам во внутренние и внешние
         *
         * Требуется создать объект даты чтобы вернуть timestamp, т.к. мы достаём из sql сырой вывод.
         */
        foreach ($rawData as $data) {
            $eventDate = \DateTime::createFromFormat('Y-m-d H:i:s', $data['event_date']); // Востанавливаем дату
            $rawResult[$data['object']][$data['type']]['all'] += 1; // Все данные по объекту
            $rawResult['all'][$data['type']] += 1; // Все данные по типу
            // тут смотрим попадание
            foreach ($dateEnter as $key => $pointDate) { // достаём границы точек
                /*
                 * Отступаем до конца точки -1 (в некоторых точках уже явно указаны границы)
                 * В графиках по году и кварталу уже стоят границы, поэтому мы устанавливаем $diffSeconds = 1
                 *
                 * В остальных графиках мы имеем только отступ (День неделя месяц)
                 * $pointDate установлена на верхнюю границу, мы должны её сдвинуть на нижнюю, для этого прибавляем дифф и отнимаем 1
                 * Смотрим, если дата меньше либо равна граничной, то мы попадаем в неё.
                 */
                $offset = $pointDate + $diffSeconds - 1; //
                if ($eventDate->getTimestamp() <= $offset) {
                    $rawResult[$data['object']][$data['type']]['data'][$key] += 1;
                    break;
                }
            }
        }

        /*
         * Заполняем массив labels
         * Для некоторых данных требуются переводы
         * Формат подписи устанавливается в зависимости от типа
         */
        $labels = [];
        foreach ($dateEnter as $pointDate) {
            $val = (new \DateTime())->setTimestamp($pointDate)->format($format);
            if ($dateType === 'week' || $dateType === 'year' || $dateType === 'quarter') {
                $labels[] = $this->translator->trans($val, [], 'statistic', $this->locale);
            } else {
                $labels[] = $val;
            }

        }

        /*
        * Тут мы готовим уже массив к отпрвке
        */
        $result = [
            'table' => [ // Данные для таблицы
                'all' => $rawResult['all']
            ],
            'chart' => [   // Данные для графика
                'labels' => $labels,
                'data' => [],
            ]
        ];

        // Нам требуется выводить данные как в таблицу так и в график, для этого надо сбить данные и вернуть их в одной (рядовой)куче
        // проблема в том, что мы в обычную таблицу тянем данные для графика, как избавиться я не очень знаю ((
        foreach ($rawResult as $preResultKey => $preResultVal) {
            $result['table'][$preResultKey] = $preResultVal;
            if ($preResultKey === 'all') {
                continue; // All не попадает в график
            }
            foreach ($preResultVal as $k => $v) {
                $data = [
                    'label' => $this->translator->trans($preResultKey . '.' . $k, [], 'statistic', $this->locale),
                    'backgroundColor' => self::COLORS_BY_TYPE[$preResultKey], // Цвет устанавливается тут,
                    'borderColor' => self::COLORS_BY_TYPE[$preResultKey],
                    'data' => $v['data'],
                    'fill' => false
                ];
                if ($k === Statistic::EVENT_EXTERNAL) {
                    $data['borderDash'] = [5, 5]; // такие дела
                }
                $result['chart']['data'][] = $data;
            }
        }

        return $result;
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;
        $this->translator->setLocale($locale);

        return $this;
    }
}
