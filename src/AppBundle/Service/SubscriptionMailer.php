<?php

namespace AppBundle\Service;

use AppBundle\Entity\Subscription;
use AppBundle\Model\SubscriptionEmail;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Templating\EngineInterface;

class SubscriptionMailer
{
    protected $mailer;
    protected $templating;
    protected $router;
    protected $parameters;

    /**
     * SubscriptionMailer constructor.
     *
     * @param \Swift_Mailer $mailer
     * @param EngineInterface $templating
     * @param UrlGeneratorInterface $router
     * @param array $parameters
     */
    public function __construct(
        \Swift_Mailer $mailer,
        EngineInterface $templating,
        UrlGeneratorInterface $router,
        array $parameters
    ) {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->router = $router;
        $this->parameters = $parameters;
    }

    public function sendConfirmationMessage(Subscription $subscription)
    {
        $confirmationUrl = $this->router->generate('subscription.confirm', [
            'hash' => $subscription->getHash(),
            'email' => $subscription->getEmail()
        ], UrlGeneratorInterface::ABSOLUTE_URL);
        $rendered = $this->templating->render('AppBundle:Email/Subscription:confirm.html.twig', [
            'subscription' => $subscription,
            'confirmationUrl' => $confirmationUrl,
        ]);

        return $this->sendMessage($rendered, $subscription->getEmail());
    }

    /**
     * @param SubscriptionEmail $subscriptionEmail
     * @param Subscription[] $subscription
     * @return int
     */
    public function sendSubscription(SubscriptionEmail $subscriptionEmail, $subscription): int
    {
        if (empty($subscription)) {
            return 1;
        }

        $files = [];
        $toEmail = $subscription[0]->getEmail();
        $bcc = [];

        /** @var UploadedFile $file */
        foreach ($subscriptionEmail->getFiles() as $file) {
            $timestamp = (new \DateTime())->getTimestamp();
            $fileMoved = $file->move('/tmp/' . $timestamp, $file->getClientOriginalName());
            $files[] = $fileMoved->getRealPath();
        }

        foreach (array_slice($subscription, 1) as $mail) {
            $bcc[] = $mail->getEmail();
        }

        return $this->sendMessage(
            $subscriptionEmail->getMessage(),
            $toEmail,
            $subscriptionEmail->getSubject(),
            $bcc,
            $files
        );
    }

    /**
     * @param $renderedTemplate
     * @param $toEmail
     * @param null $subject
     * @param array $bcc
     * @param array $attachment
     * @return int
     */
    protected function sendMessage(
        $renderedTemplate,
        $toEmail,
        $subject = null,
        array $bcc = [],
        array $attachment = []
    ) {
        if (null === $subject) {
            // Render the email, use the first line as the subject, and the rest as the body
            $renderedLines = explode("\n", trim($renderedTemplate));
            $subject = array_shift($renderedLines);
            $body = implode("\n", $renderedLines);
        } else {
            $body = $renderedTemplate;
        }

        $fromEmail = $this->parameters['from_email']['address'];
        $fromName = $this->parameters['from_email']['sender_name'];

        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom([$fromEmail => $fromName])
            ->setTo($toEmail)
            ->setBody($body, 'text/html');

        if (!empty($bcc)) {
            $message->setBcc($bcc);
        }

        if (!empty($attachment)) {
            foreach ($attachment as $item) {
                $message->attach(\Swift_Attachment::fromPath($item));
            }
        }

        return $this->mailer->send($message);
    }
}
