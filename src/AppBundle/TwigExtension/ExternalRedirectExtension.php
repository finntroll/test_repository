<?php

namespace AppBundle\TwigExtension;

use AppBundle\Entity\University;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class ExternalRedirectExtension extends \Twig_Extension
{
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function getFunctions()
    {
        return array(
            'redirectLink' => new \Twig_SimpleFunction('redirectLink', [$this, 'redirectLink'])
        );
    }

    public function redirectLink($object)
    {
        $reflection = new \ReflectionClass($object);
        $objectClass = \lcfirst($reflection->getShortName());

        $objectId = $object->getId();
        $un = null;

        if ($object instanceof University) {
            $un = $object->getId();
        } else {
            if ($reflection->hasMethod('getUniversity')) {
                $university = $object->getUniversity();
                if ($university) {
                    $un = $university->getId();
                }
            }
        }

        return $this->router->generate(
            'redirect.external', [
                'ob' => $objectClass,
                'id' => $objectId,
                'un' => $un,
            ]
        );
    }

}
