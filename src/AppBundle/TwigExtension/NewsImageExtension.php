<?php

namespace AppBundle\TwigExtension;

use AppBundle\Entity\News;

class NewsImageExtension extends \Twig_Extension
{
    public function getName()
    {
        return 'news_image';
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('news_image', [$this, 'newsImagePath'], ['is_safe' => ['html']]),
        ];
    }

    public function newsImagePath(News $news)
    {
        $defaultLogoPath = '/bundles/app/css/img/news-1.jpg';
        $srcPath = $news->getImage() ? $news->getImagePath() : $defaultLogoPath;
        $alt = $news->translate()->getTitle();

        return '<img src="'.$srcPath.'" alt="'.$alt.'" />';
    }
}
