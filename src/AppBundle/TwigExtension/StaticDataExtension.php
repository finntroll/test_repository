<?php

namespace AppBundle\TwigExtension;

use Doctrine\ORM\EntityManagerInterface;

class StaticDataExtension extends \Twig_Extension
{
    private $em;

    /**
     * StaticDataExtension constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getName()
    {
        return'static_data';
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('static_data', [$this, 'getStaticDataByTag']),
            new \Twig_SimpleFunction('personality', [$this, 'getPersonalityByAssociationPosition'])
        ];
    }

    public function getStaticDataByTag(string $tag, $locale = null)
    {
        return $this->em->getRepository('AppBundle:StaticPageData')->findOneBy(['tag' => $tag]);
    }

    public function getPersonalityByAssociationPosition($position, $limit = null)
    {
        return $this->em->getRepository('AppBundle:Personality')->findByAssociationPositionKey($position, $limit);
    }
}
