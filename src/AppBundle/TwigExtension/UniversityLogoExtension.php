<?php

namespace AppBundle\TwigExtension;

use AppBundle\Entity\Partner;
use AppBundle\Entity\University;
use Symfony\Component\HttpFoundation\RequestStack;

class UniversityLogoExtension extends \Twig_Extension
{
    private $requestStack;

    /**
     * UniversityLogoExtension constructor.
     *
     * @param $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function getName()
    {
        return 'uni_logo';
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('uni_logo', [$this, 'universityLogoPath'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('partner_logo', [$this, 'partnerLogoPath'], ['is_safe' => ['html']]),
        ];
    }

    public function universityLogoPath(University $university = null, $locale = null)
    {
        $locale = $locale ?? $this->requestStack->getCurrentRequest()->getLocale();

        if ($university) {
            $universityTranslation = $university->translate($locale);

            $defaultLogoPath = '/bundles/app/css/img/university/logo/' . $university->getLogo();
            $srcPath = $universityTranslation->getImage() ? $universityTranslation->getImagePath() : $defaultLogoPath;
            $alt = $universityTranslation->getShortName();

            return '<img src="' . $srcPath . '" alt="' . $alt . '" />';
        }
        return '';
    }

    public function partnerLogoPath(Partner $partner = null)
    {
        if ($partner) {
            $srcPath = $partner->getImage() ? $partner->getImagePath() : '#';
            $alt = $partner->translate()->getShortName();

            return '<img src="' . $srcPath . '" alt="' . $alt . '" />';
        }

        return '';
    }
}
