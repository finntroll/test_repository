<?php

namespace AppBundle\TwigExtension;

class UrlBuilderExtension extends \Twig_Extension
{
    public function getName()
    {
        return 'url_builder';
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('build_url', [$this, 'buildUrl']),
        ];
    }

    public function buildUrl($uri, array $query)
    {
        return $uri.'?'.http_build_query($query);
    }
}
