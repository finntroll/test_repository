<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class BestPracticeCount
 * @package AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class BestPracticeCount extends Constraint
{
    public $message = 'You already have max count of Best Practices for this Scope: {{ maxCount }}';
    public $maxCount = 5;

    public function validatedBy()
    {
        return BestPracticeCountValidator::class;
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
