<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Entity\Scope;
use AppBundle\Entity\University;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class BestPracticeCountValidator extends ConstraintValidator
{
    private $em;

    /**
     * BestPracticeCountValidator constructor.
     *
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function validate($bestPractice, Constraint $constraint)
    {
        $oldBestPractice = $this->em->getUnitOfWork()->getOriginalEntityData($bestPractice);
        $repo = $this->em->getRepository('AppBundle:BestPractice');
        $scope = $bestPractice->getScope();
        $university = $bestPractice->getUniversity();

        if (!$scope instanceof Scope || !$university instanceof University) {
            return; // Because it's impossible to validate without Scope and University
        }

        $isNew = empty($oldBestPractice);

        /**
         * If User changes Scope or University on existing BestPractice we want to check total count,
         * but if User changes another fields we don't.
         */
        if (!$isNew) {
            $isChangedScope = $scope->getId() !== $oldBestPractice['scope_id'];
            $isChangedUniversity = $university->getId() !== $oldBestPractice['university_id'];
        } else {
            $isChangedScope = false;
            $isChangedUniversity = false;
        }

        $count = $repo->getCountByScopeAndUniversity($scope, $university);
        $isExceededMaxCount = $count >= $constraint->maxCount;
        if (($isChangedScope || $isChangedUniversity || $isNew) && $isExceededMaxCount) {
            $this->context->buildViolation($constraint->message)
                          ->setParameter('{{ maxCount }}', $constraint->maxCount)
                          ->atPath('scope')
                          ->addViolation();
        }
    }
}
