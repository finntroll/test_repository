<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Entity\Project;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ProjectMemberValidator extends ConstraintValidator
{
    /**
     * Checks if the passed value is valid.
     *
     * @param mixed      $value      The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        /**
         * @var Project $value
         */
        $requiredCount = $value->getManager() ? 2 : 3;
        if ($value->getProjectMembers()->count() < $requiredCount) {
            $this->context->buildViolation('project.required_members_count')
                ->atPath('projectMembers')
                ->addViolation();
        }
    }
}
