<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class ProjectMembersCount extends Constraint
{
    public $message = 'project.required_members_count';

    /**
     * Count of personalities excluding head of project
     */
    public $min = 3;

    public function validatedBy()
    {
        return ProjectMemberValidator::class;
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
