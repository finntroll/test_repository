<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Entity\UploadedDocuments\UploadedDocument;
use Symfony\Component\Validator\Constraint;

/**
 * Class UploadedDocumentsCount
 * @package AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class UploadedDocumentsCount extends Constraint
{
    public $maxMessage = 'This collection must contain not greater than {{ max_count }} documents';
    public $minMessage = 'This collection must contain not less than {{ min_count }} documents';
    public $maxCount = 5;
    public $minCount = 0;
    public $status = [UploadedDocument::CONTENT_STATUS_PROJECT, UploadedDocument::CONTENT_STATUS_APPROVED, null];

    public function validatedBy()
    {
        return UploadedDocumentsCountValidator::class;
    }

    public function getTargets()
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
