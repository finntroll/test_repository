<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Entity\UploadedDocuments\UploadedDocument;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UploadedDocumentsCountValidator extends ConstraintValidator
{
    /**
     * Checks if the passed value is valid.
     *
     * @param mixed      $value      The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        $checkStatuses = (array) $constraint->status;
        $maxCount = $constraint->maxCount;
        $minCount = $constraint->minCount;

        if ($value instanceof Collection) {
            $docs = $value;

            $count = 0;
            /** @var UploadedDocument $doc */
            foreach ($docs as $doc) {
                if (in_array($doc->getContentStatus(), $checkStatuses)) {
                    $count++;
                }
            }

            if ($count > $maxCount) {
                $this->context->buildViolation($constraint->maxMessage)
                              ->setParameters(['{{ max_count }}' => $maxCount])
                              ->atPath('uploadedDocuments')
                              ->addViolation();
            }

            if ($count < $minCount) {
                $this->context->buildViolation($constraint->minMessage)
                              ->setParameters(['{{ min_count }}' => $minCount])
                              ->atPath('uploadedDocuments')
                              ->addViolation();
            }
        }
    }
}
