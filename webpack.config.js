var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
var devPlugins = [
    new webpack.ProvidePlugin({
        '$': 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
    }),
    new ExtractTextPlugin("[name].css"),
];

var prodPlugins = [
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: '"production"'
        }
    }),
    new webpack.ProvidePlugin({
        '$': 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
    }),
    new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false
        }
    }),
    new ExtractTextPlugin("[name].css"),
    new OptimizeCssAssetsPlugin({})
];
module.exports = {
    entry: {
        "app-front": "./web/bundles/app/js/app-front.js",
        "app-profile": "./web/bundles/app/js/app-profile.js",
        "styles": "./web/bundles/app/js/styles.js"
    },
    output: {
        path: path.join(__dirname, "/web"),
        filename: "[name].js"
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: "css-loader"
            }) },
            { test: /\.(woff|woff2|eot|ttf)$/, loader: 'url-loader?limit=100000&name=/fonts/[name].[ext]' },
            { test: /\.(png|svg|jpg|jpeg)$/, loader: 'file-loader?name=/img/[name].[ext]' }
        ]
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.common'
        }
    },
    plugins: process.env.NODE_ENV === 'prod' || process.env.NODE_ENV === 'production' ? prodPlugins : devPlugins
};
